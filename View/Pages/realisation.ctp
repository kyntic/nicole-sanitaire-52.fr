<?php
$this->extend('/Templates/template_default');
$this->element('balises_metas', array('metas' => $page['Page']));

$this->assign('h1', $page['Page']['name']);


foreach($_breadcrumb['Chemin'] as $v) $this->Html->addCrumb($v[$_breadcrumb['Model']]['name'], $v[$_breadcrumb['Model']]['url']);

$this->start('body');

?>

    <div class="full-width-container white-bg gray-border-top gray-border-bott" id="zone_metier">

        <div class="container padding-top48 padding-bottom96 text-center">


            <h2><?=$page['Page']['h1'];?></h2>
            <p class="padding-bottom96"><?=$page['Page']['sous_titre'];?></p>

            <div class="container  padding-bottom24">

                <ul id="container" class="no-style margin0 q-sand-target four-columns-q-sand padding0 super-list variable-sizes clearfix isotope">
                    <?php foreach($Refs as $v) : ;?>
                    <li class="pull-left border-box relative overflow-hidden element * projet_type_<?=$v['Actualite']['page_id'];?> isotope-item">
                        <?=$this->element('bloc_real', array('v' => $v));?>
                    </li>
                <?php endforeach;?>

                </ul>

            </div>

        </div>

    </div>

<?php
$this->end();
?>
