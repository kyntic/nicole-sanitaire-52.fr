<?php
$this->extend('/Templates/template_default');
$this->element('balises_metas', array('metas' => $page['Page']));

$this->assign('h1', $page['Page']['h1']);

foreach($_breadcrumb['Chemin'] as $v) $this->Html->addCrumb($v[$_breadcrumb['Model']]['name'], $v[$_breadcrumb['Model']]['url']);

$this->start('body');

?>

<div class="full-width-container">
    <div class="container padding-top96 padding-bottom48">
    <div class="row">
    <div class="col-md-8">

        <?php foreach($Actus as $v) : ;?>
        <div class="relative always-visible-element padding-bottom96">
            <?=(!empty($v['Actualite']['vignette'])) ? $this->Html->image($v['Actualite']['vignette']->File->url, array('alt' => $v['Actualite']['h1'])) : $this->Html->image('/themeforest/Site/images/blog/1.jpg', array('alt' => $v['Actualite']['h1']));?>
            <a href="<?=$v['Actualite']['url'];?>" class="date-month blog-grid font-dosis font600 text-center border-box">
                <div class="date-posted black-letters"><?=date('d', strtotime($v['Actualite']['created']));?></div>
                <div class="month-posted letters-white"><?=strtoupper(date('M', strtotime($v['Actualite']['created'])));?></div>
                <div class="date-month-tri"></div>
            </a>
            <div>
                <div class="padding-top24">
                    <h4><a href="<?=$v['Actualite']['url'];?>"><?=$v['Actualite']['name'];?></a></h4>
                    <a class="author-name" href="#"><?=(empty($v['Auteur']['nom_connexion'])) ? '' : $v['Auteur']['nom_connexion'];?></a>
                    <p class="text-indent margin-top12"><?=$v['Actualite']['resume'];?></p>
                </div><!-- static element + triangle -->
                <div class="underpost-list gray-border-bott gray-border-top font-dosis font600 margin-top12">
                    <ul class="margin0 no-style padding0">
                        <li>
                            <ul class="margin0 no-style blogpost-socials padding0">
                                <li><a data-color="#3b5998" class="relative" href="#"><img class="static" src="/themeforest/Site/images/port-item/1.png" alt=""><img class="dynamic" src="/themeforest/Site/images/port-item/1-1.png" alt=""><span class="port-item-soch"></span></a></li>
                                <li><a data-color="#007bb6" class="relative" href="#"><img class="static" src="/themeforest/Site/images/port-item/2.png" alt=""><img class="dynamic" src="/themeforest/Site/images/port-item/2-2.png" alt=""><span class="port-item-soch"></span></a></li>
                                <li><a data-color="#cb2027" class="relative" href="#"><img class="static" src="/themeforest/Site/images/port-item/3.png" alt=""><img class="dynamic" src="/themeforest/Site/images/port-item/3-3.png" alt=""><span class="port-item-soch"></span></a></li>
                                <li><a data-color="#00aced" class="relative" href="#"><img class="static" src="/themeforest/Site/images/port-item/4.png" alt=""><img class="dynamic" src="/themeforest/Site/images/port-item/4-4.png" alt=""><span class="port-item-soch"></span></a></li>
                            </ul>
                        </li>
                        <li><a class="green-letters font-dosis font600 read-more-link" href="<?=$v['Actualite']['url'];?>"><span class="read-more-text">LIRE LA SUITE </span><span class="read-more-plus">+</span></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    <?php endforeach;?>

        <div class="text-center padding-top48 padding-bottom48">
            <ul class="no-style margin0 padding0 inline-block blog-pagination">
                <?php
                $prev = $this->Paginator->request->params['paging']['Actualite']['page'] - 1;
                $next = $this->Paginator->request->params['paging']['Actualite']['page'] + 1;
                ?>
                <li><?=($this->Paginator->request->params['paging']['Actualite']['prevPage']) ? '<a href="/'.$this->Paginator->request->url.'?page='.$prev.'">&lt;</a>' : '';?></li>
                <?php for($i = 1; $i <= $this->Paginator->request->params['paging']['Actualite']['pageCount']; $i++) echo '<li><a href="/'.$this->Paginator->request->url.'?page='.$i.'">'.$i.'</a></li>';?>
                <li><?=($this->Paginator->request->params['paging']['Actualite']['nextPage']) ? '<a href="/'.$this->Paginator->request->url.'?page='.$next.'">&gt;</a>' : '';?></li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-md-4 sidebar">
        <h6 class="padding-bottom-first">ARTICLES POPULAIRES</h6>
        <div>
            <ul class="margin0 no-style padding0">
                <?php foreach($pops as $v) : ;?>
                <li class="relative margin-top12 gray-border-bott padding-bottom12">
                    <div class="pull-left image-date-wrapper" style="max-width: 150px;">
                        <?=(!empty($v['Actualite']['vignette'])) ? $this->Html->image($v['Actualite']['vignette']->File->url, array('class' => 'yellow-border-top', )) : $this->Html->image('/themeforest/Site/images/blog/grid/4.jpg', array('class' => 'yellow-border-top'));?>

                        <span class="wrapper-date-posted font-dosis font600 text-center">
                            <span class="number-style black-letters"><?=date('d', strtotime($v['Actualite']['created']));?></span>
                            <span class="month-style letters-white "><?=strtoupper(date('M', strtotime($v['Actualite']['created'])));?></span>
                            <span class="border-triangle"></span>
                        </span>
                    </div>
                    <div class="pull-left image-date-description">
                        <a href="<?=$v['Actualite']['url'];?>"><h5><?=$v['Actualite']['name'];?></h5></a>
                        <div class="author-name margin-top12"><?=(empty($v['Auteur']['nom_connexion'])) ? '' : $v['Auteur']['nom_connexion'];?></div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <?php endforeach;?>
            </ul>
        </div>


        <div class="padding-top48 tags-sidebar gray-border-bott padding-bottom12">
            <h6 class="padding-bottom24 padding-top12">TAGS</h6>
            <ul class="margin0 no-style padding0">
                <?php foreach($sousPage as $v) : ;?>
                <li><a href="<?=$v['Page']['url'];?>"><span><?=$v['Page']['name'];?></span></a></li>
                <?php endforeach;?>
            </ul>
        </div>


    </div>
    <div class="clearfix"></div>
    </div>
    </div>
</div>


<a href="/contact/p-5">
    <div class="full-width-container info_box">
        <div class="container">
            <div class="row padding-top24 padding-bottom24">
                <div class="col-md-12 letters-white">
                    <div class="postslider letters-white pull-left">Recevez le meilleur de nos actualités dans votre boite mail</div>
                    <div class="pull-right arrow-blue-bg-link"><img src="/img/pointer-right.png" alt=""></div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</a>

<?php
$this->end();
?>