<?php
$this->extend('/Templates/template_default');
$this->element('balises_metas', array('metas' => $page['Page']));

$this->assign('h1', $page['Page']['h1']);

foreach($_breadcrumb['Chemin'] as $v) $this->Html->addCrumb($v[$_breadcrumb['Model']]['name'], $v[$_breadcrumb['Model']]['url']);

$this->start('body');
?>


<!-- CORP -->
<div class="full-width-container white-bg gray-border-top gray-border-bott">
    <div class="container padding-bottom96 padding-top48">
        <div class="row">
            <div class="display-table-stat">
                <div>
                    <div class="col-md-6 padding-top48">
                        <div class="postslider">
                            <div class="margin-bottom12"><h2><?=$page['Page']['sous_titre'];?></h2></div>
                            <div class="line-height26"><?=$page['Page']['body'];?></div>
                        </div>
                    </div>
                    <div class="col-md-6 text-left padding-top48">
                        <?php
                        if(!empty($page['Page']['vignette']))
                            echo $this->Html->image($page['Page']['vignette']->File->url, array('class' => 'inline-block', 'alt' => $page['Page']['vignette']->File->name));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Autre Block -->
<?php

if (!empty($ContentModules)) {

    foreach ($ContentModules as $k => $module) {

        switch ($module['ContentModule']['module']) {

            default :

	            $element = 'ModuleManager.' . $module['ContentModule']['module'];
	            if (!empty($module['ContentModule']['template'])) {
		            $element .= $module['ContentModule']['template'];
	            }

                echo $this->element(
                    $element,
                    array(
                        'content' => $module['ContentModule'],
                        'classBlock' => ($k%2) ? 'pale-gray-bg' : 'white-bg'
                    )
                );

                break;
        }
    }
}

?>

    <!-- LISTE DES REFERENCES -->
<?php if(!empty($Refs)) : ;?>
    <div class="full-width-container pale-gray-bg gray-border-top">
        <div class="container padding-top96 padding-bottom96">
            <div class="margin-bottom-2 margin-top12">
                <h3 class="padding-bottom12">NOS REALISATIONS EN <?php echo strtoupper($page['Page']['name']);?></h3>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div id="myCarousel2" class="carousel slide carousel-3col margin0">
                        <div class="carousel-nav">
                            <a class="carousel-nav-3col font-dosis black-letters pull-left font600" href="#myCarousel2" data-slide="prev">&lt;&nbsp;PRECEDENT</a>
                            <a class="carousel-nav-3col font-dosis black-letters pull-left font600" href="#myCarousel2" data-slide="next">SUIVANT&nbsp;&gt;</a>
                            <div class="clearfix"></div>
                        </div>
                        <div class="carousel-inner">
                            <?php
                            foreach($Refs as $k => $v) : ;
                                $e = $k%3;

                                if($e == 0) {

                                    $active = (!$k) ? 'active' : '';
                                    echo '<div class="item '.$active.'">';
                                    echo '<div class="row">';
                                    $open = 1;

                                }
                                ?>

                                <div class="col-md-4 border-box relative overflow-hidden item element design isotope-item">
                                    <?=$this->element('bloc_real', array('v' => $v));?>
                                </div>

                                <?php
                                $this->Html->scriptStart(array('block' => 'scriptSuperBottom', 'inline' => false));
                                ?>
                                $('.carousel-image').carousel({
                                interval: 5000,
                                pause: null
                                });
                                <?php
                                $this->Html->scriptEnd();
                                ?>

                                <?php
                                if($e == 2) {

                                    echo '</div>';
                                    echo '</div>';

                                    $open = 0;

                                }
                            endforeach;

                            if($open) echo '</div></div>';
                            ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>

<?php endif;?>


<a href="<?=$_lienContact['Page']['url'];?>">
    <div class="full-width-container info_box">
        <div class="container">
            <div class="row padding-top24 padding-bottom24">
                <div class="col-md-12 letters-white">
                    <div class="postslider letters-white pull-left">Nous contacter</div>
                    <div class="pull-right arrow-blue-bg-link"><img src="/img/pointer-right.png" alt=""></div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</a>

<?php
$this->end();
?>