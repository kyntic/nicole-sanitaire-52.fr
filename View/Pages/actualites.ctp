<?php
$this->extend('/Templates/template_default');

$this->element('balises_metas', array('metas' => $Cont['Page']));

foreach($_breadcrumb['Chemin'] as $v) $this->Html->addCrumb($v[$_breadcrumb['Model']]['name'], $v[$_breadcrumb['Model']]['url']);

$this->assign('h1', $Cont['Page']['h1']);

/**
 * Start the body content with modules
 */
$this->start('body');


foreach($ContentModules as $module) {

    switch($module['ContentModule']['module']) {

        default :

            echo $this->element('ModuleManager.'.$module['ContentModule']['module'], array('content' => $module['ContentModule']));

            break;
    }


}
?>
<section class="news-wrapper">
    <?php foreach($Actus as $v) : ;?>
    <article class="news-item page-row has-divider clearfix row">
        <figure class="thumb col-md-2 col-sm-3 col-xs-4">
            <?=(!empty($v['Actualite']['vignette'])) ? $this->Html->image($v['Actualite']['vignette']->File->url, array('class' => 'img-responsive', 'url' => $v['Actualite']['url'])) : '';?>
        </figure>
        <div class="details col-md-10 col-sm-9 col-xs-8">
            <h3 class="title"><a href="news-single.html"><?=$v['Actualite']['name']?></a></h3>
            <p><?=$this->Text->truncate($v['Actualite']['resume'], 120, array('ellipsis' => '...'))?></p>
            <a class="btn btn-theme read-more" href="<?=$v['Actualite']['url'];?>"><?=__('en savoir +');?><i class="fa fa-chevron-right"></i></a>
        </div>
    </article>
    <?php endforeach;?>
</section>

<?php
$this->end();

if(!empty($evenements)) $this->assign('event_col', $this->element('Blog.liste_evenement_col', array('evenements' => $evenements)));


//Petit resume sur la colone de droite
if(!empty($Cont['Page']['resume']))
{
    $this->start('resume_col');
    ?>
    <section class="widget has-divider">
        <h3 class="title"><?php echo $Cont['Page']['name'];?></h3>
        <p><?php echo $Cont['Page']['resume'];?></p>

        <?php
        //Si des elements du portfolio existe , on les rend telechargeable
        if (!empty($portFolio)) {
            foreach($portFolio as $v):
                if($v['File']['type'] == 'file'){
                    echo $this->Html->link(
                        '<i class="fa fa-download"> Télécharger la brochure</i>',
                        '/files/' . $v['File']['url'],
                        array('class' => 'btn btn-theme', 'escape' => false, 'target' => '_blank')
                    );

                }
            endforeach;
        }
        ?>
    </section>
    <?php
    $this->end();
}
