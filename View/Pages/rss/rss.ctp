<?php
$this->set('channelData', array(
        'title' => $channelData['Page']['rss_name'],
        'link' => $channelData['Page']['url'],
        'description' => $channelData['Page']['rss_description'],
        'language' => 'fr-fr'
    )
);

foreach ($posts as $post) {

    // Retire & échappe tout HTML pour être sûr que le contenu va être validé.
    $bodyText = h(strip_tags($post['Actualite']['resume']));
    $bodyText = $this->Text->truncate($bodyText, 400, array(
        'ending' => '...',
        'exact'  => true,
        'html'   => true,
    ));

    echo  $this->Rss->item(array(), array(
        'title'         => $post['Actualite']['h1'],
        'link'          => $post['Actualite']['url'],
        'guid'          => array('url' => $post['Actualite']['url'], 'isPermaLink' => 'true'),
        'description'   => $bodyText,
        'pubDate'       => $post['Actualite']['created']
    ));
}