<?php
$this->extend('/Templates/template_default');

$this->element('balises_metas', array('metas' => $Cont['Page']));

foreach($_breadcrumb['Chemin'] as $v) $this->Html->addCrumb($v[$_breadcrumb['Model']]['name'], $v[$_breadcrumb['Model']]['url']);

$this->assign('h1', $Cont['Page']['h1']);
/**
 * Start the body content with modules for FAQ
 */
$this->start('faq');
    if(!empty($ContentModules)) {?>
        <div class="panel-group" id="accordion">
        <?php
        foreach($ContentModules as $k => $module) {
            echo $this->element('ModuleManager.faq', array('content' => $module['ContentModule'], 'index' => $k));
        }
        ?>
        </div>
    <?php
    } else {
        echo $Cont['Page']['body'];
}
$this->end();