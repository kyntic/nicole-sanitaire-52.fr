<?php
$this->extend('/Templates/template_default');
$this->element('balises_metas', array('metas' => $page['Page']));

$this->assign('h1', $page['Page']['h1']);


foreach($_breadcrumb['Chemin'] as $v) $this->Html->addCrumb($v[$_breadcrumb['Model']]['name'], $v[$_breadcrumb['Model']]['url']);

$this->start('body');
?>
/view/Pages/home_metier.ctp
<?=$this->element('metiers', array('metiers' => $metiers, 'titre' => $page['Page']['sous_titre']));?>

<a href="/realisations/p-3">
    <div class="full-width-container info_box">
        <div class="container">
            <div class="row padding-top24 padding-bottom24">
                <div class="col-md-12 letters-white">
                    <div class="postslider letters-white pull-left">Découvrez nos réalisations</div>
                    <div class="pull-right arrow-blue-bg-link"><img src="/img/pointer-right.png" alt=""></div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</a>

<?php
$this->end();
?>