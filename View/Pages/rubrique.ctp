<?php
	$this->extend('/Templates/template_default');
	$this->element('balises_metas', array('metas' => $page['Page']));

	$this->assign('h1', $page['Page']['h1']);

	foreach($_breadcrumb['Chemin'] as $v) $this->Html->addCrumb($v[$_breadcrumb['Model']]['name'], $v[$_breadcrumb['Model']]['url']);

?>

<?php $this->start('body'); ?>

	<div class="container">

		<div class="row">

			<div class="col-md-3">

				<p>Affiner la recherche</p>
				<?= $this->element('Catalogue.filtres'); ?>

			</div>

			<div class="col-md-9">

				<div class="barreProduits">
					<div class="pagination">
						<?php if ($this->Paginator->request->params['paging']['Product']['prevPage']) echo $this->Paginator->prev(''); ?>
						<?= $this->Paginator->numbers(); ?>
						<?php if ($this->Paginator->request->params['paging']['Product']['nextPage']) echo $this->Paginator->next(''); ?>
					</div>
					<p class="libelle"><strong><?= $page['Page']['name']; ?></strong> <?= $nbProduits; ?> résultat(s)</p>
					<?= $this->Form->create('Rubrique', array('url' => $page['Page']['url'])); ?>
					<?= $this->Form->input('order', array('label' => 'Trier par :', 'div' => false, 'empty' => '', 'options' => array('prix_asc' => 'Prix croissant', 'prix_desc' => 'Prix décroissant'))); ?>
					<?= $this->Form->end(); ?>
				</div>

				<?php $this->Html->scriptStart($options = array('block' => 'scriptBottom')); ?>

				$('.barreProduits form select').change(function(){

					$('.barreProduits form').submit();

				});

				<?php $this->Html->scriptEnd(); ?>

				<section class="listeProduits">

					<div class="row">

						<?php foreach ($produits as $produit) { ?>

							<div class="col-md-4 col-sm-4">

								<div class="produit">

									<div class="image">

										<?php
											$title = '';
											foreach($_breadcrumb['Chemin'] as $v) $title .= $v['Page']['name'].' ';
											$title .= ' '.$produit['Product']['name'];
										?>

										<?php if (!empty($produit['Product']['vignette'])) { ?>
											<?= $this->Html->link($this->WhFile->show($produit['Product']['vignette'], array('x' => 768, 'y' => 500), array('alt' => $page['Page']['name'].' '.$produit['Product']['name'])), $produit['Product']['url'], array('escape' => false, 'title' => $title)); ?>
										<?php } else { ?>
											<?= $this->Html->link($this->Html->image('visuel-non-dispo-site.jpg'), $produit['Product']['url'], array('title' => $title, 'escape' => false)); ?>
										<?php } ?>
									</div>

									<div class="texte" >
										<h3><?= $this->Html->link($produit['Product']['name'], $produit['Product']['url'], array('title' => $title)); ?></h3>
										<p class="resume"><?= $produit['Product']['resume']; ?></p>
										<p class="prix"><?= $this->WhPrix->show($produit['Product']['prix_ttc']); ?></p>
										<div class="plus"><?= $this->Html->link('', $produit['Product']['url'], array('title' => $title)); ?></div>
									</div>

								</div>

							</div>

						<?php } ?>

					</div>

				</section>

				<div class="barreProduits bas">
					<div class="pagination">
						<?php if ($this->Paginator->request->params['paging']['Product']['prevPage']) echo $this->Paginator->prev(''); ?>
						<?= $this->Paginator->numbers(); ?>
						<?php if ($this->Paginator->request->params['paging']['Product']['nextPage']) echo $this->Paginator->next(''); ?>
					</div>
				</div>

				<?php $this->Html->scriptStart($options = array('block' => 'scriptBottom')); ?>

				var fixeHauteurBlocProduit = function() {

				var hauteurMinimum = 0;

				if ($(window).width() > 767) {

				$('section.listeProduits .produit .texte').each(function(){

				var hauteur = $(this).height() + parseInt($(this).css('padding-top')) + parseInt($(this).css('padding-bottom'));

				if (hauteur > hauteurMinimum) {
				hauteurMinimum = hauteur;
				}

				});

				}

				$('section.listeProduits .produit .texte').each(function(){

				$(this).css('min-height', hauteurMinimum + 'px');

				});

				}

				$(window).load(function(){
				fixeHauteurBlocProduit();
				});

				$(window).resize(function(){
				fixeHauteurBlocProduit();
				});

				<?php $this->Html->scriptEnd(); ?>

			</div>

		</div>

	</div>

<?php $this->end(); ?>