
<?php
$this->element('balises_metas', array('metas' => $page['Page']));?>

<section class="inner-intro bg-img12 light-color overlay-dark parallax parallax-background2" style="background-position: 0% 0px;">
    <div class="container">
        <div class="row title">
            <p class="titre"><?=$page['Page']['h1'];?></p>
           
        </div>
    </div>
</section>
 

<div class="container">



<!-- CORP -->
<div class="full-width-container white-bg gray-border-top gray-border-bott">
    <div class="container padding-bottom96 padding-top48">
        <div class="row">
            <div class="display-table-stat">
                <div>
                    <div class="col-md-12 padding-top48">
                        <div class="postslider">
                            <div class="margin-bottom12"><h1><?=$page['Page']['sous_titre'];?></h1></div>
                            <div class="line-height26"><?=$page['Page']['body'];?></div>
                        </div>
                    </div>
                   <!-- <div class="col-md-6 text-left top10">
                        <?php
                        if(!empty($page['Page']['vignette']))
                            echo $this->Html->image($page['Page']['vignette']->File->url, array('class' => 'inline-block', 'alt' => $page['Page']['vignette']->File->name));
                        ?>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Autre Block -->
<?php
if (!empty($ContentModules)) {

    foreach ($ContentModules as $k => $module) {

        switch ($module['ContentModule']['module']) {

            default :

                echo $this->element(
                    'ModuleManager.' . $module['ContentModule']['module'],
                    array(
                        'page' => $page['Page'],
                        'content' => $module['ContentModule'],
                        'classBlock' => ($k%2) ? 'pale-gray-bg' : 'white-bg'
                    )
                );

            break;
        }
    }
}

?>
<?php
$this->end();
?>

</div>