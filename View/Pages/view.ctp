<?php
$this->extend('/Templates/template_default');
$this->element('balises_metas', array('metas' => $page['Page']));

$this->assign('h1', $page['Page']['h1']);

foreach($_breadcrumb['Chemin'] as $v) $this->Html->addCrumb($v[$_breadcrumb['Model']]['name'], $v[$_breadcrumb['Model']]['url']);

$this->start('body');
?>

<!-- CORP -->
<div class="full-width-container white-bg gray-border-top gray-border-bott">
    <div class="container padding-bottom96 padding-top48">
        <div class="row">
            <div class="display-table-stat">
                <div>
                    <div class="col-md-6 padding-top48">
                        <div class="postslider">
                            <div class="margin-bottom12"><h2><?=$page['Page']['sous_titre'];?></h2></div>
                            <div class="line-height26"><?=$page['Page']['body'];?></div>
                        </div>
                    </div>
                    <div class="col-md-6 text-left padding-top48">
                        <?php
                        if(!empty($page['Page']['vignette']))
                            echo $this->Html->image($page['Page']['vignette']->File->url, array('class' => 'inline-block', 'alt' => $page['Page']['vignette']->File->name));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">

	<?php

		if (!empty($ContentModules)) {

			foreach ($ContentModules as $k => $module) {

				switch ($module['ContentModule']['module']) {

					default :

						$element = 'ModuleManager.' . $module['ContentModule']['module'];
						if (!empty($module['ContentModule']['template'])) {
							$element .= $module['ContentModule']['template'];
						}

						echo $this->element(
						          $element,
						          array(
							          'page' => $page['Page'],
							          'content' => $module['ContentModule'],
							          'classBlock' => ($k%2) ? 'pale-gray-bg' : 'white-bg'
						          )
						);

						break;
				}
			}
		}

	?>

</div>




    <a href="<?=$_lienContact['Page']['url'];?>">
    <div class="full-width-container info_box">
        <div class="container">
            <div class="row padding-top24 padding-bottom24">
                <div class="col-md-12 letters-white">
                    <div class="postslider letters-white pull-left">... contactez nous</div>
                    <div class="pull-right arrow-blue-bg-link"><img src="/img/pointer-right.png" alt=""></div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</a>

<?php
$this->end();
?>