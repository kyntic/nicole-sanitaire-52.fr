<?php
$this->extend('/Templates/template_membre');
$this->start('body');
?>

<div class="row">

	<div class="col-md-6">

		<h2>Vous souhaitez changer d'adresse email ? </h2>

		<?= $this->Form->create('User', array('url' => '/membre/users/acces/email')); ?>
		<?= $this->Form->input('User.email', array('label' => 'Email')); ?>

		<div style="text-align:right;" >
			<?= $this->WhForm->submit('Modifier mon adresse email'); ?>
		</div>

		<?= $this->Form->end(); ?>

	</div>

	<div class="col-md-6">

		<h2>Vous souhaitez changer de mot de passe ? </h2>

		<?= $this->Form->create('User', array('url' => '/membre/users/acces/mdp')); ?>
		<?= $this->Form->input('User.password', array('label' => 'Mot de passe actuel', 'type' => 'password', 'value' => '')); ?>
		<?= $this->Form->input('User.motdepasse', array('label' => 'Nouveau mot de passe', 'type' => 'password', 'value' => '')); ?>
		<?= $this->Form->input('User.confirm_password', array('label' => 'Confirmez votre nouveau mot de passe', 'type' => 'password', 'value' => '')); ?>

		<div style="text-align:right;" >
			<?= $this->WhForm->submit('Modifier mon mot de passe'); ?>
		</div>

		<?= $this->Form->end(); ?>

	</div>

</div>


<?php
$this->end();
?>
