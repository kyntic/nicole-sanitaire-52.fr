<?php
App::uses('AppHelper', 'View/Helper');

class WhTreeHelper extends AppHelper {

    public $helpers    = array('Html');


    /**
     * name property
     *
     * @var string 'Tree'
     * @access public
     */
    public $name = 'Tree';

    /**
     * contient le HTML généré
     *
     * @var string 'html
     * @access private
     */
    public $html = '';

    /**
     * contient le HTML généré
     *
     * @var string 'html
     * @access private
     */
    public $Model = 'MenuItem';


    /**
     * contient le niveau en cours
     *
     * @var string 'niveau
     * @access private
     */
    private $niveau = 0;

    /**
     * contient le file d'ariane permettant de vérifer les pages actives
     *
     * @var array 'breadcrumb
     * @access public
     */
    public $breadcrumb = array();

    /**
     * contient le file d'ariane après traitement
     *
     * @var array 'breadcrumb
     * @access public
     */
    public $tab_breadcrumb = false;

    /**
     * Quelle vairable afficher
     *
     * @var string 'texte
     * @access public
     */
    public $texte = 'name';


    /**
     *
     * Initialisation du helper
     * @param array $setting
     *
     */
    public function __construct(View $view, $settings = array()) {

        if(isset($settings['breadcrumb'])) $this->breadcrumb    = $settings['breadcrumb'];

        $this->tab_breadcrumb();

        parent::__construct($view, $settings);

    }

    /**
     *
     * Fonction récursive générant le code HTML pour représenter un 'tree'
     * @param array $categories
     * @return string
     */
    public function generate_nav_reponsive($categories, $smenu = false, $recursif = false) {

        if($recursif==false) $this->sortie ='';

        if (!is_array($categories)) {

            return false;

        }

        $this->niveau++;

        //Determination de la class suivant la niveau
        $class_ul = '';
        if(!$smenu)
            $class_ul = 'nav';
        elseif($this->niveau == 2)
            $class_ul = 'dropdown-menu';


        $this->sortie .= '<ul class="'.$class_ul.'">';

        foreach ($categories as $key => $categorie) {

            $class_li   = 'niveau_'.$this->niveau.' page_'.$categorie['Content']['id'];
            $class_a    = '';
            $carret     = '';

            $url        = (!$categorie['Content']['non_cliquable']) ? $categorie['Content']['url_r'] : 'javascript:void(0);';

            if(!empty($categorie['children']) && $this->niveau == 1) {

                $class_li   .= ' dropdown';
                $class_a    = (!empty($categorie['children'])) ? 'class="dropdown-toggle" data-toggle="dropdown"' : '';
                $carret     = (!empty($categorie['children'])) ? '<b class="caret"></b>' : '';

            }

            if(in_array($categorie['Content']['id'], $this->tab_breadcrumb)) $class_li  .= ' active';

            $this->sortie .= '<li class="'.$class_li.'" data-num="'.$categorie['Content']['id'].'">';
            $this->sortie .= $this->Html->link($categorie['Content']['nom_raccourci'].$carret, $url, array('class' => $class_a, 'escape' => false));

            if (!empty($categorie['children'])) {

                $this->generate_nav_reponsive($categorie['children'], true, true);

            }

            $this->sortie .= '</li>';

        }

        $this->niveau--;

        $this->sortie .= '</ul>';

        return $this->sortie;

    }

    public function generate_nav_simple($items) {


        if (!is_array($items)) return false;

        $params_ul = array();

        if($this->niveau == 0) {

            $this->sortie = '';

            $params_ul[] = 'nav';
            $params_ul[] = 'font-dosis';
            $params_ul[] = 'font600';
            $params_ul[] = 'margin0';

        } else if($this->niveau == 1) {

	        $params_ul[] = 'no-style';
	        $params_ul[] = 'padding0';
	        $params_ul[] = 'margin0';
	        $params_ul[] = 'dropdown';

        }else{

            $params_ul[] = 'no-style';
            $params_ul[] = 'padding0';
            $params_ul[] = 'margin0';
            $params_ul[] = 'submenu';

        }

        $this->niveau++;

        $this->sortie .= '<ul class="'.implode(' ', $params_ul).'">';

        foreach ($items as $key => $item) {

            if(empty($item[$this->Model])) continue;

            $class_li = array();
            $class_li[]  = 'item';
            $class_li[]  = 'niveau_'.$this->niveau;
            $class_li[]  = strtolower($this->Model).'_'.$item[$this->Model]['id'];

            if(in_array($item[$this->Model]['id'], $this->tab_breadcrumb)) $class_li[]  = ' active';

            if($item[$this->Model]['url'] == '/') $item[$this->Model]['url'] = FULL_BASE_URL;

            $this->sortie .= '<li class="'.implode(' ', $class_li).'" data-num="'.$item[$this->Model]['id'].'">';
            $this->sortie .= $this->Html->link($item[$this->Model][$this->texte], $item[$this->Model]['url'], array('title' => $item[$this->Model]['meta_title'], 'data-num' => $item[$this->Model]['id'], 'escape' => false));

            if (!empty($item['children'])) {

                $this->generate_nav_simple($item['children']);

            }

            $this->sortie .= '</li>';

        }

        $this->niveau--;

        $this->sortie .= '</ul>';
        return $this->sortie;

    }



    private function tab_breadcrumb () {

        $this->tab_breadcrumb = array();

        if(!is_array($this->breadcrumb)) return $this->tab_breadcrumb;

        foreach($this->breadcrumb as $v) {

            $this->tab_breadcrumb[] = $v['Content']['id'];

        }


    }




}
?> 