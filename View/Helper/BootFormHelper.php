 <?php

/**
 * Helper WhForm
 * Ver 1.3, modified : 09/10/2012 : Création de la méthode submit, ajoute les boutons de validation
 * Ver 1.2, modified : 01/10/2012 : Création de la méthode select, passage de inputDep en variable
 * Ver 1.1, modified : 17/09/2012
 * 
 */

App::uses('AppHelper', 'View/Helper');

class BootFormHelper extends AppHelper {
    
    public $helpers    = array('Form', 'Html');
    public $inputDef  = array(
            'label'     => array(
                'class'     => 'label', 
                'text'      => null
            ), 
            'div'       => array('tag' => 'div', 'class' => 'form-group"'),  
            'error'     => array('attributes' => array('wrap' => 'span', 'class' => 'help-block note')), 
            'class'     => 'form-control'

        );
    

    /**
     *
     * Initialisation du helper
     * @param array $setting
     * 
     */
    public function __construct(View $view, $settings = array()) {
        
        if(isset($settings['inputDef'])) $this->inputDef      = $settings['inputDef'];

        parent::__construct($view, $settings);

    }

    private function prepare_input($label = null, $options = array(), $type = 'input') {

        $input = $this->inputDef;
        
        if($label) $input['label']['text'] = $label;

        if(!empty($options['legende'])) $input['after'] = '<span class="note pull-right"><i>'.$options['legende'].'</i></span></div>';

        if(!empty($options['append'])) $input['between'] = '<label class="input"> <i class="icon-append fa '.$options['append'].'"></i>';

        if(!empty($options['tooltip'])) $input['after'] = '<b class="tooltip tooltip-top-right">'.$options['tooltip'].'</b></label>';

        if(!empty($options['options'])) {

            $input['between'] = '<div class="select">';
            $input['after'] = '<i></i></div>';            
        }


        $input = array_merge($input, $options);
        
        return $input;   
    }


    public function create($name = false, $options = array()) {

        if(empty($options['class'])) $options['class'] = 'fill-up validatable';

        return $this->Form->create($name, $options);
    }

    public function end() {

        return $this->Form->end();
    }

    public function fieldsetOpen() {

        return '<fieldset>';       
    }

    public function fieldsetClose() {

        return '</fieldset>';       
    }

    public function input($name, $label = null, $options = array()) {

        $input = $this->prepare_input($label, $options);

        if(Configure::read('multilingue')) {

            if(!empty($input['trad'])) {

                $html = '';

                $t_name = explode('.', $name);

                // On récupère les éventuelles traductions existantes :
                $T_champ = 'Translate'.Inflector::camelize($t_name[1]);

                if(isset($this->data[$t_name[0]][$T_champ])) {

                    $value = Set::combine($this->data[$t_name[0]][$T_champ], '{n}.locale', '{n}.content');

                } elseif(isset($this->data[$T_champ])) {

                    $value = Set::combine($this->data[$T_champ], '{n}.locale', '{n}.content');
                    
                } 
                
                foreach(Configure::read('Config.languages') as $k => $v) : ;

                    $new_name = $name.'.'.$v;
                    $new_input = $input;

                    if(!empty($input['label']['text'])) $new_input['label']['text'] = $new_input['label']['text'].' ('.$k.')';

                    $new_input['value'] = (isset($value[$v])) ? $value[$v] : '';

                    $html .= $this->Form->input($new_name, $new_input);

                endforeach;

                return $html;
            }
        }

        return $this->Form->input($name, $input);    
    }

    public function textarea($name, $label = null, $options = array()) {

        $input = $this->prepare_input($label, $options);

        if(Configure::read('multilingue')) {

            if(!empty($input['trad'])) {

                $html = '';

                $t_name = explode('.', $name);

                // On récupère les éventuelles traductions existantes :
                $T_champ = 'Translate'.Inflector::camelize($t_name[1]);

                if(isset($this->data[$t_name[0]][$T_champ])) {

                    $value = Set::combine($this->data[$t_name[0]][$T_champ], '{n}.locale', '{n}.content');

                } elseif(isset($this->data[$T_champ])) {

                    $value = Set::combine($this->data[$T_champ], '{n}.locale', '{n}.content');
                    
                } 
                
                foreach(Configure::read('Config.languages') as $k => $v) : ;

                    $new_name = $name.'.'.$v;
                    $new_input = $input;

                    if(!empty($input['label']['text'])) $new_input['label']['text'] = $new_input['label']['text'].' ('.$k.')';

                    $new_input['value'] = (isset($value[$v])) ? $value[$v] : '';

                    $html .= $this->Form->input($new_name, $new_input);

                endforeach;

                return $html;
            }
        }

        return $this->Form->input($name, $input);    
    }


    public function file($name, $label = null, $options = array()) {

        $placeholder = (!empty($options['placeholder'])) ? $options['placeholder'] : 'choisir un fichier';

        $options['between'] = '<label class="input input-file"><div class="button">';
        $options['after']   = 'Chercher</div><input type="text" placeholder="'.$placeholder.'" readonly=""></label>';
        $options['type']    = 'file';
        $options['onchange']= 'this.parentNode.nextSibling.value = this.value';


        $input = $this->prepare_input($label, $options);

        return $this->Form->input($name, $input);    

    }


    public function hidden($name, $params = array()) {

        $params['type'] = 'hidden';

        return $this->Form->input($name, $params);       
    }

    public function select($name, $label = null, $select, $options = array()) {

        $input = $this->inputDef;
        
        $html = '';
        $html .= '<section>';
        $html .= '<label class="label">';
        $html .= $label;
        $html .= '</label>';
        $html .= '<label class="select">';
        $html .= $this->Form->select($name, $select, $options);
        $html .= '<i></i> </label>';
        $html .= '</section>';

        return $html;
    } 

    public function selectWithoutSection($name, $label = null, $select, $options = array())
    {
        $input = $this->inputDef;
        
        $html = '';        
        $html .= '<label class="label">';
        $html .= $label;
        $html .= '</label>';
        $html .= '<label class="select">';
        $html .= $this->Form->select($name, $select, $options);
        $html .= '</label>';        

        return $html;
    }  

    public function multi($model, $tabInit, $select) {

        $input = '';
        
        foreach($select as $k => $v) {

            if(is_array($v)) {

                $input .= '<optgroup label="'.$k.'">';

                foreach($v as $x => $y) {$q = (in_array($x, $tabInit)) ? 'selected="selected"' : ''; $input .= '<option '.$q.' value="'.$x.'">'.$y.'</option>';}

                $input .= '<optgroup label="'.$k.'">';$input .= '</optgroup>';

            } else {

                $q = (in_array($k, $tabInit)) ? 'selected="selected"' : ''; $input .= '<option '.$q.' value="'.$k.'">'.$v.'</option>';
            }
        }

        return '<select multiple="multiple" name="data['.$model.'][]" class="chzn-select">'.$input.'</select>';
    }  

    public function submit($txt_btn = null, $lien_annuler = null) {
        
        $txt_btn = (empty($txt_btn)) ? __('Enregistrer') : $txt_btn;

        $html = '';
        $html .= $this->Form->button($txt_btn, array('class' => 'btn btn-primary', 'type' => 'submit'));
        
        return $html;
    }

    public function check($name, $label = null, $options = array()) {

        $options['before']   = '<label class="checkbox">';
        $options['after']   = '<i></i>'.$label.'</label>';
        $options['label']   = false;
        $options['div']     = array('tag' => 'section', 'class' => '');

        return $this->Form->input($name, $options);
    }

    public function toogle($name, $label = null, $options = array()) {

        $html = '<section>';
        $html .= '<label class="toggle">';
        $html .= $this->Form->input($name, array('type' => 'checkbox', 'label' => false, 'div' => false));
        $html .= '<i data-swchon-text="ON" data-swchoff-text="OFF"></i>'.$label.'</label>';
        $html .= '</label>';
        $html .= '</section>';

        return $html;
    }


    public function vide($label = null, $contenu = null, $options = array()) {
        
        $html = '';
        $html .= '<div class="control-group">';
        $html .= '<label class="control-label">';
        $html .= $label;
        $html .= '</label>';
        $html .= '<div class="controls">';
        $html .= $contenu;
        if(isset($options['legende'])) $html .= '<span class="help-block"><i>'.$options['legende'].'</i></span>';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }    
}