<?php
App::uses('AppHelper', 'View/Helper');

class WhPrixHelper extends AppHelper {

	function show ($montant, $monnaie = '€') {

		$montant = explode('.', $montant);

		if(!isset($montant[1])) $montant[1] = '00';
		if(strlen($montant[1]) == 1) $montant[1] = $montant[1].'0';

		
		$prix  = '';
		$prix .= '<span class="montant">';
		$prix .= $montant[0];
		$prix .= '<span class="cents">,'.$montant[1].$monnaie.'</span>';
		$prix .= '</span>';

		return $prix;

	}


	function promotion ($montant, $monnaie, $montant_promo, $promo_type, $texte) {

		$prix  = '';
		$prix .= '<span class="promotion">';
		$prix .= '<span class="text">'.$texte.'</span>';
		$prix .= '<span class="nouv_prix">'.$this->show($montant, $monnaie).'</span>';
		$prix .= '<span class="anc_prix">'.$this->show($montant_promo, $promo_type).'</span>';
		$prix .= '</span>';

		return $prix;

	}

}
?>