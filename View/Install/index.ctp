<?php
	$this->extend('/Templates/template_default');
	$this->element('balises_metas', array('metas' => 'install'));
?>

<?php $this->assign('h1', 'Installation'); ?>

<?php $this->start('body'); ?>

	<div class="full-width-container white-bg gray-border-top gray-border-bott">
	    <div class="container padding-bottom96 padding-top48">
	        <div class="row">

	            <article class="col-md-8 col-sm-7 page-row">

	                <h3 class="title">Procédure d’installation</h3>

	                <?= $this->Form->create('Install', array('inputDefaults' => array('div' => false, 'label' => false, 'class' => 'form-control'))); ?>

						<h4>Base de données</h4>

	                    <div class="form-group ">
	                        <label>Nom de la base de données<span class="required">*</span></label>
	                        <?= $this->Form->input('Install.database', array('placeholder' => 'Nom de la bdo', 'autocomplete' => 'off')); ?>
	                    </div>

	                    <div class="form-group ">
	                        <label>Login<span class="required">*</span></label>
	                        <?= $this->Form->input('Install.login', array('placeholder' => 'Login', 'autocomplete' => 'off')); ?>
	                    </div>

	                    <div class="form-group ">
	                        <label>Mot de passe<span class="required">*</span></label>
	                        <?= $this->Form->input('Install.password', array('placeholder' => 'Mot de passe', 'autocomplete' => 'off')); ?>
	                    </div>

	                    <button type="submit" class="btn btn-theme">Créer le site web</button>

	                <?= $this->Form->end(); ?>

	            </article>

	        </div>
	    </div>
	</div>

<?php $this->end(); ?>
