<div class="row-fluid">	
	<div class="span8">
	<?php
		echo $this->WhForm->input($Modele.'.h1', 'Titre de la page');
		echo $this->WhForm->input($Modele.'.name', 'Titre dans le menu');
		echo $this->WhForm->input($Modele.'.url_r', 'Url de redirection');

		if(!empty($Parents)) echo $this->WhForm->select($Modele.'.parent_id', 'Page parente : ' , $Parents, array('class' => 'chzn-select'));
		if(!empty($MenuItems)) {

			echo $this->Form->hidden($Modele.'.menu_item_id');
			echo $this->Form->hidden('MenuItem.id');
			echo $this->Form->hidden('MenuItem.content_type_id', array('value' => $this->data[$Modele]['content_type_id']));
			echo $this->Form->hidden('MenuItem.model_id', array('value' => $this->data[$Modele]['id']));

			echo $this->WhForm->select('MenuItem.parent_id', 'Page parente : ' , $MenuItems, array('empty' => 'Racine', 'class' => 'chzn-select'));

		}
	?>
	</div>
	<div class="span4">
	<?php
		echo $this->element('FileManager.admin/admin_vignette', array('params' => array(
			'content_type_id' 	=> strtolower($Modele), //Obligatoire, sinon l'association ne se fera pas
			'id'				=> $this->data[$Modele]['id'], 
			'nbr_max' 			=> 1, 
			'group'				=> 'vignette',
			'default'			=> 1,
			'titre'				=> 'Vignette'
		)));
	?>
	</div>
</div>
<div class="row-fluid">
<?php
	echo $this->WhForm->input($Modele.'.resume', 'Résumé : ', array('class' => 'span12'));
	echo $this->WhForm->input($Modele.'.body', 'Contenu : ', array('style' => 'min-height:450px;', 'class' => 'span12 tinyMce'));
?>
</div>