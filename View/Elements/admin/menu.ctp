<?php
$html = '';
$MenuAdminActives = (isset($MenuAdminActives)) ? $MenuAdminActives : array();

echo genere_tree($tab, 0, $html, $breadcrumb = array(), $MenuAdminActives);


function genere_tree($items, $niveau = 0, &$html, $breadcrumb, $active, $id = false, $in = false) {

    if (!is_array($items)) return '';

    $niveau++;

    $html .= '<ul>';

    foreach ($items as $item) {

        $class_li = array();
        $class_li[] = (in_array($item['AdminMenu']['id'], $active)) ? 'active' : '';

        $html .= '<li class="'.implode(' ', $class_li).'">';

        if(!empty($item['AdminMenu']['name'])) {

            $item['AdminMenu']['url'] = (empty($item['AdminMenu']['url'])) ? 'javascript:void(0);' : $item['AdminMenu']['url'];
            
            if($niveau == 1) {

                $html .=  '<a href="'.$item['AdminMenu']['url'].'">';
                $html .=  (!empty($item['AdminMenu']['icon'])) ? '<i class="fa fa-lg fa-fw '.$item['AdminMenu']['icon'].'"></i>' : '<i class="fa fa-lg fa-fw fa-cog"></i>';
                $html .= '<span class="menu-item-parent">'.$item['AdminMenu']['name'].'</span>';
                $html .= '</a>';
                 

            }else{

                $html .=  '<a href="'.$item['AdminMenu']['url'].'">'.$item['AdminMenu']['name'].'</a>';

            }

        }
    
        if (!empty($item['children'])) {

            genere_tree($item['children'], $niveau, $html, $breadcrumb, $active, $id = false, $in = false);

        }

        $html .= '</li>';

    }

    $niveau--;
    
    $html .= '</ul>';

    return $html;

}
?>
