<div class="full-width-container pale-gray-bg gray-border-top gray-border-bott" id="zone_metier">

    <div class="container padding-top48 padding-bottom96 text-center">

        <br/>

        <?= ($titre) ? '<h2>'.$titre.'</h2>' : ''; ?>

        <div class="row text-center full-circles">

	        <?php if (!empty($metiers)) { ?>

		        <?php foreach ($metiers as $k => $metier) { ?>

			        <div class="col-md-3 padding-top48">
				        <a href="<?= $metier['Page']['url']; ?>" title="<?= $metier['Page']['meta_title']; ?>">
					        <?php if (!empty($metier['Page']['icon'])) { ?>
						        <div class="inline-block services-icon static-icon" style="padding: 24px 30px;">
							        <?= $this->Html->image($metier['Page']['icon']->File->url); ?>
						        </div>
					        <?php } ?>
					        <div class="padding-top24">
						        <h5 class="padding-bottom12"><?= $metier['Page']['name']; ?></h5>
						        <p><?= $metier['Page']['resume']; ?></p>
					        </div>
				        </a>
			        </div>

			        <?php if($k && !$k%3) { ?>
			            <div class="clearfix"></div>
			        <?php } ?>
			        
		        <?php } ?>
		        
	        <?php } ?>
	        
        </div>

    </div>

</div>
