<div class="relative">
    <?=(!empty($v['Actualite']['vignette'])) ? $this->Html->image($v['Actualite']['vignette']->File->url, array('alt' => $v['Actualite']['h1'])) : $this->Html->image('/themeforest/Site/images/portfolio/1.jpg', array('alt' => $v['Actualite']['h1']));?>
    <span class="hover-wrapper-home">
        <span class="overflow-hidden display-table">
            <span>
                <span class="padding-left24 padding-right24 project-links">
                    <span class="project-info text-center display-block">
                        <span class="inline-block white-border-bott letters-white padding-bottom12 hover-big-letters"><?=$v['Actualite']['h1'];?></span>
                    </span>
                    <a href="<?=$v['Actualite']['url'];?>" class="inline-block letters-white margin-top12"><?=$v['Actualite']['name'];?></a>
                    <span class="clearfix"></span>
                    <a href="<?=$v['Actualite']['url'];?>" class="inline-block letters-white">View Details</a> <span class="display-block clearfix"></span>
                </span>
            </span>
        </span>
    </span>
</div>