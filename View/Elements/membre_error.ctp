<div class="alert alert-block alert-danger">
	<a class="close" data-dismiss="alert" href="#">×</a>
	<h4 class="alert-heading"><i class="fa fa-warning"></i> Une erreur est survenue</h4>
	<p>
		<?php echo $message ?>
	</p>
</div>