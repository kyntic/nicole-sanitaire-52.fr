<!DOCTYPE html>
<html lang="en" data-ng-app="myAppAdmin">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="{{app.description}}">
    <meta name="keywords" content="app, responsive, angular, bootstrap, dashboard, admin">
    <title data-ng-bind="pageTitle()">Angle - Angular Bootstrap Admin Template</title>
    <link rel="stylesheet" href="<?=WH_ROOT;?>/app_admin/assets/css/app.css" data-ng-if="!app.layout.isRTL">
    <link rel="stylesheet" href="<?=WH_ROOT;?>/app_admin/assets/css/app-rtl.css" data-ng-if="app.layout.isRTL">
    <link rel="stylesheet" href="<?=WH_ROOT;?>/app_admin/assets/css/style.css">

    <link id="autoloaded-stylesheet" rel="stylesheet" href="<?=WH_ROOT;?>/app_admin/assets/css/theme-e.css">

    <script>
        var WH_ROOT = '<?=WH_ROOT;?>';
    </script>
</head>

<body data-ng-class="{ 'layout-fixed' : app.layout.isFixed, 'aside-collapsed' : app.layout.isCollapsed, 'layout-boxed' : app.layout.isBoxed }">

    <?= $this->fetch('content'); ?>

    <script src="<?=WH_ROOT;?>/app_admin/base.js"></script>

    <script src="<?=WH_ROOT;?>/app_admin/auth-service.js"></script>

    <script src="<?=WH_ROOT;?>/app_admin/app-login.js"></script>
</body>

</html>
