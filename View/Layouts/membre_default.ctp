<!DOCTYPE html>
<html lang="en" data-ng-app="myAppAdmin">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="{{app.description}}">
    <meta name="keywords" content="app, responsive, angular, bootstrap, dashboard, admin">
    <title data-ng-bind="pageTitle()">Angle - Angular Bootstrap Admin Template</title>
    <link rel="stylesheet" href="/app_membre/assets/css/app.css">
    <link rel="stylesheet" href="/app_membre/assets/css/style.css">
    <link id="autoloaded-stylesheet" rel="stylesheet" href="/app_admin/assets/css/theme-d.css">

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-5427601-3', 'auto');
        <?php if(!empty($this->data['Recherche']['Recherche'])) echo "ga('set', 'dimension1', '".$this->data['Recherche']['Recherche']."');"."\r\n";?>
        ga('send', 'pageview');



    </script>

</head>

<body data-ng-class="{ 'layout-fixed' : app.layout.isFixed, 'aside-collapsed' : app.layout.isCollapsed, 'layout-boxed' : app.layout.isBoxed }">

<div data-ui-view="" data-autoscroll="false" data-ng-class="mainViewAnimation" class="wrapper"></div>
<script>
    var params = {

        Url: {
            menu: '/app_membre/config/sidebar-menu.json'
        },
        Multilingue : <?=json_encode(Configure::read('multilingue'));?>,
        Locales : '<?=json_encode(Configure::read('Config.languages'));?>'

    }
</script>

<script src="/app_membre/base.js"></script>

<script src="http://code.angularjs.org/1.0.8/i18n/angular-locale_fr-fr.js"></script>

<script src="/vendor/angular-checklist/angular-checklist.js"></script>
<script src="/app_membre/auth-service.js"></script>
<script src="/vendor/angular-sortable/sortable.js"></script>
<script src="/vendor/angular-ui-tree-master/dist/angular-ui-tree.min.js"></script>


<script src="/app_membre/app.js"></script>
<script src="/app_membre/js/wh-file-manager.js"></script>
<script src="/app_membre/js/wh-modules.js"></script>
<script src="/app_membre/js/wh-dashboard.js"></script>
<script src="/app_membre/js/wh-pages.js"></script>
<script src="/app_membre/js/wh-actualites.js"></script>
<script src="/app_membre/js/wh-messagerie.js"></script>
<script src="/app_membre/js/wh-parametres.js"></script>
<script src="/app_membre/js/wh-users.js"></script>
<script src="/app_membre/js/wh-groups.js"></script>
<script src="/app_membre/js/wh-agenda.js"></script>

</body>



</html>