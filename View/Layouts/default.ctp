<!DOCTYPE html>

<head>

    <?php
    echo $this->Html->charset();
    echo '<title>' . $this->fetch('meta_title') . '</title>';
    echo $this->Html->meta('description', $this->fetch('meta_description'));
    echo $this->Html->meta('keywords', $this->fetch('meta_keywords'));
    echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));
    echo $this->Html->meta(array('name' => 'author', 'content' => 'Whatson Web'));
    echo $this->Html->meta(array('name' => 'robots', 'content' => $this->fetch('meta_robots')));

    ?>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="<?=WH_ROOT;?>/img/favicon.png">

    <!-- ///////////LE TEMPLATE///////////  /themeforest/Site/ -->
    <?= $this->Html->css(
        'http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,200italic,300,300italic,400italic,700'
    ); ?>
    <?= $this->Html->css(
        'http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic'
    ); ?>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>

    <!-- Bootstrap CSS and JS Files -->
    <link rel="stylesheet" type="text/css" href="<?=WH_ROOT;?>/themeforest/Site/css/bootstrap.min.css"/>
    <script type="text/javascript" src="<?=WH_ROOT;?>/themeforest/Site/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <?= $this->fetch('carroussel_script'); ?>

    <!-- Theme CSS and JS Files files -->

    <link rel="stylesheet" type="text/css" href="<?=WH_ROOT;?>/themeforest/Site/css/margins-paddings.css"/>
    <link rel="stylesheet" href="<?=WH_ROOT;?>/themeforest/Site/css/idangerous.swiper.css">
    <link rel="stylesheet" type="text/css" href="<?=WH_ROOT;?>/themeforest/Site/css/prettyPhoto.css"/>

    <?= $this->fetch('usquare'); ?>
    <?= $this->fetch('rerefence'); ?>

    <?= $this->Minify->css('/themeforest/Site/style.css'); ?>
    <?= $this->Minify->css('/themeforest/Site/css/responsive'); ?>

    <?= $this->Minify->css('/css/layout.css'); ?>
    <?= $this->fetch('css'); ?>

    <?= $this->Minify->script('/themeforest/Site/js/prettyPhoto.js'); ?>
    <?= $this->Minify->script('/themeforest/Site/js/prettyPhoto-ini.js'); ?>
    <?= $this->Minify->script('/themeforest/Site/js/ie.html5.js'); ?>
    <?= $this->Minify->script('/themeforest/Site/js/jquery-color.js'); ?>
    <?= $this->Minify->script('/themeforest/Site/js/header.js'); ?>
    <?= $this->Minify->script('/themeforest/Site/js/common.js'); ?>
    <?= $this->Minify->script('/themeforest/Site/js/jquery.mousewheel.js'); ?>
    <?= $this->Minify->script('/themeforest/Site/js/jquerry.easing.1.3.js'); ?>
    <?= $this->Minify->script('/themeforest/Site/js/swipe-screen.js'); ?>
    <?= $this->Minify->script('/themeforest/Site/js/idangerous.swiper-2.1.min.js'); ?>
    <?= $this->Minify->script('/themeforest/Site/js/idangerous.swiper.scrollbar-2.1.js'); ?>

    <?= $this->Minify->css('/themeforest/Site/css/style.css'); ?>

</head>

<body class="relative">
<?php
//Google tag manager
$ga = Configure::read('Params.google_tag_manager');
echo $ga;
?>

<div class="peter-river">

    <?=$this->fetch('content');?>

    <div style="position:absolute;top:0px;left:0px;width:100%;">
        <header class="relative">
            <div class="header_nav no-border-bott relative">
                <!-- MENU -->
                <div class="white-bg header-box-shadow">
                    <div class="container header_height">
                        <div class="row text-center relative">
                            <div class="header-nav-wrap relative text-right">
                                <div class="zero-logo pull-left">
                                    <?=$this->Html->link('<img src="'.WH_ROOT.'/img/logo.png" alt="" />',
                                        FULL_BASE_URL,
                                        array('escape' => false, 'id' => 'logoHeader')
                                    );?>
                                </div>
                                <nav class="inline-block text-right">
                                    <div class="navbar margin0 relative">
                                        <div class="navbar-inner">
                                            <?php
                                            if(!empty($_menuPages)) {
                                                $this->WhTree->html = '';
                                                $this->WhTree->Model = 'Page';
                                                $this->WhTree->niveau = 0;
                                                echo $this->WhTree->generate_nav_simple($_menuPages);
                                            }
                                            ?>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </nav>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- header_nav -->
        </header>
    </div>

    <footer>
        <div class="footer-bg">
            <div class="container padding-bottom12">
                <!-- ABOUT -->
                <div class="col-md-4 relative">
                    <div class="border-box">
                        <div class="gray-border-footer">
                            <h6 class="padding-top48 padding-bottom12">ABOUT</h6>
                        </div>
                        <div class="about-footer-txt padding-top12"><?=nl2br(Configure::read('Params.about'));?></div>
                    </div>
                </div>
                <!-- MENU -->
                <div class="col-md-1">
                    <div class="gray-border-footer">
                        <h6 class="padding-top48 padding-bottom12">MENU</h6>
                    </div>
                    <div class="about-footer-txt padding-top12">
                        <ul class="margin0 no-style footer-nav padding0">
                            <?php
                            foreach($_menuPages as $v) : ;
                                echo '<li>'.$this->Html->link($v['Page']['name'], $v['Page']['url']).'</li>';
                            endforeach;
                            ?>
                        </ul>
                    </div>
                </div>
                <!-- CONTACT -->
                <div class="col-md-3">
                    <div class="gray-border-footer">
                        <h6 class="padding-top48 padding-bottom12">CONTACT</h6>
                    </div>
                    <div class="about-footer-txt padding-top12">
                        <ul class="margin0 no-style margin-top12 contact-info padding0">
                            <li><?php echo $this->Html->link($this->Html->image('logo.png'), FULL_BASE_URL, array('escape' => false)); ?></li>
                            <li><?=Configure::read('Params.adresse_1');?> <?=Configure::read('Params.cp');?>  <?=Configure::read('Params.ville');?></li>
                            <li>Tel.: <?=Configure::read('Params.telephone');?></li>
                            <li><?php echo $this->Html->link('Nous contacter', $_lienContact['Page']['url']);?></li>
                            <li><?php echo $this->Html->link('Mentions légales', Configure::read('Params.mentions_page'), array('class' => 'mention_legale')); ?></li>
                            <li>Développement : <?php echo $this->Html->link('Kobaltis','http://www.kobaltis.com', array('class' => 'credit', 'target' => '_blank', 'title' => 'Web agency', 'style' => 'display:inline-block;vertical-align:top;')); ?></li>
                        </ul>
                    </div>
                </div>
                <!-- TWEET -->
                <div class="col-md-4">

                    <div class="gray-border-footer">
                        <h6 class="padding-top48 padding-bottom12">NOUS SUIVRE</h6>
                    </div>
                    <div class="about-footer-txt padding-top12">
                        <?=$this->element('NewsletterManager.formulaire');?>
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="container padding-bottom24">
                <!-- ABOUT -->
                <div class="clearfix"></div>
            </div>
        </div>
    </footer>


    <div class="modal hide fade" id="popup_ajx">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="popupLabelTitre"></h3>
        </div>
        <div class="modal-body"></div>
    </div>

</div>
<?php
$this->Html->scriptStart(array('inline' => false, 'block' => 'scriptBottom'));
?>
var $ = jQuery;

<?php


if (!empty($_breadcrumb)) {

    $item = array();
    foreach ($_breadcrumb['Chemin'] as $v) {
        $item[] = '.item.' . strtolower(
                $_breadcrumb['Model']
            ) . '_' . $v[$_breadcrumb['Model']]['id'];
    }

    echo '$(\'' . implode(', ', $item) . '\').addClass(\'active\');';


}
?>


<?php
$this->Html->scriptEnd();
echo $this->Minify->script('loading/jquery.showLoading.min.js');
echo $this->fetch('scriptBottom');
echo $this->Js->writeBuffer(); // Le code jascript en cache

echo $this->fetch('scriptSuperBottom');
?>
</body>

</html>