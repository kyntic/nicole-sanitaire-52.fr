<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php 
foreach($SiteMap as $v) {
	echo '<url>';
	echo 	'<loc>'.$v['loc'].'</loc>';
	echo 	'<lastmod>'.$v['lastmod'].'</lastmod>';
	echo 	'<changefreq>'.$v['changefreq'].'</changefreq>';
	echo 	'<priority>'.$v['priority'].'</priority>';
	echo '</url>';
}
?>
</urlset>
