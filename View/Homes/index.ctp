<?php
$this->Html->css('home.css', null, array('inline' => false));
$this->element('balises_metas', array('metas' => $page['Page']));
?>
<!-- CAROUSSEL -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php foreach($portfolioItems as $k => $item) : ;?>
            <li data-target="#myCarousel" data-slide-to="<?=$k;?>" class="<?=(!$k) ? 'active' : '';?>"></li>
        <?php endforeach;?>
    </ol>
    <div class="carousel-inner" role="listbox">
        <?php foreach($portfolioItems as $k => $item) : ;?>
            <div class="item <?=(!$k) ? 'active' : '';?>">
                <?=$this->Html->image($item['File']['url'], array('alt' => $item['File']['name']));?>
                <div class="container">
                    <div class="carousel-caption">
                        <h2><?=$item['File']['name'];?></h2>
                        <p><?=$item['File']['description'];?></p>
                        <?php if(!empty($item['File']['lien'])) : ;?><p><a class="" href="<?=$item['File']['lien'];?>" role="button">Lire la suite</a></p><?php endif;?>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<!-- CORP -->
<div class="full-width-container white-bg gray-border-top gray-border-bott">
    <div class="container padding-bottom96 padding-top48">
        <div class="row">
            <div class="display-table-stat">
                <div>
                    <div class="col-md-6 padding-top48">
                        <div class="postslider">
                            <div class="margin-bottom12"><h1><?=$page['Page']['h1'];?></h1></div>
                            <div class="line-height26"><?=$page['Page']['body'];?></div>
                        </div>
                    </div>
                    <div class="col-md-6 text-left padding-top48">
                        <?php
                        if(!empty($page['Page']['vignette']))
                            echo $this->Html->image($page['Page']['vignette']->File->url, array('class' => 'inline-block', 'alt' => $page['Page']['vignette']->File->name));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
	if (!empty($ContentModules)) {

		foreach ($ContentModules as $k => $module) {

			switch ($module['ContentModule']['module']) {

				default :

					$element = 'ModuleManager.' . $module['ContentModule']['module'];
					if (!empty($module['ContentModule']['template'])) {
						$element .= $module['ContentModule']['template'];
					}

					echo $this->element(
			          $element,
			          array(
				          'page' => $page['Page'],
				          'content' => $module['ContentModule'],
				          'classBlock' => ($k%2) ? 'pale-gray-bg' : 'white-bg'
			          )
					);

					break;
			}
		}
	}

?>

<?= $this->element('metiers', array('metiers' => $metiers, 'titre' => 'Nos métiers'));?>

<a href="/realisations/p-3">
    <div class="full-width-container info_box">
        <div class="container">
            <div class="row padding-top24 padding-bottom24">
                <div class="col-md-12 letters-white">
                    <div class="postslider letters-white pull-left">Découvrez nos réalisations</div>
                    <div class="pull-right arrow-blue-bg-link"><img src="/img/pointer-right.png" alt=""></div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</a>