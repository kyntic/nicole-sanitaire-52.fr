
<?php foreach($Modules as $module) : ;?>
    <tr>
        <td><?=$module['ContentModule']['model_asso'];?></td>
        <td><?=$module['ContentModule']['model_asso_id'];?></td>
        <td></td>
        <td style="text-align:right;">
            <?=$this->Html->link('<i class="fa fa-trash-o"></i> ', '/admin/content_modules/delete/'.$module['ContentModule']['id'], array('class' => 'btn btn-danger btn-xs', 'escape' => false), 'Etes vous sûre de vouloir supprimer ce module ?');?>
        </td>
    </tr>
<?php endforeach;?>