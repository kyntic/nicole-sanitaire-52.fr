-- MySQL dump 10.13  Distrib 5.6.42, for Linux (x86_64)
--
-- Host: localhost    Database: nicole-sanitaire-52-fr
-- ------------------------------------------------------
-- Server version	5.6.42-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acos`
--

DROP TABLE IF EXISTS `acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acos`
--

LOCK TABLES `acos` WRITE;
/*!40000 ALTER TABLE `acos` DISABLE KEYS */;
/*!40000 ALTER TABLE `acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actualites`
--

DROP TABLE IF EXISTS `actualites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actualites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_type_id` varchar(128) NOT NULL,
  `etat_id` varchar(128) NOT NULL,
  `page_id` int(11) NOT NULL,
  `acces_membre` tinyint(1) DEFAULT '0',
  `type` varchar(128) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `h1` varchar(256) NOT NULL,
  `sous_titre` varchar(256) DEFAULT NULL,
  `vignette` text NOT NULL,
  `url` varchar(256) NOT NULL,
  `url_r` varchar(256) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_title` varchar(256) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_robots` varchar(128) NOT NULL,
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `publication_date` int(11) DEFAULT NULL,
  `publication_date_fin` int(11) DEFAULT NULL,
  `agenda_id` int(11) DEFAULT NULL,
  `even_date_deb` datetime NOT NULL,
  `even_date_fin` datetime NOT NULL,
  `all_day` tinyint(1) NOT NULL DEFAULT '0',
  `suscribe` tinyint(1) NOT NULL DEFAULT '0',
  `suscribe_nbr` int(11) NOT NULL DEFAULT '0',
  `color` varchar(128) DEFAULT NULL,
  `adresse` text NOT NULL,
  `recurence` varchar(128) NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  `push_accueil` tinyint(1) NOT NULL,
  `auteur_id` int(11) DEFAULT NULL,
  `from` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actualites`
--

LOCK TABLES `actualites` WRITE;
/*!40000 ALTER TABLE `actualites` DISABLE KEYS */;
/*!40000 ALTER TABLE `actualites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adresses`
--

DROP TABLE IF EXISTS `adresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adresses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `civilite_id` int(11) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `prenom` varchar(128) NOT NULL,
  `nom_entreprise` varchar(256) NOT NULL,
  `adresse_1` varchar(256) NOT NULL,
  `adresse_2` varchar(256) NOT NULL,
  `cp` varchar(16) NOT NULL,
  `ville` varchar(256) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `mobile` varchar(32) DEFAULT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adresses`
--

LOCK TABLES `adresses` WRITE;
/*!40000 ALTER TABLE `adresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `adresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agendas`
--

DROP TABLE IF EXISTS `agendas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agendas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `page_id` int(11) NOT NULL,
  `last_even_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agendas`
--

LOCK TABLES `agendas` WRITE;
/*!40000 ALTER TABLE `agendas` DISABLE KEYS */;
/*!40000 ALTER TABLE `agendas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros`
--

DROP TABLE IF EXISTS `aros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros`
--

LOCK TABLES `aros` WRITE;
/*!40000 ALTER TABLE `aros` DISABLE KEYS */;
/*!40000 ALTER TABLE `aros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros_acos`
--

DROP TABLE IF EXISTS `aros_acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros_acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) unsigned NOT NULL,
  `aco_id` int(10) unsigned NOT NULL,
  `_create` char(2) NOT NULL DEFAULT '0',
  `_read` char(2) NOT NULL DEFAULT '0',
  `_update` char(2) NOT NULL DEFAULT '0',
  `_delete` char(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros_acos`
--

LOCK TABLES `aros_acos` WRITE;
/*!40000 ALTER TABLE `aros_acos` DISABLE KEYS */;
/*!40000 ALTER TABLE `aros_acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute_families`
--

DROP TABLE IF EXISTS `attribute_families`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_families` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `name_admin` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `speciale` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_families`
--

LOCK TABLES `attribute_families` WRITE;
/*!40000 ALTER TABLE `attribute_families` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_families` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attributes`
--

DROP TABLE IF EXISTS `attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_family_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `vignette` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attributes`
--

LOCK TABLES `attributes` WRITE;
/*!40000 ALTER TABLE `attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_assocs`
--

DROP TABLE IF EXISTS `content_assocs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_assocs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(128) DEFAULT NULL,
  `model_id` int(11) NOT NULL,
  `model_asso` varchar(128) DEFAULT NULL,
  `model_asso_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_assocs`
--

LOCK TABLES `content_assocs` WRITE;
/*!40000 ALTER TABLE `content_assocs` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_assocs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_modules`
--

DROP TABLE IF EXISTS `content_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(128) NOT NULL,
  `model_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `module` varchar(128) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_modules`
--

LOCK TABLES `content_modules` WRITE;
/*!40000 ALTER TABLE `content_modules` DISABLE KEYS */;
INSERT INTO `content_modules` VALUES (69,'Page',1,0,'galerie','{\"title\":\"\",\"files\":[{\"File\":{\"id\":\"27\",\"name\":\"Entreprise Nicole\",\"description\":\"un savoir faire familial et artisanal depuis 1915 !\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"slide_556d70d2297cd0\",\"nom_fichier\":\"slide_556d70d2297cd0.jpg\",\"poids\":\"514\",\"dimenssions\":{\"width\":1920,\"height\":529,\"reco\":\"trop grande\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/03\\/slide_556d70d2297cd0.jpg\",\"created\":\"2016-03-02 16:56:18\",\"updated\":\"2016-03-02 16:56:18\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"26\",\"name\":\"Entreprise Nicole\",\"description\":\"installateur expert d\\u2019\\u00e9nergies renouvelables\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"slide_456d70d1fc52a2\",\"nom_fichier\":\"slide_456d70d1fc52a2.jpg\",\"poids\":\"1219\",\"dimenssions\":{\"width\":1920,\"height\":531,\"reco\":\"trop grande\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/03\\/slide_456d70d1fc52a2.jpg\",\"created\":\"2016-03-02 16:56:15\",\"updated\":\"2016-03-02 16:56:15\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"25\",\"name\":\"Entreprise Nicole\",\"description\":\"propose divers syst\\u00e8mes de traitement de l\'eau\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"slide_356d70d0cd1969\",\"nom_fichier\":\"slide_356d70d0cd1969.jpg\",\"poids\":\"831\",\"dimenssions\":{\"width\":1920,\"height\":531,\"reco\":\"trop grande\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/03\\/slide_356d70d0cd1969.jpg\",\"created\":\"2016-03-02 16:55:56\",\"updated\":\"2016-03-02 16:55:56\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"24\",\"name\":\"Entreprise Nicole\",\"description\":\"prend en charge tous vos travaux de plomberie\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"slide_256d70d00b5798\",\"nom_fichier\":\"slide_256d70d00b5798.jpg\",\"poids\":\"517\",\"dimenssions\":{\"width\":1920,\"height\":531,\"reco\":\"trop grande\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/03\\/slide_256d70d00b5798.jpg\",\"created\":\"2016-03-02 16:55:44\",\"updated\":\"2016-03-02 16:55:44\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"23\",\"name\":\"Entreprise Nicole\",\"description\":\"chaudi\\u00e8res et des chauffe-eau s\\u00e9lectionn\\u00e9s chez les plus grands constructeurs\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"slide_156d70cf9a0819\",\"nom_fichier\":\"slide_156d70cf9a0819.jpg\",\"poids\":\"472\",\"dimenssions\":{\"width\":1920,\"height\":531,\"reco\":\"trop grande\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/03\\/slide_156d70cf9a0819.jpg\",\"created\":\"2016-03-02 16:55:37\",\"updated\":\"2016-03-02 16:55:37\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}}],\"position\":1}'),(89,'Page',8,0,'txt','{\"title\":\"\",\"txt\":\"Votre texte\"}'),(90,'Page',7,0,'txt','{\"title\":\"\",\"txt\":\"<iframe src=\\\"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m14!1m8!1m3!1d10671.07892075661!2d5.226516!3d48.037474!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xad0866b06c2a0486!2sEntreprise+Nicole!5e0!3m2!1sfr!2sfr!4v1456908825810\\\" style=\\\"border:0\\\" allowfullscreen=\\\"\\\" frameborder=\\\"0\\\" height=\\\"450\\\" width=\\\"100%\\\">\\n<\\/iframe>\"}'),(94,'Page',6,0,'galerie','{\"title\":\"\",\"files\":[{\"File\":{\"id\":\"35\",\"name\":\"Cr\\u00e9ation d\'une douche \\u00e0 l\'italienne\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"17556d85ed37d16f\",\"nom_fichier\":\"17556d85ed37d16f.jpg\",\"poids\":\"36\",\"dimenssions\":{\"width\":600,\"height\":800,\"reco\":\"normal\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2016\\/03\\/17556d85ed37d16f.jpg\",\"created\":\"2016-03-03 16:57:07\",\"updated\":\"2016-03-03 16:57:07\"},\"Folder\":{\"id\":\"1\",\"name\":\"real\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"36\",\"name\":\"Cr\\u00e9ation d\'une douche \\u00e0 l\'italienne\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"17656d85ed4759f4\",\"nom_fichier\":\"17656d85ed4759f4.jpg\",\"poids\":\"32\",\"dimenssions\":{\"width\":600,\"height\":800,\"reco\":\"normal\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2016\\/03\\/17656d85ed4759f4.jpg\",\"created\":\"2016-03-03 16:57:08\",\"updated\":\"2016-03-03 16:57:08\"},\"Folder\":{\"id\":\"1\",\"name\":\"real\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"34\",\"name\":\"Cr\\u00e9ation d\'une douche \\u00e0 l\'italienne\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"17356d85ed23eed7\",\"nom_fichier\":\"17356d85ed23eed7.jpg\",\"poids\":\"39\",\"dimenssions\":{\"width\":600,\"height\":800,\"reco\":\"normal\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2016\\/03\\/17356d85ed23eed7.jpg\",\"created\":\"2016-03-03 16:57:06\",\"updated\":\"2016-03-03 16:57:06\"},\"Folder\":{\"id\":\"1\",\"name\":\"real\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"32\",\"name\":\"Cr\\u00e9ation d\'une douche \\u00e0 l\'italienne\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"18156d85ed05ef6f\",\"nom_fichier\":\"18156d85ed05ef6f.jpg\",\"poids\":\"35\",\"dimenssions\":{\"width\":600,\"height\":800,\"reco\":\"normal\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2016\\/03\\/18156d85ed05ef6f.jpg\",\"created\":\"2016-03-03 16:57:04\",\"updated\":\"2016-03-03 16:57:04\"},\"Folder\":{\"id\":\"1\",\"name\":\"real\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"33\",\"name\":\"Cr\\u00e9ation d\'une douche \\u00e0 l\'italienne\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"18256d85ed0eaa7e\",\"nom_fichier\":\"18256d85ed0eaa7e.jpg\",\"poids\":\"37\",\"dimenssions\":{\"width\":600,\"height\":800,\"reco\":\"normal\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2016\\/03\\/18256d85ed0eaa7e.jpg\",\"created\":\"2016-03-03 16:57:04\",\"updated\":\"2016-03-03 16:57:04\"},\"Folder\":{\"id\":\"1\",\"name\":\"real\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"31\",\"name\":\"Cr\\u00e9ation d\'une douche \\u00e0 l\'italienne\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"18056d85ecf59f75\",\"nom_fichier\":\"18056d85ecf59f75.jpg\",\"poids\":\"33\",\"dimenssions\":{\"width\":600,\"height\":800,\"reco\":\"normal\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2016\\/03\\/18056d85ecf59f75.jpg\",\"created\":\"2016-03-03 16:57:03\",\"updated\":\"2016-03-03 16:57:03\"},\"Folder\":{\"id\":\"1\",\"name\":\"real\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"29\",\"name\":\"Cr\\u00e9ation d\'une douche \\u00e0 l\'italienne\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"17856d85ece1a677\",\"nom_fichier\":\"17856d85ece1a677.jpg\",\"poids\":\"31\",\"dimenssions\":{\"width\":600,\"height\":800,\"reco\":\"normal\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2016\\/03\\/17856d85ece1a677.jpg\",\"created\":\"2016-03-03 16:57:02\",\"updated\":\"2016-03-03 16:57:02\"},\"Folder\":{\"id\":\"1\",\"name\":\"real\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"30\",\"name\":\"Cr\\u00e9ation d\'une douche \\u00e0 l\'italienne\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"17956d85ecee4c45\",\"nom_fichier\":\"17956d85ecee4c45.jpg\",\"poids\":\"29\",\"dimenssions\":{\"width\":600,\"height\":800,\"reco\":\"normal\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2016\\/03\\/17956d85ecee4c45.jpg\",\"created\":\"2016-03-03 16:57:02\",\"updated\":\"2016-03-03 16:57:02\"},\"Folder\":{\"id\":\"1\",\"name\":\"real\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"28\",\"name\":\"Cr\\u00e9ation d\'une douche \\u00e0 l\'italienne\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"17756d85eccd1c02\",\"nom_fichier\":\"17756d85eccd1c02.jpg\",\"poids\":\"25\",\"dimenssions\":{\"width\":600,\"height\":800,\"reco\":\"normal\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2016\\/03\\/17756d85eccd1c02.jpg\",\"created\":\"2016-03-03 16:57:00\",\"updated\":\"2016-03-03 16:57:00\"},\"Folder\":{\"id\":\"1\",\"name\":\"real\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"37\",\"name\":\"Chauffage\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"17156d85faebf271\",\"nom_fichier\":\"17156d85faebf271.jpg\",\"poids\":\"38\",\"dimenssions\":{\"width\":600,\"height\":800,\"reco\":\"normal\"},\"file_folder_id\":\"2\",\"url\":\"\\/files\\/2016\\/03\\/17156d85faebf271.jpg\",\"created\":\"2016-03-03 17:00:46\",\"updated\":\"2016-03-03 17:00:46\"},\"Folder\":{\"id\":\"2\",\"name\":\"chauffage\",\"parent_id\":\"1\",\"lft\":\"2\",\"rght\":\"3\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}}],\"position\":1}'),(95,'Page',5,0,'txtimg','{\"title\":\"\",\"txt\":\"<h2>Syst\\u00e8me de traitement de l\'eau \\u00e0 Foulain<\\/h2><p> Profitez de vos installations sanitaires ou de loisirs sans crainte gr\\u00e2ce aux divers <strong>syst\\u00e8mes de traitement de l\'eau<\\/strong> propos\\u00e9es par l\'<strong>entreprise Nicole<\\/strong> \\u00e0 <strong>Foulain<\\/strong>, pr\\u00e8s de <strong>Chaumont<\\/strong> (52).Nos \\u00e9quipes vous proposent un syst\\u00e8me de traitement d\'eau efficace pour votre installation afin de rendre votre eau plus pure tout en luttant contre le calcaire et la corrosion.<br> <\\/p><ul><li><strong>Chaudi\\u00e8res<\\/strong><\\/li><li><strong>Robinetterie<\\/strong><\\/li><li><strong>Electrom\\u00e9nager<\\/strong><\\/li><li><strong>Piscine<\\/strong><\\/li><li><strong>Eau potable<\\/strong><\\/li><\\/ul><p> .... C\'est pourquoi nos professionnels s\'engagent \\u00e0 vous installer un syst\\u00e8me de traitement d\'eau efficace pour que vous ne rencontriez plus ces d\\u00e9sagr\\u00e9ments. <\\/p>\",\"img\":{\"File\":{\"id\":\"38\",\"name\":\"img-traitement\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"img_traitement56d860137a894\",\"nom_fichier\":\"img_traitement56d860137a894.jpg\",\"poids\":\"15\",\"dimenssions\":{\"width\":300,\"height\":214,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/03\\/img_traitement56d860137a894.jpg\",\"created\":\"2016-03-03 17:02:27\",\"updated\":\"2016-03-03 17:02:27\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},\"params\":{\"prop\":\"img-right\"},\"position\":4}'),(96,'Page',5,1,'txt','{\"title\":\"\",\"txt\":\"<h2>Ventilations<\\/h2><p>\\n\\tEl\\u00e9ment incontournable de l\'habitat moderne la VMC assure un renouvellement de l\'air et participe au bon fonctionnement de votre chauffage.\\n<\\/p><p>\\n\\tPar un syst\\u00e8me simple de r\\u00e9gulation hygror\\u00e9glable qui limite l\'air extrait en fonction des besoins ou par la r\\u00e9cup\\u00e9ration des calories de l\'air extrait gr\\u00e2ce \\u00e0 un \\u00e9changeur qui permet de r\\u00e9chauffer l\'air neuf.\\n<\\/p><p>\\n\\tL\'<strong>entreprise Nicole<\\/strong> \\u00e0 <strong>Foulain<\\/strong>, pr\\u00e8s de <strong>Chaumont<\\/strong> (52) vous guidera pour bien choisir entre les diff\\u00e9rents types de ventilations possibles.\\n<br>\\n<\\/p><ul>\\n\\t<li>Ventilation m\\u00e9canique contr\\u00f4l\\u00e9e<\\/li>\\n\\t<li>Tourelle d\'extraction<\\/li>\\n\\t<li>Plafond filtrant<\\/li>\\n\\t<li>Hotte grande cuisine<\\/li>\\n\\t<li>Gaine acier \\u2013 aluminium<\\/li>\\n\\t<li>A\\u00e9rateur ind\\u00e9pendant<\\/li>\\n\\t<li>D\\u00e9senfumage<\\/li>\\n\\t<li>Ventilateur<\\/li>\\n\\t<li>Grille et diffuseur<\\/li>\\n<\\/ul>\",\"position\":2}'),(97,'Page',5,2,'txt','{\"title\":\"\",\"txt\":\"<p>\\n\\t<strong>Vous avez un projet ?<\\/strong> Contactez-nous au <strong>03 25 03 14 54<\\/strong> ou laissez-nous un message sur notre site Internet.<span><\\/span>\\n<\\/p>\",\"position\":3}'),(98,'Page',4,0,'txtimg','{\"title\":\"\",\"txt\":\"<h2>Energies Renouvelables<\\/h2><p> Profitez de l\'exp\\u00e9rience de <strong>l\'entreprise Nicole<\\/strong> \\u00e0 Foulain, pr\\u00e8s de Chaumont (52), <strong>installateur expert \\u00e9nergies renouvelables<\\/strong>. <\\/p><p> Nous vous proposons des installations \\u00e9conomiques en terme de prix et de consommation. Nos solutions de chauffage en \\u00e9nergies renouvelables vous permettront de r\\u00e9duire votre facture de chauffage de 30%.Energies<\\/p><ul><li>A\\u00e9rothermie<\\/li><li>G\\u00e9othermie<\\/li><li>Solaire<\\/li><li>Bois<\\/li><\\/ul><h2>Diffusion<\\/h2><ul><li>Radiateurs caloporteurs<\\/li><li>Plancher chauffant<\\/li><li>S\\u00e8che-serviettes<\\/li><\\/ul><p> De l\'installation compl\\u00e8te \\u00e0 la mise en place de syst\\u00e8mes de r\\u00e9gulation, en passant par l\'ensemble des r\\u00e9glages, nous vous proposons des <strong>chaudi\\u00e8res et des chauffe-eau s\\u00e9lectionn\\u00e9s chez les plus grands constructeurs<\\/strong> toujours \\u00e0 la recherche du meilleur compromis qualit\\u00e9 \\/ prix. <\\/p>\",\"img\":{\"File\":{\"id\":\"39\",\"name\":\"img-energies\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"img_energies56d8606784fb2\",\"nom_fichier\":\"img_energies56d8606784fb2.jpg\",\"poids\":\"20\",\"dimenssions\":{\"width\":300,\"height\":201,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/03\\/img_energies56d8606784fb2.jpg\",\"created\":\"2016-03-03 17:03:51\",\"updated\":\"2016-03-03 17:03:51\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},\"params\":{\"prop\":\"img-right\"},\"position\":4}'),(99,'Page',4,1,'txtimg','{\"title\":\"\",\"txt\":\"<h2>Chauffage traditionnel<\\/h2><p> B\\u00e9n\\u00e9ficiez du confort offert par les installations de chauffage de l\'entreprise Nicole \\u00e0 Foulain, pr\\u00e8s de Chaumont (52). <\\/p><p> Nous vous faisons profiter de nos qualifications et de nos comp\\u00e9tences en mati\\u00e8re de <strong>chauffage toutes \\u00e9nergies<\\/strong>. Nos \\u00e9quipes vous conseillent et vous orientent vers l\'installation de chauffage la mieux adapt\\u00e9e \\u00e0 votre b\\u00e2timent et \\u00e0 votre style de vie.En fonction de votre projet, nous vous proposerons des solutions respectueuses de l\'environnement en vous apportant une chaleur douce et saine, id\\u00e9ale pour votre confort. <\\/p><p> Nous sommes en mesure de r\\u00e9nover ou d\'effectuer, votre installation de chauffage en vous proposant au choix :<br> <br> <\\/p><ul><li><strong>Chauffage bois<\\/strong><\\/li><li><strong>Chauffage gaz<\\/strong><\\/li><li><strong>Chauffage fioul<\\/strong><\\/li><\\/ul><p> De l\'installation compl\\u00e8te \\u00e0 la mise en place de syst\\u00e8mes de r\\u00e9gulation, en passant par l\'ensemble des r\\u00e9glages, nous vous proposons des <strong>chaudi\\u00e8res et des chauffe-eau s\\u00e9lectionn\\u00e9s chez les plus grands constructeurs<\\/strong> toujours \\u00e0 la recherche du meilleur compromis qualit\\u00e9 \\/ prix. <\\/p>\",\"img\":{\"File\":{\"id\":\"40\",\"name\":\"img-chauffage\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"img_chauffage56d8606825366\",\"nom_fichier\":\"img_chauffage56d8606825366.jpg\",\"poids\":\"15\",\"dimenssions\":{\"width\":300,\"height\":204,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/03\\/img_chauffage56d8606825366.jpg\",\"created\":\"2016-03-03 17:03:52\",\"updated\":\"2016-03-03 17:03:52\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},\"params\":{\"prop\":\"img-right\"},\"position\":5}'),(100,'Page',4,2,'txt','{\"title\":\"\",\"txt\":\"<p>\\n\\t<strong>Vous avez un projet ?<\\/strong> Contactez-nous au <strong>03 25 03 14 54<\\/strong> ou remplissez le formulaire de contact.<span><\\/span>\\n<\\/p>\",\"position\":3}'),(101,'Page',3,0,'txt','{\"title\":\"Plomberie\",\"txt\":\"<h2>Plomberie<\\/h2><p>\\n\\tVous souhaitez b\\u00e9n\\u00e9ficier de nouveaux sanitaires et lavabos ? Remplacer une plomberie us\\u00e9es ? L\'entreprise Nicole \\u00e0 Foulain pr\\u00e8s de Chaumont (52) prend en charge tous vos travaux de plomberie.\\n<\\/p><p>\\n\\tNous nous occupons de l\'installation, de la <strong>r\\u00e9novation et du d\\u00e9pannage de votre tuyauterie, de vos sanitaires et de vos appareils de distribution d\'eau et de gaz.<\\/strong>\\n<\\/p><p>\\n\\tNous sommes qualifi\\u00e9s pour vous offrir une prestation compl\\u00e8te. Nous intervenons sur tous les types de tuyaux: acier, cuivre ou PVC.\\n<\\/p><p>\\n\\tFa\\u00eetes nous confiance pour vous apporter toute notre exp\\u00e9rience et notre savoir faire pour r\\u00e9gler :<br>\\n<\\/p><ul>\\n\\t<li><strong>vos probl\\u00e8mes de fuites d\'eau<\\/strong><\\/li>\\n\\t<li><strong>remettre \\u00e0 neuf vos installations<\\/strong><\\/li>\\n\\t<li><strong>r\\u00e9aliser vos projets de plomberie<\\/strong><\\/li>\\n<\\/ul><p>\\n\\tPas de surprises sur les factures et respect des d\\u00e9lais !\\n<\\/p>\",\"template\":\"\"}'),(102,'Page',3,1,'txt','{\"title\":\"Sanitaires\",\"txt\":\"<h2>Sanitaires<\\/h2><p>\\n\\tVous souhaitez disposer d\'une cuisine ou d\'une salle de bain utilisant les derni\\u00e8res nouveaut\\u00e9s en mati\\u00e8re de sanitaires ?\\n<\\/p><p>\\n\\tL\'entreprise Nicole \\u00e0 Foulain, pr\\u00e8s de Chaumont, prend en charge vos <strong>travaux neufs et r\\u00e9novation de cuisine et bains.<\\/strong>\\n<\\/p><p>\\n\\tNos \\u00e9quipes assurent la pose et le remplacement de tous les \\u00e9l\\u00e9ments indispensables au bon fonctionnement sanitaire de votre salle de bain ou de votre cuisine : <strong>chauffe-eau, cumulus, raccordements, robinetteries...<\\/strong>\\n<\\/p><h2>Cuisine<\\/h2><p>\\n\\tNous r\\u00e9alisons tous les travaux de plomberie de votre cuisine.\\n<\\/p><p>\\n\\tNous assurons la pose et le remplacement de la robinetterie de votre cuisine : mitigeurs, m\\u00e9langeurs\\u2026 Nous vous proposons un large choix de mat\\u00e9riels, accessible \\u00e0 tous les budgets.\\n<\\/p><h2><img src=\\\"http:\\/\\/www.plombier-nicole-52.com\\/images\\/img-sanitaire.jpg\\\" style=\\\"float: left; margin: 0px 10px 10px 0px;\\\" alt=\\\"\\\">Salle de bain<\\/h2><p>\\n\\tNous effectuons tous les travaux de plomberie de votre salle de bain :\\n<\\/p><p>\\n\\tInstallation et cr\\u00e9ation de salle de bain, pose de cabine de douche, de baignoire, remplacement de robinetteries de salle de bain : mitigeurs et m\\u00e9langeurs\\u2026\\n<\\/p><h2>Spas-Baln\\u00e9oth\\u00e9rapie<\\/h2><p>\\n\\tNous r\\u00e9alisons l\'ensemble de vos travaux concourant \\u00e0 la d\\u00e9tente et \\u00e0 la relaxation :\\n<\\/p><p>\\n\\tSaunas, spas, baln\\u00e9oth\\u00e9rapie ou encore hydroth\\u00e9rapie...\\n<\\/p><h2>Fournisseur<\\/h2><p>\\n\\t<a href=\\\"http:\\/\\/www.cedeo.fr\\/\\\" target=\\\"_blank\\\"><img src=\\\"http:\\/\\/www.plombier-nicole-52.com\\/images\\/cedeo.jpg\\\" alt=\\\"CEDEO\\\"><\\/a>\\n<\\/p>\",\"position\":2}'),(103,'Page',3,2,'txt','{\"title\":\"\",\"txt\":\"<p>\\n\\t<strong>Vous avez un projet ?<\\/strong> Contactez-nous au <strong>03 25 03 14 54<\\/strong> ou remplissez le <strong>formulaire de contact<\\/strong> en ligne.<span><\\/span>\\n<\\/p>\",\"position\":3}'),(107,'Page',2,0,'txtimg','{\"title\":\"\",\"txt\":\"<p> Situ\\u00e9e \\u00e0 <strong>Foulain pr\\u00e8s de Chaumont et Langres (52)<\\/strong>, l\'<strong>entreprise Nicole<\\/strong> en Haute Marne est sp\\u00e9cialis\\u00e9e dans tous les <strong>travaux d\'installation, d\'entretien, de d\\u00e9pannage en plomberie, sanitaire et chauffage, pour les particuliers et les professionnels<\\/strong>. <\\/p><p> Depuis 1940, et gr\\u00e2ce \\u00e0 son \\u00e9quipe de plombiers-chauffagistes motiv\\u00e9s, l\'entreprise Nicole fait preuve de polyvalence et de r\\u00e9activit\\u00e9 pour vous garantir un travail de qualit\\u00e9 et ex\\u00e9cut\\u00e9 dans les d\\u00e9lais impartis. <\\/p><p> A votre \\u00e9coute, <strong>l\'\\u00e9quipe de plombiers chauffagistes de l\'entreprise Nicole <\\/strong>\\u00e9tudie avec vous la solution la mieux adapt\\u00e9e \\u00e0 vos installations pour qu\'elles soient synonymes de s\\u00e9curit\\u00e9, de r\\u00e9sistance et d\'efficacit\\u00e9. <\\/p><p> Confiez vos travaux \\u00e0 une entreprise comp\\u00e9tente et exp\\u00e9riment\\u00e9e, l\'entreprise Nicole est la solution plomberie - chauffage qu\'il vous faut ! <\\/p><h3>Notre savoir-faire<\\/h3><h4>Installations<\\/h4><p> Nous vous proposons des installations sur mesure pour vos sanitaires, salle de bains et chaudi\\u00e8re. Sp\\u00e9cialiste des \\u00e9conomies d\'\\u00e9nergies, nous vous proposons des solutions vous permettant d\'\\u00e9conomiser jusqu\'\\u00e0 30% de vos factures annuelles. <\\/p><p> En fonctions de vos projets, nous vous orienterons vers la solution la plus adapt\\u00e9e \\u00e0 vos besoins. <\\/p>\",\"img\":{\"File\":{\"id\":\"21\",\"name\":\"carte565c26c712325\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"png\",\"type\":\"image\",\"slug\":\"carte565c26c71232556d6a59811c0e\",\"nom_fichier\":\"carte565c26c71232556d6a59811c0e.png\",\"poids\":\"93\",\"dimenssions\":{\"width\":377,\"height\":500,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/03\\/carte565c26c71232556d6a59811c0e.png\",\"created\":\"2016-03-02 09:34:32\",\"updated\":\"2016-03-02 09:34:32\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"png\",\"icone\":\"picture\",\"type\":\"image\"}},\"params\":{\"prop\":\"img-right\"},\"position\":3}'),(108,'Page',2,1,'txtimg','{\"title\":\"\",\"txt\":\"<br><h4>Entretien<\\/h4><p> Nous entretenons vos installations sanitaires et chauffages. <\\/p><p> En fonction de votre type d\'habitation, nous assurons la p\\u00e9rennit\\u00e9 de vos installations. <\\/p><p> Exemples de travaux : D\\u00e9tartrage d\'un ballon d\'eau chaude, entretien des conduits d\'\\u00e9vacuation, ramonage ... <\\/p><h4>D\\u00e9pannage<\\/h4><p> Notre \\u00e9quipe intervient dans un rayon de 50 km autour de Chaumont. <\\/p><p> Nous assurons vos d\\u00e9pannages dans nos trois grands secteurs d\'activit\\u00e9s : <br> <\\/p><ul><li>Plomberie<\\/li><li>Chauffage<\\/li><li>Ventilation<\\/li><\\/ul><p> <strong>Pour une intervention rapide<\\/strong>, n\'h\\u00e9sitez pas \\u00e0 nous contacter directement au <strong>03 25 03 14 54<\\/strong> Joignable du<strong> Lundi au Samedi, de 8h \\u00e0 19h.<\\/strong> <\\/p>\",\"img\":{\"File\":{\"id\":\"22\",\"name\":\"img_savoir565c27474ffd3\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"img_savoir565c27474ffd356d6a5c230a8f\",\"nom_fichier\":\"img_savoir565c27474ffd356d6a5c230a8f.jpg\",\"poids\":\"14\",\"dimenssions\":{\"width\":300,\"height\":200,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/03\\/img_savoir565c27474ffd356d6a5c230a8f.jpg\",\"created\":\"2016-03-02 09:35:14\",\"updated\":\"2016-03-02 09:35:14\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},\"params\":{\"prop\":\"img-right\"},\"position\":3}');
/*!40000 ALTER TABLE `content_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_types`
--

DROP TABLE IF EXISTS `content_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_types` (
  `id` varchar(128) NOT NULL COMMENT 'Nom de la class model',
  `name` varchar(128) NOT NULL,
  `model` varchar(256) NOT NULL,
  `class_model` varchar(256) NOT NULL,
  `controleur` varchar(256) NOT NULL,
  `prefixe_url` varchar(256) NOT NULL,
  `sufixe_url` varchar(128) DEFAULT NULL,
  `construct_url` tinyint(1) DEFAULT NULL,
  `description` varchar(256) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `ordre` int(11) NOT NULL DEFAULT '0',
  `arborescence` tinyint(1) DEFAULT NULL,
  `associable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_types`
--

LOCK TABLES `content_types` WRITE;
/*!40000 ALTER TABLE `content_types` DISABLE KEYS */;
INSERT INTO `content_types` VALUES ('page','Page','Page','Page','pages','p-','',1,'Une page','file',2,1,1),('home','Accueil','Home','Home','homes','','',NULL,'La page d\'accueil','home',1,1,0),('contact','Contact','ContactManager.Contact','Contact','contact_manager/contacts','contact-','',1,'Une page contact','envelope',3,1,1),('actualite','Actualite','Blog.Actualite','Actualite','blog/actualites','news-','',NULL,'Une actualite','',0,0,1),('image','Image','','','','','',NULL,'','',0,0,0),('file','Fichier','','','','','',NULL,'','',0,0,0),('marque','Marque','Marque','Catalogue.Marque','catalogue/marques','',NULL,NULL,'','',0,NULL,0);
/*!40000 ALTER TABLE `content_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etats`
--

DROP TABLE IF EXISTS `etats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etats` (
  `id` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL,
  `class` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etats`
--

LOCK TABLES `etats` WRITE;
/*!40000 ALTER TABLE `etats` DISABLE KEYS */;
INSERT INTO `etats` VALUES ('draft','brouillon','danger'),('publish','en ligne','success');
/*!40000 ALTER TABLE `etats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `even_inscrits`
--

DROP TABLE IF EXISTS `even_inscrits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `even_inscrits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `even_id` int(11) NOT NULL,
  `first_name` varchar(128) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `even_id` (`even_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `even_inscrits`
--

LOCK TABLES `even_inscrits` WRITE;
/*!40000 ALTER TABLE `even_inscrits` DISABLE KEYS */;
/*!40000 ALTER TABLE `even_inscrits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extentions`
--

DROP TABLE IF EXISTS `extentions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extentions` (
  `id` varchar(10) CHARACTER SET latin1 NOT NULL,
  `icone` varchar(128) NOT NULL,
  `type` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extentions`
--

LOCK TABLES `extentions` WRITE;
/*!40000 ALTER TABLE `extentions` DISABLE KEYS */;
INSERT INTO `extentions` VALUES ('png','picture','image'),('jpg','picture','image'),('gif','picture','image'),('jpeg','picture','image'),('pdf','pdf','fichier'),('doc','word','fichier'),('xls','excel','fichier'),('ppt','powerpoint','fichier'),('docx','word','fichier'),('xlsx','excel','fichier'),('zip','archive','fichier'),('mp3','audio','audio'),('mp4','video','video'),('flv','video','video');
/*!40000 ALTER TABLE `extentions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_assocs`
--

DROP TABLE IF EXISTS `file_assocs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_assocs` (
  `id` int(11) NOT NULL,
  `file_id` int(11) DEFAULT NULL,
  `model` varchar(128) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `group` varchar(256) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `default` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_assocs`
--

LOCK TABLES `file_assocs` WRITE;
/*!40000 ALTER TABLE `file_assocs` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_assocs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_folders`
--

DROP TABLE IF EXISTS `file_folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_folders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_folders`
--

LOCK TABLES `file_folders` WRITE;
/*!40000 ALTER TABLE `file_folders` DISABLE KEYS */;
INSERT INTO `file_folders` VALUES (1,'real',NULL,1,4),(2,'chauffage',1,2,3);
/*!40000 ALTER TABLE `file_folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_olds`
--

DROP TABLE IF EXISTS `file_olds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_olds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `lien` varchar(256) NOT NULL,
  `embed` text NOT NULL,
  `extention_id` varchar(10) DEFAULT NULL,
  `type` varchar(256) DEFAULT NULL,
  `slug` varchar(256) DEFAULT NULL,
  `nom_fichier` varchar(256) DEFAULT NULL,
  `poids` decimal(10,0) DEFAULT NULL,
  `dossier` varchar(256) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_olds`
--

LOCK TABLES `file_olds` WRITE;
/*!40000 ALTER TABLE `file_olds` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_olds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `lien` varchar(256) NOT NULL,
  `embed` text NOT NULL,
  `extention_id` varchar(10) DEFAULT NULL,
  `type` varchar(256) DEFAULT NULL,
  `slug` varchar(256) DEFAULT NULL,
  `nom_fichier` varchar(256) DEFAULT NULL,
  `poids` decimal(10,0) DEFAULT NULL,
  `dimenssions` text NOT NULL,
  `file_folder_id` int(11) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (40,'img-chauffage','','','','jpg','image','img_chauffage56d8606825366','img_chauffage56d8606825366.jpg',15,'{\"width\":300,\"height\":204,\"reco\":\"normal\"}',NULL,'/files/2016/03/img_chauffage56d8606825366.jpg','2016-03-03 17:03:52','2016-03-03 17:03:52'),(37,'171','','','','jpg','image','17156d85faebf271','17156d85faebf271.jpg',38,'{\"width\":600,\"height\":800,\"reco\":\"normal\"}',2,'/files/2016/03/17156d85faebf271.jpg','2016-03-03 17:00:46','2016-03-03 17:00:46'),(38,'img-traitement','','','','jpg','image','img_traitement56d860137a894','img_traitement56d860137a894.jpg',15,'{\"width\":300,\"height\":214,\"reco\":\"normal\"}',NULL,'/files/2016/03/img_traitement56d860137a894.jpg','2016-03-03 17:02:27','2016-03-03 17:02:27'),(39,'img-energies','','','','jpg','image','img_energies56d8606784fb2','img_energies56d8606784fb2.jpg',20,'{\"width\":300,\"height\":201,\"reco\":\"normal\"}',NULL,'/files/2016/03/img_energies56d8606784fb2.jpg','2016-03-03 17:03:51','2016-03-03 17:03:51'),(7,'carte565c26c712325','','','','png','image','carte565c26c71232556d6a4e5b998f','carte565c26c71232556d6a4e5b998f.png',93,'{\"width\":377,\"height\":500,\"reco\":\"normal\"}',NULL,'/files/2016/03/carte565c26c71232556d6a4e5b998f.png','2016-03-02 09:31:33','2016-03-02 09:31:33'),(8,'chauffage','','','','jpg','image','chauffage56d6a4e60b1ef','chauffage56d6a4e60b1ef.jpg',5,'{\"width\":135,\"height\":180,\"reco\":\"normal\"}',NULL,'/files/2016/03/chauffage56d6a4e60b1ef.jpg','2016-03-02 09:31:34','2016-03-02 09:31:34'),(11,'3','','','','jpg','image','356d6a4f854f17','356d6a4f854f17.jpg',4,'{\"width\":135,\"height\":180,\"reco\":\"normal\"}',0,'/files/2016/03/356d6a4f854f17.jpg','2016-03-02 09:31:52','2016-03-02 09:31:52'),(12,'4','','','','jpg','image','456d6a4f9ec687','456d6a4f9ec687.jpg',3,'{\"width\":135,\"height\":180,\"reco\":\"normal\"}',0,'/files/2016/03/456d6a4f9ec687.jpg','2016-03-02 09:31:53','2016-03-02 09:31:53'),(13,'5','','','','jpg','image','556d6a4fa369d6','556d6a4fa369d6.jpg',4,'{\"width\":135,\"height\":180,\"reco\":\"normal\"}',0,'/files/2016/03/556d6a4fa369d6.jpg','2016-03-02 09:31:54','2016-03-02 09:31:54'),(14,'6','','','','jpg','image','656d6a4fa8274d','656d6a4fa8274d.jpg',3,'{\"width\":135,\"height\":180,\"reco\":\"normal\"}',0,'/files/2016/03/656d6a4fa8274d.jpg','2016-03-02 09:31:54','2016-03-02 09:31:54'),(15,'7','','','','jpg','image','756d6a4fac35e4','756d6a4fac35e4.jpg',4,'{\"width\":135,\"height\":180,\"reco\":\"normal\"}',0,'/files/2016/03/756d6a4fac35e4.jpg','2016-03-02 09:31:54','2016-03-02 09:31:54'),(16,'8','','','','jpg','image','856d6a4fb09fcf','856d6a4fb09fcf.jpg',4,'{\"width\":135,\"height\":180,\"reco\":\"normal\"}',0,'/files/2016/03/856d6a4fb09fcf.jpg','2016-03-02 09:31:55','2016-03-02 09:31:55'),(17,'carte565c26c712325','','','','png','image','carte565c26c71232556d6a4fbb4445','carte565c26c71232556d6a4fbb4445.png',93,'{\"width\":377,\"height\":500,\"reco\":\"normal\"}',0,'/files/2016/03/carte565c26c71232556d6a4fbb4445.png','2016-03-02 09:31:55','2016-03-02 09:31:55'),(18,'chauffage','','','','jpg','image','chauffage56d6a4fbed37a','chauffage56d6a4fbed37a.jpg',5,'{\"width\":135,\"height\":180,\"reco\":\"normal\"}',0,'/files/2016/03/chauffage56d6a4fbed37a.jpg','2016-03-02 09:31:55','2016-03-02 09:31:55'),(19,'1','','','','jpg','image','156d6a4fc3d741','156d6a4fc3d741.jpg',4,'{\"width\":135,\"height\":180,\"reco\":\"normal\"}',0,'/files/2016/03/156d6a4fc3d741.jpg','2016-03-02 09:31:56','2016-03-02 09:31:56'),(20,'2','','','','jpg','image','256d6a4fc77f16','256d6a4fc77f16.jpg',4,'{\"width\":135,\"height\":180,\"reco\":\"normal\"}',0,'/files/2016/03/256d6a4fc77f16.jpg','2016-03-02 09:31:56','2016-03-02 09:31:56'),(21,'carte565c26c712325','','','','png','image','carte565c26c71232556d6a59811c0e','carte565c26c71232556d6a59811c0e.png',93,'{\"width\":377,\"height\":500,\"reco\":\"normal\"}',NULL,'/files/2016/03/carte565c26c71232556d6a59811c0e.png','2016-03-02 09:34:32','2016-03-02 09:34:32'),(22,'img_savoir565c27474ffd3','','','','jpg','image','img_savoir565c27474ffd356d6a5c230a8f','img_savoir565c27474ffd356d6a5c230a8f.jpg',14,'{\"width\":300,\"height\":200,\"reco\":\"normal\"}',NULL,'/files/2016/03/img_savoir565c27474ffd356d6a5c230a8f.jpg','2016-03-02 09:35:14','2016-03-02 09:35:14'),(23,'slide-1','','','','jpg','image','slide_156d70cf9a0819','slide_156d70cf9a0819.jpg',472,'{\"width\":1920,\"height\":531,\"reco\":\"trop grande\"}',NULL,'/files/2016/03/slide_156d70cf9a0819.jpg','2016-03-02 16:55:37','2016-03-02 16:55:37'),(24,'slide-2','','','','jpg','image','slide_256d70d00b5798','slide_256d70d00b5798.jpg',517,'{\"width\":1920,\"height\":531,\"reco\":\"trop grande\"}',NULL,'/files/2016/03/slide_256d70d00b5798.jpg','2016-03-02 16:55:44','2016-03-02 16:55:44'),(25,'slide-3','','','','jpg','image','slide_356d70d0cd1969','slide_356d70d0cd1969.jpg',831,'{\"width\":1920,\"height\":531,\"reco\":\"trop grande\"}',NULL,'/files/2016/03/slide_356d70d0cd1969.jpg','2016-03-02 16:55:56','2016-03-02 16:55:56'),(26,'slide-4','','','','jpg','image','slide_456d70d1fc52a2','slide_456d70d1fc52a2.jpg',1219,'{\"width\":1920,\"height\":531,\"reco\":\"trop grande\"}',NULL,'/files/2016/03/slide_456d70d1fc52a2.jpg','2016-03-02 16:56:15','2016-03-02 16:56:15'),(27,'slide-5','','','','jpg','image','slide_556d70d2297cd0','slide_556d70d2297cd0.jpg',514,'{\"width\":1920,\"height\":529,\"reco\":\"trop grande\"}',NULL,'/files/2016/03/slide_556d70d2297cd0.jpg','2016-03-02 16:56:18','2016-03-02 16:56:18'),(28,'177','','','','jpg','image','17756d85eccd1c02','17756d85eccd1c02.jpg',25,'{\"width\":600,\"height\":800,\"reco\":\"normal\"}',1,'/files/2016/03/17756d85eccd1c02.jpg','2016-03-03 16:57:00','2016-03-03 16:57:00'),(29,'178','','','','jpg','image','17856d85ece1a677','17856d85ece1a677.jpg',31,'{\"width\":600,\"height\":800,\"reco\":\"normal\"}',1,'/files/2016/03/17856d85ece1a677.jpg','2016-03-03 16:57:02','2016-03-03 16:57:02'),(30,'179','','','','jpg','image','17956d85ecee4c45','17956d85ecee4c45.jpg',29,'{\"width\":600,\"height\":800,\"reco\":\"normal\"}',1,'/files/2016/03/17956d85ecee4c45.jpg','2016-03-03 16:57:02','2016-03-03 16:57:02'),(31,'180','','','','jpg','image','18056d85ecf59f75','18056d85ecf59f75.jpg',33,'{\"width\":600,\"height\":800,\"reco\":\"normal\"}',1,'/files/2016/03/18056d85ecf59f75.jpg','2016-03-03 16:57:03','2016-03-03 16:57:03'),(32,'181','','','','jpg','image','18156d85ed05ef6f','18156d85ed05ef6f.jpg',35,'{\"width\":600,\"height\":800,\"reco\":\"normal\"}',1,'/files/2016/03/18156d85ed05ef6f.jpg','2016-03-03 16:57:04','2016-03-03 16:57:04'),(33,'182','','','','jpg','image','18256d85ed0eaa7e','18256d85ed0eaa7e.jpg',37,'{\"width\":600,\"height\":800,\"reco\":\"normal\"}',1,'/files/2016/03/18256d85ed0eaa7e.jpg','2016-03-03 16:57:04','2016-03-03 16:57:04'),(34,'173','','','','jpg','image','17356d85ed23eed7','17356d85ed23eed7.jpg',39,'{\"width\":600,\"height\":800,\"reco\":\"normal\"}',1,'/files/2016/03/17356d85ed23eed7.jpg','2016-03-03 16:57:06','2016-03-03 16:57:06'),(35,'175','','','','jpg','image','17556d85ed37d16f','17556d85ed37d16f.jpg',36,'{\"width\":600,\"height\":800,\"reco\":\"normal\"}',1,'/files/2016/03/17556d85ed37d16f.jpg','2016-03-03 16:57:07','2016-03-03 16:57:07'),(36,'176','','','','jpg','image','17656d85ed4759f4','17656d85ed4759f4.jpg',32,'{\"width\":600,\"height\":800,\"reco\":\"normal\"}',1,'/files/2016/03/17656d85ed4759f4.jpg','2016-03-03 16:57:08','2016-03-03 16:57:08');
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_elements`
--

DROP TABLE IF EXISTS `form_elements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `short_name` varchar(16) NOT NULL,
  `type` varchar(16) DEFAULT NULL,
  `sub_type` varchar(64) DEFAULT NULL,
  `mandatory` tinyint(1) NOT NULL,
  `configuration` text NOT NULL,
  `position` int(11) NOT NULL,
  `created` datetime NOT NULL COMMENT 'CakePHP',
  `updated` datetime NOT NULL COMMENT 'CakePHP',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_elements`
--

LOCK TABLES `form_elements` WRITE;
/*!40000 ALTER TABLE `form_elements` DISABLE KEYS */;
/*!40000 ALTER TABLE `form_elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_entries`
--

DROP TABLE IF EXISTS `form_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_ip` varchar(15) NOT NULL,
  `form_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `data` text NOT NULL,
  `collection_id` varchar(13) NOT NULL,
  `created` datetime NOT NULL COMMENT 'CakePHP',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_entries`
--

LOCK TABLES `form_entries` WRITE;
/*!40000 ALTER TABLE `form_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `form_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forms`
--

DROP TABLE IF EXISTS `forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `elements_number` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) DEFAULT NULL,
  `etat_id` varchar(128) DEFAULT NULL,
  `created` datetime NOT NULL COMMENT 'CakePHP',
  `updated` datetime NOT NULL COMMENT 'CakePHP',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forms`
--

LOCK TABLES `forms` WRITE;
/*!40000 ALTER TABLE `forms` DISABLE KEYS */;
/*!40000 ALTER TABLE `forms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,NULL,1,2,'Administrateurs','2015-01-19 11:37:29'),(2,NULL,0,0,'Redacteurs','2016-03-02 09:14:00');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homes`
--

DROP TABLE IF EXISTS `homes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homes` (
  `id` int(11) NOT NULL,
  `content_type_id` varchar(128) NOT NULL,
  `etat_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL DEFAULT '',
  `h1` varchar(256) NOT NULL,
  `url` varchar(1) DEFAULT '/',
  `url_r` varchar(1) NOT NULL DEFAULT '/',
  `meta_description` text NOT NULL,
  `meta_title` varchar(256) NOT NULL DEFAULT '',
  `meta_keywords` text NOT NULL,
  `meta_robots` varchar(128) NOT NULL DEFAULT '',
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `menu_item_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homes`
--

LOCK TABLES `homes` WRITE;
/*!40000 ALTER TABLE `homes` DISABLE KEYS */;
/*!40000 ALTER TABLE `homes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `i18n`
--

DROP TABLE IF EXISTS `i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `i18n` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `i18n`
--

LOCK TABLES `i18n` WRITE;
/*!40000 ALTER TABLE `i18n` DISABLE KEYS */;
/*!40000 ALTER TABLE `i18n` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_campaigns`
--

DROP TABLE IF EXISTS `newsletter_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_campaigns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(128) DEFAULT NULL COMMENT 'sms ou email',
  `etat_id` varchar(128) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `template_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `list_id` int(11) NOT NULL,
  `default_from_name` varchar(128) NOT NULL,
  `default_from_email` varchar(256) NOT NULL,
  `default_from_telephone` varchar(128) DEFAULT NULL,
  `default_from_subject` varchar(256) NOT NULL,
  `how_on_list` text NOT NULL,
  `send` tinyint(1) NOT NULL DEFAULT '0',
  `nbr` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_campaigns`
--

LOCK TABLES `newsletter_campaigns` WRITE;
/*!40000 ALTER TABLE `newsletter_campaigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_campaigns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_emails`
--

DROP TABLE IF EXISTS `newsletter_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_emails` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) CHARACTER SET utf16 NOT NULL,
  `first_name` varchar(128) NOT NULL,
  `last_name` varchar(128) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unsuscribe_link` text NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `liste_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_emails`
--

LOCK TABLES `newsletter_emails` WRITE;
/*!40000 ALTER TABLE `newsletter_emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_inscrits`
--

DROP TABLE IF EXISTS `newsletter_inscrits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_inscrits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_inscrits`
--

LOCK TABLES `newsletter_inscrits` WRITE;
/*!40000 ALTER TABLE `newsletter_inscrits` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_inscrits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_list_emails`
--

DROP TABLE IF EXISTS `newsletter_list_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_list_emails` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `list_id` int(10) NOT NULL,
  `email_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_list_emails`
--

LOCK TABLES `newsletter_list_emails` WRITE;
/*!40000 ALTER TABLE `newsletter_list_emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_list_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_lists`
--

DROP TABLE IF EXISTS `newsletter_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_lists` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_lists`
--

LOCK TABLES `newsletter_lists` WRITE;
/*!40000 ALTER TABLE `newsletter_lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_templates`
--

DROP TABLE IF EXISTS `newsletter_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `folder_name` varchar(256) NOT NULL,
  `is_uploaded` int(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_templates`
--

LOCK TABLES `newsletter_templates` WRITE;
/*!40000 ALTER TABLE `newsletter_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_olds`
--

DROP TABLE IF EXISTS `page_olds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_olds` (
  `id` int(11) NOT NULL,
  `menu_item_id` int(11) DEFAULT NULL,
  `content_type_id` varchar(128) NOT NULL,
  `etat_id` tinyint(1) DEFAULT NULL,
  `menu_id` tinyint(1) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `h1` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `url_r` varchar(256) NOT NULL DEFAULT '',
  `meta_description` text NOT NULL,
  `meta_title` varchar(256) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_robots` varchar(128) NOT NULL,
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  `icone` varchar(128) DEFAULT NULL,
  `template` varchar(128) DEFAULT NULL,
  `slug` varchar(256) NOT NULL,
  `rght` int(11) NOT NULL,
  `lft` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_olds`
--

LOCK TABLES `page_olds` WRITE;
/*!40000 ALTER TABLE `page_olds` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_olds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_type_id` varchar(128) NOT NULL DEFAULT 'page',
  `etat_id` varchar(128) NOT NULL DEFAULT 'draft',
  `publication_date` date NOT NULL,
  `menu_id` tinyint(1) DEFAULT NULL,
  `acces_membre` tinyint(1) DEFAULT '0',
  `name` varchar(128) NOT NULL,
  `h1` varchar(256) NOT NULL,
  `sous_titre` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `url_r` varchar(256) NOT NULL DEFAULT '',
  `vignette` text NOT NULL,
  `icon` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_title` varchar(256) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_robots` varchar(128) NOT NULL,
  `section` varchar(128) DEFAULT NULL,
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `rss_name` varchar(128) DEFAULT NULL,
  `rss_description` text,
  `rss_niveau` varchar(20) DEFAULT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  `icone` varchar(128) DEFAULT NULL,
  `template` varchar(128) DEFAULT NULL,
  `slug` varchar(256) NOT NULL,
  `position` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'page','publish','2016-03-02',1,0,'Accueil','Accueil','Le meilleur plombier et chauffagiste à Foulain près de Langres et Chaumont','/accueil/p-1','','','','','Plombier et chauffagiste à Foulain près de Langres - Entreprise Nicole','','',NULL,'','<p>Installée à  <strong>Foulain, près de Chaumont en Haute-Marne</strong> (52), nous proposons nos services auprès des particuliers et des professionnels en associant notre savoir-faire traditionnel aux technologies de pointe.<br></p><p>\r\n	Entreprise qualifiée, <strong>QUALIBAT</strong>, nous vous apporteront conseils et expertise pour l\'installation d\'équipements en matière de : <img src=\"http://www.plombier-nicole-52.com/images/qualibat.jpg\" alt=\"QUALIBAT RGE\" style=\"float: right; margin: 0px 0px 10px 10px;\"><br>\r\n	<br>\r\n</p><ul>\r\n	<li><strong>Plomberie</strong></li>\r\n	<li><strong>Sanitaires</strong></li>\r\n	<li><strong>Chauffage</strong></li>\r\n	<li><strong>Traitement de l\'eau</strong></li>\r\n	<li><strong>Ventilation</strong></li>\r\n</ul><p>\r\n	Pour l\'ensemble de vos travaux, neufs ou rénovation, faites appel à nos services. L\'<strong>entreprise Nicole</strong> à  <strong>Foulain</strong>, vous accompagne dans le choix des équipements de votre installation en vous proposant différentes gammes de produits et réalise leur mise en oeuvre...\r\n</p><p>\r\n	<strong>Vous avez un projet ?</strong> Contactez-nous au <strong>03 25 03 14 54</strong> ou laissez-nous depuis notre formulaire de contact.\r\n</p>',NULL,NULL,NULL,'2016-03-03 16:51:31','2016-03-02 09:26:27',NULL,'home','',0,NULL,1,2,''),(2,'page','publish','2016-03-02',1,0,'Présentation','Présentation','Entreprise Nicole, le spécialiste en énergies renouvelables à Foulain !','/presentation/p-2','','','','','Plombier-chauffagiste à Foulain près de Chaumont et Joinville - Entreprise Nicole','','',NULL,'','<br>',NULL,NULL,NULL,'2016-03-03 16:07:05','2016-03-02 09:27:11',NULL,'view-presentation','',1,NULL,3,4,''),(3,'page','publish','2016-03-02',1,0,'Plomberie - Sanitaire','Plomberie - Sanitaire','Plomberie - Sanitaire','/plomberie-sanitaire/p-3','','','','','Plomberie et sanitaires à Foulain près de Chaumont - Entreprise Nicole','','',NULL,'','',NULL,NULL,NULL,'2016-03-03 16:05:14','2016-03-02 09:37:13',NULL,'view-plomberie','',2,NULL,5,6,''),(4,'page','publish','2016-03-02',1,0,'Energies renouvelable - Chauffage traditionnel','Energies renouvelable - Chauffage traditionnel','Energies renouvelable - Chauffage traditionnel','/energies-renouvelable-chauffage-traditionnel/p-4','','','','','Chauffagiste et énergies renouvelables près de Chaumont et Langres - Entreprise Nicole','','',NULL,'','',NULL,NULL,NULL,'2016-03-03 16:04:28','2016-03-02 09:48:13',NULL,'view-energies','',3,NULL,7,8,''),(5,'page','publish','2016-03-02',1,0,'Autres prestations','Autres prestations','Autres prestations','/autres-prestations/p-5','','','','','Système de traitement de l\'eau et ventilation près de Foulain et Chaumont','','',NULL,'','',NULL,NULL,NULL,'2016-03-03 16:02:50','2016-03-02 09:49:57',NULL,'view-presentation','',4,NULL,9,10,''),(6,'page','publish','2016-03-02',1,0,'Nos réalisations','Nos réalisations','Nos réalisations','/nos-realisations/p-6','','','','','Réalisations de plomberie, sanitaire à Foulain près de Chaumont et Joinville - Entreprise Nicole','','',NULL,'','<p>Découvrez en image, toutes les <strong>réalisations de l\'entreprise Nicole</strong> sur <strong>Foulain</strong> et les alentours. Grâce à ces photos, vous pourrez juger par vous-même de la très bonne qualité de travail de cette équipe de professionnels.</p>',NULL,NULL,NULL,'2016-03-03 16:01:03','2016-03-02 09:51:20',NULL,'view-realisations','',5,NULL,11,12,''),(7,'page','publish','2016-03-02',1,0,'Contact','Contact','','/contact/p-7','','','','','Contactez un plombier-chauffagiste expérimenté à Foulain près de Joinville et Langres - Entreprise Nicole','','',NULL,'','<p style=\"text-align: center;\"><strong>Entreprise Nicole</strong></p><p style=\"text-align: center;\">1 route de nogent 52800 FOULAIN</p><p style=\"text-align: center;\">Tel.: 03 25 03 14 54</p>',NULL,NULL,NULL,'2016-03-03 15:52:01','2016-03-02 09:53:04',NULL,'contact','',6,NULL,13,14,''),(8,'page','publish','2016-03-03',NULL,0,'Mentions légales','Mentions légales','','/mentions-legales/p-8','','','','','','','',NULL,'','<p>En vertu de l\'article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l\'économie numérique, il est précisé aux utilisateurs du site <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a> l\'identité des différents intervenants dans le cadre de sa réalisation et de son suivi :</p><p> <strong>Propriétaire Mr NICOLAS Benoît<span></span></strong><br> <strong>Webdesign :</strong><a href=\"http://www.kobaltis.com/\" target=\"_blank\"> Agence KOBALTIS</a> - <strong>Développements :</strong><a href=\"http://www.kobaltis.com/\" target=\"_blank\"> KOBALTIS</a><br> <strong>Responsable publication : </strong><a href=\"mailto:kobaltis-webmaster@kobaltis.com\" target=\"_blank\">kobaltis-webmaster@kobaltis.com</a><br> Le responsable publication est une personne physique ou une personne morale.<br> <strong>Hébergeur :</strong> OVH - 2 rue Kellermann 59100 Roubaix - France</p><p><br> <br> <strong>2. Conditions générales d\'utilisation du site et des services proposés.</strong><br> L\'utilisation du site <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a> implique l\'acceptation pleine et entière des conditions générales d\'utilisation ci-après décrites. Ces conditions d\'utilisation sont susceptibles d\'être modifiées ou complétées à tout moment, les utilisateurs du site <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a> sont donc invités à les consulter de manière régulière. Ce site est normalement accessible à tout moment aux utilisateurs. Une interruption pour raison de maintenance technique peut être toutefois décidée par Matra Electronique. Le site <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a> est mis à jour régulièrement. De la même façon, les mentions légales peuvent être modifiées à tout moment : elles s\'imposent néanmoins à l\'utilisateur qui est invité à s\'y référer le plus souvent possible afin d\'en prendre connaissance.<br> Les copies et téléchargements ne sont autorisées que pour un usage personnel, privé et non-commercial.</p><p><br> <br> <strong>3. Description des services fournis.</strong> Le site <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a> a pour objet de fournir une information concernant l\'ensemble des activités de la société.<br> <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a> possible. Toutefois, il ne pourra être tenue responsable des omissions, des inexactitudes et des carences dans la mise à jour, qu\'elles soient de son fait ou du fait des tiers partenaires qui lui fournissent ces informations. Tous les informations indiquées sur le site <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a> sont données à titre indicatif, et sont susceptibles d\'évoluer. Par ailleurs, les renseignements figurant sur le site <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a> ne sont pas exhaustifs. Ils sont donnés sous réserve de modifications ayant été apportées depuis leur mise en ligne.</p><p><br> <br> <strong>4. Limitations contractuelles sur les données techniques.</strong><br> KOBALTIS ne pourra être tenu responsable de dommages matériels liés à  l\'utilisation du site. De plus, l\'utilisateur du site s\'engage à accéder au site en utilisant un matériel récent, ne contenant pas de virus et avec un navigateur de dernière génération mis-à-jour.</p><p><br> <br> <strong>5. Propriété intellectuelle et contrefaçons.</strong> <strong><strong data-redactor-tag=\"strong\">Mr NICOLAS Benoît</strong><span></span></strong> est propriétaire des droits de propriété intellectuelle ou détient les droits d\'usage sur tous les éléments accessibles sur le site, notamment les textes, images, graphismes, logo, icônes, sons, logiciels.<br> Toute reproduction, représentation, modification, publication, adaptation de tout ou partie des éléments du site, quel que soit le moyen ou le procédé utilisé, est interdite, sauf autorisation écrite préalable de : ************.<br> Toute exploitation non autorisée du site ou de l\'un quelconque des éléments qu\'il contient sera considérée comme constitutive d\'une contrefaçon et poursuivie conformément aux dispositions des articles L.335-2 et suivants du Code de Propriété Intellectuelle.</p><p><br> <br> <strong>6. Limitations de responsabilité.</strong> <strong><strong data-redactor-tag=\"strong\">Mr NICOLAS Benoît</strong><span></span></strong> ne pourra être tenue responsable des dommages directs et indirects causés au matériel de l\'utilisateur, lors de l\'accès au site <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a> , et résultant soit de l\'utilisation d\'un matériel ne répondant pas aux spécifications indiquées au point 4, soit de l\'apparition d\'un bug ou d\'une incompatibilité.<br> <strong><strong data-redactor-tag=\"strong\">Mr NICOLAS Benoît</strong><span></span></strong> ne pourra également être tenue responsable des dommages indirects (tels par exemple qu\'une perte de marché ou perte d\'une chance) consécutifs à l\'utilisation du site <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a></p><p><br> <br> <strong>7. Gestion des données personnelles.</strong> En France, les données personnelles sont notamment protégées par la loi n° 78-87 du 6 janvier 1978, la loi n° 2004-801 du 6 août 2004, l\'article L. 226-13 du Code pénal et la Directive Européenne du 24 octobre 1995.<br> A l\'occasion de l\'utilisation du site <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a>, peuvent être recueillies : l\'URL des liens par l\'intermédiaire desquels l\'utilisateur a accédé au site <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a>, le fournisseur d\'accès de l\'utilisateur, l\'adresse de protocole Internet (IP) de l\'utilisateur.<br> En tout état de cause <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a> ne collecte des informations personnelles relatives à l\'utilisateur que pour le besoin de certains services proposÃ©s par le site <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a> . L\'utilisateur fournit ces informations en toute connaissance de cause, notamment lorsqu\'il procède par lui-même à leur saisie. Il est alors précisé à l\'utilisateur du site <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a> l\'obligation ou non de fournir ces informations.<br> ConformÃ©ment aux dispositions des articles 38 et suivants de la loi 78-17 du 6 janvier 1978 relative à l\'informatique, aux fichiers et aux libertés, tout utilisateur dispose d\'un droit d\'accès, de rectification et d\'opposition aux données personnelles le concernant, en effectuant sa demande écrite et signée, accompagnée d\'une copie du titre d\'identité avec signature du titulaire de la pièce, en précisant l\'adresse à laquelle la réponse doit être envoyée.<br> Aucune information personnelle de l\'utilisateur du site <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a> n\'est publiée à l\'insu de l\'utilisateur, échangée, transférée, cédée ou vendue sur un support quelconque à des tiers.</p><p><br> <br> <strong>8. Liens hypertextes et cookies.</strong><br> Le site <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a> contient un certain nombre de liens hypertextes vers d\'autres sites, mis en place avec l\'autorisation de <strong><strong data-redactor-tag=\"strong\">Mr NICOLAS Benoît</strong><span></span></strong>. Cependant, <strong><strong data-redactor-tag=\"strong\">Mr NICOLAS Benoît</strong><span></span></strong> n\'a pas la possibilité de vérifier le contenu des sites ainsi visités, et n\'assumera en conséquence aucune responsabilité de ce fait.<br> La navigation sur le site <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a> est susceptible de provoquer l\'installation de cookie(s) sur l\'ordinateur de l\'utilisateur. Un cookie est un fichier de petite taille, qui ne permet pas l\'identification de l\'utilisateur, mais qui enregistre des informations relatives à la navigation d\'un ordinateur sur un site. Les données ainsi obtenues visent à faciliter la navigation ultérieure sur le site, et ont également vocation à permettre diverses mesures de fréquentation.</p><p><br> <br> <strong>9. Droit applicable et attribution de juridiction.</strong> Tout litige en relation avec l\'utilisation du site <a href=\"http://www.nicole-sanitaire-52.fr/\" target=\"_blank\">http://www.nicole-sanitaire-52.fr/</a> est soumis au droit français. Il est fait attribution exclusive de juridiction aux tribunaux compétents de Lille.</p><p><br> <br> <strong>10. Les lois concernées.</strong> Loi n° 78-87 du 6 janvier 1978, notamment modifiée par la loi n° 2004-801 du 6 août 2004 relative à l\'informatique, aux fichiers et aux libertés.<br> Loi n° 2004-575 du 21 juin 2004 pour la confiance dans l\'économie numérique.<br> Informations personnelles : « les informations qui permettent, sous quelque forme que ce soit, directement ou non, l\'identification des personnes physiques auxquelles elles s\'appliquent » (article 4 de la loi n° 78-17 du 6 janvier 1978).</p>',NULL,NULL,NULL,'2016-03-03 16:51:31','2016-03-03 11:14:19',NULL,'page','',7,NULL,15,16,'');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parametres`
--

DROP TABLE IF EXISTS `parametres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parametres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `raison_sociale` varchar(128) DEFAULT NULL,
  `about` text,
  `contact_name` varchar(128) DEFAULT NULL,
  `contact_email` varchar(128) DEFAULT NULL,
  `telephone` varchar(128) DEFAULT NULL,
  `adresse_1` varchar(128) DEFAULT NULL,
  `adresse_2` varchar(128) DEFAULT NULL,
  `cp` varchar(128) DEFAULT NULL,
  `ville` varchar(128) DEFAULT NULL,
  `ndd` varchar(128) DEFAULT NULL,
  `telephone_noreply` varchar(128) DEFAULT NULL,
  `email_noreply` varchar(128) DEFAULT NULL,
  `email_name` varchar(128) DEFAULT NULL,
  `email_serveur_smtp` varchar(128) DEFAULT NULL,
  `email_serveur_port` varchar(128) DEFAULT NULL,
  `email_serveur_login` varchar(128) DEFAULT NULL,
  `email_serveur_mdp` varchar(128) DEFAULT NULL,
  `lien_facebook` varchar(128) DEFAULT NULL,
  `lien_twitter` varchar(128) DEFAULT NULL,
  `lien_linkedin` varchar(128) DEFAULT NULL,
  `lien_youtube` varchar(128) DEFAULT NULL,
  `lien_pinterest` varchar(128) DEFAULT NULL,
  `lien_google_plus` varchar(128) DEFAULT NULL,
  `active_brochure` tinyint(1) DEFAULT NULL,
  `google_ua` varchar(128) DEFAULT NULL,
  `google_login` varchar(128) DEFAULT NULL,
  `google_mdp` varchar(128) DEFAULT NULL,
  `contact_page` varchar(128) DEFAULT NULL,
  `updated` datetime NOT NULL,
  `siren` varchar(255) DEFAULT NULL,
  `hebergeur` varchar(255) DEFAULT NULL,
  `resp_publication` varchar(255) DEFAULT NULL,
  `url_site_web` varchar(255) DEFAULT NULL,
  `google_tag_manager` text,
  `mentions_page` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parametres`
--

LOCK TABLES `parametres` WRITE;
/*!40000 ALTER TABLE `parametres` DISABLE KEYS */;
INSERT INTO `parametres` VALUES (1,'ENTREPRISE NICOLE',NULL,'Mr NICOLAS BenoÃ®t','nicole-sanitaire@orange.fr','03 25 03 14 54','1 route de nogent',NULL,'52800','FOULAIN',NULL,'03/25/03/14/54','nicole-sanitaire@orange.fr',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-03-03 16:51:01',NULL,NULL,NULL,NULL,'<script>\n  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n  })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n\n  ga(\'create\', \'UA-75415936-1\', \'auto\');\n  ga(\'send\', \'pageview\');\n\n</script>','');
/*!40000 ALTER TABLE `parametres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partenaire_types`
--

DROP TABLE IF EXISTS `partenaire_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partenaire_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `etat_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partenaire_types`
--

LOCK TABLES `partenaire_types` WRITE;
/*!40000 ALTER TABLE `partenaire_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `partenaire_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partenaires`
--

DROP TABLE IF EXISTS `partenaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partenaires` (
  `id` int(11) NOT NULL,
  `partenaire_type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `etat_id` int(11) NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partenaires`
--

LOCK TABLES `partenaires` WRITE;
/*!40000 ALTER TABLE `partenaires` DISABLE KEYS */;
/*!40000 ALTER TABLE `partenaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_attributes`
--

DROP TABLE IF EXISTS `product_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `attribute_family_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_attributes`
--

LOCK TABLES `product_attributes` WRITE;
/*!40000 ALTER TABLE `product_attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_type_id` varchar(128) DEFAULT NULL,
  `etat_id` varchar(32) NOT NULL,
  `rubrique_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `h1` varchar(255) NOT NULL,
  `resume` varchar(255) NOT NULL,
  `prix_ttc` decimal(10,2) NOT NULL,
  `description` text NOT NULL,
  `vignette` text NOT NULL,
  `galerie` text NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taches`
--

DROP TABLE IF EXISTS `taches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taches` (
  `id` int(11) NOT NULL,
  `plugin` varchar(256) NOT NULL,
  `controller` varchar(256) NOT NULL,
  `action` varchar(256) NOT NULL,
  `fond` int(11) NOT NULL DEFAULT '1',
  `author` varchar(256) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `etat` int(11) NOT NULL DEFAULT '0' COMMENT '0 : en attente, 1 : tache commencee, 2 taches terminee',
  `donnees` text NOT NULL,
  `resultat` longtext NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taches`
--

LOCK TABLES `taches` WRITE;
/*!40000 ALTER TABLE `taches` DISABLE KEYS */;
/*!40000 ALTER TABLE `taches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tests`
--

DROP TABLE IF EXISTS `tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tests`
--

LOCK TABLES `tests` WRITE;
/*!40000 ALTER TABLE `tests` DISABLE KEYS */;
/*!40000 ALTER TABLE `tests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `txts`
--

DROP TABLE IF EXISTS `txts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `txts` (
  `id` varchar(128) CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1 NOT NULL,
  `txt` text CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `txts`
--

LOCK TABLES `txts` WRITE;
/*!40000 ALTER TABLE `txts` DISABLE KEYS */;
/*!40000 ALTER TABLE `txts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typepages`
--

DROP TABLE IF EXISTS `typepages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typepages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typepages`
--

LOCK TABLES `typepages` WRITE;
/*!40000 ALTER TABLE `typepages` DISABLE KEYS */;
INSERT INTO `typepages` VALUES (1,'Page d\'accueil','home','Liste toutes les pages mÃ©tiers'),(2,'Page classique','page',''),(3,'Page mÃ©tier','metier',''),(4,'Blog (liste des articles)','blog','Liste toutes les actualitÃ©s'),(5,'Page contact','contact',''),(6,'Accueil liste des metiers','home_metier','Liste toutes les pages de type mÃ©tier'),(7,'Page d\'accueil rÃ©fÃ©rences','realisation','Liste toutes les actualitÃ©s flaguÃ©es \"rÃ©fÃ©rences\"'),(8,'Mention lÃ©gale','mention_legale',''),(0,'Page rubrique','rubrique',''),(0,'view-chauffage.ctp','view-chauffage',''),(0,'view-energies.ctp','view-energies',''),(0,'view-plomberie.ctp','view-plomberie',''),(0,'view-presentation.ctp','view-presentation',''),(0,'view-realisations.ctp','view-realisations','');
/*!40000 ALTER TABLE `typepages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_groups`
--

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
INSERT INTO `user_groups` VALUES (2,2,2),(4,3,2),(26,4,2),(40,607123,1),(42,607130,1),(43,607125,1);
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` mediumint(8) unsigned NOT NULL,
  `nom_connexion` varchar(256) NOT NULL,
  `nom` varchar(256) NOT NULL,
  `prenom` varchar(256) NOT NULL,
  `nom_entreprise` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `mobile` varchar(32) NOT NULL,
  `password` char(40) NOT NULL,
  `vignette` text NOT NULL,
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,1,'Jerome Lebleu','Lebleu','Jerome','','jerome@whatson-web.com','','','8cfaeb35eeb36111e2c70cc824fe92d5d46903db','','','',0,1,'2016-03-02 09:14:00'),(3,1,'Prod Kobaltis','Kobaltis','Prod','','prod@kobaltis.net','','','fe2af495788471d1dcc3146601b19e630e58bf87','','','',0,1,'2016-03-02 09:14:00'),(4,2,'Redacteur Kobaltis','Kobaltis','Redacteur','','redacteur@kobaltis.net','','','fe2af495788471d1dcc3146601b19e630e58bf87','','','',0,1,'2016-03-02 09:14:00'),(5,1,'Jerome Lebleu','Lebleu','Jerome','','jerome@whatson-web.com','','','8cfaeb35eeb36111e2c70cc824fe92d5d46903db','','','',0,1,'2016-03-02 10:04:49'),(6,1,'Prod Kobaltis','Kobaltis','Prod','','prod@kobaltis.net','','','fe2af495788471d1dcc3146601b19e630e58bf87','','','',0,1,'2016-03-02 10:04:49'),(7,2,'Redacteur Kobaltis','Kobaltis','Redacteur','','redacteur@kobaltis.net','','','fe2af495788471d1dcc3146601b19e630e58bf87','','','',0,1,'2016-03-02 10:04:49'),(8,2,'BenoÃ®t Nicolas','Nicolas','BenoÃ®t','','nicole-sanitaire@orange.fr','','','a64a2953e59aa867e2022d79cff321bf22bc4673','','','',0,1,'2016-03-03 17:20:32');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-25 11:01:55
