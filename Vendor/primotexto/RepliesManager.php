<?php
    require_once 'BaseManager.php';
    class RepliesManager extends BaseManager {

        public static function getReplies () {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/replies");
            $result = curl_exec($curl);
            $result = json_decode($result);
            foreach ($result as $r) {
            $campaignId = $r->campaign->id;
            $id = $r->id;
            $identifier = $r->identifier;
            $message = $r->message;
            $data = array ('campaignId' => "$campaignId", 'id' => "$id", 'identifier' => "$identifier", 'message' => "$message");
            $data = json_encode ($data);
            echo $data;
            }
            curl_close($curl);
        }
    }
?>