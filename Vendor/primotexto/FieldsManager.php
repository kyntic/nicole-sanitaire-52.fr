<?php
    require_once 'BaseManager.php';
    class FieldsManager extends BaseManager {

        public static function getFields ($listId) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/lists/$listId/fields");
            $result = curl_exec($curl);
            foreach (json_decode($result) as $s) {
                $fields = json_encode(array ('id' => "$s->id", 'name' => "$s->name"));
                echo $fields;
            }
            curl_close($curl);
        }
        
        public static function addField ($listId, $name, $type) {
            CredentialsHelper::ensureLogin();
            $field = array('list' => array ("id" => "$listId"), 'name' => "$name", 'type' => "$type");
            $curl = parent::getPostCurl(BaseManager::$baseURL."/lists/$listId/fields", json_encode($field, JSON_FORCE_OBJECT));
            $result = curl_exec($curl);
            return $result;
            curl_close($curl);
        }
        
        public static function delField ($listId, $id) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getDeleteCurl(BaseManager::$baseURL."/lists/$listId/fields/$id");
            $result = curl_exec($curl);
            return $result;
            curl_close($curl);
        }
        
        public static function getListsIdFieldsId ($listId, $fieldId) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/lists/$listId/fields/$fieldId");
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
    }
?>