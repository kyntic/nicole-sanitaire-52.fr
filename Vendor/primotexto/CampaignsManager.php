<?php
    require_once 'BaseManager.php';
    require_once 'TimeManager.php';
    class CampaignsManager extends BaseManager {

        public static function sendSms ($number, $message, $sender, $campaignName, $category) {
            CredentialsHelper::ensureLogin();
            $data = array('number' => $number, 'message' => $message, 'sender' => $sender, 'campaignName' => $campaignName, 'category' => $category);
            $curl = parent::getPutCurl(BaseManager::$baseURL.'/campaigns?a=notification', json_encode($data));
            $result = curl_exec($curl);
            //echo $result;
            curl_close($curl);
        }
        
        public static function newCampaign ($data) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getPostCurl(BaseManager::$baseURL.'/campaigns', json_encode($data, JSON_FORCE_OBJECT));
            $result = curl_exec($curl);
            $campaignId = json_decode($result)->id;
            return $campaignId;
            curl_close($curl);
        }
        
        public static function testCampaign ($data) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getPostCurl(BaseManager::$baseURL.'/campaigns/test', json_encode($data));
            $result = curl_exec($curl);           
            return $result;
            curl_close($curl);
        }
        
           public static function sendCampaign ($id) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/campaigns/$id");
            $data = curl_exec($curl);
            $curl = parent::getPutCurl(BaseManager::$baseURL.'/campaigns/$campaignId/send', $data);
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        
        
        
        public static function sendScheduledCampaign($data, $scheduled) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getPostCurl(BaseManager::$baseURL.'/campaigns', $data);
            $result = curl_exec($curl);
            $result = json_decode($result);
            $campaignId = $result->id;
            $result->date = TimeManager::getTSFromDate($scheduled);
            $result->scheduledType = 'delayed';
            $curl = parent::getPutCurl(BaseManager::$baseURL."/campaigns/$campaignId", json_encode($result));
            $result = curl_exec($curl);
            $curl = parent::getGetCurl(BaseManager::$baseURL."/campaigns/$campaignId");
            $data = curl_exec($curl);
            $curl = parent::getPutCurl(BaseManager::$baseURL.'/campaigns/$campaignId/send', $data);
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
            
        }
        
        public static function getCampaigns () {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL.'/campaigns');
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        public static function getCampaign ($id) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/campaigns/$id");
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        public static function postCampaigns ($data) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getPostCurl(BaseManager::$baseURL.'/campaigns', $data);
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        public static function putCampaignsId() {
            CredentialsHelper::ensureLogin();
        }
        
        public static function deleteCampaignsId() {
            CredentialsHelper::ensureLogin();
        }
        
        public static function putCampaignsThumbnail () {
            CredentialsHelper::ensureLogin();
        }
        
        public static function getCampaignsLast () {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL.'/campaigns/last');
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        public static function putCampaignsIdSend () {
            CredentialsHelper::ensureLogin();
            $curl = parent::getPutCurl(BaseManager::$baseURL.'/campaigns/$campaignId/send', $data);
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        public static function putCampaignsIdUnscheduled () {
            CredentialsHelper::ensureLogin();
        }
        
        public static function postCampaignsExpress () {
            CredentialsHelper::ensureLogin();
        }
        
        public static function postCampaignsPreviewTest () {
            CredentialsHelper::ensureLogin();
        }
        
        public static function postCampaignsFlashTest () {
            CredentialsHelper::ensureLogin();
        }
        
        public static function postCampaignsTest () {
            CredentialsHelper::ensureLogin();
        }
        
        public static function getCampaignsIdCredits ($id) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/campaigns/$id/credits");
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        public static function getCampaignsIdRecipients($id) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/campaigns/$id/recipients");
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
    }
?>