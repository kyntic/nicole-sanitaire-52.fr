<?php
    require_once 'BaseManager.php';
    class StatsManager extends BaseManager {

        public static function getCampaign ($id) {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/stats/campaigns/$id");
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
    }
?>