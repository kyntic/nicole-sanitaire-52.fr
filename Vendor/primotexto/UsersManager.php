<?php
    require_once 'BaseManager.php';
    class UsersManager extends BaseManager {

        public static function getUsersCurrent () {
            CredentialsHelper::ensureLogin();
            $curl = parent::getGetCurl(BaseManager::$baseURL."/users/current");
            $result = curl_exec($curl);
            echo $result;
            curl_close($curl);
        }
        
        public static function PutUsersId () {
            CredentialsHelper::ensureLogin();
        }
        
        public static function postUsers () {
            CredentialsHelper::ensureLogin();
        }
    }
?>
