<?php
    class CredentialsHelper {

        private static $tokensUrl = "https://www.primotexto.com/app/tokens";
        private static $tokens;
        private static $email;
        private static $password;

        public static function setCredentials($email, $password) {
            self::$email = $email;
            self::$password = $password;
        }

        private static function credentialsAreSet() {
            return isset(self::$email) && isset(self::$password);
        }

        private static function userIsLoggedIn() {
            return isset(self::$tokens['userId']) && isset(self::$tokens['hash']) && isset(self::$tokens['timestamp']);
        }

        public static function ensureLogin() {
            if (!self::userIsLoggedIn()) {
                self::login();
            }
        }

        private static function login() {
            if (!self::credentialsAreSet()) {
                die ('Credentials need to be set first.');
                return;
            }
            // HTTP REQUEST
            $ch = curl_init(self::$tokensUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array()));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              // Json
              'Content-Type: application/json',
              // Email
              "email:".self::$email,
              // Password
              "password:".self::$password)
            );

            $result = curl_exec($ch);
            $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
	    if (array_key_exists('userId', json_decode($result))) {
		self::$tokens = json_decode($result, true);
	    } else {
		die ('Authentication failed');
	    }

        }

        public static function getTokens() {
            return self::$tokens;
        }
    }
?>
