<?php
    require_once 'BaseManager.php';
    class TimeManager extends BaseManager {
	 
        public static function getTSToDate($timestamp) {
            date_default_timezone_set('Europe/Paris');
            $time = date("d M Y H:i", $timestamp / 1000);
            return $time;
        } 
        public static function getTSFromDate($scheduled) {
            date_default_timezone_set('Europe/Paris');
            $time = strtotime($scheduled) * 1000;
            return $time;
        }
    }
?>
