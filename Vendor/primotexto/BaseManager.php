<?php
    require_once 'CredentialsHelper.php';
    require_once 'AccountsManager.php';
    require_once 'CampaignsManager.php';
    require_once 'RepliesManager.php';
    require_once 'ContactsManager.php';
    require_once 'FieldsManager.php';
    require_once 'ListsManager.php';
    require_once 'StatsManager.php';
    require_once 'UsersManager.php';
    require_once 'TimeManager.php';

    class BaseManager {
        protected static $baseURL = 'https://www.primotexto.com/app';

        private static function getCurlWithTokens($url) {
            $curl = curl_init($url);
            $tokens = CredentialsHelper::getTokens();
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                "userId:".$tokens['userId'],
                "hash:".$tokens['hash'],
                "timestamp:".$tokens['timestamp']
            ));
            return $curl;
        }
        protected static function getPutCurl($url, $post_fields) {
            $curl = self::getCurlWithTokens($url);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post_fields);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            return $curl;
        }
	protected static function getPostCurl($url, $post_fields) {
            $curl = self::getCurlWithTokens($url);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post_fields);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            return $curl;
        }
	public static function getGetCurl($url) {
            $curl = self::getCurlWithTokens($url);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            return $curl;
        }
        protected static function getDeleteCurl($url) {
            $curl = self::getCurlWithTokens($url);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            return $curl;
        }
    }
?>
