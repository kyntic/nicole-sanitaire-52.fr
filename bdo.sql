-- phpMyAdmin SQL Dump
-- version 4.2.13
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 30 Juillet 2015 à 10:40
-- Version du serveur :  5.6.24
-- Version de PHP :  5.3.29-pl0-gentoo

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `cms-angular-fr`
--

-- --------------------------------------------------------

--
-- Structure de la table `acos`
--

CREATE TABLE IF NOT EXISTS `acos` (
`id` int(10) unsigned NOT NULL,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `actualites`
--

CREATE TABLE IF NOT EXISTS `actualites` (
`id` int(11) NOT NULL,
  `content_type_id` varchar(128) NOT NULL,
  `etat_id` varchar(128) NOT NULL,
  `page_id` int(11) NOT NULL,
  `acces_membre` tinyint(1) DEFAULT '0',
  `type` varchar(128) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `h1` varchar(256) NOT NULL,
  `sous_titre` varchar(256) DEFAULT NULL,
  `vignette` text NOT NULL,
  `url` varchar(256) NOT NULL,
  `url_r` varchar(256) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_title` varchar(256) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_robots` varchar(128) NOT NULL,
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `publication_date` int(11) DEFAULT NULL,
  `publication_date_fin` int(11) DEFAULT NULL,
  `agenda_id` int(11) DEFAULT NULL,
  `even_date_deb` datetime NOT NULL,
  `even_date_fin` datetime NOT NULL,
  `all_day` tinyint(1) NOT NULL DEFAULT '0',
  `suscribe` tinyint(1) NOT NULL DEFAULT '0',
  `suscribe_nbr` int(11) NOT NULL DEFAULT '0',
  `color` varchar(128) DEFAULT NULL,
  `adresse` text NOT NULL,
  `recurence` varchar(128) NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  `push_accueil` tinyint(1) NOT NULL,
  `auteur_id` int(11) DEFAULT NULL,
  `from` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `adresses`
--

CREATE TABLE IF NOT EXISTS `adresses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `civilite_id` int(11) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `prenom` varchar(128) NOT NULL,
  `nom_entreprise` varchar(256) NOT NULL,
  `adresse_1` varchar(256) NOT NULL,
  `adresse_2` varchar(256) NOT NULL,
  `cp` varchar(16) NOT NULL,
  `ville` varchar(256) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `mobile` varchar(32) DEFAULT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `agendas`
--

CREATE TABLE IF NOT EXISTS `agendas` (
`id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `page_id` int(11) NOT NULL,
  `last_even_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `aros`
--

CREATE TABLE IF NOT EXISTS `aros` (
`id` int(10) unsigned NOT NULL,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `aros_acos`
--

CREATE TABLE IF NOT EXISTS `aros_acos` (
`id` int(10) unsigned NOT NULL,
  `aro_id` int(10) unsigned NOT NULL,
  `aco_id` int(10) unsigned NOT NULL,
  `_create` char(2) NOT NULL DEFAULT '0',
  `_read` char(2) NOT NULL DEFAULT '0',
  `_update` char(2) NOT NULL DEFAULT '0',
  `_delete` char(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `attributes`
--

CREATE TABLE IF NOT EXISTS `attributes` (
`id` int(11) NOT NULL,
  `attribute_family_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `attribute_families`
--

CREATE TABLE IF NOT EXISTS `attribute_families` (
`id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `name_admin` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `content_assocs`
--

CREATE TABLE IF NOT EXISTS `content_assocs` (
`id` int(11) NOT NULL,
  `model` varchar(128) DEFAULT NULL,
  `model_id` int(11) NOT NULL,
  `model_asso` varchar(128) DEFAULT NULL,
  `model_asso_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `content_modules`
--

CREATE TABLE IF NOT EXISTS `content_modules` (
`id` int(11) NOT NULL,
  `model` varchar(128) NOT NULL,
  `model_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `module` varchar(128) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `content_types`
--

CREATE TABLE IF NOT EXISTS `content_types` (
  `id` varchar(128) NOT NULL COMMENT 'Nom de la class model',
  `name` varchar(128) NOT NULL,
  `model` varchar(256) NOT NULL,
  `class_model` varchar(256) NOT NULL,
  `controleur` varchar(256) NOT NULL,
  `prefixe_url` varchar(256) NOT NULL,
  `sufixe_url` varchar(128) DEFAULT NULL,
  `construct_url` tinyint(1) DEFAULT NULL,
  `description` varchar(256) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `ordre` int(11) NOT NULL DEFAULT '0',
  `arborescence` tinyint(1) DEFAULT NULL,
  `associable` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `content_types`
--

INSERT INTO `content_types` (`id`, `name`, `model`, `class_model`, `controleur`, `prefixe_url`, `sufixe_url`, `construct_url`, `description`, `icon`, `ordre`, `arborescence`, `associable`) VALUES
('page', 'Page', 'Page', 'Page', 'pages', 'p-', '', 1, 'Une page', 'file', 2, 1, 1),
('home', 'Accueil', 'Home', 'Home', 'homes', '', '', NULL, 'La page d''accueil', 'home', 1, 1, 0),
('contact', 'Contact', 'ContactManager.Contact', 'Contact', 'contact_manager/contacts', 'contact-', '', 1, 'Une page contact', 'envelope', 3, 1, 1),
('actualite', 'Actualite', 'Blog.Actualite', 'Actualite', 'blog/actualites', 'news-', '', NULL, 'Une actualite', '', 0, 0, 1),
('image', 'Image', '', '', '', '', '', NULL, '', '', 0, 0, 0),
('file', 'Fichier', '', '', '', '', '', NULL, '', '', 0, 0, 0),
('marque', 'Marque', 'Marque', 'Catalogue.Marque', 'catalogue/marques', '', NULL, NULL, '', '', 0, NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `etats`
--

CREATE TABLE IF NOT EXISTS `etats` (
  `id` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL,
  `class` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `etats`
--

INSERT INTO `etats` (`id`, `name`, `class`) VALUES
('draft', 'brouillon', 'danger'),
('publish', 'en ligne', 'success');

-- --------------------------------------------------------

--
-- Structure de la table `even_inscrits`
--

CREATE TABLE IF NOT EXISTS `even_inscrits` (
`id` int(11) NOT NULL,
  `even_id` int(11) NOT NULL,
  `first_name` varchar(128) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `extentions`
--

CREATE TABLE IF NOT EXISTS `extentions` (
  `id` varchar(10) CHARACTER SET latin1 NOT NULL,
  `icone` varchar(128) NOT NULL,
  `type` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `extentions`
--

INSERT INTO `extentions` (`id`, `icone`, `type`) VALUES
('png', 'picture', 'image'),
('jpg', 'picture', 'image'),
('gif', 'picture', 'image'),
('jpeg', 'picture', 'image'),
('pdf', 'pdf', 'fichier'),
('doc', 'word', 'fichier'),
('xls', 'excel', 'fichier'),
('ppt', 'powerpoint', 'fichier'),
('docx', 'word', 'fichier'),
('xlsx', 'excel', 'fichier'),
('zip', 'archive', 'fichier'),
('mp3', 'audio', 'audio'),
('mp4', 'video', 'video'),
('flv', 'video', 'video');

-- --------------------------------------------------------

--
-- Structure de la table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
`id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `lien` varchar(256) NOT NULL,
  `embed` text NOT NULL,
  `extention_id` varchar(10) DEFAULT NULL,
  `type` varchar(256) DEFAULT NULL,
  `slug` varchar(256) DEFAULT NULL,
  `nom_fichier` varchar(256) DEFAULT NULL,
  `poids` decimal(10,0) DEFAULT NULL,
  `dimenssions` text NOT NULL,
  `file_folder_id` int(11) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `file_assocs`
--

CREATE TABLE IF NOT EXISTS `file_assocs` (
  `id` int(11) NOT NULL,
  `file_id` int(11) DEFAULT NULL,
  `model` varchar(128) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `group` varchar(256) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `default` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `file_folders`
--

CREATE TABLE IF NOT EXISTS `file_folders` (
`id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `file_olds`
--

CREATE TABLE IF NOT EXISTS `file_olds` (
`id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `lien` varchar(256) NOT NULL,
  `embed` text NOT NULL,
  `extention_id` varchar(10) DEFAULT NULL,
  `type` varchar(256) DEFAULT NULL,
  `slug` varchar(256) DEFAULT NULL,
  `nom_fichier` varchar(256) DEFAULT NULL,
  `poids` decimal(10,0) DEFAULT NULL,
  `dossier` varchar(256) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `forms`
--

CREATE TABLE IF NOT EXISTS `forms` (
`id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `elements_number` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) DEFAULT NULL,
  `etat_id` varchar(128) DEFAULT NULL,
  `created` datetime NOT NULL COMMENT 'CakePHP',
  `updated` datetime NOT NULL COMMENT 'CakePHP',
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `form_elements`
--

CREATE TABLE IF NOT EXISTS `form_elements` (
`id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `short_name` varchar(16) NOT NULL,
  `type` varchar(16) DEFAULT NULL,
  `sub_type` varchar(64) DEFAULT NULL,
  `mandatory` tinyint(1) NOT NULL,
  `configuration` text NOT NULL,
  `position` int(11) NOT NULL,
  `created` datetime NOT NULL COMMENT 'CakePHP',
  `updated` datetime NOT NULL COMMENT 'CakePHP'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `form_entries`
--

CREATE TABLE IF NOT EXISTS `form_entries` (
`id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_ip` varchar(15) NOT NULL,
  `form_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `data` text NOT NULL,
  `collection_id` varchar(13) NOT NULL,
  `created` datetime NOT NULL COMMENT 'CakePHP'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
`id` mediumint(8) unsigned NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `groups`
--

INSERT INTO `groups` (`id`, `parent_id`, `lft`, `rght`, `name`, `created`) VALUES ('1', NULL, '1', '2', 'Administrateur', '2015-01-19 11:37:29');

-- --------------------------------------------------------

--
-- Structure de la table `homes`
--

CREATE TABLE IF NOT EXISTS `homes` (
  `id` int(11) NOT NULL,
  `content_type_id` varchar(128) NOT NULL,
  `etat_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL DEFAULT '',
  `h1` varchar(256) NOT NULL,
  `url` varchar(1) DEFAULT '/',
  `url_r` varchar(1) NOT NULL DEFAULT '/',
  `meta_description` text NOT NULL,
  `meta_title` varchar(256) NOT NULL DEFAULT '',
  `meta_keywords` text NOT NULL,
  `meta_robots` varchar(128) NOT NULL DEFAULT '',
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `menu_item_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `i18n`
--

CREATE TABLE IF NOT EXISTS `i18n` (
`id` int(10) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `newsletter_campaigns`
--

CREATE TABLE IF NOT EXISTS `newsletter_campaigns` (
`id` int(11) NOT NULL,
  `type` varchar(128) DEFAULT NULL COMMENT 'sms ou email',
  `etat_id` varchar(128) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `template_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `list_id` int(11) NOT NULL,
  `default_from_name` varchar(128) NOT NULL,
  `default_from_email` varchar(256) NOT NULL,
  `default_from_telephone` varchar(128) DEFAULT NULL,
  `default_from_subject` varchar(256) NOT NULL,
  `how_on_list` text NOT NULL,
  `send` tinyint(1) NOT NULL DEFAULT '0',
  `nbr` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `newsletter_emails`
--

CREATE TABLE IF NOT EXISTS `newsletter_emails` (
`id` int(10) NOT NULL,
  `email` varchar(128) CHARACTER SET utf16 NOT NULL,
  `first_name` varchar(128) NOT NULL,
  `last_name` varchar(128) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unsuscribe_link` text NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `liste_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `newsletter_lists`
--

CREATE TABLE IF NOT EXISTS `newsletter_lists` (
`id` int(10) NOT NULL,
  `name` varchar(128) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `newsletter_list_emails`
--

CREATE TABLE IF NOT EXISTS `newsletter_list_emails` (
`id` int(10) NOT NULL,
  `list_id` int(10) NOT NULL,
  `email_id` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `newsletter_templates`
--

CREATE TABLE IF NOT EXISTS `newsletter_templates` (
`id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `folder_name` varchar(256) NOT NULL,
  `is_uploaded` int(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
`id` int(11) NOT NULL,
  `content_type_id` varchar(128) NOT NULL DEFAULT 'page',
  `etat_id` varchar(128) NOT NULL DEFAULT 'draft',
  `publication_date` date NOT NULL,
  `menu_id` tinyint(1) DEFAULT NULL,
  `acces_membre` tinyint(1) DEFAULT '0',
  `name` varchar(128) NOT NULL,
  `h1` varchar(256) NOT NULL,
  `sous_titre` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `url_r` varchar(256) NOT NULL DEFAULT '',
  `vignette` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_title` varchar(256) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_robots` varchar(128) NOT NULL,
  `section` varchar(128) DEFAULT NULL,
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `rss_name` varchar(128) DEFAULT NULL,
  `rss_description` text,
  `rss_niveau` varchar(20) DEFAULT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  `icone` varchar(128) DEFAULT NULL,
  `template` varchar(128) DEFAULT NULL,
  `slug` varchar(256) NOT NULL,
  `position` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `page_olds`
--

CREATE TABLE IF NOT EXISTS `page_olds` (
  `id` int(11) NOT NULL,
  `menu_item_id` int(11) DEFAULT NULL,
  `content_type_id` varchar(128) NOT NULL,
  `etat_id` tinyint(1) DEFAULT NULL,
  `menu_id` tinyint(1) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `h1` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `url_r` varchar(256) NOT NULL DEFAULT '',
  `meta_description` text NOT NULL,
  `meta_title` varchar(256) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_robots` varchar(128) NOT NULL,
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  `icone` varchar(128) DEFAULT NULL,
  `template` varchar(128) DEFAULT NULL,
  `slug` varchar(256) NOT NULL,
  `rght` int(11) NOT NULL,
  `lft` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `parametres`
--

CREATE TABLE IF NOT EXISTS `parametres` (
`id` int(11) NOT NULL,
  `raison_sociale` varchar(128) DEFAULT NULL,
  `about` TEXT DEFAULT NULL,
  `contact_name` varchar(128) DEFAULT NULL,
  `contact_email` varchar(128) DEFAULT NULL,
  `telephone` varchar(128) DEFAULT NULL,
  `adresse_1` varchar(128) DEFAULT NULL,
  `adresse_2` varchar(128) DEFAULT NULL,
  `cp` varchar(128) DEFAULT NULL,
  `ville` varchar(128) DEFAULT NULL,
  `ndd` varchar(128) DEFAULT NULL,
  `telephone_noreply` varchar(128) DEFAULT NULL,
  `email_noreply` varchar(128) DEFAULT NULL,
  `email_name` varchar(128) DEFAULT NULL,
  `email_serveur_smtp` varchar(128) DEFAULT NULL,
  `email_serveur_port` varchar(128) DEFAULT NULL,
  `email_serveur_login` varchar(128) DEFAULT NULL,
  `email_serveur_mdp` varchar(128) DEFAULT NULL,
  `lien_facebook` varchar(128) DEFAULT NULL,
  `lien_twitter` varchar(128) DEFAULT NULL,
  `lien_linkedin` varchar(128) DEFAULT NULL,
  `lien_youtube` varchar(128) DEFAULT NULL,
  `lien_pinterest` varchar(128) DEFAULT NULL,
  `lien_google_plus` varchar(128) DEFAULT NULL,
  `active_brochure` tinyint(1) DEFAULT NULL,
  `google_ua` varchar(128) DEFAULT NULL,
  `google_login` varchar(128) DEFAULT NULL,
  `google_mdp` varchar(128) DEFAULT NULL,
  `contact_page` varchar(128) DEFAULT NULL,
  `updated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `partenaires`
--

CREATE TABLE IF NOT EXISTS `partenaires` (
  `id` int(11) NOT NULL,
  `partenaire_type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `etat_id` int(11) NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `partenaire_types`
--

CREATE TABLE IF NOT EXISTS `partenaire_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `etat_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
`id` int(11) NOT NULL,
  `content_type_id` varchar(128) DEFAULT NULL,
  `etat_id` varchar(32) NOT NULL,
  `rubrique_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `h1` varchar(255) NOT NULL,
  `resume` varchar(255) NOT NULL,
  `prix_ttc` decimal(10,2) NOT NULL,
  `description` text NOT NULL,
  `vignette` text NOT NULL,
  `galerie` text NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `taches`
--

CREATE TABLE IF NOT EXISTS `taches` (
  `id` int(11) NOT NULL,
  `plugin` varchar(256) NOT NULL,
  `controller` varchar(256) NOT NULL,
  `action` varchar(256) NOT NULL,
  `fond` int(11) NOT NULL DEFAULT '1',
  `author` varchar(256) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `etat` int(11) NOT NULL DEFAULT '0' COMMENT '0 : en attente, 1 : tache commencee, 2 taches terminee',
  `donnees` text NOT NULL,
  `resultat` longtext NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tests`
--

CREATE TABLE IF NOT EXISTS `tests` (
`id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `txts`
--

CREATE TABLE IF NOT EXISTS `txts` (
  `id` varchar(128) CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1 NOT NULL,
  `txt` text CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  `nom_connexion` varchar(256) NOT NULL,
  `nom` varchar(256) NOT NULL,
  `prenom` varchar(256) NOT NULL,
  `nom_entreprise` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `mobile` varchar(32) NOT NULL,
  `password` char(40) NOT NULL,
  `vignette` text NOT NULL,
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `user_groups`
--

CREATE TABLE IF NOT EXISTS `user_groups` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user_groups`
--

INSERT INTO `user_groups` (`id`, `user_id`, `group_id`) VALUES
(2, 2, 2),
(4, 3, 2),
(26, 4, 2),
(40, 607123, 1),
(42, 607130, 1),
(43, 607125, 1);



--
-- Structure de la table `typepages`
--

CREATE TABLE IF NOT EXISTS `typepages` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `typepages`
--

INSERT INTO `typepages` (`id`, `name`, `code`, `description`) VALUES
(1, 'Page d''accueil', 'home', 'Liste toutes les pages mÃ©tiers'),
(2, 'Page classique', 'page', ''),
(3, 'Page mÃ©tier', 'metier', ''),
(4, 'Blog (liste des articles)', 'blog', 'Liste toutes les actualitÃ©s'),
(5, 'Page contact', 'contact', ''),
(6, 'Accueil liste des metiers', 'home_metier', 'Liste toutes les pages de type mÃ©tier'),
(7, 'Page d''accueil rÃ©fÃ©rences', 'realisation', 'Liste toutes les actualitÃ©s flaguÃ©es "rÃ©fÃ©rences"'),
(8, 'Mention lÃ©gale', 'mention_legale', '');

--
-- Index pour les tables exportées
--


--
-- Index pour les tables exportées
--

--
-- Index pour la table `acos`
--
ALTER TABLE `acos`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `actualites`
--
ALTER TABLE `actualites`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `agendas`
--
ALTER TABLE `agendas`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `aros`
--
ALTER TABLE `aros`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `aros_acos`
--
ALTER TABLE `aros_acos`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `attributes`
--
ALTER TABLE `attributes`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `attribute_families`
--
ALTER TABLE `attribute_families`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `content_assocs`
--
ALTER TABLE `content_assocs`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `content_modules`
--
ALTER TABLE `content_modules`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `content_types`
--
ALTER TABLE `content_types`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `etats`
--
ALTER TABLE `etats`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `even_inscrits`
--
ALTER TABLE `even_inscrits`
 ADD PRIMARY KEY (`id`), ADD KEY `even_id` (`even_id`);

--
-- Index pour la table `files`
--
ALTER TABLE `files`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `file_folders`
--
ALTER TABLE `file_folders`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `file_olds`
--
ALTER TABLE `file_olds`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `forms`
--
ALTER TABLE `forms`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `form_elements`
--
ALTER TABLE `form_elements`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `form_entries`
--
ALTER TABLE `form_entries`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `i18n`
--
ALTER TABLE `i18n`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `newsletter_campaigns`
--
ALTER TABLE `newsletter_campaigns`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `newsletter_emails`
--
ALTER TABLE `newsletter_emails`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- Index pour la table `newsletter_lists`
--
ALTER TABLE `newsletter_lists`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `newsletter_list_emails`
--
ALTER TABLE `newsletter_list_emails`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `newsletter_templates`
--
ALTER TABLE `newsletter_templates`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pages`
--
ALTER TABLE `pages`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `parametres`
--
ALTER TABLE `parametres`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tests`
--
ALTER TABLE `tests`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user_groups`
--
ALTER TABLE `user_groups`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `acos`
--
ALTER TABLE `acos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `actualites`
--
ALTER TABLE `actualites`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `agendas`
--
ALTER TABLE `agendas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `aros`
--
ALTER TABLE `aros`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `aros_acos`
--
ALTER TABLE `aros_acos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `attributes`
--
ALTER TABLE `attributes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `attribute_families`
--
ALTER TABLE `attribute_families`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `content_assocs`
--
ALTER TABLE `content_assocs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `content_modules`
--
ALTER TABLE `content_modules`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `even_inscrits`
--
ALTER TABLE `even_inscrits`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `files`
--
ALTER TABLE `files`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `file_folders`
--
ALTER TABLE `file_folders`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `file_olds`
--
ALTER TABLE `file_olds`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `forms`
--
ALTER TABLE `forms`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `form_elements`
--
ALTER TABLE `form_elements`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `form_entries`
--
ALTER TABLE `form_entries`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `groups`
--
ALTER TABLE `groups`
MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `i18n`
--
ALTER TABLE `i18n`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--

-- AUTO_INCREMENT pour la table `newsletter_campaigns`
--
ALTER TABLE `newsletter_campaigns`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `newsletter_emails`
--
ALTER TABLE `newsletter_emails`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `newsletter_lists`
--
ALTER TABLE `newsletter_lists`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `newsletter_list_emails`
--
ALTER TABLE `newsletter_list_emails`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `newsletter_templates`
--
ALTER TABLE `newsletter_templates`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pages`
--
ALTER TABLE `pages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `parametres`
--
ALTER TABLE `parametres`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `products`
--
ALTER TABLE `products`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `tests`
--
ALTER TABLE `tests`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `user_groups`
--
ALTER TABLE `user_groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

ALTER TABLE `parametres` ADD `siren` VARCHAR(255) NULL DEFAULT NULL , ADD `hebergeur` VARCHAR(255) NULL DEFAULT NULL , ADD `resp_publication` VARCHAR(255) NULL DEFAULT NULL , ADD `url_site_web` VARCHAR(255) NULL DEFAULT NULL ;
ALTER TABLE `parametres` ADD `google_tag_manager` TEXT ;

--
-- Structure de la table `product_attributes`
--

CREATE TABLE IF NOT EXISTS `product_attributes` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `attribute_family_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `product_attributes`
--
ALTER TABLE `product_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `parametres` ADD `mentions_page` VARCHAR(255) NOT NULL ;

ALTER TABLE `pages` ADD `icon` TEXT NOT NULL AFTER `vignette`;

INSERT INTO `typepages` (`id`, `name`, `code`, `description`) VALUES ('', 'Page rubrique', 'rubrique', '');

-- Jessy@kobaltis
ALTER TABLE `attributes` ADD `vignette` TEXT NOT NULL;
ALTER TABLE `attribute_families` ADD `speciale` TINYINT(1) NOT NULL DEFAULT '0' ;

ALTER TABLE `pages` ADD `email` VARCHAR(255) NOT NULL ;


/** Newsletter **/
CREATE TABLE IF NOT EXISTS `newsletter_inscrits` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `newsletter_inscrits`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `newsletter_inscrits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
