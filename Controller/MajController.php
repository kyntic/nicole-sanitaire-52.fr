<?php

/**
 * Class PagesController
 */
class MajController extends AppController
{


    /**
     * Attention :
     * Enlever le afterSave de page avant
     */
    public function admin_index() {

        /*
         * Mise à jour depuis menu item
         */
        /*
        $this->loadModel('MenuItem');

        $menu = $this->MenuItem->find('all');

        foreach($menu as $v) {

            $data = array('Page' =>
                array(
                    'id'        => $v['MenuItem']['id'],
                    'etat_id'   => ($v['MenuItem']['etat_id']) ? 'publish' : 'draft',
                    'name'      => $v['MenuItem']['name'],
                    'h1'        => $v['MenuItem']['h1'],
                    'meta_title'=> $v['MenuItem']['meta_title'],
                    'parent_id' => $v['MenuItem']['parent_id'],
                    'lft'       => $v['MenuItem']['lft'],
                    'rght'      => $v['MenuItem']['rght'],
                    'position'  => $v['MenuItem']['rght'],
                    'template'  => $v['MenuItem']['content_type_id'],
                    'publication_date' => date('Y-m-d')
                )
            );

            if($this->Page->save($data)) {

                $v['MenuItem']['id'].'<br />';

            }else{

                echo 'erreur : '.$v['MenuItem']['id'].'<br />';

            }

        }
        */

        /**
         * Mise à jour des contenus
         */

        /*
        $this->loadModel('ModuleManager.ContentModule');

        $this->loadModel('PageOld');

        $cont = $this->PageOld->find('all');

        foreach($cont as $v) {


            if($this->ContentModule->save(array('ContentModule' => array(
                'id'        => null,
                'model'     => 'Page',
                'model_id'  => $v['PageOld']['menu_item_id'],
                'position'  => 0,
                'module'    => 'text',
                'content'   =>  array('txt' => $v['PageOld']['body'])

            )))) {

                echo 'Ok '.'<br />';

            }else{
                echo 'not Ok '.'<br />';
            }


        }

        */

        /**
         * Mise à jour des url
         */

        /*
        $c = $this->Page->find('all');

        foreach($c as $v) {

            $this->Page->save(array('Page' => array(
                'id' => $v['Page']['id'],
                'name' => $v['Page']['name']

            )));

        }
        */

        /**
         * Mise à jour des files
         */

        /*
        $this->loadModel('FileManager.File');
        $this->loadModel('FileOld');

        $r = $this->FileOld->find('all');

        foreach($r as $v) {

            $this->File->save(array('File' => array(
                'id' => $v['FileOld']['id'],
                'name' => $v['FileOld']['name'],
                'description' => $v['FileOld']['description'],
                'lien' => $v['FileOld']['lien'],
                'embed' => $v['FileOld']['embed'],
                'extention_id' => $v['FileOld']['extention_id'],
                'type' => $v['FileOld']['type'],
                'slug' => $v['FileOld']['slug'],
                'id' => $v['FileOld']['id'],
                'nom_fichier' => $v['FileOld']['nom_fichier'],
                'poids' => $v['FileOld']['poids'],
                'url' => '/files/'.$v['FileOld']['url'],
                'dossier' => '/files/'.$v['FileOld']['dossier']

            )));

        }
        */

        /**
         * Mise à jours des langues
         */

        /*
        $model = 'ContentModule';
        $ListeChamps = array('content');

        $this->loadModel('I18nModel');
        $this->loadModel($model);
        $Pages = $this->$model->find('all');

        foreach($Pages as $v)
        {

            foreach(Configure::read('Config.languages') as $langue)
            {

                foreach($ListeChamps as $champ)
                {

                    $i18n = array();
                    $i18n['I18nModel']['id'] = null;
                    $i18n['I18nModel']['locale'] = $langue;
                    $i18n['I18nModel']['model'] = $model;
                    $i18n['I18nModel']['foreign_key'] = $v[$model]['id'];
                    $i18n['I18nModel']['field'] = $champ;
                    $i18n['I18nModel']['content'] = $v[$model][$champ];

                    debug($this->I18nModel->save($i18n));

                }

            }


        }

        */

        exit();



    }



}