<?php
/**
 * controllers/UsersController.php
 */
class TypepagesController extends AppController
{
    /**
     * The controller name
     *
     * @var string $name
     */
    var $name = 'Typepages';

    /**
     * Admin index
     *
     * @return void
     */
    function admin_index()
	{

        $conditions = array();

        $typepages = $this->Typepage->find(
            'all',
            array(
                'conditions'    =>  $conditions,
                'order'         =>  array('Typepage.name' => 'ASC')
            )
        );

        exit(
            json_encode(
                array(
                    'statut'    =>  1,
                    'data'      =>  $typepages
                )
            )
        );

	}


    function admin_liste()
    {

        $conditions = array();

        $typepages = $this->Typepage->find(
            'list',
            array(
                'fields'        => array('Typepage.code', 'Typepage.name'),
                'conditions'    =>  $conditions,
                'order'         =>  array('Typepage.name' => 'ASC')
            )
        );
        
        $return = array();
        
        foreach($typepages as $k => $v){
            
            $return[] = array('id' => $k, 'label' => $v);

        }

        exit(
            json_encode(
                array(
                    'statut'    =>  1,
                    'data'      =>  $return
                )
            )
        );

    }
    /**
     * Admin edit
     *
     * @param null $id
     */
    function admin_edit($id = null)
	{

		if (!empty($this->data)) {

			if ($this->Typepage->save($this->data)) {

	            $txt = (!empty($this->data['Typepage']['id'])) ? 'Type de page modifié' : 'Type de page créé';

	            $this->Session->setFlash('<strong>'.$txt.' !</strong>', 'success');

	            $this->redirect(array('action' => 'index'));

			} else {

				$this->Session->setFlash('<strong>Le formulaire contient des erreurs</strong>', 'error');
			}
		}

		if ($id) {

			$this->Typepage->id = $id;

			$this->data = $this->Typepage->read();
		}

		$this->set('Typepages', $this->Typepage->generateTreeList(null, '{n}.Typepage.id', '{n}.Typepage.name', ' - '));

		$this->set('MenuAdminActives', array(45, 47));
	}

    /**
     * Admin delete
     *
     * @param $id
     */
    function admin_delete($id)
	{
 		$this->Typepage->delete($id);
		$
		$this->Session->setFlash('Le type de page a été supprimé.', 'success');

		$this->redirect('index');
	}
}