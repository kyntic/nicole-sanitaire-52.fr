<?php

/**
 * Class AdminMenusController
 */
class AdminMenusController extends AppController
{

	public function admin_getMenu()
	{

		$menu = array();

		$menu[] = array(
			'text'      =>  'Statistiques',
			'heading'   =>  true
		);

		$menu[] = array(
			'text'      =>  'Tableau de bord',
			'sref'      =>  'app.dashboard',
			'icon'      =>  'icon-speedometer'
		);

		$menu[] = array(
			'text'      =>  'Publier',
			'heading'   =>  true
		);

		$menu[] = array(
			'text'      =>  'Les Pages',
			'sref'      =>  'app.pages.index',
			'icon'      =>  'icon-directions'
		);

		$menu[] = array(
			'text'      =>  'Les Actualités',
			'sref'      =>  'app.actualites.index',
			'icon'      =>  'fa fa-newspaper-o'
		);

		$menu[] = array(
			'text'      =>  'Les Médias',
			'sref'      =>  'app.files',
			'icon'      =>  'icon-picture'
		);

		$menu[] = array(
			'text'      =>  'Les Calendriers',
			'sref'      =>  'app.agenda.index',
			'icon'      =>  'icon-calendar'
		);

		$menu[] = array(
			'text'      =>  'Les Partenaires',
			'sref'      =>  'app.partenaires.index',
			'icon'      =>  'fa fa-thumbs-o-up'
		);

		$menu[] = array(
			'text'      =>  'Catalogue',
			'heading'   =>  true
		);

		$menu[] = array(
			'text'      =>  'Les Produits',
			'sref'      =>  'app.products.index',
			'icon'      =>  'fa fa-shopping-cart'
		);

		$menu[] = array(
			'text'      =>  'Les Attributs',
			'sref'      =>  'app.attributes_families.index',
			'icon'      =>  'fa fa-gears'
		);

		if (AuthComponent::user('group_id') == 1) {

			$menu[] = array(
				'text'      =>  'Communiquer',
				'heading'   =>  true
			);

            $menu[] = array(
                'text'      =>  'Membres',
                'sref'      =>  'app.users.index',
                'icon'      =>  'icon-users'
            );

            $menu[] = array(
                'text'      =>  'Inscrits à la newsletter',
                'sref'      =>  'app.newsletter_inscrits.index',
                'icon'      =>  'icon-envelope'
            );

			$menu[] = array(
				'text'      =>  'Paramètres',
				'heading'   =>  true
			);

			$menu[] = array(
				'text'      =>  'Les Paramètres',
				'sref'      =>  'app.parametres',
				'icon'      =>  'fa fa-cog'
			);

			$menu[] = array(
				'text'      =>  'Les Groupes',
				'sref'      =>  'app.groups',
				'icon'      =>  'fa fa-group'
			);

			$menu[] = array(
				'text'      =>  'Les templates',
				'sref'      =>  'app.typepages.index',
				'icon'      =>  'fa fa-newspaper-o'
			);

			$menu[] = array(
				'text'      =>  'Documentation',
				'sref'      =>  'app.documentation',
				'icon'      =>  'icon-graduation'
			);

		}

		exit(
			json_encode(
				$menu
			)
		);

	}

}
