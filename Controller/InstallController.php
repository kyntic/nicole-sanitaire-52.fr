<?php

/**
 * Class InstallController
 */
class InstallController extends AppController
{

	/**
	 * Installation d'un site
	 */
	public function index()
	{

        if (!empty($this->request->data)) {

	        $data = $this->request->data;

	        // Ecriture des paramètres de la BDD

	        $fileDatabasePath = APP . 'Config/database.php';

	        $fileDatabase = file_get_contents($fileDatabasePath);

	        $fileDatabase = preg_replace('#DummySource#', 'Database/Mysql', $fileDatabase);
	        $fileDatabase = preg_replace('#%%%login%%%#', $data['Install']['login'], $fileDatabase);
	        $fileDatabase = preg_replace('#%%%password%%%#', $data['Install']['password'], $fileDatabase);
	        $fileDatabase = preg_replace('#%%%database%%%#', $data['Install']['database'], $fileDatabase);

	        $handle = fopen($fileDatabasePath, "w");
	        fwrite($handle, $fileDatabase);
	        fclose($handle);

            // Installation de la BDD
	        App::uses('ConnectionManager', 'Model');
	        $db = ConnectionManager::getDataSource("default");

            if (!$db->isConnected()) {

                $this->Session->setFlash('error', 'Les paramètres de connexion ne sont pas corrects');
                $this->redirect('/install');

            }

	        $sql = file_get_contents(APP . '/bdo.sql');
			$db->rawQuery($sql);

			// Initialisation des groupes et des utilisateurs par défaut
	        $this->loadModel('Group');
	        $this->loadModel('User');

	        $this->Group->save(
                array(
                    'id'    =>  1,
                    'name'  =>  'Administrateurs'
                )
	        );

	        $this->Group->save(
                array(
                    'id'    =>  2,
                    'name'  =>  'Redacteurs'
                )
	        );

            $this->User->save(
                array(
	                'id'                => null,
                    'email'             => 'jerome@whatson-web.com',
                    'nom'               => 'Lebleu',
                    'prenom'            => 'Jerome',
                    'motdepasse'        => 'admin',
                    'confirm_password'  => 'admin',
                    'group_id'          => 1,
                    'verified'          => 1
                ),
                array(
                    'validate' => false
                )
            );

	        $this->User->save(
               array(
                   'id'                => null,
                   'email'             => 'prod@kobaltis.net',
                   'nom'               => 'Kobaltis',
                   'prenom'            => 'Prod',
                   'motdepasse'        => 'mdpkobaltis2000',
                   'confirm_password'  => 'mdpkobaltis2000',
                   'group_id'          => 1,
                   'verified'          => 1
               ),
               array(
                   'validate' => false
               )
	        );

	        $this->User->save(
               array(
                   'id'                => null,
                   'email'             => 'redacteur@kobaltis.net',
                   'nom'               => 'Kobaltis',
                   'prenom'            => 'Redacteur',
                   'motdepasse'        => 'mdpkobaltis2000',
                   'confirm_password'  => 'mdpkobaltis2000',
                   'group_id'          => 2,
                   'verified'          => 1
               ),
               array(
                   'validate' => false
               )
	        );

            $this->redirect('/admin');

        }

        $this->layout = 'install';

	}

}
