<?php

/**
 * Class HomesController
 */
class HomesController extends AppController
{
    /**
     * Index
     *
     * @return void
     */
    public function index ()
    {


        $this->loadModel('Page');
        $this->loadModel('ModuleManager.ContentModule');
        $this->loadModel('Blog.Actualite');

        $page = $this->Page->findByTemplate('home');
        $this->set('page', $page);

        $_breadcrumb = array(
            'Model' => 'Page',
            'Chemin' => $this->Page->getPath($page['Page']['id'])
        );
        $this->set('_breadcrumb', $_breadcrumb);

        $portfolioItems = $this->ContentModule->find(
            'first',
            array(
                'conditions' => array(
                    'ContentModule.model'       =>  'Page',
                    'ContentModule.model_id'    =>  $page['Page']['id'],
                    'ContentModule.module'      =>  'galerie'
                )
            )
        );


        if (!$portfolioItems) {

            $portfolioItems = array();

        } else {

            $portfolioItems = $portfolioItems['ContentModule']['content']->files;
            $portfolioItems = json_decode(json_encode($portfolioItems), true);

        }
        $this->set('portfolioItems', $portfolioItems);


	    //Liste des contenus de la page
	    $ContentModules = $this->ContentModule->find(
	      'all',
	      array(
	          'conditions' => array(
	              'ContentModule.model'       =>  'page',
	              'ContentModule.model_id'    =>  $page['Page']['id'],
	          ),
	          'order' => array(
	              'ContentModule.position' => 'ASC'
	          )
	      )
	    );

	    $portfolioDetecte = false;

	    foreach ($ContentModules as $k => $ContentModule) {

		    if ($ContentModule['ContentModule']['module'] == 'galerie' && !$portfolioDetecte) {

			    $portfolioDetecte = true;
			    unset($ContentModules[$k]);
		    }

	    }

	    $this->set('ContentModules', $ContentModules);


        //Les métiers
        $metiers = $this->Page->find('all', array('conditions' => array('Page.template' => 'metier'), 'order' => 'Page.position'));

        //Les métiers
        $actus = $this->Actualite->find('all', array('conditions' => array('Actualite.type !=' => 'reference', 'Actualite.push_accueil' => 1), 'order' => 'Actualite.created DESC'));


        $this->set('metiers', $metiers);
        $this->set('actus', $actus);
        $this->render('index');

    }


    public function admin_dashboard() {

        $this->loadModel('Blog.Actualite');
        $this->loadModel('Blog.Even');
        $this->loadModel('Page');
        $this->loadModel('NewsletterManager.NewsletterCampaign');

        $data['Actualite'] = $this->Actualite->find('first', array('order' => array('Actualite.created DESC')));
        $data['Even'] = $this->Even->find('first', array('order' => array('Even.created DESC')));
        $data['Page'] = $this->Page->find('first', array('order' => array('Page.created DESC')));

        $data['NewsletterCampaign'] = $this->NewsletterCampaign->find('first', array('order' => array('NewsletterCampaign.created DESC')));

        $data['Actualite']['date'] = strtotime($data['Actualite']['Actualite']['created']) * 1000;
        $data['Even']['date'] = strtotime($data['Even']['Even']['created']) * 1000;
        $data['Page']['date'] = strtotime($data['Page']['Page']['created']) * 1000;
        $data['NewsletterCampaign']['date'] = strtotime($data['NewsletterCampaign']['NewsletterCampaign']['created']) * 1000;





        $data['Actualite']['nbr'] = $this->Actualite->find('count');
        $data['Even']['nbr'] = $this->Even->find('count');
        $data['Page']['nbr'] = $this->Page->find('count');
        $data['PageNonPub']['nbr'] = $this->Page->find('count', array('conditions' => array('Page.etat_id' => 'draft')));
        $data['NewsletterCampaign']['nbr'] = $this->NewsletterCampaign->find('count');

        echo json_encode($data);

        exit();


    }


}
