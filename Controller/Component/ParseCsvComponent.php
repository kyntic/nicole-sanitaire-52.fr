<?php
/**
* Composant consistant à traiter et enregistrer les scores issue d'un fichier ffnex.xml
*/
// app/controllers/components/traitement_scores.php
class ParseCsvComponent extends Object 
{

	function import($filename) {

        $ARRAY = array();
        
        if ( $FILE=fopen($filename,"r") ) {              // ouverture du fichier

            while ($ARRAY[] = fgetcsv($FILE,1024,';'));
            
            fclose($FILE) ;                              // ferme le fichier
      
            array_pop($ARRAY);                           // efface la derniere ligne
         
            return $ARRAY;                              // renvoie le tableau
        
        }
    
    }	
 

}
?>