<?php
/**
 * Class ContentModulesController
 */
class ContentModulesController extends AppController
{


    /**
     * Liste des modules d'une pages
     * @param $id
     * @param $model
     * @param $module
     */
    public function admin_index ($model, $id) {

        $opts = array();

        $opts['conditions']['ContentModule.model'] = $model;
        $opts['conditions']['ContentModule.model'] = $id;

        $this->set('Modules', $this->ContentModule->find('all', array('fields' => array('ContentModule.*'), 'order' => 'position', 'conditions' => array('ContentModule.model' => $model, 'ContentModule.model_id' => $id))));

        $this->layout = 'ajax';

    }


    /**
     * Admin delete
     *
     * @param $id
     */
    public function admin_delete($id)
    {
        $this->ContentModule->delete($id);
        $this->Session->setFlash(__('Module supprimé'), 'success');
        $this->redirect(Controller::referer());
    }



}