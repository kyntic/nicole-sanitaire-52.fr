<?php

/**
 * Class SitemapController
 */
class SitemapController extends AppController
{
    /**
     * The components
     *
     * @var array $components
     */
    var $components = array('RequestHandler');

    /**
     * The helpers
     *
     * @var array $helper
     */
    var $helper = array('Xml');

    /**
     * Index
     *
     * @return void
     */
    public function index () 
    {
        $NewSiteMap = array();

        $this->loadModel('Home');
        $Homes = $this->Home->find('all');

        foreach ($Homes as $v) {

            $NewSiteMap[] = array(
                'loc'       => FULL_BASE_URL.$v['Home']['url'], 
                'lastmod'   => $v['Home']['updated'][0], 
                'changefreq'=> (empty($v['Home']['changefreq'])) ? 'weekly' : $v['Home']['changefreq'], 
                'priority'  => (empty($v['Home']['priority'])) ? '0.5' : $v['Home']['priority']
            );
        }

        $this->loadModel('Page');

        $Pages = $this->Page->find('all');

        foreach ($Pages as $v) {

            $NewSiteMap[] = array(
                'loc'       => FULL_BASE_URL.$v['Page']['url'], 
                'lastmod'   => $v['Page']['updated'][0], 
                'changefreq'=> (empty($v['Page']['changefreq'])) ? 'weekly' : $v['Page']['changefreq'], 
                'priority'  => (empty($v['Page']['priority'])) ? '0.5' : $v['Page']['priority']
            );

        }

        $this->loadModel('Blog.Actualite');

        $Actualites = $this->Actualite->find('all');

        foreach ($Actualites as $v) {

            $NewSiteMap[] = array(
                'loc'       => FULL_BASE_URL.$v['Actualite']['url'], 
                'lastmod'   => $v['Actualite']['updated'][0], 
                'changefreq'=> (empty($v['Actualite']['changefreq'])) ? 'weekly' : $v['Actualite']['changefreq'], 
                'priority'  => (empty($v['Actualite']['priority'])) ? '0.5' : $v['Actualite']['priority']
            );
        }

        $this->loadModel('Blog.Evenement');

        $Evenements = $this->Evenement->find('all');

        foreach ($Evenements as $v) {

            $NewSiteMap[] = array(
                'loc'       => FULL_BASE_URL.$v['Evenement']['url'], 
                'lastmod'   => $v['Evenement']['updated'][0], 
                'changefreq'=> (empty($v['Evenement']['changefreq'])) ? 'weekly' : $v['Evenement']['changefreq'], 
                'priority'  => (empty($v['Evenement']['priority'])) ? '0.5' : $v['Evenement']['priority']
            );
        }

        $this->loadModel('Catalogue.Rubrique');

        $Rubriques = $this->Rubrique->find('all');

        foreach ($Rubriques as $v) {

            $NewSiteMap[] = array(
                'loc'       => FULL_BASE_URL.$v['Rubrique']['url'], 
                'lastmod'   => $v['Rubrique']['updated'][0], 
                'changefreq'=> (empty($v['Rubrique']['changefreq'])) ? 'weekly' : $v['Rubrique']['changefreq'], 
                'priority'  => (empty($v['Rubrique']['priority'])) ? '0.5' : $v['Rubrique']['priority']
            );
        }

        $this->loadModel('Catalogue.Produit');

        $Produits = $this->Produit->find('all');

        foreach ($Produits as $v) {

            $NewSiteMap[] = array(
                'loc'       => FULL_BASE_URL.$v['Produit']['url'], 
                'lastmod'   => $v['Produit']['updated'][0], 
                'changefreq'=> (empty($v['Produit']['changefreq'])) ? 'weekly' : $v['Produit']['changefreq'], 
                'priority'  => (empty($v['Produit']['priority'])) ? '0.5' : $v['Produit']['priority']
            );
        }

        header('Content-type: text/xml');

        $this->set('SiteMap', $NewSiteMap);
    }
}