<?php

/**
 * Class ParametresController
 */
class ParametresController extends AppController
{
    /**
     * The menu list
     *
     * @var array $menuList
     */
    private $menuList = array();

    /**
     * Admin index
     *
     * @return void
     */
    function admin_index()
	{

        $params = $this->Parametre->find('first');

		if (!empty($this->data)) {

            $res['ok'] = false;

            $data = $this->data;


            if($params) {
                $data['Parametre']['id'] = $params['Parametre']['id'];
            }
            else {
                $data['Parametre']['id'] = null;
            }

			if($this->Parametre->save($data)) {

                $res['ok'] = true;

            }

            header("Content-type: application/json; charset=utf-8");

            echo json_encode($res);

            exit();

		}

        $data['Parametre']['about'] = 'About';

        $t = $this->Parametre->find('first');

        header("Content-type: application/json; charset=utf-8");


		if($t) echo json_encode($t);



        exit();

	}





}