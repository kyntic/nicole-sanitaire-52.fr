<?php

/**
 * Class Parametre
 */
class Parametre extends AppModel
{
    public function prepare_txt ($tmpl, $var) {

        $remp = array();

        $i = 0;

        foreach($var as $tab) {

            foreach($tab as $k => $v) {

                foreach($v as $x => $y) {

                    if(!is_array($y)) {

                        $patt[$i] = '#\{'.$k.'.'.$x.'\}#';
                        $remp[$i] = $y; 

                        $i++;

                    }

                }

            }

        }


        $patt['FULL_BASE_URL'] = '#\{FULL_BASE_URL\}#';
        $remp['FULL_BASE_URL'] = FULL_BASE_URL;

        $txt = preg_replace($patt, $remp, $tmpl);

        

        return $txt;
    }
}