<?php
class Page extends AppModel {

    public $belongsTo = array(
        'ContentType',
        'Etat'
    );

    public $actsAs = array(
        'Tree'
    );



    /**
     * Before Save
     * Modificaiton du format de la date
     * @return bool
     */
    public function beforeSave($options = array()) {

        if(isset($this->data[$this->alias]['publication_date_js'])) {

            if(!empty($this->data[$this->alias]['publication_date_js'])) {

                $date = new DateTime($this->data[$this->alias]['publication_date_js']);
                $this->data[$this->alias]['publication_date'] = $date->format('Y-m-d');

            }

        }

        if(empty($this->data[$this->alias]['name']) && !empty($this->data[$this->alias]['h1'])) {

            $this->data[$this->alias]['name'] = $this->data[$this->alias]['h1'];

        }

	    if(!empty($this->data[$this->alias]['vignette'])) $this->data[$this->alias]['vignette'] = json_encode($this->data[$this->alias]['vignette']);

	    if(!empty($this->data[$this->alias]['icon'])) $this->data[$this->alias]['icon'] = json_encode($this->data[$this->alias]['icon']);

        return true;

    }

	/**
	 * @param mixed $results
	 * @param bool  $primary
	 *
	 * @return mixed
	 */
	public function afterFind($results, $primary = false) {

    	foreach ($results as &$v) {

            if(!empty($v[$this->alias])) {

                if(empty($v[$this->alias]['url']) && !empty($v[$this->alias]['id'])) $v[$this->alias]['url'] = '/pages/view/'.$v[$this->alias]['id'];

                if(!empty($v[$this->alias]['url_r'])) $v[$this->alias]['url'] = $v[$this->alias]['url_r'];

                if(!empty($v[$this->alias]['template']) && $v[$this->alias]['template'] == 'home') $v[$this->alias]['url'] = '/';


	            if (!empty($v[$this->alias]['vignette'])) {

		            $v[$this->alias]['vignette'] = json_decode($v[$this->alias]['vignette']);

	            }

	            if (!empty($v[$this->alias]['icon'])) {

		            $v[$this->alias]['icon'] = json_decode($v[$this->alias]['icon']);

	            }
            }

        }

        return $results;


    }

    /**
     * After save  :
     * Modification des urls
     */
    public function afterSave($created) {

        if(isset($this->data[$this->alias]['name'])) {

            $dev = ' > /dev/null';
            //$dev = ' ';
            exec(APP.'cakephp/vendors/cakeshell menu_tree fabrique_url '.$this->id.' Page -cli /usr/bin -console /cakephp/lib/Cake/Console -app ' . APP . $dev, $res);

            //debug($res);
        }

    }


}
