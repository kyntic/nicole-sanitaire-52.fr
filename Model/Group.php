<?php

/**
 * Class Group
 */
class Group extends AppModel
{
    /**
     * The act as
     *
     * @var array $actAs
     */
	public $actsAs = array('Tree');

    /**
     * The validates
     *
     * @var array $validate
     */
	public $validate = array(
		'name' => array(
			'rule' 		=> 'notEmpty', 
			'required' 	=> true, 
			'message' 	=> 'Vous devez entrer un nom de groupe'
		)
	);

    /**
     * Get the parent node
     *
     * @return null
     */
    public function parentNode()
    {
        return null;
    }
}