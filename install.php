<?php

    // Copie des fichiers default.php
	$defaultFiles = array(
		array(
			'name'          =>  'core.php',
			'defaultName'   =>  'core.default.php',
			'folder'        =>  'Config/'
		),
		array(
			'name'          =>  'database.php',
			'defaultName'   =>  'database.default.php',
			'folder'        =>  'Config/'
		),
		array(
			'name'          =>  'email.php',
			'defaultName'   =>  'email.default.php',
			'folder'        =>  'Config/'
		),
		array(
			'name'          =>  '.htaccess',
			'defaultName'   =>  '.htaccess.default',
			'folder'        =>  'htdocs/'
		)
	);

	foreach ($defaultFiles as $defaultFile) {

		copy(getcwd() . '/' . $defaultFile['folder'] . $defaultFile['defaultName'], getcwd() . '/' . $defaultFile['folder'] . $defaultFile['name']);

	}

    // CHMOD à 755 pour les fichiers de la Console de cakePHP
    $cakeConsoleFiles = array(
        array(
            'name'          =>  'cakeshell',
            'folder'        =>  'cakephp/vendors/'
        ),
        array(
            'name'          =>  'cake',
            'folder'        =>  'cakephp/lib/Cake/Console/'
        ),
        array(
            'name'          =>  'cake.bat',
            'folder'        =>  'cakephp/lib/Cake/Console/'
        ),
        array(
            'name'          =>  'cake',
            'folder'        =>  'Console/'
        ),
        array(
            'name'          =>  'cake.bat',
            'folder'        =>  'Console/'
        ),
    );

    foreach ($cakeConsoleFiles as $cakeConsoleFile) {

        exec('chmod 755 ./' . $cakeConsoleFile['folder'] . $cakeConsoleFile['name']);
    }
