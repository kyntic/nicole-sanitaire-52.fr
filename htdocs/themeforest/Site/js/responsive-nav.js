(function($){


$(document).ready(function(){
	$('.shop_sidebar_navigation').children('li').children('a').click(function(e){
		if(!$(this).hasClass('sidebar_active')) {
			$('.sidebar_active').parent().find('ul').stop(true).slideUp(400);
			$('.sidebar_active').removeClass('sidebar_active');
			
			if($(this).parent().find('ul').length > 0){
			e.preventDefault();
			$(this).addClass('sidebar_active');
			var $shop_sub = $(this).parent().children('ul');
			$shop_sub.stop(true,true).slideDown(400);	
			}	
		}
		else {
			e.preventDefault();
			$('.sidebar_active').parent().find('ul').stop(true).slideUp(400);
			$(this).removeClass('sidebar_active');
		}
	});
});


})(jQuery);