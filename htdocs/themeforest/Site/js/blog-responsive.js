(function($){
	$(window).resize(function(){
		var $win_width = $(window).width();
		if($win_width < 700){
			blog_splitting();
		}
		else{
			blog_normal();
		}
	});
	$(window).load(function(){
		var $win_width = $(window).width();
		if($win_width < 700){
			blog_splitting();
		}
		else{
			blog_normal();
		}
	});
	function blog_normal(){
		$('.grid-layout-select').show();
		$('.grid-layout-select li:first a').trigger('click');
	}
	function blog_splitting(){
		$('.grid-layout-select li:last a').trigger('click');
		$('.grid-layout-select').hide();
	}
})(jQuery);