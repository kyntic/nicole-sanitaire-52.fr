(function($){
	$(document).ready(function(){
		
		$('.select-icon i.cp-trigger').click(function(){
			if(parseInt($('.colorpicker-wrapper').css('right')) != 0){
				$(this).parent().parent().stop(true).animate({right : 0},250);
				$(this).addClass('cp-open');
			}
			else{
				$(this).parent().parent().stop(true).animate({right : '-200px'},250);
				$(this).removeClass('cp-open');
			}
		});
		
		$('.cp-select-list li a').click(function(){
			var $cpColor = $(this).attr('class');
			$('.colorpicker-wrapper').parent().attr('class', $cpColor);
		});
		
		$('.box-layout a').click(function(){
			$(this).parent().parent().find('.active').removeClass('active');
			$(this).addClass('active');
			$('.footer-bg, header, .header_nav, .fullwidthbanner-container, .sp-slideshow').addClass('container');
			$('.full-width-container, .header_height').css({"padding-left" : "15px", "padding-right" : "15px"});
			$('.sliding_tabs_content_wrapper.creative-page').parent().css({"overflow" : "hidden"});
			$('.full-width-container').each(function(){
				$(this).addClass('container');
			});
			$('.paralax-space-wrapper').each(function(){
				$(this).addClass('container');
			});
			$('.usquare_module_wrapper').closest('.full-width-container').css({"padding" : "0"});
		});
		$('.wide-layout a').click(function(){
			$(this).parent().parent().find('.active').removeClass('active');
			$(this).addClass('active');
			$('.full-width-container, .header_height').css({"padding-left" : "0", "padding-right" : "0"});
			$('.footer-bg, header, .header_nav, .fullwidthbanner-container, .sp-slideshow').removeClass('container');
			$('.full-width-container').each(function(){
				$(this).removeClass('container');
			});
			$('.paralax-space-wrapper').each(function(){
				$(this).removeClass('container');
			});
		});
	});
})(jQuery);