(function($){
	
window.itemCatContentString = new Array(0);
window.itemCatContentSelect = new Array('');
window.itemCatFlitered = new Array(0);
window.itemCatFliteredIndex = new Array(0);
window.itemInitialArray = new Array(0,0);
window.columnSelectVar = 0;	
window.iterationCounter = 0;
	


/* FORM */

$(document).ready(function(){
	$('form').each(function(){
		$(this).find('input,textarea').focus(function(){
			if($(this).attr('data-value')==$(this).val()){
				$(this).val('');
			}	
		});
		$(this).find('input,textarea').focusout(function(){
			if($(this).val() == ''){
				$(this).val($(this).attr('data-value'));
			}	
		});
	});
	
/*  twitter  */
// $('.tweet-list').each(function(){
// 	var $this = $(this);
// 	$.get('twitter.php?list=true',function(ret){
// 		$this.html(ret);
// 	});
// });	

// $('.single-twitter-post').each(function(){
// 	var $this = $(this);
// 	$.get('twitter.php',function(ret){
// 		$this.html(ret);
// 	});
// });



$(document).on('click', '.submenu-trigger', function(e){
	e.preventDefault();
	if($(this).closest('li').find('ul').hasClass('opened')){
		$(this).closest('li').find('ul').stop(true).slideUp(250).removeClass('opened');
	}
	else{
		$(this).closest('li').find('ul').stop(true).slideDown(250).addClass('opened');
	}
});

		/*  Prevent Default - Default for all links  */
	$(document).on('click', 'a', function(e){
		if($(this).attr('href').length < 2)
		e.preventDefault();
	});
	
	$('.extension-on-hover').each(function(){
		var startLeft = $(this).parent().find('.cathegory-title').width() - 16;
		$(this).css({"left" : startLeft});
	});
	
	$(document).on('mouseenter', '.blog-image-wrap', function(){
		var $elementWidth = $(this).find('.cathegory-title').width() + 16;
		$(this).find('.extension-on-hover').stop(true).animate({left : $elementWidth}, 250);
	});
	$(document).on('mouseleave', '.blog-image-wrap', function(){
		var startLeft = $(this).find('.cathegory-title').width() - 16;
		$(this).find('.extension-on-hover').stop(true).animate({left : startLeft}, 250);
	});

	$(document).on('show', '.accordion', function (e) {
         $(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('active');
    });

    $(document).on('hide', '.accordion', function (e) {
        $(this).find('.accordion-toggle').not($(e.target)).removeClass('active');
    });
	
		/*  Blog search  */
		var $inpWid = $('input.search-query').closest('form').parent().width();
		$('input.search-query').closest('form').css({width : 94});
		$('input.search-query').css({width : 54});
	$('input.search-query').focusin(function(){
		var $inpWid = $(this).closest('form').parent().width();
		$(this).closest('form').addClass('search-border');
		$(this).closest('form').stop(true).animate({width : $inpWid-5}, 300);
		$(this).stop(true).animate({width : $inpWid - 45}, 300);
	});
	$('input.search-query').focusout(function(){
		$(this).closest('form').removeClass('search-border');
		$(this).closest('form').stop(true).animate({width : 94}, 300);
		$(this).stop(true).animate({width : 54}, 300);
	});
	
	/*  Footer submit  */
	$('.about-footer-txt .form-search input').focusin(function(){
		$(this).closest('form').addClass('search-border');
	});
	$('.about-footer-txt .form-search input').focusout(function(){
		$(this).closest('form').removeClass('search-border');
	});
	
	/*  Shop - Rating  */
	$('.product-rating').each(function(index){
		var $activeRating = $(this).find('a').attr('data-value');
		$(this).find('i').each(function(){
			if($(this).index()+1 <= $activeRating){
				$(this).addClass('rated');
			}
		});
	});
	$('.product-rating a i').click(function(){
		var rateClicked = $(this).index();
		$(this).parent().find('i').removeClass('rated');
		$(this).parent().find('i').each(function(){
			if($(this).index() <= rateClicked){
				$(this).addClass('rated');
			}
		});
	});
	
	$(document).on('click', '.team-members-list li', function(){
		var $blockHeight = $(this).find('.inner-hover-wrap').height();
		var $blockMargin = parseInt($(this).find('.inner-hover-wrap').css("marginTop"));
		var $elementH = $blockHeight + $blockMargin;
		if($(this).find('.hover-wrapper').data('closing') === true) {
			$(this).find('.hover-wrapper').data('closing', false);
		}
		else {
			$(this).parent().find('.opened').find('.team-close').trigger('click').closest('.hover-wrapper').removeClass('opened');
			$(this).find('.hover-wrapper').addClass('opened').stop(true).animate({height : $elementH}, 300);
		}
	});
	$(document).on('click', '.team-close', function(){
		$(this).parent().parent().data('closing', true).stop(true).animate({height : '2px'}, 200);
		$(this).parent().parent().removeClass('opened');
	});
	$(document).on('mouseenter', 'img.stat-team', function(){
		var $bgColor = $(this).attr('data-color');
		$(this).stop(true).animate({backgroundColor : $bgColor}, 300);
		$(this).parent().find('.img-triangle').stop(true).animate({borderLeftColor : $bgColor}, 300);
		$(this).parent().find('.img-triangle').stop(true).animate({borderTopColor : $bgColor}, 300);
	});
	$(document).on('mouseleave', 'img.stat-team', function(){
		$(this).stop(true).animate({backgroundColor : 'transparent'}, 200);
		$(this).parent().find('.img-triangle').stop(true).animate({borderColor : 'transparent'}, 200);
	});
	
	$(document).on('mouseenter' , '.port-item-socials a, .blogpost-socials a, .social-footer a', function(){
		var $bgColor = $(this).attr('data-color');
		$(this).find('.static').stop(true).animate({opacity : 0}, 300);
		$(this).find('.dynamic').stop(true).animate({opacity : 1}, 300);
		$(this).stop(true).animate({backgroundColor : $bgColor}, 300);
		$(this).find('.port-item-soch').stop(true).animate({borderLeftColor : $bgColor, borderTopColor : $bgColor}, 300);
	});
	$(document).on('mouseleave', '.port-item-socials a, .blogpost-socials a, .social-footer a', function(){
		$(this).find('.static').stop(true).animate({opacity : 1}, 300);
		$(this).find('.dynamic').stop(true).animate({opacity : 0}, 300);
		$(this).stop(true).animate({backgroundColor : 'transparent'}, 300);
		$(this).find('.port-item-soch').stop(true).animate({borderColor : 'transparent'}, 300);
	});
	$(document).on('click', 'a.reply-blog-post', function(){
		var $clonedForm = $('#blogpost-form').clone();
		var $formHeight = $('#blogpost-form').outerHeight();
		$clonedForm.css({height : 0});
		if(!$(this).closest('.post-reply-wrap').find('#blogpost-form').is(':animated')){
			if($(this).hasClass('formIn')){
				$(this).closest('.post-reply-wrap').find('#blogpost-form').stop(true).animate({height : 0}, 300,function(){
					$(this).remove();
				});
				$(this).removeClass('formIn');
			}
			else{
				$('.formIn').trigger('click');
				$(this).addClass('formIn');
				$(this).closest('.post-reply-wrap').append($clonedForm).find('#blogpost-form').stop(true).animate({height : $formHeight}, 300);
			}
		}
	});
	

	$(document).on('mouseenter', '.square-portfolio a', function(){
		var $blockHeight = $(this).height();
		$(this).find('.image-text, .image-double').stop(true).animate({bottom : 0}, 300);
		$(this).find('.image-static').stop(true).animate({top : - $blockHeight}, 300);
	});	
		$(document).on('mouseleave', '.square-portfolio a', function(){
		var $textHeight = $(this).find('.image-text').outerHeight();
		$(this).find('.image-double').stop(true).animate({bottom : '-100%'}, 300);
		$(this).find('.image-text').stop(true).animate({bottom : -$textHeight}, 300);
		$(this).find('.image-static').stop(true).animate({top : 0}, 300);
	});	
	
	/*  Shop Hover  */
	
	var $textWrapHeight = $(this).find('.product-details-hover').outerHeight();
	$('.product-details-hover').css({bottom : -$textWrapHeight});
	
	$(document).on('mouseenter', '.single-product-wrapper', function(){
		$(this).find('.static-product-img').stop(true).animate({top : '-100%'}, 300);
		$(this).find('.dynamic-product-img').stop(true).animate({top : 0}, 300);
		$(this).find('.product-details-hover').stop(true).animate({bottom : 0}, 300);
	});
	
	$(document).on('mouseleave', '.single-product-wrapper', function(){
		$(this).find('.static-product-img').stop(true).animate({top : 0}, 300);
		$(this).find('.dynamic-product-img').stop(true).animate({top : '100%'}, 300);
		$(this).find('.product-details-hover').stop(true).animate({bottom : -$textWrapHeight}, 300);
	});


//					SLIDING TABS


	$('.sliding_tabs_wrapper.fullwidth_sliding_tabs').each(function(index){
		$(this).find('.sliding_tabs_content_wrapper').parent().append('<div class="sliding_tabs_target'+index+'"></div>');
		$(this).attr('data-index', index);
	}); 
		
	
	$(document).on('click', '.sliding_tabs_nav li a', function(e){
		e.preventDefault();
		

		if(!$(this).closest('li').hasClass('active')) {
			$(this).closest('.sliding_tabs_wrapper').find('.clone').animate({opacity :0}, 300, function(){$(this).remove();});
			
			$(this).closest('.sliding_tabs_nav').find('li').removeClass('active');
			$(this).closest('li').addClass('active');
			window.slidingTabsAnchor = $(this).attr('href');
			window.slidingTabsWidth = $(this).closest('.sliding_tabs_wrapper').find('.sliding_tabs_content_wrapper').width();
			
			
			$(this).closest('.sliding_tabs_wrapper').find('.sliding_tabs_content_wrapper').find('.sliding_tabs_visible').stop().animate({'left': -window.slidingTabsWidth}, 600, function(){
				$(this).removeClass('sliding_tabs_visible');
				$(this).closest('.sliding_tabs_content_wrapper').find(window.slidingTabsAnchor).addClass('sliding_tabs_visible');
				$(this).hide();	
			});
			$(this).closest('.sliding_tabs_wrapper').find(window.slidingTabsAnchor).addClass('sliding_tabs_visible');		
			$(this).closest('.sliding_tabs_wrapper').find('.sliding_tabs_content_wrapper').find(window.slidingTabsAnchor).css({'left' : window.slidingTabsWidth}).show().stop().animate({'left':0}, 600);

			if ($(this).closest('.sliding_tabs_wrapper').hasClass('fullwidth_sliding_tabs')){
				
				var fstSelectL = $(this).closest('li').index()-1;
				var fstSelectR = $(this).closest('li').index()+1;
				if(fstSelectL < 0) {fstSelectL = $(this).closest('.sliding_tabs_nav').find('li').length-1; }
				if(fstSelectR>$(this).closest('.sliding_tabs_nav').find('li').length-1) {fstSelectR = 0;}

				$this = $(this);
				
								
					var fstsel = $(this).closest('.sliding_tabs_wrapper');
					var fsttar = $(this).find('.sliding_tabs_target');
					var fstclone = fstsel.find('.sliding_tabs_content').eq(fstSelectL).clone().addClass('clone left_clone').insertAfter('.sliding_tabs_target'+fstsel.attr('data-index')).show().css({opacity :0,left:'-100%'}).show().stop(true).delay(400).animate({opacity: 0.4}, 600);
			
				
				
				
					var fstsel = $(this).closest('.sliding_tabs_wrapper');
					var fstclone = fstsel.find('.sliding_tabs_content').eq(fstSelectR).clone().addClass('clone right_clone').insertAfter('.sliding_tabs_target'+fstsel.attr('data-index')).show().css({opacity :0,left:'100%'}).show().stop(true).delay(400).animate({opacity: 0.4}, 600);
				
			}
		}
		
	});
	
	
	$('.sliding_tabs_nav').each(function(){	
		$(this).find('li a:first').trigger('click');
	});
	
//				blog posts 



$('.blog_main_image_wrapper').each(function(index){
		$(this).closest('.blog-post').attr({'id' : 'b-postid'+index});
		$(this).closest('.blog-post').find('.blog_opening_trigger').attr({'href' : '#b-postid'+index});
		
		
	});
$('.blog-initial-content-wrapper').each(function(){
	window.iterationCounter = 0;
	while (window.iterationCounter < $('.blog-initial-content-wrapper').find('.blog-post').length) {
		window.itemInitialArray[window.iterationCounter] = $(this).find('#b-postid'+window.iterationCounter).clone(true, true);
		window.iterationCounter++;
	}
	});



/*  CART  */

$(document).ready(function(){
	$('.cart_arr_up, .cart_arr_down').click(function(e){
		e.preventDefault();
		var value = parseInt($(this).parent().find('input').val());
		if($(this).hasClass('cart_arr_up')){
			value++;
			$(this).parent().find('input').val(value);
		}
		else{
			if(value>0){
				value--;
				$(this).parent().find('input').val(value);
			}
		}
	});
});
	
	
itemInitialSorting();

//		categories
	
	$('.blog-category-select').find('a').on('click', function(e){
		e.preventDefault();
		if(!$(this).parent().hasClass('active')){
			itemCategorization($(this));
			$(this).closest('.blog-category-select').children('li').removeClass('active');
			$(this).parent().addClass('active');
		}

	});


//		opening

	$(document).on('click', '.blog_content_opening_trigger', function(e){
		e.preventDefault();
		
		if (!$(this).hasClass('clicked')) {
			$(this).closest('.blog-post').find('.extension-on-hover i').removeClass('icon-arrow-down').addClass('icon-arrow-up');
			anchorToTarget($(this).closest('.blog-post').find('.blog_opening_trigger'));
			window.blogPostOpeningImgDataHeight = $(this).closest('.blog-post').find('.blog_main_image_wrapper img').height();
			$(this).closest('.blog-post').find('.blog_main_image_wrapper').stop(true).animate({height:window.blogPostOpeningImgDataHeight}, 300).find('img').stop(true).animate({top: 0}, 300);
			$(this).closest('.blog-post').find('.blog_main_content_wrapper').each(function(){
				window.blogPostOpeningContentHeight = $(this).children('div').height();
				$(this).children('div').stop(true).animate({opacity :1}, 150);
				$(this).stop(true).animate({height: window.blogPostOpeningContentHeight}, 300);
			});
			
			window.blogPostOpeningReadMore = $(this).children('span.read-more-text').html();
			$(this).attr('data-text', window.blogPostOpeningReadMore);
			$(this).html($(this).attr('data-alttext')+'<span class="read-more-plus">-</span>').addClass('clicked');
		
		} else {
			$(this).closest('.blog-post').find('.extension-on-hover i').removeClass('icon-arrow-up').addClass('icon-arrow-down');
			$(this).closest('.blog_initial_content_wrapper').find('.blog_main_image_wrapper img').each(function(){
				window.blogPostOpeningImgDataHeight = $(this).attr('data-height2');
			});
			
			$(this).closest('.blog-post').find('.blog_main_image_wrapper').stop(true).animate({height: 150}, 300);
			$(this).closest('.blog-post').find('.blog_main_image_wrapper img').stop(true).animate({top: -window.blogPostOpeningImgDataHeight/4}, 300);
			$(this).closest('.blog-post').find('.blog_main_content_wrapper').each(function(){
				window.blogPostOpeningContentHeight = $(this).children('div').height();
				$(this).children('div').stop(true).animate({opacity :0}, 150);
				$(this).stop(true).animate({height: 0}, 300);
			});
			
			window.blogPostOpeningReadMore =($(this).attr('data-text')+'<span class="read-more-plus">+</span>');
			$(this).html(window.blogPostOpeningReadMore).removeClass('clicked');
		}
		
	});
	
	$(document).on('click', '.blog_opening_trigger', function(e){
		e.preventDefault();
		$(this).closest('.blog-post').find('.blog_content_opening_trigger').trigger('click');
		});
	
//		post comment
	
	$(document).on('click', '.blog_content_form_trigger', function(e) {
		e.preventDefault();		
		if(!$(this).hasClass('opened')) {
		$(this).closest('.blog-post').find('.blog_content_comment_form').stop().animate({height : $(this).closest('.blog-post').find('.blog_content_comment_form > form').css('height'), 'margin-bottom': 24, opacity: 1}, 500);
		$(this).addClass('opened');
		} else {
			$(this).closest('.blog-post').find('.blog_content_comment_form').stop().animate({height : 0, 'margin-bottom': 0, opacity: 0}, 500);
			$(this).removeClass('opened');
		}
		
	});
//		 comments opening
	
	$(document).on('click', '.blog_content_comments_trigger', function(e) {
		e.preventDefault();	
		if(!$(this).hasClass('opened')) {	
		$(this).closest('.blog-post').find('.blog_content_comments').stop().animate({height : $(this).closest('.blog-post').find('.blog_content_comments > div').css('height'), opacity: 1}, 500);
		$(this).closest('.blog-post').find('.blog_content_comment_form').stop().animate({height :$(this).closest('.blog-post').find('.blog_content_comment_form > form').css('height'), 'margin-bottom': 24, opacity: 1}, 500);
		$(this).closest('.blog-post').find('.blog_content_form_trigger').addClass('opened');
		$(this).addClass('opened');
		} else {
			$(this).closest('.blog-post').find('.blog_content_comments').stop().animate({height : 0, opacity: 0}, 500);
			$(this).removeClass('opened');
			$(this).closest('.blog-post').find('.blog_content_comment_form').stop().animate({height : 0, 'margin-bottom': 0, opacity: 0}, 500);
			$(this).closest('.blog-post').find('.blog_content_form_trigger').removeClass('opened');
		}
	});	
	
//		list type switch 
	$('.grid-layout-select').find('a').eq(1).on('click', function(e){
		e.preventDefault();
		
		$('.blog_content_opening_trigger').each(function(){
			if($(this).hasClass('clicked')) {
				$(this).trigger('click');
			}
		});
			if(!$(this).parent().hasClass('active')) {
				$('.blog-category-select').children('li').removeClass('active');
				$('.blog-category-select').children('li:first').addClass('active');
				$('.column_item_category_sorter_content').animate({opacity : 0}, 300, function(){
					itemInitialUnsorting();
					
				$(this).find('.blog_main_image_wrapper img').each(function(){
					var imgHeight = parseInt($(this).height())/2- parseInt($(this).parent().height())/2;
					$(this).css({top : -imgHeight}).attr('data-height', $(this).height());
				});
					$('.blog-initial-content-wrapper').animate({opacity : 1}, 300);
				});
				
				$(this).parent().parent().find('li').removeClass('active');
				$(this).parent().addClass('active');
			}

		
	});
	
		
			
			
	$('.grid-layout-select').find('a').eq(0).on('click', function(e){
		e.preventDefault();
		if(!$(this).parent().hasClass('active')) {
			$('.blog-category-select').children('li').removeClass('active');
			$('.blog-category-select').children('li:first').addClass('active');
			$('.blog-initial-content-wrapper').animate({opacity : 0}, 300, function(){
				itemInitialSorting();
				$(this).find('.blog_main_image_wrapper img').each(function(){
					var imgHeight = parseInt($(this).height())/2- parseInt($(this).parent().height())/2;
					$(this).css({top : -imgHeight}).attr('data-height', $(this).height());
				});
				$('.column_item_category_sorter_content').animate({opacity : 1}, 300);
			});
			
			$(this).parent().parent().find('li').removeClass('active');
			$(this).parent().addClass('active');
		}
	});
	
	

	
	
});



$(window).load(function(){

//				sliding tabs
	$('.sliding_tabs_wrapper').each(function() {
		var $this = $(this);
		window.slidingTabsHeight = 0;
		$(this).find('.sliding_tabs_content_wrapper > ul').each(function(){
			$(this).children('li').each(function(ind){
				if($(this).height() > window.slidingTabsHeight) {
				window.slidingTabsHeight = $(this).height();
				}
				$(this).css('margin-bottom','20px');
				if($this.hasClass('multicolumn_responsive') && ind==1 && $(window).width() <= 991){	
					$('<li class="clearfix" style="float:none !important;"></li>').insertAfter($(this));		
				}
				
				if($this.hasClass('multicolumn_responsive') && $(window).width() <= 530){	
					$('<li class="clearfix" style="float:none !important;"></li>').insertAfter($(this));		
				}
			});
			
		});

		
		
		$(this).find('.sliding_tabs_content_wrapper').css({height : window.slidingTabsHeight+2});
		$(this).find('.sliding_tabs_content').css({height : window.slidingTabsHeight+2});
		if ($(window).width() <= 991 && $this.hasClass('multicolumn_responsive')){
			$(this).find('.sliding_tabs_content_wrapper').css({height : (window.slidingTabsHeight+2)*2});
			$(this).find('.sliding_tabs_content').css({height : (window.slidingTabsHeight+2)*2});
		}
		if ($(window).width() <= 530 && $this.hasClass('multicolumn_responsive')){
			$(this).find('.sliding_tabs_content_wrapper').css({height : (window.slidingTabsHeight+2)*4});
			$(this).find('.sliding_tabs_content_wrapper > ul').children('li').css({width: '100%'});
			$(this).find('.sliding_tabs_content').css({height : (window.slidingTabsHeight+2)*4});
		}
		
	});
		
//			blog posts opening
	
	$('.blog_main_image_wrapper').each(function(index){
		var imgHeight = parseInt($(this).find('img').height())/2- parseInt($(this).height())/2;
		$(this).find('img').css({top : -imgHeight}).attr('data-height', $(this).find('img').height());
		$(this).closest('.blog-post').css('height', 'auto');
		
	});
	$('.blog-category-select').find('a').each(function(index){
		$this = $(this);
		$(this).closest('.column_item_category_sorter').find('.column_item_category_sorter_content .blog-post').each(function(){
			window.itemCatContentString[index] = $(this).clone(true, true);
			if ($(this).attr('data-category').indexOf($this.attr('data-value')) >= 0 || $this.attr('data-value') == 'all'){
				if (window.itemCatContentSelect[index] == undefined){
					window.itemCatContentSelect[index] = '';
				}
				
				if (window.itemCatContentSelect[index] == '') {
					window.itemCatContentSelect[index] += ($(this).attr('id'));
				} else {
				window.itemCatContentSelect[index] += (',' + $(this).attr('id'));
				}
			}
			
		});
	});
	
	
	$('.grid-layout-select').children('li').each(function(){
		if($(this).hasClass('default')) {
			$(this).find('a').trigger('click');
		}
});
	
	
});

$(window).resize(function(){
	$('.blog_main_image_wrapper').each(function(index){
		var imgHeight = parseInt($(this).find('img').height())/2- parseInt($(this).height())/2;
		$(this).find('img').css({top : -imgHeight}).attr('data-height', $(this).find('img').height());	
	});
});




function anchorToTarget(zeroAttTarget) {
	$(zeroAttTarget).each(function () {
		    var target = this.hash,	
		    $target = $(target);
		    $('html, body').stop().animate({
		        'scrollTop': $target.offset().top - 60
		    }, 500, 'swing', function () {
		    });
	
		});
		
}


function itemCategorization(catThis) {
	window.itemCatFliteredUnsorted = [];
	window.itemCatFlitered = [];
	window.itemCatFliteredUnsorted = window.itemCatContentSelect[catThis.parent().index()].split(',');
	window.iterationCounter = 0;
		while (window.iterationCounter < window.itemCatFliteredUnsorted.length) {
			window.itemCatFliteredUnsorted[window.iterationCounter] = parseInt(window.itemCatFliteredUnsorted[window.iterationCounter].substr(8));
			window.iterationCounter++;
		}
		window.itemCatFliteredUnsorted.sort(function(a,b){return a-b});
		window.iterationCounter = 0;
		while (window.iterationCounter < window.itemCatFliteredUnsorted.length) {
			window.itemCatFlitered[window.iterationCounter] = 'b-postid'+window.itemCatFliteredUnsorted[window.iterationCounter];
			window.iterationCounter++;
		}
		
		

	if($('.blog-initial-content-wrapper').length > 0) {
			itemSorting(catThis);	
	} else {
		if($('.column_item_category_sorter_content').length > 0) {
			$('.column_item_category_sorter_content').each(function(){
				itemSorting(catThis);		
			});
		}
	}
		
	
}

function itemSorting(refThis) {
	if($('.blog-initial-content-wrapper').length > 0) {
		
	refThis.closest('.column_item_category_sorter').find('.blog-initial-content-wrapper .blog-post').hide().parent().remove();
		
			window.iterationCounter = 0;
		
		
			while (window.itemCatFlitered[window.iterationCounter] != undefined) {

			$('.blog-initial-content-wrapper').append('<li></li>').find('li:last').html(window.itemInitialArray[parseInt(window.itemCatFlitered[window.iterationCounter].substr(8))].clone()).find('.blog-post').css({opacity : 0, 'display' : 'block'}).delay(300+window.iterationCounter*100).animate({opacity : 1}, 300);
			
			
			window.iterationCounter++;
		}
			
		
		
		
	} else {
		if($('.column_item_category_sorter_content').length > 0) {
			
			refThis.closest('.column_item_category_sorter').find('.column_item_category_sorter_content .blog-post').hide().remove();

				window.iterationCounter = 0;
				while (window.itemCatFlitered[window.iterationCounter] != undefined) {
					$('.column_item_category_sorter_content').children('li').eq(window.columnSelectVar).each(function(){
						$(this).append(window.itemInitialArray[parseInt(window.itemCatFlitered[window.iterationCounter].substr(8))].clone()).find('.blog-post').css({opacity : 0, 'display' : 'block'}).delay(300+window.iterationCounter*100).animate({opacity : 1}, 300);
			
					});
					if (window.columnSelectVar == 0){
						window.columnSelectVar++;
					} else {
						window.columnSelectVar--;
					}
					window.iterationCounter++;	
					
					
				}
			
			
		}
	}
	
	
	
	
}

function itemInitialSorting() {
	
	

	$('.blog-initial-content-wrapper').addClass('q-sand-blog-target column_item_category_sorter_content').html('<li></li><li></li>').removeClass('blog-initial-content-wrapper');
		window.iterationCounter = 0;
	while (window.itemInitialArray[window.iterationCounter] != undefined) {
		$('.column_item_category_sorter_content').children('li').eq(window.columnSelectVar).each(function(){
			$(this).append(window.itemInitialArray[window.iterationCounter]).find('.blog-post').css({opacity : 0, 'display' : 'block'}).delay(window.iterationCounter*50).animate({opacity : 1}, 300);
		});
		if (window.columnSelectVar == 0){
			window.columnSelectVar++;
		} else {
			window.columnSelectVar--;
		}
		window.iterationCounter++;
		
	}

	
}

function itemInitialUnsorting() {
	$('.column_item_category_sorter_content').addClass('blog-initial-content-wrapper').removeClass('q-sand-blog-target column_item_category_sorter_content').html(' ');
	
	
	
		window.iterationCounter = 0;
		$('.blog-initial-content-wrapper').each(function(){
			while (window.itemInitialArray[window.iterationCounter] != undefined) {
			$(this).append('<li></li>').find('li:last').append(window.itemInitialArray[window.iterationCounter]).find('.blog-post').css({opacity : 0, 'display' : 'block'}).delay(window.iterationCounter*50).animate({opacity : 1}, 300);
			
			window.iterationCounter++;
			
			}
			
		
		});
		
		

}

})(jQuery);