(function($){
	var $flagSelect = false;
	var $flagResize = false;
	$(document).ready(function(){
		var windWidth = $(window).width();
		if(windWidth < 400){
			$flagResize = true;
		}
		else{
			$flagResize = false;
		}
		blogSelectResize();
	});
	$(window).resize(function(){
		var windWidth = $(window).width();
		if(windWidth < 400){
			$flagResize = true;
		}
		else{
			$flagResize = false;
		}
		blogSelectResize();
	});
	function blogSelectResize(){
		if($flagResize == true){
			var $selectMenu = $('.blog-category-select.home-page-qsand-select').clone() .addClass('cloned-select');
			if(!$('.blog-category-select.home-page-qsand-select').parent().hasClass('relative')){
				$('.blog-category-select.home-page-qsand-select').css({"display" : "none"}).addClass('original');
				$('.blog-category-select.home-page-qsand-select').parent().css({"padding-top" : 75}).prepend('<span class="selection-wrap"><span class="now-selected">Error</span></span>');
				$('.selection-wrap').append($selectMenu);
				$('.blog-category-select.home-page-qsand-select').parent().addClass('relative');
				blogSelectHover();
			}
		}
		if($flagResize == false){
			$('.blog-category-select.home-page-qsand-select').parent().css({"padding-top" : 0}).removeClass('relative');
			$('.selection-wrap').remove();
			$('.blog-category-select.home-page-qsand-select').css({"display" : "block"});
		}
	}
	function blogSelectHover(){
		var $liHeight = $('.cloned-select li').outerHeight();
		var $liLength = $('.cloned-select li').parent().find('li').length;
		var $ulHeight = $liHeight * $liLength;
		$('.cloned-select').css({"height" : 0});
		$('.selection-wrap').hover(function(){
			$(this).find('.cloned-select').stop(true).animate({height : $ulHeight},300);
		},function(){
			$(this).find('.cloned-select').stop(true).animate({height : 0},300);
		});
		var $selectedFilter = $('.cloned-select li.active').html();
		$('.now-selected').html($selectedFilter);
		$('.cloned-select li').each(function(index){
			$(this).click(function(e){
				e.preventDefault();
				var $selectedFilter = $(this).html();
				$('.original li a').eq(index).trigger('click');
				$(this).parent().find('.active').removeClass('active');
				$(this).addClass('active');
				$(this).closest('.selection-wrap').find('.now-selected').html($selectedFilter);
			});
		});
	}
})(jQuery);