(function($){
var $flagDrag = false;
var $dragStart;
var $flagReady = false;
$(document).ready(function(){
	var windWidth = $(window).width();
	if(windWidth < 975){
		$flagReady = true;
	}
	else{
		$flagReady = false;
	}
	swipe_screen();
});
$(window).resize(function(){
	var windWidth = $(window).width();
	if(windWidth < 975){
		$flagReady = true;
	}
	else{
		$flagReady = false;
	}
	swipe_screen();
});
function swipe_screen(){
	if($flagReady === true){
		/*  responsive-menu opening - closing  */
		$('.menu-trigger i').click(function(){
			var $contentWidth = $(this).parent().parent().width();
			$(this).parent().parent().css({width : $contentWidth});
			$(this).parent().parent().find('.responsive-nav').stop(true).animate({left : 0}, 200);
			$(this).parent().parent().animate({marginLeft : 230}, 200);
		});
		$('.menu-closing i').click(function(){
			$(this).closest('.responsive-nav').stop(true).animate({left : -230}, 200);
			$(this).closest('.responsive-nav').parent().animate({marginLeft : 0}, 200, function(){
				$(this).css({width : 'auto'});
			});
		});
	   $('body').on('mousedown', function(event){
	   	if($flagReady === true){
		    if($flagDrag !== true) 
		     $dragStart = event.pageX;
		    $flagDrag = true;
		}
	   });
	   $('body').on('mouseup', function(event){
	   	if($flagReady === true){
		    event.preventDefault();
		    if($flagDrag === true){
		     var $dragMeasure = event.pageX;
		     var $dragResult = $dragMeasure - $dragStart;
		      if($dragResult > 200){
				var $contentWidth = $(this).parent().width();
				$(this).find('.responsive-nav').parent().css({width : $contentWidth});
				$(this).find('.responsive-nav').stop(true).animate({left : 0}, 200);
				$(this).find('.responsive-nav').parent().animate({marginLeft : 230}, 200);
				$dragStart = $dragMeasure;
		      }
		      if($dragResult < (-200)){
				$(this).find('.responsive-nav').stop(true).animate({left : -230}, 200);
				$(this).find('.responsive-nav').parent().animate({marginLeft : 0}, 200, function(){
					$(this).css({width : 'auto'});
				});
		        $dragStart = $dragMeasure;
		      }
		     }
		}
	    $flagDrag = false;
	   });
	}
}
})(jQuery);