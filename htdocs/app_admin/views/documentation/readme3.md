Kobaltis
=====

## Présentation

Ce document vise à vous aider dans l'utilisation de votre site web. Nous esperons qu'il répondra à toutes vos attentes, si toutes fois vous ne trouviez pas l'information qu'il vous faut. N'hésitez pas à contacter votre agence.


## Page d'accueil


## Page

###Supprimer une page

Cliquez sur l'onglet "page" à gauche

Séléctionnez la page parente de la page à supprimer

Dans la partie centrale, cliquez sur la poubelle, puis confirmez.

Par mesure de sécurité et pour éviter les suppressions massive, vous ne pouvez supprimer une page qui en contient d'autre.


###Modifier l'ordre des pages

Cliquez sur l'onglet "page" à gauche

Séléctionnez la page parente des pages dont il faut changer l'ordre.

Glissez / déposez les pages dans la partie centrale pour changer l'ordre



###Associer une icône à une rubrique

Cliquez sur l'onglet "page" à gauche

Séléctionnez une page enfant directe d'une des pages Lycées, pastoral, primaire, ...

Dans l'onglet "info techniques", collez le nom de l'icone souhaitée.


###Dans la gestion des menus, à quel endroit intégrer les images ou vidéos ?

Sur une page dans la partie droite, en bas, il y a un  bouton vignette vous permettant de séléctionner l'image ou la video souhaitée.


###Insérer un formulaire dans le contenu d'une page :

Rendez vous dans la rubrique formulaire, puis sur le formualire en question, indiquez la page parente.


## Actualité

## Evènements



**Astuce : **
Les évènements dont la date de début et de fin sont suppérieur au jour en cours, apparaissent automatiquement dans le menu du site.


## Le cms

### Insérer des fichiers

Vos fichiers peuvent être déposés directement dans la bibliothèque par glissé dépoé quand ils pèsent moins de 30Mo. Dans le cas contraire, vous pouvez utiliser le système de synchronisation des gros fichiers (voir plus bas).

#### Insérer une image

#### Insérer une vidéo

Lors de l'edition d'une page, cliquez sur le l'onglet video.

Dans le bloc video, vous avez 3 possibilités d'ajout :

- En y insérant un lien, youtube : Copiez / collez tout simplement le lien youtube dans le champ adequat

- En y insérant un code dit "embed" : Copiez / collez tout simplement le lien embed dans le champ adequat

- En utilisant la bibliothèque : Cliquez sur le bouton "ajouter une vidéo" puis séléctionnez la video dans la bibliothèque. Attention pour avoir la certitude que la vidéo soit lu par tous les navigateur, la video devra être déposée au format mp4, ogv et webm. En choisissant le format mp4, les autres format seront automatiquement jointes au lecteur.



**Astuce :**

Pour ajouter une video sur l'acueil "video tour", créez un article, sur la droite choisissez type "video", cochez "pousser sur l'accueil".

Ajoutez votre vidéo avec la méthode de votre choix.

N'oubliez pas d'associer une vignette à la vidéo.


#### Insérer un document


#### Dépôt de fichier volumineux

Déposez vos fichiers volumineux par ftp sur dans le dossier "deposit" à la racine du projet web.

Dans l'administration Cliquez à gauche sur l'onglet "medias" :

![Boutons import](app_admin/assets/img/documentation/import.png)

Séléctionnez le dossier dans lequel vous souhaitez effectuer votre synchronisation, puis cliquez sur le bouton vert en haut à droite de l'interface.

Vos fichier appraitront alors au centre et peuvent être ajouté à votre site web.


