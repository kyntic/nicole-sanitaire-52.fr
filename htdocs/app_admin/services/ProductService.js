App.service('Product', function ($http, $q, $log, Module) {

    this.parametres = {

        urls: {
            index           :   '/admin/catalogue/products/index/',
            view            :   '/admin/catalogue/products/view/',
            edit            :   '/admin/catalogue/products/edit/',
            delete          :   '/admin/catalogue/products/delete/',
            duplique        :   '/admin/catalogue/products/duplique/',
            ordre           :   '/admin/catalogue/products/ordre/'
        }

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.findAll = function (id) {

        var _this = this, deffered = $q.defer();

        url = this.parametres.urls.index;

        if (angular.isDefined(id)) {
            url += id;
        }

        $http.get(url)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.getById = function (id) {

        var _this = this, deffered = $q.defer();

        $http.get(this.parametres.urls.view + id)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.save = function (data) {

        var _this = this, deffered = $q.defer();

        url = this.parametres.urls.edit;

        if (angular.isDefined(data.Product.id)) {
            url += data.Product.id;
        }

        $http.post(url, data)
            .then(function(response) {

                deffered.resolve(response.data);

            }, function(x) {

                deffered.reject(x);
            }
        );

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.delete = function (id) {

        var _this = this, deffered = $q.defer();

        $http.get(this.parametres.urls.delete + id)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.duplique = function (id) {

        var _this = this, deffered = $q.defer();

        $http.get(this.parametres.urls.duplique + id)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Enregistre l'ordre 
     */
    this.ordre = function (data) {
        console.log(data);

        var _this = this, deffered = $q.defer();

        $http.post(this.parametres.urls.ordre, {data:data})
            .then(function(response) {

                deffered.resolve(response.data);

            }, function(x) {

                deffered.reject(x);
            }
        );

        return deffered.promise;

    };


});