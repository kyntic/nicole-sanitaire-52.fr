App.service('AttributeFamily', function ($http, $q, $log, Module) {

    this.parametres = {

        urls: {
            index           :   '/admin/catalogue/attribute_families/index/',
            tree            :   '/admin/catalogue/attribute_families/tree/',
            getAssociables  :   '/admin/catalogue/attribute_families/getAssociables/',
            view            :   '/admin/catalogue/attribute_families/view/',
            edit            :   '/admin/catalogue/attribute_families/edit/',
            delete          :   '/admin/catalogue/attribute_families/delete/',
            ordre           :   '/admin/catalogue/attribute_families/ordre/',
            getOptions      :   '/admin/catalogue/attribute_families/getOptions/',
        }

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.findAll = function (id) {

        var _this = this, deffered = $q.defer();

        url = this.parametres.urls.index;

        if (angular.isDefined(id)) {
            url += id;
        }

        $http.get(url)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut qui ont des attributs
     */
    this.getAssociables = function () {

        var _this = this, deffered = $q.defer();

        $http.get(this.parametres.urls.getAssociables)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.findTree = function () {

        var _this = this, deffered = $q.defer();

        $http.get(this.parametres.urls.tree)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.getById = function (id) {

        var _this = this, deffered = $q.defer();

        $http.get(this.parametres.urls.view + id)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.save = function (data) {

        var _this = this, deffered = $q.defer();

        url = this.parametres.urls.edit;

        if (angular.isDefined(data.AttributeFamily.id)) {
            url += data.AttributeFamily.id;
        }

        $http.post(url, data)
            .then(function(response) {

                deffered.resolve(response.data);

            }, function(x) {

                deffered.reject(x);
            }
        );

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.delete = function (id) {

        var _this = this, deffered = $q.defer();

        $http.get(this.parametres.urls.delete + id)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Enregistre l'ordre des familles d'attribut
     */
    this.ordre = function (data) {
        console.log(data);

        var _this = this, deffered = $q.defer();

        $http.post(this.parametres.urls.ordre, {data:data})
            .then(function(response) {

                deffered.resolve(response.data);

            }, function(x) {

                deffered.reject(x);
            }
        );

        return deffered.promise;

    };

    /**
     * Retourne les options pour un select de famille d'attribut
     */
    this.getOptions = function () {

        var _this = this, deffered = $q.defer();

        $http.get(this.parametres.urls.getOptions)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

});