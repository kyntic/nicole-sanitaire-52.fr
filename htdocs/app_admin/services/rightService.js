/**
 * Ldap Service | Get all the groups/users
 */
App.service('Right', function ($http, $q) {

    this.load       = false;
    this.ldapData   = [];

    /**
     * Get all the users/groups in LDAP
     */
    this.getAll = function ($modelId) {
        var d       = $q.defer();
        var _this   = this;

        $http.get(WH_ROOT + '/admin/ldap/rights/get_all/page/' + $modelId)
            .success(function (data) {

                /**
                 * Assign the data
                 */
                _this.ldapData = data;

                d.resolve(_this.ldapData);
            })
            .error(function (e) {

                /**
                 * Unhandled error | Return message
                 */
                d.reject(e);
            });

        return d.promise;
    };

    /**
     * Add a group
     *
     * @returns {*}
     */
    this.addGroup = function(data) {

        var deferred    = $q.defer();
        var _this       = this;

        $http.post(WH_ROOT + '/admin/ldap/rights/add_group', data)
            .then(function(response) {

                _this.loaded = false;

                deferred.resolve(response.data);

            }, function (x) {

                deferred.reject(x);
            }
        );

        return deferred.promise;
    };

    /**
     * Edit the rights
     *
     * @returns {*}
     */
    this.editRights = function() {

        var deferred    = $q.defer();
        var _this       = this;

        $http.post(WH_ROOT + '/admin/ldap/rights/add_group', data)

            .then(function(response) {

                _this.loaded = false;

                deferred.resolve(response.data);

            }, function (x) {

                deferred.reject(x);
            }
        );

        return deferred.promise;
    };
});