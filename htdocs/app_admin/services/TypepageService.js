App.service('Typepage', function ($http, $q, $log, Module) {    
    this.parametres = {

        urls: {
            index           :   '/admin/typepages/index/',
            view            :   '/admin/typepages/view/',
            liste           :   '/admin/typepages/liste',
            edit            :   '/admin/typepages/edit/',
            delete          :   '/admin/typepages/delete/'
        }

    };

    /**
     * Récuéper un tableau à deux dimenssions :
     */
    this.getList = function () {

        var _this = this, deffered = $q.defer();

        /**
         * Get the list from server
         */
        $http.get(this.parametres.urls.liste + '/')
            .success(function (response) {

                _this.liste = response.data;
                _this.loaded = true;

                deffered.resolve(_this.liste);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;
    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.findAll = function (id) {

        var _this = this, deffered = $q.defer();

        url = this.parametres.urls.index;

        if (angular.isDefined(id)) {
            url += id;
        }

        $http.get(url)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.getById = function (id) {

        var _this = this, deffered = $q.defer();

        $http.get(this.parametres.urls.view + id)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.save = function (data) {

        var _this = this, deffered = $q.defer();

        url = this.parametres.urls.edit;

        if (angular.isDefined(data.Typepage.id)) {
            url += data.Typepage.id;
        }

        $http.post(url, data)
            .then(function(response) {

                deffered.resolve(response.data);

            }, function(x) {

                deffered.reject(x);
            }
        );

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.delete = function (id) {

        var _this = this, deffered = $q.defer();

        $http.get(this.parametres.urls.delete + id)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };
});
