App.service('Attribute', function ($http, $q, $log, Module) {

    this.parametres = {

        urls: {
            index               :   '/admin/catalogue/attributes/index/',
            view                :   '/admin/catalogue/attributes/view/',
            edit                :   '/admin/catalogue/attributes/edit/',
            delete              :   '/admin/catalogue/attributes/delete/',
            ordre               :   '/admin/catalogue/attributes/ordre/',
            getOptions          :   '/admin/catalogue/attributes/getOptions/',
            getOptionsMultiple  :   '/admin/catalogue/attributes/getOptionsMultiple/',
        }

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.findAll = function (attribute_family_id) {

        var _this = this, deffered = $q.defer();

        url = this.parametres.urls.index;

        if (angular.isDefined(attribute_family_id)) {
            url += attribute_family_id;
        }

        $http.get(url)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.getById = function (id) {

        var _this = this, deffered = $q.defer();

        $http.get(this.parametres.urls.view + id)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.save = function (data) {

        var _this = this, deffered = $q.defer();

        url = this.parametres.urls.edit;

        if (angular.isDefined(data.Attribute.id)) {
            url += data.Attribute.id;
        }

        $http.post(url, data)
            .then(function(response) {

                deffered.resolve(response.data);

            }, function(x) {

                deffered.reject(x);
            }
        );

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.delete = function (id) {

        var _this = this, deffered = $q.defer();

        $http.get(this.parametres.urls.delete + id)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Enregistre l'ordre des familles d'attribut
     */
    this.ordre = function (data) {
        console.log(data);

        var _this = this, deffered = $q.defer();

        $http.post(this.parametres.urls.ordre, {data:data})
            .then(function(response) {

                deffered.resolve(response.data);

            }, function(x) {

                deffered.reject(x);
            }
        );

        return deffered.promise;

    };

    /**
     * Retourne les options pour un select de famille d'attribut
     */
    this.getOptions = function () {

        var _this = this, deffered = $q.defer();

        $http.get(this.parametres.urls.getOptions)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Retourne les options pour un select mulitple de famille d'attribut
     */
    this.getOptionsMultiple = function (attribute_family_id) {

        var _this = this, deffered = $q.defer();

        url = this.parametres.urls.getOptionsMultiple;

        if (angular.isDefined(attribute_family_id)) {
            url += attribute_family_id;
        }

        $http.get(url)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

});