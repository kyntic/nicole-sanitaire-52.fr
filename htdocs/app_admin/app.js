/*!
 * 
 * Angle - Bootstrap Admin App + AngularJS
 * 
 * Author: @themicon_co
 * Website: http://themicon.co
 * License: http://support.wrapbootstrap.com/knowledge_base/topics/usage-licenses
 * 
 */



if (typeof $ === 'undefined') { throw new Error('This application\'s JavaScript requires jQuery'); }

// APP START
// ----------------------------------- 
var App = angular.module('myAppAdmin', ['ngRoute', 'ngAnimate', 'ngStorage', 'ngCookies', 'ngSanitize', 'pascalprecht.translate', 'ui.bootstrap', 'ui.router', 'oc.lazyLoad', 'cfp.loadingBar', 'AuthServices', 'checklist-model', 'ui.sortable', 'ui.tree', 'ui.select'])
    .run(["$rootScope", "$state", "$stateParams",  '$window', '$templateCache', '$log', '$location', 'UserService', function ($rootScope, $state, $stateParams, $window, $templateCache, $log, $location, UserService) {
        // Set reference to access them from any scope
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.$storage = $window.localStorage;
        $rootScope.WH_ROOT = WH_ROOT;


        // Scope Globals
        // -----------------------------------
        $rootScope.app = {
            name        : 'Kobaltis',
            description : 'Administration de votre site web',
            year        : ((new Date()).getFullYear()),
            layout: {
                isFixed: true,
                isCollapsed: false,
                isBoxed: false,
                isRTL: false
            },
            viewAnimation: 'ng-fadeInUp'
        };
        $rootScope.user = {
            name:   localStorage.getItem('User.name'),
            id:     localStorage.getItem('User.id'),
            group_id:  localStorage.getItem('User.group_id')
            //picture:  'app_admin/assets/img/user/02.jpg'
        };

        //Paramètres divers :
        $rootScope._params = params;
        $rootScope._params.Locales = angular.fromJson($rootScope._params.Locales);

        /**
         * Function deconnexion
         */
        $rootScope.logout = function () {

            UserService.logout();
        };

        /**
         * Sécurisation des pages sécurisé
         */
        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

            if (toState.securized && !UserService.currentUser.isLoggedIn && !UserService.connected()) {

                $window.location.href = WH_ROOT + '/admin/users/login';

            } else {
                /**
                 * On fait quoi ici ?
                 */
            }
        });
    }
    ]);




/**=========================================================
 * Module: config.js
 * App routes and resources configuration
 =========================================================*/

App.config(['$stateProvider','$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$ocLazyLoadProvider', 'APP_REQUIRES',
        function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $ocLazyLoadProvider, appRequires) {
            'use strict';

            App.controller = $controllerProvider.register;
            App.directive  = $compileProvider.directive;
            App.filter     = $filterProvider.register;
            App.factory    = $provide.factory;
            App.service    = $provide.service;
            App.constant   = $provide.constant;
            App.value      = $provide.value;

            // LAZY MODULES
            // -----------------------------------

            $ocLazyLoadProvider.config({
                debug: false,
                events: true,
                modules: appRequires.modules
            });


            // defaults to dashboard
            $urlRouterProvider.otherwise('/app/dashboard');


            var racine = '/app_admin/';

            //
            // Application Routes
            // -----------------------------------
            $stateProvider
                .state('app', {
                    url: '/app',
                    abstract: true,
                    templateUrl: basepath('app.html'),
                    controller: 'AppController',
                    resolve: resolveFor('fastclick', 'modernizr', 'icons', 'screenfull', 'animo', 'sparklines', 'slimscroll', 'classyloader', 'toaster', 'csspiner', 'datatables', 'datatables-pugins', 'jquery-ui', 'inputmask', 'chosen'),
                    securized: true
                })
                .state('app.documentation', {
                    url: '/documentation',
                    title: 'Documentation',
                    templateUrl: basepath('documentation/index.html'),
                    controller: 'NullController',
                    resolve: resolveFor('flatdoc')
                })
                .state('app.dashboard', {
                    url: '/dashboard',
                    title: 'Dashboard',
                    templateUrl: basepath('dashboard/index.html'),
                    resolve: resolveFor('flot-chart','flot-chart-plugins'),
                    securized: true
                })
                .state('app.parametres', {
                    url: '/parametres',
                    title: 'Paramètres',
                    templateUrl: basepath('parametres/index.html'),
                    securized: true
                })

                // Media
                // -----------------------------------
                .state('app.files', {
                    url: '/files',
                    title: 'Medias',
                    templateUrl: basepath('file-manager/app.html'),
                    securized: true
                })

                // Actualite
                // -----------------------------------
                .state('app.actualites', {
                    url: '/actualites',
                    title: 'Actualités',
                    templateUrl: basepath('actualites/app.html'),
                    securized: true
                })
                .state('app.actualites.index', {
                    url: '/index',
                    title: 'Actualités',
                    templateUrl: basepath('actualites/index.html'),
                    securized: true
                })
                .state('app.actualites.edit', {
                    url: '/edit/{id:[0-9]*}',
                    title: 'Edition d’une actualité',
                    templateUrl: basepath('actualites/edit.html'),
                    securized: true,
                    resolve: resolveFor('jquery-ui', 'redactor')
                })

                // Formulaires
                // -----------------------------------
                .state('app.formulaires', {
                    url: '/formulaires',
                    title: 'Formulaire',
                    templateUrl: basepath('formulaires/app.html'),
                    securized: true
                })
                .state('app.formulaires.index', {
                    url: '/index',
                    title: 'Formulaire',
                    templateUrl: basepath('formulaires/index.html'),
                    securized: true
                })
                .state('app.formulaires.edit', {
                    url: '/edit/{id:[0-9]*}',
                    title: 'Edition d’un formulaire',
                    templateUrl: basepath('formulaires/edit.html'),
                    securized: true,
                    resolve: resolveFor('jquery-ui', 'redactor')
                })

                // Elements de formulaires
                // -----------------------------------
                .state('app.formulaire_elements', {
                    url: '/formulaire_elements',
                    title: 'Eléments de formulaire',
                    //controller : 'PagesCtrl',
                    templateUrl: basepath('formulaire_elements/app.html'),
                    securized: true
                })
                .state('app.formulaire_elements.index', {
                    url: '/index/:form_id',
                    title: 'Eléments de formulaire',
                    templateUrl: basepath('formulaire_elements/index.html'),
                    securized: true
                })
                .state('app.formulaire_elements.edit', {
                    url: '/edit/{id:[0-9]*}',
                    title: 'Edition d’un formulaire',
                    templateUrl: basepath('formulaire_elements/edit.html'),
                    securized: true,
                    resolve: resolveFor('jquery-ui', 'redactor')
                })

                // Partenaires
                // -----------------------------------
                .state('app.partenaires', {
                    url: '/partenaires',
                    title: 'Partenaires',
                    templateUrl: basepath('partenaires/app.html'),
                    securized: true
                })
                .state('app.partenaires.index', {
                    url: '/index',
                    title: 'Partenaires',
                    templateUrl: basepath('partenaires/index.html'),
                    securized: true
                })
                .state('app.partenaires.edit', {
                    url: '/edit/{id:[0-9]*}',
                    title: 'Edition d’un partenaire',
                    templateUrl: basepath('partenaires/edit.html'),
                    securized: true,
                    resolve: resolveFor('jquery-ui', 'redactor')
                })

                // Agenda
                // -----------------------------------
                .state('app.agenda', {
                    url: '/agenda',
                    title: 'Agenda',
                    templateUrl: basepath('agenda/app.html'),
                    securized: true
                })
                .state('app.agenda.index', {
                    url: '/index/{id:[0-9]*}',
                    title: 'Agenda',
                    resolve: resolveFor('moment', 'fullcalendar'),
                    templateUrl: basepath('agenda/index.html'),
                    securized: true
                })
                .state('app.agenda.new', {
                    url: '/new_even/{agenda_id:[0-9]*}',
                    title: 'Nouvel evenement',
                    templateUrl: basepath('agenda/edit.html'),
                    resolve: resolveFor('jquery-ui', 'redactor'),
                    securized: true
                })
                .state('app.agenda.edit', {
                    url: '/edit_even/{id:[0-9]*}',
                    title: 'Edition d’evenement',
                    templateUrl: basepath('agenda/edit.html'),
                    resolve: resolveFor('jquery-ui', 'redactor'),
                    securized: true
                })


                // Page
                // -----------------------------------
                .state('app.pages', {
                    url: '/pages',
                    title: 'Page',
                    //controller : 'PagesCtrl',
                    templateUrl: basepath('pages/app.html'),
                    securized: true
                })
                .state('app.pages.index', {
                    url: '/index/:page_id',
                    title: 'Pages',
                    templateUrl: basepath('pages/index.html'),
                    securized: true
                })
                .state('app.pages.edit', {
                    url: '/edit/{id:[0-9]*}',
                    title: 'Edition d’une page',
                    templateUrl: basepath('pages/edit.html'),
                    securized: true,
                    resolve: resolveFor('jquery-ui', 'redactor')
                })
                .state('app.pages.parent', {
                    url: '/parent/:parent_id',
                    title: 'Pages',
                    templateUrl: basepath('pages/edit.html'),
                    securized: true,
                    resolve: resolveFor('jquery-ui', 'redactor')
                })
                .state('app.pages.view', {
                    url: '/view',
                    title: 'Voir la page',
                    templateUrl: basepath('pages/view.html'),
                    securized: true
                })

                // User
                // -----------------------------------
                .state('app.users', {
                    url: '/users',
                    title: 'Gestion du staff',
                    //controller : 'PagesCtrl',
                    templateUrl: basepath('users/app.html'),
                    securized: true
                })
                .state('app.users.index', {
                    url: '/view',
                    title: 'Gestion des users',
                    templateUrl: basepath('users/index.html'),
                    securized: true
                })
                .state('app.users.edit', {
                    url: '/edit/{id:[0-9]*}',
                    title: 'Edition d’un user',
                    templateUrl: basepath('users/edit.html'),
                    securized: true,
                    resolve: resolveFor('jquery-ui', 'redactor')
                })

                .state('app.groups', {
                    url: '/groupes',
                    title: 'Gestion des groupes',
                    templateUrl: basepath('groupes/app.html'),
                    securized: true
                })

                .state('app.typepages', {
                    url: '/typepages',
                    templateUrl: basepath('typepages/app.html'),
                    securized: true
                })
                .state('app.typepages.index', {
                    url: '/index/',
                    templateUrl: basepath('typepages/index.html'),
                    securized: true
                })
                .state('app.typepages.edit', {
                    url: '/edit/:product_id',
                    templateUrl: basepath('typepages/edit.html'),
                    securized: true,
                    resolve: resolveFor('redactor')
                })


                //Rights
                .state('app.rights', {
                    url: '/rights',
                    title: 'Gestion des droits',
                    templateUrl: basepath('rights/app.html'),
                    securized: true
                })
                .state('app.rights.index', {
                    url: '/index',
                    title: 'Gestion des droits',
                    templateUrl: basepath('rights/index.html'),
                    securized: true
                })
                .state('app.rights.edit', {
                    url: '/edit/{id:[0-9]*}',
                    title: 'Gestion des droits',
                    templateUrl: basepath('rights/edit.html'),
                    securized: true
                })

                // Mailbox
                // -----------------------------------
                .state('app.mailbox', {
                    url: '/mailbox',
                    title: 'Mailbox',
                    abstract: true,
                    templateUrl: basepath('messagerie/mailbox.html'),
                    controller: 'MailboxController'
                })
                .state('app.mailbox.inbox', {
                    url: '/inbox/{type:[a-z]*}',
                    title: 'Mailbox',
                    templateUrl: basepath('messagerie/mailbox-inbox.html'),
                    controller: 'MailboxIndoxController'
                })
                .state('app.mailbox.compose', {
                    url: '/compose/{id:[0-9]*}',
                    title: 'Mailbox',
                    templateUrl: basepath('messagerie/mailbox-compose.html'),
                    controller: 'NullController',
                    resolve: resolveFor('redactor')
                })
                .state('app.mailbox.view', {
                    url: '/view/{id:[0-9]*}',
                    title: 'View mail',
                    templateUrl: basepath('messagerie/mailbox-view.html'),
                    controller: 'NullController',
                    resolve: resolveFor('ngWig')
                })



            //
            // Page publics
            // -----------------------------------
                .state('app.products', {
                    url: '/products',
                    templateUrl: basepath('products/app.html'),
                    securized: true
                })
                .state('app.products.index', {
                    url: '/index/',
                    templateUrl: basepath('products/index.html'),
                    securized: true
                })
                .state('app.products.edit', {
                    url: '/edit/:product_id',
                    templateUrl: basepath('products/edit.html'),
                    securized: true,
                    resolve: resolveFor('redactor')
                })
                .state('app.attributes_families', {
                    url: '/attributes_families',
                    templateUrl: basepath('attributes_families/app.html'),
                    securized: true
                })
                .state('app.attributes_families.index', {
                    url: '/index/:attribute_family_id',
                    templateUrl: basepath('attributes_families/index.html'),
                    securized: true
                })
                .state('app.attributes_families.edit', {
                    url: '/edit/:attribute_family_id',
                    templateUrl: basepath('attributes_families/edit.html'),
                    securized: true
                })


                //
                // Page publics
                // -----------------------------------
                .state('public', {
                    url: '/public',
                    templateUrl: basepath('public/page.html'),
                    title: 'Espace public',
                    resolve: resolveFor('modernizr', 'icons', 'parsley')
                })
                .state('public.login', {
                    url: '/login',
                    title: "Login",
                    templateUrl: basepath('public/login.html')
                })
                .state('public.register', {
                    url: '/register',
                    title: "Register",
                    templateUrl: basepath('public/register.html')
                })
                .state('public.recover', {
                    url: '/recover',
                    title: "Recover",
                    templateUrl: basepath('public/recover.html')
                })
                .state('public.lock', {
                    url: '/lock',
                    title: "Lock",
                    templateUrl: basepath('public/lock.html')
                })

                // Newsletter
                // -----------------------------------
                .state('app.newsletter_inscrits', {
                    url: '/newsletter_inscrits',
                    title: 'Newsletter',
                    templateUrl: basepath('newsletter_inscrits/app.html'),
                    securized: true
                })
                .state('app.newsletter_inscrits.index', {
                    url: '/index',
                    title: 'Newsletter',
                    templateUrl: basepath('newsletter_inscrits/index.html'),
                    securized: true
                })


            ;

            // Set here the base of the relative path
            // for all app views
            function basepath(uri) {
                return WH_ROOT + '/app_admin/views/' + uri;
            }



            // Generates a resolve object by passing script names
            // previously configured in constant.APP_REQUIRES
            function resolveFor() {
                var _args = arguments;
                return {
                    deps: ['$ocLazyLoad','$q', function ($ocLL, $q) {
                        // Creates a promise chain for each argument
                        var promise = $q.when(1); // empty promise
                        for(var i=0, len=_args.length; i < len; i ++){
                            promise = andThen(_args[i]);
                        }
                        return promise;

                        // creates promise to chain dynamically
                        function andThen(_arg) {
                            // also support a function that returns a promise
                            if(typeof _arg == 'function')
                                return promise.then(_arg);
                            else
                                return promise.then(function() {
                                    // if is a module, pass the name. If not, pass the array
                                    var whatToLoad = getRequired(_arg);
                                    // simple error check
                                    if(!whatToLoad) return $.error('Route resolve: Bad resource name [' + _arg + ']');
                                    // finally, return a promise
                                    return $ocLL.load( whatToLoad );
                                });
                        }
                        // check and returns required data
                        // analyze module items with the form [name: '', files: []]
                        // and also simple array of script files (for not angular js)
                        function getRequired(name) {
                            if (appRequires.modules)
                                for(var m in appRequires.modules)
                                    if(appRequires.modules[m].name && appRequires.modules[m].name === name)
                                        return appRequires.modules[m];
                            return appRequires.scripts && appRequires.scripts[name];
                        }

                    }]};
            }

        }]).config(['$translateProvider', function ($translateProvider) {

        $translateProvider.useStaticFilesLoader({
            prefix : WH_ROOT + '/app_admin/assets/i18n/',
            suffix : '.json'
        });
        $translateProvider.preferredLanguage('fr');
        $translateProvider.useLocalStorage();

    }]).config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeBar = true;
        cfpLoadingBarProvider.includeSpinner = false;
        cfpLoadingBarProvider.latencyThreshold = 500;
        cfpLoadingBarProvider.parentSelector = '.wrapper > section';
    }])
    .controller('NullController', function() {});

/**=========================================================
 * Module: constants.js
 * Define constants to inject across the application
 =========================================================*/
App
    .constant('APP_COLORS', {
        'primary':                '#5d9cec',
        'success':                '#27c24c',
        'info':                   '#23b7e5',
        'warning':                '#ff902b',
        'danger':                 '#f05050',
        'inverse':                '#131e26',
        'green':                  '#37bc9b',
        'pink':                   '#f532e5',
        'purple':                 '#7266ba',
        'dark':                   '#3a3f51',
        'yellow':                 '#fad732',
        'gray-darker':            '#232735',
        'gray-dark':              '#3a3f51',
        'gray':                   '#dde6e9',
        'gray-light':             '#e4eaec',
        'gray-lighter':           '#edf1f2'
    })
    .constant('APP_MEDIAQUERY', {
        'desktopLG':             1200,
        'desktop':                992,
        'tablet':                 768,
        'mobile':                 480
    })
    .constant('redactorOptions', {

    })
    .constant('APP_REQUIRES', {
        scripts: {
            'jquery':             [WH_ROOT+'/vendor/jquery/jquery.min.js'],
            'icons':              [WH_ROOT+'/vendor/skycons/skycons.js', WH_ROOT+'/vendor/fontawesome/css/font-awesome.min.css',WH_ROOT + '/vendor/simplelineicons/simple-line-icons.css', WH_ROOT+'/vendor/weathericons/css/weather-icons.min.css'],
            'modernizr':          [WH_ROOT+'/vendor/modernizr/modernizr.js'],
            'fastclick':          [WH_ROOT+'/vendor/fastclick/fastclick.js'],
            'filestyle':          [WH_ROOT+'/vendor/filestyle/bootstrap-filestyle.min.js'],
            'csspiner':           [WH_ROOT+'/vendor/csspinner/csspinner.min.css'],
            'animo':              [WH_ROOT+'/vendor/animo/animo.min.js'],
            'sparklines':         [WH_ROOT+'/vendor/sparklines/jquery.sparkline.min.js'],
            'slimscroll':         [WH_ROOT+'/vendor/slimscroll/jquery.slimscroll.min.js'],
            'screenfull':         [WH_ROOT+'/vendor/screenfull/screenfull.min.js'],
            'classyloader':       [WH_ROOT+'/vendor/classyloader/js/jquery.classyloader.min.js'],
            'vector-map':         [WH_ROOT+'/vendor/jvectormap/jquery-jvectormap-1.2.2.min.js', WH_ROOT+'/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js', WH_ROOT+'/vendor/jvectormap/jquery-jvectormap-1.2.2.css'],
            'loadGoogleMapsJS':   [WH_ROOT+'/vendor/gmap/load-google-maps.js'],
            'google-map':         [WH_ROOT+'/vendor/gmap/jquery.gmap.min.js'],
            'flot-chart':         [WH_ROOT+'/vendor/flot/jquery.flot.min.js'],
            'flot-chart-plugins': [WH_ROOT+'/vendor/flot/jquery.flot.tooltip.min.js',WH_ROOT+'/vendor/flot/jquery.flot.resize.min.js',WH_ROOT + '/vendor/flot/jquery.flot.pie.min.js',WH_ROOT+'/vendor/flot/jquery.flot.time.min.js',WH_ROOT+'/vendor/flot/jquery.flot.categories.min.js',WH_ROOT+'/vendor/flot/jquery.flot.spline.min.js'],
            'jquery-ui':          [WH_ROOT+'/vendor/jqueryui/jquery-ui.min.js', WH_ROOT+'/vendor/touch-punch/jquery.ui.touch-punch.min.js'],
            'chosen':             [WH_ROOT+'/vendor/chosen/chosen.jquery.min.js', WH_ROOT+'/vendor/chosen/chosen.min.css'],
            'slider':             [WH_ROOT+'/vendor/slider/js/bootstrap-slider.js', WH_ROOT+'/vendor/slider/css/slider.css'],
            'moment' :            [WH_ROOT+'/vendor/moment/min/moment-with-locales.min.js'],
            'fullcalendar':       [WH_ROOT+'/vendor/fullcalendar/dist/fullcalendar.min.js', WH_ROOT+'/vendor/fullcalendar/dist/fullcalendar.css'],
            'codemirror':         [WH_ROOT+'/vendor/codemirror/lib/codemirror.js', WH_ROOT+'/vendor/codemirror/lib/codemirror.css'],
            'codemirror-plugins': [WH_ROOT+'/vendor/codemirror/addon/mode/overlay.js',WH_ROOT+'/vendor/codemirror/mode/markdown/markdown.js','app_admin/vendor/codemirror/mode/xml/xml.js','app_admin/vendor/codemirror/mode/gfm/gfm.js','/vendor/marked/marked.js'],
            'taginput' :          [WH_ROOT+'/vendor/tagsinput/bootstrap-tagsinput.min.js', WH_ROOT+'/vendor/tagsinput/bootstrap-tagsinput.css'],
            'inputmask':          [WH_ROOT+'/vendor/inputmask/jquery.inputmask.bundle.min.js'],
            'bwizard':            [WH_ROOT+'/vendor/wizard/js/bwizard.min.js'],
            'parsley':            [WH_ROOT+'/vendor/parsley/parsley.min.js'],
            'datatables':         [WH_ROOT+'/vendor/datatable/media/js/jquery.dataTables.min.js', WH_ROOT+'/vendor/datatable/extensions/datatable-bootstrap/css/dataTables.bootstrap.css'],
            'datatables-pugins':  [WH_ROOT+'/vendor/datatable/extensions/datatable-bootstrap/js/dataTables.bootstrap.js',WH_ROOT+'/vendor/datatable/extensions/datatable-bootstrap/js/dataTables.bootstrapPagination.js',WH_ROOT+'/vendor/datatable/extensions/ColVis/js/dataTables.colVis.min.js', WH_ROOT+'/vendor/datatable/extensions/ColVis/css/dataTables.colVis.css'],
            'flatdoc':            [WH_ROOT+'/vendor/flatdoc/flatdoc.js'],
            'redactor':           [WH_ROOT+'/vendor/redactor/redactor/redactor.js', WH_ROOT+'/vendor/redactor/redactor/definedlinks.js', WH_ROOT+'/vendor/redactor/redactor/anchorlinks.js', WH_ROOT+'/vendor/redactor/redactor/mp3Player.js' ,WH_ROOT+'/vendor/redactor/redactor/redactor.css', WH_ROOT+'/assets/css/redac-style.css']
        },
        modules: [
            { name: 'toaster',         files: [WH_ROOT+'/vendor/toaster/toaster.js', WH_ROOT+'/vendor/toaster/toaster.css'] },
            { name: 'ngWig',          files: [WH_ROOT+'/vendor/ngwig/ng-wig.min.js'] }
        ]
    })
;
/**=========================================================
 * Module: access-login.js
 * Demo for login api
 =========================================================*/

App.controller('LoginFormController', ['$scope', '$http', '$state', 'UserService', '$location', function($scope, $http, $state, UserService, $location) {

    // bind here all data from the form
    $scope.data = {};
    // place the message if something goes wrong
    $scope.authMsg = '';

    $scope.login = function() {

        $scope.loading = true;

        $scope.authMsg = '';

        UserService.login($scope.data).then(function(res) {

            $scope.loading = false;

            if(res) {

                if($location.search().returnUrl) $location.url($location.search().returnUrl);
                else $state.go('app.dashboard');

            }else{
                $scope.authMsg = 'Email ou mot de passe inconnu.';
            }


        }, function() {

            $scope.authMsg = 'Email ou mot de passe inconnu.';

        });


    };

}]);

/**=========================================================
 * Module: main.js
 * Main Application Controller
 =========================================================*/

App.controller('AppController',
    ['$rootScope', '$scope', '$state', '$translate', '$window', '$localStorage', '$timeout', 'toggleStateService', 'colors', 'browser', 'cfpLoadingBar', '$modal', '$parse',
        function($rootScope, $scope, $state, $translate, $window, $localStorage, $timeout, toggle, colors, browser, cfpLoadingBar, $modal, $parse) {
            "use strict";

            // Loading bar transition
            // -----------------------------------
            var thBar;
            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                if($('.wrapper > section').length) // check if bar container exists
                    thBar = $timeout(function() {
                        cfpLoadingBar.start();
                    }, 0); // sets a latency Threshold
            });
            $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                event.targetScope.$watch("$viewContentLoaded", function () {
                    $timeout.cancel(thBar);
                    cfpLoadingBar.complete();
                });
            });


            // Hook not found
            $rootScope.$on('$stateNotFound',
                function(event, unfoundState, fromState, fromParams) {
                    console.log(unfoundState.to); // "lazy.state"
                    console.log(unfoundState.toParams); // {a:1, b:2}
                    console.log(unfoundState.options); // {inherit:false} + default options
                });

            // Hook success
            $rootScope.$on('$stateChangeSuccess',
                function(event, toState, toParams, fromState, fromParams) {
                    // display new view from top
                    $window.scrollTo(0, 0);
                    // Save the route title
                    $rootScope.currTitle = $state.current.title;
                });

            $rootScope.currTitle = $state.current.title;
            $rootScope.pageTitle = function() {
                return $rootScope.app.name + ' - ' + ($rootScope.currTitle || $rootScope.app.description);
            };

            // iPad may presents ghost click issues
            // if( ! browser.ipad )
            // FastClick.attach(document.body);

            // Close submenu when sidebar change from collapsed to normal
            $rootScope.$watch('app.layout.isCollapsed', function(newValue, oldValue) {
                if( newValue === false )
                    $rootScope.$broadcast('closeSidebarMenu');
            });

            // Restore layout settings
            if( angular.isDefined($localStorage.layout) )
                $scope.app.layout = $localStorage.layout;
            else
                $localStorage.layout = $scope.app.layout;

            $rootScope.$watch("app.layout", function () {
                $localStorage.layout = $scope.app.layout;
            }, true);


            // Allows to use branding color with interpolation
            // {{ colorByName('primary') }}
            $scope.colorByName = colors.byName;

            // Hides/show user avatar on sidebar
            $scope.toggleUserBlock = function(){
                $scope.$broadcast('toggleUserBlock');
            };

            // Internationalization
            // ----------------------

            $scope.language = {
                // Handles language dropdown
                listIsOpen: false,
                // list of available languages
                available: {
                    'en':       'English',
                    'es_AR':    'Español'
                },
                // display always the current ui language
                init: function () {
                    var proposedLanguage = $translate.proposedLanguage() || $translate.use();
                    var preferredLanguage = $translate.preferredLanguage(); // we know we have set a preferred one in app.config
                    $scope.language.selected = $scope.language.available[ (proposedLanguage || preferredLanguage) ];
                },
                set: function (localeId, ev) {
                    // Set the new idiom
                    $translate.use(localeId);
                    // save a reference for the current language
                    $scope.language.selected = $scope.language.available[localeId];
                    // finally toggle dropdown
                    $scope.language.listIsOpen = ! $scope.language.listIsOpen;
                }
            };

            $scope.language.init();

            // Restore application classes state
            toggle.restoreState( $(document.body) );

            // Applies animation to main view for the next pages to load
            $timeout(function(){
                $rootScope.mainViewAnimation = $rootScope.app.viewAnimation;
            });
        }]);

/**=========================================================
 * Module: flot-chart.js
 * Initializes the flot chart plugin and attaches the
 * plugin to elements according to its type
 =========================================================*/

App.controller('FlotChartController', ['$scope', '$window','$http', function($scope, $window, $http) {
    'use strict';

    /**
     * Global object to load data for charts using ajax
     * Request the chart data from the server via post
     * Expects a response in JSON format to init the plugin
     * Usage
     *   chart = new floatChart(domSelector || domElement, 'server/chart-data.php')
     *   ...
     *   chart.requestData(options);
     *
     * @param  Chart element placeholder or selector
     * @param  Url to get the data via post. Response in JSON format
     */
    $window.FlotChart = function (element, url) {
        // Properties
        this.element = $(element);
        this.url = url;

        // Public method
        this.requestData = function (option, method, callback) {
            var self = this;

            // support params (option), (option, method, callback) or (option, callback)
            callback = (method && $.isFunction(method)) ? method : callback;
            method = (method && typeof method == 'string') ? method : 'POST';

            self.option = option; // save options

            $http({
                url:      self.url,
                cache:    false,
                method:   method
            }).success(function (data) {

                $.plot( self.element, data, option );

                if(callback) callback();

            }).error(function(){
                $.error('Bad chart request.');
            });

            return this; // chain-ability

        };

        // Listen to refresh events
        this.listen = function() {
            var self = this,
                chartPanel = this.element.parents('.panel').eq(0);

            // attach custom event
            chartPanel.on('panel-refresh', function(event, panel) {
                // request data and remove spinner when done
                self.requestData(self.option, function(){
                    panel.removeSpinner();
                });

            });

            return this; // chain-ability
        };

    };

    //
    // Start of Demo Script
    //
    angular.element(document).ready(function () {

        // Bar chart
        (function () {
            var Selector = '.chart-bar';
            $(Selector).each(function() {
                var source = $(this).data('source') || $.error('Bar: No source defined.');
                var chart = new FlotChart(this, source),
                //panel = $(Selector).parents('.panel'),
                    option = {
                        series: {
                            bars: {
                                align: 'center',
                                lineWidth: 0,
                                show: true,
                                barWidth: 0.6,
                                fill: 0.9
                            }
                        },
                        grid: {
                            borderColor: '#eee',
                            borderWidth: 1,
                            hoverable: true,
                            backgroundColor: '#fcfcfc'
                        },
                        tooltip: true,
                        tooltipOpts: {
                            content: '%x : %y'
                        },
                        xaxis: {
                            tickColor: '#fcfcfc',
                            mode: 'categories'
                        },
                        yaxis: {
                            position: ($scope.app.layout.isRTL ? 'right' : 'left'),
                            tickColor: '#eee'
                        },
                        shadowSize: 0
                    };
                // Send Request
                chart.requestData(option);
            });

        })();
        // Bar Stacked chart
        (function () {
            var Selector = '.chart-bar-stacked';
            $(Selector).each(function() {
                var source = $(this).data('source') || $.error('Bar Stacked: No source defined.');
                var chart = new FlotChart(this, source),
                    option = {
                        series: {
                            stack: true,
                            bars: {
                                align: 'center',
                                lineWidth: 0,
                                show: true,
                                barWidth: 0.6,
                                fill: 0.9
                            }
                        },
                        grid: {
                            borderColor: '#eee',
                            borderWidth: 1,
                            hoverable: true,
                            backgroundColor: '#fcfcfc'
                        },
                        tooltip: true,
                        tooltipOpts: {
                            content: '%x : %y'
                        },
                        xaxis: {
                            tickColor: '#fcfcfc',
                            mode: 'categories'
                        },
                        yaxis: {
                            position: ($scope.app.layout.isRTL ? 'right' : 'left'),
                            tickColor: '#eee'
                        },
                        shadowSize: 0
                    };
                // Send Request
                chart.requestData(option);
            });
        })();
        // Spline chart
        (function () {
            var Selector = '.chart-spline';
            $(Selector).each(function() {
                var source = $(this).data('source') || $.error('Spline: No source defined.');
                var chart = new FlotChart(this, source),
                    option = {
                        series: {
                            lines: {
                                show: false
                            },
                            points: {
                                show: true,
                                radius: 4
                            },
                            splines: {
                                show: true,
                                tension: 0.4,
                                lineWidth: 1,
                                fill: 0.5
                            }
                        },
                        grid: {
                            borderColor: '#eee',
                            borderWidth: 1,
                            hoverable: true,
                            backgroundColor: '#fcfcfc'
                        },
                        tooltip: true,
                        tooltipOpts: {
                            content: '%x : %y'
                        },
                        xaxis: {
                            tickColor: '#fcfcfc',
                            mode: 'categories'
                        },
                        yaxis: {
                            min: 0,
                            tickColor: '#eee',
                            position: ($scope.app.layout.isRTL ? 'right' : 'left'),
                            tickFormatter: function (v) {
                                return v/* + ' visitors'*/;
                            }
                        },
                        shadowSize: 0
                    };

                // Send Request and Listen for refresh events
                chart.requestData(option).listen();

            });
        })();
        // Area chart
        (function () {
            var Selector = '.chart-area';
            $(Selector).each(function() {
                var source = $(this).data('source') || $.error('Area: No source defined.');
                var chart = new FlotChart(this, source),
                    option = {
                        series: {
                            lines: {
                                show: true,
                                fill: 0.8
                            },
                            points: {
                                show: true,
                                radius: 4
                            }
                        },
                        grid: {
                            borderColor: '#eee',
                            borderWidth: 1,
                            hoverable: true,
                            backgroundColor: '#fcfcfc'
                        },
                        tooltip: true,
                        tooltipOpts: {
                            content: '%x : %y'
                        },
                        xaxis: {
                            tickColor: '#fcfcfc',
                            mode: 'categories'
                        },
                        yaxis: {
                            min: 0,
                            tickColor: '#eee',
                            position: ($scope.app.layout.isRTL ? 'right' : 'left'),
                            tickFormatter: function (v) {
                                return v + ' visitors';
                            }
                        },
                        shadowSize: 0
                    };

                // Send Request and Listen for refresh events
                chart.requestData(option).listen();

            });
        })();
        // Line chart
        (function () {
            var Selector = '.chart-line';
            $(Selector).each(function() {
                var source = $(this).data('source') || $.error('Line: No source defined.');
                var chart = new FlotChart(this, source),
                    option = {
                        series: {
                            lines: {
                                show: true,
                                fill: 0.01
                            },
                            points: {
                                show: true,
                                radius: 4
                            }
                        },
                        grid: {
                            borderColor: '#eee',
                            borderWidth: 1,
                            hoverable: true,
                            backgroundColor: '#fcfcfc'
                        },
                        tooltip: true,
                        tooltipOpts: {
                            content: '%x : %y'
                        },
                        xaxis: {
                            tickColor: '#eee',
                            mode: 'categories'
                        },
                        yaxis: {
                            position: ($scope.app.layout.isRTL ? 'right' : 'left'),
                            tickColor: '#eee'
                        },
                        shadowSize: 0
                    };
                // Send Request
                chart.requestData(option);
            });
        })();
        // Pïe
        (function () {
            var Selector = '.chart-pie';
            $(Selector).each(function() {
                var source = $(this).data('source') || $.error('Pie: No source defined.');
                var chart = new FlotChart(this, source),
                    option = {
                        series: {
                            pie: {
                                show: true,
                                innerRadius: 0,
                                label: {
                                    show: true,
                                    radius: 0.8,
                                    formatter: function (label, series) {
                                        return '<div class="flot-pie-label">' +
                                            //label + ' : ' +
                                            Math.round(series.percent) +
                                            '%</div>';
                                    },
                                    background: {
                                        opacity: 0.8,
                                        color: '#222'
                                    }
                                }
                            }
                        }
                    };
                // Send Request
                chart.requestData(option);
            });
        })();
        // Donut
        (function () {
            var Selector = '.chart-donut';
            $(Selector).each(function() {
                var source = $(this).data('source') || $.error('Donut: No source defined.');
                var chart = new FlotChart(this, source),
                    option = {
                        series: {
                            pie: {
                                show: true,
                                innerRadius: 0.5 // This makes the donut shape
                            }
                        }
                    };
                // Send Request
                chart.requestData(option);
            });
        })();
    });

}]);
/**=========================================================
 * Module: modals.js
 * Provides a simple way to implement bootstrap modals from templates
 =========================================================*/

App.controller('ModalGmapController', ['$scope', '$modal', 'gmap', function ($scope, $modal, gmap) {

    $scope.open = function (size) {

        var modalInstance = $modal.open({
            templateUrl: '/myModalContent.html',
            controller: ModalInstanceCtrl,
            size: size
        });
    };

    // Please note that $modalInstance represents a modal window (instance) dependency.
    // It is not the same as the $modal service used above.

    var ModalInstanceCtrl = function ($scope, $modalInstance) {

        $modalInstance.opened.then(function () {
            // When modal has been opened
            // set to true the initialization param
            $scope.initGmap = true;

        });

        $scope.ok = function () {
            $modalInstance.close('closed');
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    };

}]);

/**=========================================================
 * Module: modals.js
 * Provides a simple way to implement bootstrap modals from templates
 =========================================================*/

App.controller('ModalController', ['$scope', '$modal', function ($scope, $modal) {

    $scope.open = function (size) {

        var modalInstance = $modal.open({
            templateUrl: '/myModalContent.html',
            controller: ModalInstanceCtrl,
            size: size
        });

        var state = $('#modal-state');
        modalInstance.result.then(function () {
            state.text('Modal dismissed with OK status');
        }, function () {
            state.text('Modal dismissed with Cancel status');
        });
    };

    // Please note that $modalInstance represents a modal window (instance) dependency.
    // It is not the same as the $modal service used above.

    var ModalInstanceCtrl = function ($scope, $modalInstance) {

        $scope.ok = function () {
            $modalInstance.close('closed');
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

}]);



/**=========================================================
 * Module: youtube
 * Service and directive
 =========================================================*/

App.service ('youtubeEmbedUtils', ['$window', '$rootScope', function ($window, $rootScope) {
    var Service = {}

    // adapted from http://stackoverflow.com/a/5831191/1614967
    var youtubeRegexp = /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube(?:-nocookie)?\.com\S*[^\w\s-])([\w-]{11})(?=[^\w-]|$)(?![?=&+%\w.-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/ig;
    var timeRegexp = /t=(\d+)[ms]?(\d+)?s?/;

    function contains(str, substr) {
        return (str.indexOf(substr) > -1);
    }

    Service.getIdFromURL = function getIdFromURL(url) {
        var id = url.replace(youtubeRegexp, '$1');

        if (contains(id, ';')) {
            var pieces = id.split(';');

            if (contains(pieces[1], '%')) {
                // links like this:
                // "http://www.youtube.com/attribution_link?a=pxa6goHqzaA&amp;u=%2Fwatch%3Fv%3DdPdgx30w9sU%26feature%3Dshare"
                // have the real query string URI encoded behind a ';'.
                // at this point, `id is 'pxa6goHqzaA;u=%2Fwatch%3Fv%3DdPdgx30w9sU%26feature%3Dshare'
                var uriComponent = decodeURIComponent(id.split(';')[1]);
                id = ('http://youtube.com' + uriComponent)
                    .replace(youtubeRegexp, '$1');
            } else {
                // https://www.youtube.com/watch?v=VbNF9X1waSc&amp;feature=youtu.be
                // `id` looks like 'VbNF9X1waSc;feature=youtu.be' currently.
                // strip the ';feature=youtu.be'
                id = pieces[0];
            }
        } else if (contains(id, '#')) {
            // id might look like '93LvTKF_jW0#t=1'
            // and we want '93LvTKF_jW0'
            id = id.split('#')[0];
        }

        return id;
    };

    Service.getTimeFromURL = function getTimeFromURL(url) {
        url = url || '';

        // t=4m20s
        // returns ['t=4m20s', '4', '20']
        // t=46s
        // returns ['t=46s', '46']
        // t=46
        // returns ['t=46', '46']
        var times = url.match(timeRegexp);

        if (!times) {
            // zero seconds
            return 0;
        }

        // assume the first
        var full = times[0],
            minutes = times[1],
            seconds = times[2];

        // t=4m20s
        if (typeof seconds !== 'undefined') {
            seconds = parseInt(seconds, 10);
            minutes = parseInt(minutes, 10);

            // t=4m
        } else if (contains(full, 'm')) {
            minutes = parseInt(minutes, 10);
            seconds = 0;

            // t=4s
            // t=4
        } else {
            seconds = parseInt(minutes, 10);
            minutes = 0;
        }

        // in seconds
        return seconds + (minutes * 60);
    };

    // Inject YouTube's iFrame API
    (function () {
        var tag = document.createElement('script');
        tag.src = 'https://www.youtube.com/iframe_api';
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }());

    Service.ready = false;

    // Youtube callback when API is ready
    $window.onYouTubeIframeAPIReady = function () {
        $rootScope.$apply(function () {
            Service.ready = true;
        });
    };

    return Service;
}])
    .directive('youtubeVideo', ['youtubeEmbedUtils', function (youtubeEmbedUtils) {
        var uniqId = 1;

        // from YT.PlayerState
        var stateNames = {
            '-1': 'unstarted',
            0: 'ended',
            1: 'playing',
            2: 'paused',
            3: 'buffering',
            5: 'queued'
        };

        var eventPrefix = 'youtube.player.';

        return {
            restrict: 'EA',
            scope: {
                videoId: '=?',
                videoUrl: '=?',
                player: '=?',
                playerVars: '=?',
                playerHeight: '=?',
                playerWidth: '=?'
            },
            link: function (scope, element, attrs) {
                // allows us to $watch `ready`
                scope.utils = youtubeEmbedUtils;

                // player-id attr > id attr > directive-generated ID
                var playerId = attrs.playerId || element[0].id || 'unique-youtube-embed-id-' + uniqId++;
                element[0].id = playerId;

                // Attach to element
                scope.playerHeight = scope.playerHeight || 390;
                scope.playerWidth = scope.playerWidth || 640;
                scope.playerVars = scope.playerVars || {};

                // YT calls callbacks outside of digest cycle
                function applyBroadcast () {
                    var args = Array.prototype.slice.call(arguments);
                    scope.$apply(function () {
                        scope.$emit.apply(scope, args);
                    });
                }

                function onPlayerStateChange (event) {
                    var state = stateNames[event.data];
                    if (typeof state !== 'undefined') {
                        applyBroadcast(eventPrefix + state, scope.player, event);
                    }
                    scope.$apply(function () {
                        scope.player.currentState = state;
                    });
                }

                function onPlayerReady (event) {
                    applyBroadcast(eventPrefix + 'ready', scope.player, event);
                }

                function createPlayer () {
                    var playerVars = angular.copy(scope.playerVars);
                    playerVars.start = playerVars.start || scope.urlStartTime;
                    var player = new YT.Player(playerId, {
                        height: scope.playerHeight,
                        width: scope.playerWidth,
                        videoId: scope.videoId,
                        playerVars: playerVars,
                        events: {
                            onReady: onPlayerReady,
                            onStateChange: onPlayerStateChange
                        }
                    });

                    player.id = playerId;
                    return player;
                }

                function loadPlayer () {
                    if (scope.videoId || scope.playerVars.list) {
                        if (scope.player && scope.player.d &&
                            typeof scope.player.destroy === 'function') {
                            scope.player.destroy();
                        }

                        scope.player = createPlayer();
                    }
                };

                var stopWatchingReady = scope.$watch(
                    function () {
                        return scope.utils.ready
                            // Wait until one of them is defined...
                            && (typeof scope.videoUrl !== 'undefined'
                            ||  typeof scope.videoId !== 'undefined'
                            ||  typeof scope.playerVars.list !== 'undefined');
                    },
                    function (ready) {
                        if (ready) {
                            stopWatchingReady();

                            // URL takes first priority
                            if (typeof scope.videoUrl !== 'undefined') {
                                scope.$watch('videoUrl', function (url) {
                                    scope.videoId = scope.utils.getIdFromURL(url);
                                    scope.urlStartTime = scope.utils.getTimeFromURL(url);

                                    loadPlayer();
                                });

                                // then, a video ID
                            } else if (typeof scope.videoId !== 'undefined') {
                                scope.$watch('videoId', function () {
                                    scope.urlStartTime = null;
                                    loadPlayer();
                                });

                                // finally, a list
                            } else {
                                scope.$watch('playerVars.list', function () {
                                    scope.urlStartTime = null;
                                    loadPlayer();
                                });
                            }
                        }
                    });

                scope.$watchCollection(['playerHeight', 'playerWidth'], function() {
                    if (scope.player) {
                        scope.player.setSize(scope.playerWidth, scope.playerHeight);
                    }
                });

                scope.$on('$destroy', function () {
                    scope.player && scope.player.destroy();
                });
            }
        };
    }]);


/**=========================================================
 * Module: notifications.js
 * Initializes the notifications system
 =========================================================*/
App.controller('NotificationController', ['$scope', function($scope){

    $scope.autoplace = function (context, source) {
        //return (predictTooltipTop(source) < 0) ?  "bottom": "top";
        var pos = 'top';
        if(predictTooltipTop(source) < 0)
            pos = 'bottom';
        if(predictTooltipLeft(source) < 0)
            pos = 'right';
        return pos;
    };

    // Predicts tooltip top position
    // based on the trigger element
    function predictTooltipTop(el) {
        var top = el.offsetTop;
        var height = 40; // asumes ~40px tooltip height

        while(el.offsetParent) {
            el = el.offsetParent;
            top += el.offsetTop;
        }
        return (top - height) - (window.pageYOffset);
    }

    // Predicts tooltip top position
    // based on the trigger element
    function predictTooltipLeft(el) {
        var left = el.offsetLeft;
        var width = el.offsetWidth;

        while(el.offsetParent) {
            el = el.offsetParent;
            left += el.offsetLeft;
        }
        return (left - width) - (window.pageXOffset);
    }

}]);
/**=========================================================
 * Module: portlet.js
 * Drag and drop any panel to change its position
 * The Selector should could be applied to any object that contains
 * panel, so .col-* element are ideal.
 =========================================================*/
App.controller('portletsController', [ '$scope', '$timeout', '$window', function($scope, $timeout, $window) {
    'use strict';

    // Component is optional
    if(!$.fn.sortable) return;

    var Selector = '[portlet]',
        storageKeyName = 'portletState';



    $scope.Portlets = function () {

        $timeout(function() {

            $( Selector ).sortable({
                connectWith:          Selector,
                items:                'div.panel',
                handle:               '.portlet-handler',
                opacity:              0.7,
                placeholder:          'portlet box-placeholder',
                cancel:               '.portlet-cancel',
                forcePlaceholderSize: true,
                iframeFix:            false,
                tolerance:            'pointer',
                helper:               'original',
                revert:               200,
                forceHelperSize:      true,
                start:                saveListSize,
                update:               savePortletOrder,
                create:               loadPortletOrder
            })
                // optionally disables mouse selection
                //.disableSelection()
            ;
        }, 0);

    }

    angular.element(document).ready(function () {

        $scope.Portlets();

    });

    function savePortletOrder(event, ui) {
        var self = event.target;
        var data = angular.fromJson($scope.$storage[storageKeyName]);

        if(!data) { data = {}; }

        data[self.id] = $(self).sortable('toArray');

        $scope.$storage[storageKeyName] = angular.toJson(data);

        // save portlet size to avoid jumps
        saveListSize.apply(self);
    }

    function loadPortletOrder(event) {
        var self = event.target;
        var data = angular.fromJson($scope.$storage[storageKeyName]);

        if(data) {

            var porletId = self.id,
                panels   = data[porletId];

            if(panels) {
                var portlet = $('#'+porletId);

                $.each(panels, function(index, value) {
                    $('#'+value).appendTo(portlet);
                });
            }

        }

        // save portlet size to avoid jumps
        saveListSize.apply(self);
    }

    // Keeps a consistent size in all portlet lists
    function saveListSize() {
        var $this = $(this);
        $this.css('min-height', $this.height());
    }

    /*function resetListSize() {
     $(this).css('min-height', "");
     }*/

}]);
/**=========================================================
 * Module: sidebar-menu.js
 * Provides a simple way to implement bootstrap collapse plugin using a target
 * next to the current element (sibling)
 * Targeted elements must have [data-toggle="collapse-next"]
 =========================================================*/
App.controller('SidebarController', ['$rootScope', '$scope', '$state', '$location', '$http', '$timeout', 'APP_MEDIAQUERY',
    function($rootScope, $scope, $state, $location, $http, $timeout, mq){

        var currentState = $rootScope.$state.current.name;
        var $win = $(window);
        var $html = $('html');
        var $body = $('body');

        // Adjustment on route changes
        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            currentState = toState.name;
            // Hide sidebar automatically on mobile
            $('body.aside-toggled').removeClass('aside-toggled');

            $rootScope.$broadcast('closeSidebarMenu');
        });

        // Normalize state on resize to avoid multiple checks
        $win.on('resize', function() {
            if( isMobile() )
                $body.removeClass('aside-collapsed');
            else
                $body.removeClass('aside-toggled');
        });

        // Check item and children active state
        var isActive = function(item) {

            if(!item) return;

            if( !item.sref || item.sref == '#') {
                var foundActive = false;
                angular.forEach(item.submenu, function(value, key) {
                    if(isActive(value)) foundActive = true;
                });
                return foundActive;
            }
            else
                return $state.is(item.sref);
        };

        // Load menu from json file
        // -----------------------------------

        $scope.getMenuItemPropClasses = function(item) {
            return (item.heading ? 'nav-heading' : '') +
                (isActive(item) ? ' active' : '') ;
        };

        $scope.loadSidebarMenu = function() {

            var menuJson = params.Url.menu,
                menuURL  = menuJson + '?v=' + (new Date().getTime()); // jumps cache
            $http.get(menuURL)
                .success(function(items) {
                    $rootScope.menuItems = items;
                })
                .error(function(data, status, headers, config) {
                    alert('Failure loading menu');
                });
        };

        $scope.loadSidebarMenu();

        // Handle sidebar collapse items
        // -----------------------------------
        var collapseList = [];

        $scope.addCollapse = function($index, item) {
            collapseList[$index] = !isActive(item);
        };

        $scope.isCollapse = function($index) {
            return (collapseList[$index]);
        };

        $scope.toggleCollapse = function($index) {

            // collapsed sidebar doesn't toggle drodopwn
            if( isSidebarCollapsed() && !isMobile() ) return true;
            // make sure the item index exists
            if( typeof collapseList[$index] === undefined ) return true;

            closeAllBut($index);
            collapseList[$index] = !collapseList[$index];

            return true;

            function closeAllBut($index) {
                angular.forEach(collapseList, function(v, i) {
                    if($index !== i)
                        collapseList[i] = true;
                });
            }
        };

        // Helper checks
        // -----------------------------------

        function isMobile() {
            return $win.width() < mq.tablet;
        }
        function isTouch() {
            return $html.hasClass('touch');
        }
        function isSidebarCollapsed() {
            return $body.hasClass('aside-collapsed');
        }
        function isSidebarToggled() {
            return $body.hasClass('aside-toggled');
        }
    }]);

/**=========================================================
 * Module: upload.js
 * Allow users to upload files through a file input form element or a placeholder area.
 * Based on addon from UIKit (http://getuikit.com/docs/addons_upload.html)
 *
 * Adapted version to work with Bootstrap classes
 =========================================================*/

(function($, window, document){
    'use strict';

    var UploadSelect = function(element, options) {

        var $this    = this,
            $element = $(element);

        options  = $.extend({}, xhrupload.defaults, UploadSelect.defaults, options);

        if ($element.data("uploadSelect")) return;

        this.element = $element.on("change", function() {
            xhrupload($this.element[0].files, options);
        });

        $element.data("uploadSelect", this);
    };

    UploadSelect.defaults = {};

    var UploadDrop = function(element, options) {

        var $this      = this,
            $element   = $(element),
            hasdragCls = false;

        options = $.extend({}, xhrupload.defaults, UploadDrop.defaults, options);

        if ($element.data("uploadDrop")) return;

        $element.on("drop", function(e){

            if (e.dataTransfer && e.dataTransfer.files) {

                e.stopPropagation();
                e.preventDefault();

                $element.removeClass(options.dragoverClass);

                xhrupload(e.dataTransfer.files, options);
            }

        }).on("dragenter", function(e){
            e.stopPropagation();
            e.preventDefault();
        }).on("dragover", function(e){
            e.stopPropagation();
            e.preventDefault();

            if (!hasdragCls) {
                $element.addClass(options.dragoverClass);
                hasdragCls = true;
            }
        }).on("dragleave", function(e){
            e.stopPropagation();
            e.preventDefault();
            $element.removeClass(options.dragoverClass);
            hasdragCls = false;
        });

        $element.data("uploadDrop", this);
    };

    UploadDrop.defaults = {
        'dragoverClass': 'dragover'
    };

    $.upload = { "select" : UploadSelect, "drop" : UploadDrop };

    $.support.ajaxupload = (function() {

        function supportFileAPI() {
            var fi = document.createElement('INPUT'); fi.type = 'file'; return 'files' in fi;
        }

        function supportAjaxUploadProgressEvents() {
            var xhr = new XMLHttpRequest(); return !! (xhr && ('upload' in xhr) && ('onprogress' in xhr.upload));
        }

        function supportFormData() {
            return !! window.FormData;
        }

        return supportFileAPI() && supportAjaxUploadProgressEvents() && supportFormData();
    })();

    if ($.support.ajaxupload){
        $.event.props.push("dataTransfer");
    }

    function xhrupload(files, settings) {

        if (!$.support.ajaxupload){
            return this;
        }

        settings = $.extend({}, xhrupload.defaults, settings);

        if (!files.length){
            return;
        }

        if (settings.allow !== '*.*') {

            for(var i=0,file;(file=files[i]);i++) {

                if(!matchName(settings.allow, file.name)) {

                    if(typeof(settings.notallowed) == 'string') {
                        alert(settings.notallowed);
                    } else {
                        settings.notallowed(file, settings);
                    }
                    return;
                }
            }
        }

        var complete = settings.complete;

        if (settings.single){

            var count    = files.length,
                uploaded = 0;

            settings.complete = function(response, xhr){
                uploaded = uploaded+1;
                complete(response, xhr);
                if (uploaded<count){
                    upload([files[uploaded]], settings);
                } else {
                    settings.allcomplete(response, xhr);
                }
            };

            upload([files[0]], settings);

        } else {

            settings.complete = function(response, xhr){
                complete(response, xhr);
                settings.allcomplete(response, xhr);
            };

            upload(files, settings);
        }

        function upload(files, settings){

            // upload all at once
            var formData = new FormData(), xhr = new XMLHttpRequest();

            if (settings.before(settings, files)===false) return;

            for (var i = 0, f; (f = files[i]); i++) { formData.append(settings.param, f); }
            for (var p in settings.params) { formData.append(p, settings.params[p]); }

            // Add any event handlers here...
            xhr.upload.addEventListener("progress", function(e){
                var percent = (e.loaded / e.total)*100;
                settings.progress(percent, e);
            }, false);

            xhr.addEventListener("loadstart", function(e){ settings.loadstart(e); }, false);
            xhr.addEventListener("load",      function(e){ settings.load(e);      }, false);
            xhr.addEventListener("loadend",   function(e){ settings.loadend(e);   }, false);
            xhr.addEventListener("error",     function(e){ settings.error(e);     }, false);
            xhr.addEventListener("abort",     function(e){ settings.abort(e);     }, false);

            xhr.open(settings.method, settings.action, true);

            xhr.onreadystatechange = function() {

                settings.readystatechange(xhr);

                if (xhr.readyState==4){

                    var response = xhr.responseText;

                    if (settings.type=="json") {
                        try {
                            response = $.parseJSON(response);
                        } catch(e) {
                            response = false;
                        }
                    }

                    settings.complete(response, xhr);
                }
            };

            xhr.send(formData);
        }
    }

    xhrupload.defaults = {
        'action': '',
        'single': true,
        'method': 'POST',
        'param' : 'files[]',
        'params': {},
        'allow' : '*.*',
        'type'  : 'text',

        // events
        'before'          : function(o){},
        'loadstart'       : function(){},
        'load'            : function(){},
        'loadend'         : function(){},
        'error'           : function(){},
        'abort'           : function(){},
        'progress'        : function(){},
        'complete'        : function(){},
        'allcomplete'     : function(){},
        'readystatechange': function(){},
        'notallowed'      : function(file, settings){ alert('Uniquement ces formats de fichiers sont déposables: '+settings.allow); }
    };

    function matchName(pattern, path) {

        var parsedPattern = '^' + pattern.replace(/\//g, '\\/').
            replace(/\*\*/g, '(\\/[^\\/]+)*').
            replace(/\*/g, '[^\\/]+').
            replace(/((?!\\))\?/g, '$1.') + '$';

        parsedPattern = '^' + parsedPattern + '$';

        return (path.match(new RegExp(parsedPattern)) !== null);
    }

    $.xhrupload = xhrupload;

    return xhrupload;

}(jQuery, window, document));

/**=========================================================
 * UserBlock
 *
 *
 =========================================================*/

App.controller('UserBlockController', ['$scope', function($scope) {

    $scope.userBlockVisible = true;

    $scope.$on('toggleUserBlock', function(event, args) {

        $scope.userBlockVisible = ! $scope.userBlockVisible;

    });

}]);
/**=========================================================
 * Module: utils.js
 * jQuery Utility functions library
 * adapted from the core of UIKit
 =========================================================*/

(function($, window, doc){
    'use strict';

    var $html = $("html"), $win = $(window);

    $.support.transition = (function() {

        var transitionEnd = (function() {

            var element = doc.body || doc.documentElement,
                transEndEventNames = {
                    WebkitTransition: 'webkitTransitionEnd',
                    MozTransition: 'transitionend',
                    OTransition: 'oTransitionEnd otransitionend',
                    transition: 'transitionend'
                }, name;

            for (name in transEndEventNames) {
                if (element.style[name] !== undefined) return transEndEventNames[name];
            }
        }());

        return transitionEnd && { end: transitionEnd };
    })();

    $.support.animation = (function() {

        var animationEnd = (function() {

            var element = doc.body || doc.documentElement,
                animEndEventNames = {
                    WebkitAnimation: 'webkitAnimationEnd',
                    MozAnimation: 'animationend',
                    OAnimation: 'oAnimationEnd oanimationend',
                    animation: 'animationend'
                }, name;

            for (name in animEndEventNames) {
                if (element.style[name] !== undefined) return animEndEventNames[name];
            }
        }());

        return animationEnd && { end: animationEnd };
    })();

    $.support.requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.msRequestAnimationFrame || window.oRequestAnimationFrame || function(callback){ window.setTimeout(callback, 1000/60); };
    $.support.touch                 = (
        ('ontouchstart' in window && navigator.userAgent.toLowerCase().match(/mobile|tablet/)) ||
            (window.DocumentTouch && document instanceof window.DocumentTouch)  ||
            (window.navigator['msPointerEnabled'] && window.navigator['msMaxTouchPoints'] > 0) || //IE 10
            (window.navigator['pointerEnabled'] && window.navigator['maxTouchPoints'] > 0) || //IE >=11
            false
        );
    $.support.mutationobserver      = (window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver || null);

    $.Utils = {};

    $.Utils.debounce = function(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };

    $.Utils.removeCssRules = function(selectorRegEx) {
        var idx, idxs, stylesheet, _i, _j, _k, _len, _len1, _len2, _ref;

        if(!selectorRegEx) return;

        setTimeout(function(){
            try {
                _ref = document.styleSheets;
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    stylesheet = _ref[_i];
                    idxs = [];
                    stylesheet.cssRules = stylesheet.cssRules;
                    for (idx = _j = 0, _len1 = stylesheet.cssRules.length; _j < _len1; idx = ++_j) {
                        if (stylesheet.cssRules[idx].type === CSSRule.STYLE_RULE && selectorRegEx.test(stylesheet.cssRules[idx].selectorText)) {
                            idxs.unshift(idx);
                        }
                    }
                    for (_k = 0, _len2 = idxs.length; _k < _len2; _k++) {
                        stylesheet.deleteRule(idxs[_k]);
                    }
                }
            } catch (_error) {}
        }, 0);
    };

    $.Utils.isInView = function(element, options) {

        var $element = $(element);

        if (!$element.is(':visible')) {
            return false;
        }

        var window_left = $win.scrollLeft(),
            window_top  = $win.scrollTop(),
            offset      = $element.offset(),
            left        = offset.left,
            top         = offset.top;

        options = $.extend({topoffset:0, leftoffset:0}, options);

        if (top + $element.height() >= window_top && top - options.topoffset <= window_top + $win.height() &&
            left + $element.width() >= window_left && left - options.leftoffset <= window_left + $win.width()) {
            return true;
        } else {
            return false;
        }
    };

    $.Utils.options = function(string) {

        if ($.isPlainObject(string)) return string;

        var start = (string ? string.indexOf("{") : -1), options = {};

        if (start != -1) {
            try {
                options = (new Function("", "var json = " + string.substr(start) + "; return JSON.parse(JSON.stringify(json));"))();
            } catch (e) {}
        }

        return options;
    };

    $.Utils.events       = {};
    $.Utils.events.click = $.support.touch ? 'tap' : 'click';

    $.langdirection = $html.attr("dir") == "rtl" ? "right" : "left";

    $(function(){

        // Check for dom modifications
        if(!$.support.mutationobserver) return;

        // Install an observer for custom needs of dom changes
        var observer = new $.support.mutationobserver($.Utils.debounce(function(mutations) {
            $(doc).trigger("domready");
        }, 300));

        // pass in the target node, as well as the observer options
        observer.observe(document.body, { childList: true, subtree: true });

    });

    // add touch identifier class
    $html.addClass($.support.touch ? "touch" : "no-touch");

}(jQuery, window, document));


/**=========================================================
 * Module: anchor.js
 * Disables null anchor behavior
 =========================================================*/

App.directive('href', function() {

    return {
        restrict: 'A',
        compile: function(element, attr) {
            return function(scope, element) {
                if(attr.ngClick || attr.href === '' || attr.href === '#'){
                    if( !element.hasClass('dropdown-toggle') )
                        element.on('click', function(e){
                            e.preventDefault();
                            e.stopPropagation();
                        });
                }
            };
        }
    };
});

/**=========================================================
* Module: chosen-select.js
* Initializes the chose select plugin
=========================================================*/

App.directive('chosen', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attr) {

            // update the select when data is loaded
            scope.$watch(attr.chosen, function(oldVal, newVal) {
                element.trigger('chosen:updated');
            });

            // update the select when the model changes
            scope.$watch(attr.ngModel, function() {
                element.trigger('chosen:updated');
            });

            if($.fn.chosen)
                element.chosen();
        }
    };
});
/**=========================================================
 * Module: animate-enabled.js
 * Enable or disables ngAnimate for element with directive
 =========================================================*/

App.directive("animateEnabled", ["$animate", function ($animate) {
    return {
        link: function (scope, element, attrs) {
            scope.$watch(function () {
                return scope.$eval(attrs.animateEnabled, scope);
            }, function (newValue) {
                $animate.enabled(!!newValue, element);
            });
        }
    };
}]);

/**=========================================================
 * Module: redactor
 *
 =========================================================*/

App.directive("redactor", ['$timeout', function($timeout, redactorOptions) {

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {

            // Expose scope var with loaded state of Redactor
            scope.redactorLoaded = false;

            var updateModel = function updateModel(value) {
                    // $timeout to avoid $digest collision
                    $timeout(function() {
                        scope.$apply(function() {
                            ngModel.$setViewValue(value);
                        });
                    });
                },

                options = {
                    lang : 'fr',
                    changeCallback: updateModel,
                    definedLinks: '/admin/pages/redactor_links',
                    imageUpload: '/admin/file_manager/files/upload', //Add the uploader
                    maxHeight: 800,  // pixels
                    plugins: [
                        'anchorlinks',
                        'mp3',
                        'definedlinks',
                        'whImageManager'
                    ],
                    buttonSource: true,
                    buttons: [
                        'formatting',
                        'bold',
                        'italic',
                        'deleted',
                        'unorderedlist',
                        'orderedlist',
                        'outdent',
                        'indent',
                        'image',
                        'link',
                        'alignment',
                        'horizontalrule',
                        'html'
                    ],
                    formatting: ['span', 'p', 'blockquote', 'h2', 'h3', 'h4'],
                    formattingAdd: [
                        {
                            tag: 'span',
                            title: 'Texte en rouge',
                            class: 'red-styled'
                        },
                        {
                            tag: 'span',
                            title: 'Texte en bleu',
                            class: 'blue-styled'
                        }
                    ]
                },
                additionalOptions = attrs.redactor ?
                    scope.$eval(attrs.redactor) : {},
                editor,
                $_element = angular.element(element);

            angular.extend(options, redactorOptions, additionalOptions);

            // put in timeout to avoid $digest collision.  call render() to
            // set the initial value.
            $timeout(function() {
                editor = $_element.redactor(options);
                ngModel.$render();
            });

            ngModel.$render = function() {
                if(angular.isDefined(editor)) {
                    $timeout(function() {
                        $_element.redactor('code.set', ngModel.$viewValue || '');
                        scope.redactorLoaded = true;
                    });
                }
            };
        }
    };
}]);

/**=========================================================
 * Module: chosen-select.js
 * Initializes the chose select plugin
 =========================================================*/

App.directive('chosen', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attr) {

            // update the select when data is loaded
            scope.$watch(attr.chosen, function(oldVal, newVal) {
                element.trigger('chosen:updated');
            });

            // update the select when the model changes
            scope.$watch(attr.ngModel, function() {
                element.trigger('chosen:updated');
            });

            if($.fn.chosen)
                element.chosen();
        }
    };
});
/**=========================================================
 * Module: classy-loader.js
 * Enable use of classyloader directly from data attributes
 =========================================================*/

App.directive('classyloader', function($timeout) {
    'use strict';

    var $scroller       = $(window),
        inViewFlagClass = 'js-is-in-view'; // a classname to detect when a chart has been triggered after scroll

    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            // run after interpolation
            $timeout(function(){

                var $element = $(element),
                    options  = $element.data();

                // At lease we need a data-percentage attribute
                if(options) {
                    if( options.triggerInView ) {

                        $scroller.scroll(function() {
                            checkLoaderInVIew($element, options);
                        });
                        // if the element starts already in view
                        checkLoaderInVIew($element, options);
                    }
                    else
                        startLoader($element, options);
                }

            }, 0);

            function checkLoaderInVIew(element, options) {
                var offset = -20;
                if( ! element.hasClass(inViewFlagClass) &&
                    $.Utils.isInView(element, {topoffset: offset}) ) {
                    startLoader(element, options);
                }
            }
            function startLoader(element, options) {
                element.ClassyLoader(options).addClass(inViewFlagClass);
            }
        }
    };
});

/**=========================================================
 * Module: clear-storage.js
 * Removes a key from the browser storage via element click
 =========================================================*/

App.directive('resetKey',  ['$state','$rootScope', function($state, $rootScope) {
    'use strict';

    return {
        restrict: 'A',
        scope: {
            resetKey: '='
        },
        link: function(scope, element, attrs) {

            scope.resetKey = attrs.resetKey;

        },
        controller: function($scope, $element) {

            $element.on('click', function (e) {
                e.preventDefault();

                if($scope.resetKey) {
                    delete $rootScope.$storage[$scope.resetKey];
                    $state.go($state.current, {}, {reload: true});
                }
                else {
                    $.error('No storage key specified for reset.');
                }
            });

        }

    };

}]);
/**=========================================================
 * Module: filestyle.js
 * Initializes the fielstyle plugin
 =========================================================*/

App.directive('filestyle', function() {
    return {
        restrict: 'A',
        controller: function($scope, $element) {
            var $elem = $($element);
            $elem.filestyle({
                classInput: $elem.data('classinput')
            });
        }
    };
});

/**=========================================================
 * Module: flatdoc.js
 * Creates the flatdoc markup and initializes the plugin
 =========================================================*/

App.directive('flatdoc', ['$location', function($location) {
    return {
        restrict: "EA",
        template: "<div role='flatdoc'><div role='flatdoc-menu'></div><div role='flatdoc-content'></div></div>",
        link: function(scope, element, attrs) {

            Flatdoc.run({
                fetcher: Flatdoc.file(attrs.src)
            });

            var $root = $('html, body');
            $(document).on('flatdoc:ready', function() {
                var docMenu = $('[role="flatdoc-menu"]');
                docMenu.find('a').on('click', function(e) {
                    e.preventDefault(); e.stopPropagation();

                    var $this = $(this);

                    docMenu.find('a.active').removeClass('active');
                    $this.addClass('active');

                    $root.animate({
                        scrollTop: $(this.getAttribute('href')).offset().top - ($('.topnavbar').height() + 10)
                    }, 800);
                });

            });
        }
    };

}]);
/**=========================================================
 * Module: form-wizard.js
 * Handles form wizard plugin and validation
 =========================================================*/

App.directive('formWizard', function(){
    'use strict';

    if(!$.fn.bwizard) return;

    return {
        restrict: 'EA',
        link: function(scope, element, attrs) {
            var wizard = $(element).children('.form-wizard'),
                validate = attrs.validateStep; // allow to set options via data-* attributes

            if(validate) {
                wizard.bwizard({
                    clickableSteps: false,
                    validating: function(e, ui) {

                        var $this = $(this),
                            form = $this.parent(),
                            group = form.find('.bwizard-activated');

                        if (false === form.parsley().validate( group[0].id )) {
                            e.preventDefault();
                            return;
                        }
                    }
                });
            }
            else {
                wizard.bwizard();
            }

        }
    };

});

/**=========================================================
 * Module: fullscreen.js
 * Toggle the fullscreen mode on/off
 =========================================================*/

App.directive('toggleFullscreen', function() {
    'use strict';

    return {
        restrict: 'A',
        link: function(scope, element, attrs) {

            element.on('click', function (e) {
                e.preventDefault();

                if (screenfull.enabled) {

                    screenfull.toggle();

                    // Switch icon indicator
                    if(screenfull.isFullscreen)
                        $(this).children('em').removeClass('fa-expand').addClass('fa-compress');
                    else
                        $(this).children('em').removeClass('fa-compress').addClass('fa-expand');

                } else {
                    $.error('Fullscreen not enabled');
                }

            });
        }
    };

});


/**=========================================================
 * Module: load-css.js
 * Request and load into the current page a css file
 =========================================================*/

App.directive('loadCss', function() {
    'use strict';

    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.on('click', function (e) {
                if(element.is('a')) e.preventDefault();
                var uri = attrs.loadCss,
                    link;

                if(uri) {
                    link = createLink(uri);
                    if ( !link ) {
                        $.error('Error creating stylesheet link element.');
                    }
                }
                else {
                    $.error('No stylesheet location defined.');
                }

            });

        }
    };

    function createLink(uri) {
        var linkId = 'autoloaded-stylesheet',
            oldLink = $('#'+linkId).attr('id', linkId + '-old');

        $('head').append($('<link/>').attr({
            'id':   linkId,
            'rel':  'stylesheet',
            'href': uri
        }));

        if( oldLink.length ) {
            oldLink.remove();
        }

        return $('#'+linkId);
    }


});


/**=========================================================
 * Module: masked,js
 * Initializes the masked inputs
 =========================================================*/

App.directive('masked', function() {
    return {
        restrict: 'A',
        controller: function($scope, $element) {
            var $elem = $($element);
            if($.fn.inputmask)
                $elem.inputmask();
        }
    };
});

/**=========================================================
 * Module: navbar-search.js
 * Navbar search toggler * Auto dismiss on ESC key
 =========================================================*/

App.directive('searchOpen', ['navSearch', function(navSearch) {
        'use strict';

        return {
            restrict: 'A',
            controller: function($scope, $element) {
                $element
                    .on('click', function (e) { e.stopPropagation(); })
                    .on('click', navSearch.toggle);
            }
        };

    }]).directive('searchDismiss', ['navSearch', function(navSearch) {
        'use strict';

        var inputSelector = '.navbar-form input[type="text"]';

        return {
            restrict: 'A',
            controller: function($scope, $element) {

                $(inputSelector)
                    .on('click', function (e) { e.stopPropagation(); })
                    .on('keyup', function(e) {
                        if (e.keyCode == 27) // ESC
                            navSearch.dismiss();
                    });

                // click anywhere closes the search
                $(document).on('click', navSearch.dismiss);
                // dismissable options
                $element
                    .on('click', function (e) { e.stopPropagation(); })
                    .on('click', navSearch.dismiss);
            }
        };

    }]);


/**=========================================================
 * Module: notify.js
 * Create a notifications that fade out automatically.
 * Based on Notify addon from UIKit (http://getuikit.com/docs/addons_notify.html)
 =========================================================*/

App.directive('notify', function($window){

    return {
        restrict: 'A',
        controller: function ($scope, $element) {

            $element.on('click', function (e) {
                e.preventDefault();
                notifyNow($element);
            });

        }
    };

    function notifyNow(elem) {
        var $element = $(elem),
            message = $element.data('message'),
            options = $element.data('options');

        if(!message)
            $.error('Notify: No message specified');

        $.notify(message, options || {});
    }


});


/**
 * Notify Addon definition as jQuery plugin
 * Adapted version to work with Bootstrap classes
 * More information http://getuikit.com/docs/addons_notify.html
 */

(function($, window, document){

    var containers = {},
        messages   = {},

        notify     =  function(options){

            if ($.type(options) == 'string') {
                options = { message: options };
            }

            if (arguments[1]) {
                options = $.extend(options, $.type(arguments[1]) == 'string' ? {status:arguments[1]} : arguments[1]);
            }

            return (new Message(options)).show();
        },
        closeAll  = function(group, instantly){
            if(group) {
                for(var id in messages) { if(group===messages[id].group) messages[id].close(instantly); }
            } else {
                for(var id in messages) { messages[id].close(instantly); }
            }
        };

    var Message = function(options){

        var $this = this;

        this.options = $.extend({}, Message.defaults, options);

        this.uuid    = "ID"+(new Date().getTime())+"RAND"+(Math.ceil(Math.random() * 100000));
        this.element = $([
            // @geedmo: alert-dismissable enables bs close icon
            '<div class="uk-notify-message alert-dismissable">',
            '<a class="close">&times;</a>',
            '<div>'+this.options.message+'</div>',
            '</div>'

        ].join('')).data("notifyMessage", this);

        // status
        if (this.options.status) {
            this.element.addClass('alert alert-'+this.options.status);
            this.currentstatus = this.options.status;
        }

        this.group = this.options.group;

        messages[this.uuid] = this;

        if(!containers[this.options.pos]) {
            containers[this.options.pos] = $('<div class="uk-notify uk-notify-'+this.options.pos+'"></div>').appendTo('body').on("click", ".uk-notify-message", function(){
                $(this).data("notifyMessage").close();
            });
        }
    };


    $.extend(Message.prototype, {

        uuid: false,
        element: false,
        timout: false,
        currentstatus: "",
        group: false,

        show: function() {

            if (this.element.is(":visible")) return;

            var $this = this;

            containers[this.options.pos].show().prepend(this.element);

            var marginbottom = parseInt(this.element.css("margin-bottom"), 10);

            this.element.css({"opacity":0, "margin-top": -1*this.element.outerHeight(), "margin-bottom":0}).animate({"opacity":1, "margin-top": 0, "margin-bottom":marginbottom}, function(){

                if ($this.options.timeout) {

                    var closefn = function(){ $this.close(); };

                    $this.timeout = setTimeout(closefn, $this.options.timeout);

                    $this.element.hover(
                        function() { clearTimeout($this.timeout); },
                        function() { $this.timeout = setTimeout(closefn, $this.options.timeout);  }
                    );
                }

            });

            return this;
        },

        close: function(instantly) {

            var $this    = this,
                finalize = function(){
                    $this.element.remove();

                    if(!containers[$this.options.pos].children().length) {
                        containers[$this.options.pos].hide();
                    }

                    delete messages[$this.uuid];
                };

            if(this.timeout) clearTimeout(this.timeout);

            if(instantly) {
                finalize();
            } else {
                this.element.animate({"opacity":0, "margin-top": -1* this.element.outerHeight(), "margin-bottom":0}, function(){
                    finalize();
                });
            }
        },

        content: function(html){

            var container = this.element.find(">div");

            if(!html) {
                return container.html();
            }

            container.html(html);

            return this;
        },

        status: function(status) {

            if(!status) {
                return this.currentstatus;
            }

            this.element.removeClass('alert alert-'+this.currentstatus).addClass('alert alert-'+status);

            this.currentstatus = status;

            return this;
        }
    });

    Message.defaults = {
        message: "",
        status: "normal",
        timeout: 5000,
        group: null,
        pos: 'top-center'
    };


    $["notify"]          = notify;
    $["notify"].message  = Message;
    $["notify"].closeAll = closeAll;

    return notify;

}(jQuery, window, document));

/**=========================================================
 * Module: now.js
 * Provides a simple way to display the current time formatted
 =========================================================*/

App.directive("now", ['dateFilter', '$interval', function(dateFilter, $interval){
    return {
        restrict: 'E',
        link: function(scope, element, attrs){

            var format = attrs.format;

            function updateTime() {
                var dt = dateFilter(new Date(), format);
                element.text(dt);
            }

            updateTime();
            $interval(updateTime, 1000);
        }
    };
}]);
/**=========================================================
 * Module panel-tools.js
 * Directive tools to control panels.
 * Allows collapse, refresh and dismiss (remove)
 * Saves panel state in browser storage
 =========================================================*/

App.directive('paneltool', function(){
    var templates = {
        /* jshint multistr: true */
        collapse:"<a href='#' panel-collapse='' data-toggle='tooltip' title='Collapse Panel' ng-click='{{panelId}} = !{{panelId}}' ng-init='{{panelId}}=false'> \
                <em ng-show='{{panelId}}' class='fa fa-plus'></em> \
                <em ng-show='!{{panelId}}' class='fa fa-minus'></em> \
              </a>",
        dismiss: "<a href='#' panel-dismiss='' data-toggle='tooltip' title='Close Panel'>\
               <em class='fa fa-times'></em>\
             </a>",
        refresh: "<a href='#' panel-refresh='' data-toggle='tooltip' data-spinner='{{spinner}}' title='Refresh Panel'>\
               <em class='fa fa-refresh'></em>\
             </a>"
    };

    return {
        restrict: 'E',
        template: function( elem, attrs ){
            var temp = '';
            if(attrs.toolCollapse)
                temp += templates.collapse.replace(/{{panelId}}/g, (elem.parent().parent().attr('id')) );
            if(attrs.toolDismiss)
                temp += templates.dismiss;
            if(attrs.toolRefresh)
                temp += templates.refresh.replace(/{{spinner}}/g, attrs.toolRefresh);
            return temp;
        },
        // scope: true,
        // transclude: true,
        link: function (scope, element, attrs) {
            element.addClass('pull-right');
        }
    };
})
/**=========================================================
 * Dismiss panels * [panel-dismiss]
 =========================================================*/
    .directive('panelDismiss', function(){
        'use strict';
        return {
            restrict: 'A',
            controller: function ($scope, $element) {
                var removeEvent   = 'panel-remove',
                    removedEvent  = 'panel-removed';

                $element.on('click', function () {

                    // find the first parent panel
                    var parent = $(this).closest('.panel');

                    if($.support.animation) {
                        parent.animo({animation: 'bounceOut'}, removeElement);
                    }
                    else removeElement();

                    function removeElement() {
                        // Trigger the event and finally remove the element
                        $.when(parent.trigger(removeEvent, [parent]))
                            .done(destroyPanel);
                    }

                    function destroyPanel() {
                        var col = parent.parent();
                        parent.remove();
                        // remove the parent if it is a row and is empty and not a sortable (portlet)
                        col
                            .trigger(removedEvent) // An event to catch when the panel has been removed from DOM
                            .filter(function() {
                                var el = $(this);
                                return (el.is('[class*="col-"]:not(.sortable)') && el.children('*').length === 0);
                            }).remove();

                    }
                });
            }
        };
    })
/**=========================================================
 * Collapse panels * [panel-collapse]
 =========================================================*/
    .directive('panelCollapse', ['$timeout', function($timeout){
        'use strict';

        var storageKeyName = 'panelState',
            storage;

        return {
            restrict: 'A',
            // transclude: true,
            controller: function ($scope, $element) {

                // Prepare the panel to be collapsible
                var $elem   = $($element),
                    parent  = $elem.closest('.panel'), // find the first parent panel
                    panelId = parent.attr('id');

                storage = $scope.$storage;

                // Load the saved state if exists
                var currentState = loadPanelState( panelId );
                if ( typeof currentState !== undefined) {
                    $timeout(function(){
                            $scope[panelId] = currentState; },
                        10);
                }

                // bind events to switch icons
                $element.bind('click', function() {

                    savePanelState( panelId, !$scope[panelId] );

                });
            }
        };

        function savePanelState(id, state) {
            if(!id) return false;
            var data = angular.fromJson(storage[storageKeyName]);
            if(!data) { data = {}; }
            data[id] = state;
            storage[storageKeyName] = angular.toJson(data);
        }

        function loadPanelState(id) {
            if(!id) return false;
            var data = angular.fromJson(storage[storageKeyName]);
            if(data) {
                return data[id];
            }
        }

    }])
/**=========================================================
 * Refresh panels
 * [panel-refresh] * [data-spinner="standard"]
 =========================================================*/
    .directive('panelRefresh', function(){
        'use strict';

        return {
            restrict: 'A',
            controller: function ($scope, $element) {

                var refreshEvent   = 'panel-refresh',
                    csspinnerClass = 'csspinner',
                    defaultSpinner = 'standard';

                // method to clear the spinner when done
                function removeSpinner() {
                    this.removeClass(csspinnerClass);
                }

                // catch clicks to toggle panel refresh
                $element.on('click', function () {
                    var $this   = $(this),
                        panel   = $this.parents('.panel').eq(0),
                        spinner = $this.data('spinner') || defaultSpinner
                        ;

                    // start showing the spinner
                    panel.addClass(csspinnerClass + ' ' + spinner);

                    // attach as public method
                    panel.removeSpinner = removeSpinner;

                    // Trigger the event and send the panel object
                    $this.trigger(refreshEvent, [panel]);

                });

            }
        };
    });



/**=========================================================
 * Module: play-animation.js
 * Provides a simple way to run animation with a trigger
 * Requires animo.js
 =========================================================*/

App.directive('animate', function($window){

    'use strict';

    var $scroller = $(window).add('body, .wrapper');

    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {

            // Parse animations params and attach trigger to scroll
            var $elem     = $(elem),
                offset    = $elem.data('offset'),
                delay     = $elem.data('delay')     || 100, // milliseconds
                animation = $elem.data('play')      || 'bounce';

            if(typeof offset !== 'undefined') {

                // test if the element starts visible
                testAnimation($elem);
                // test on scroll
                $scroller.scroll(function(){
                    testAnimation($elem);
                });

            }

            // Test an element visibilty and trigger the given animation
            function testAnimation(element) {
                if ( !element.hasClass('anim-running') &&
                    $.Utils.isInView(element, {topoffset: offset})) {
                    element
                        .addClass('anim-running');

                    setTimeout(function() {
                        element
                            .addClass('anim-done')
                            .animo( { animation: animation, duration: 0.7} );
                    }, delay);

                }
            }

            // Run click triggered animations
            $elem.on('click', function() {

                var $elem     = $(this),
                    targetSel = $elem.data('target'),
                    animation = $elem.data('play') || 'bounce',
                    target    = $(targetSel);

                if(target && target) {
                    target.animo( { animation: animation } );
                }

            });
        }
    };

});

/**=========================================================
 * Module: scroll.js
 * Make a content box scrollable
 =========================================================*/

App.directive('scrollable', function(){
    return {
        restrict: 'EA',
        link: function(scope, elem, attrs) {
            var defaultHeight = 250;
            elem.slimScroll({
                height: (attrs.height || defaultHeight)
            });
        }
    };
});
/**=========================================================
 * Module: sidebar.js
 * Wraps the sidebar and handles collapsed state
 =========================================================*/

App.directive('sidebar', ['$window', 'APP_MEDIAQUERY', function($window, mq) {

    var $win  = $($window);
    var $html = $('html');
    var $body = $('body');
    var $scope;
    var $sidebar;

    return {
        restrict: 'EA',
        template: '<nav class="sidebar" ng-transclude></nav>',
        transclude: true,
        replace: true,
        link: function(scope, element, attrs) {

            $scope   = scope;
            $sidebar = element;

            var eventName = isTouch() ? 'click' : 'mouseenter' ;
            $sidebar.on( eventName, '.nav > li', function() {
                if( isSidebarCollapsed() && !isMobile() )
                    toggleMenuItem( $(this) );
            });

            scope.$on('closeSidebarMenu', function() {
                removeFloatingNav();
                $('.sidebar li.open').removeClass('open');
            });
        }
    };


    // Open the collapse sidebar submenu items when on touch devices
    // - desktop only opens on hover
    function toggleTouchItem($element){
        $element
            .siblings('li')
            .removeClass('open')
            .end()
            .toggleClass('open');
    }

    // Handles hover to open items under collapsed menu
    // -----------------------------------
    function toggleMenuItem($listItem) {

        removeFloatingNav();

        var ul = $listItem.children('ul');

        if( !ul.length ) return;
        if( $listItem.hasClass('open') ) {
            toggleTouchItem($listItem);
            return;
        }

        var $aside = $('.aside');
        var mar =  $scope.app.layout.isFixed ?  parseInt( $aside.css('margin-top'), 0) : 0;

        var subNav = ul.clone().appendTo( $aside );

        toggleTouchItem($listItem);

        var itemTop = ($listItem.position().top + mar) - $sidebar.scrollTop();
        var vwHeight = $win.height();

        subNav
            .addClass('nav-floating')
            .css({
                position: $scope.app.layout.isFixed ? 'fixed' : 'absolute',
                top:      itemTop,
                bottom:   (subNav.outerHeight(true) + itemTop > vwHeight) ? 0 : 'auto'
            });

        subNav.on('mouseleave', function() {
            toggleTouchItem($listItem);
            subNav.remove();
        });

    }

    function removeFloatingNav() {
        $('.sidebar-subnav.nav-floating').remove();
    }

    function isTouch() {
        return $html.hasClass('touch');
    }
    function isSidebarCollapsed() {
        return $body.hasClass('aside-collapsed');
    }
    function isSidebarToggled() {
        return $body.hasClass('aside-toggled');
    }
    function isMobile() {
        return $win.width() < mq.tablet;
    }

}]);


/**=========================================================
 * Module: table-checkall.js
 * Tables check all checkbox
 =========================================================*/

App.directive('checkAll', function() {
    'use strict';

    return {
        restrict: 'A',
        controller: function($scope, $element){

            $element.on('change', function() {
                var $this = $(this),
                    index= $this.index() + 1,
                    checkbox = $this.find('input[type="checkbox"]'),
                    table = $this.parents('table');
                // Make sure to affect only the correct checkbox column
                table.find('tbody > tr > td:nth-child('+index+') input[type="checkbox"]')
                    .prop('checked', checkbox[0].checked);

            });
        }
    };

});
/**=========================================================
 * Module: tags-input.js
 * Initializes the tag inputs plugin
 =========================================================*/

App.directive('tagsinput', function() {
    return {
        restrict: 'A',
        controller: function($scope, $element) {
            var $elem = $($element);
            if($.fn.tagsinput)
                $elem.tagsinput();
        }
    };
});

/**=========================================================
 * Module: toggle-state.js
 * Toggle a classname from the BODY Useful to change a state that
 * affects globally the entire layout or more than one item
 * Targeted elements must have [toggle-state="CLASS-NAME-TO-TOGGLE"]
 * User no-persist to avoid saving the sate in browser storage
 =========================================================*/

App.directive('toggleState', ['toggleStateService', function(toggle) {
    'use strict';

    return {
        restrict: 'A',
        link: function(scope, element, attrs) {

            var $body = $('body');

            $(element)
                .on('click', function (e) {
                    e.preventDefault();
                    var classname = attrs.toggleState;

                    if(classname) {
                        if( $body.hasClass(classname) ) {
                            $body.removeClass(classname);
                            if( ! attrs.noPersist)
                                toggle.removeState(classname);
                        }
                        else {
                            $body.addClass(classname);
                            if( ! attrs.noPersist)
                                toggle.addState(classname);
                        }

                    }

                });
        }
    };

}]);

/**=========================================================
 * Module: masked,js
 * Initializes the jQuery UI slider controls
 =========================================================*/

App.directive('uiSlider', function() {
    return {
        restrict: 'A',
        controller: function($scope, $element) {
            var $elem = $($element);
            if($.fn.slider)
                $elem.slider();
        }
    };
});

/**=========================================================
 * Module: validate-form.js
 * Initializes the validation plugin Parsley
 =========================================================*/

App.directive('validateForm', function() {
    return {
        restrict: 'A',
        controller: function($scope, $element) {
            var $elem = $($element);
            if($.fn.parsley)
                $elem.parsley();
        }
    };
});


App.service('browser', function(){
    "use strict";

    var matched, browser;

    var uaMatch = function( ua ) {
        ua = ua.toLowerCase();

        var match = /(opr)[\/]([\w.]+)/.exec( ua ) ||
            /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
            /(version)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec( ua ) ||
            /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
            /(msie) ([\w.]+)/.exec( ua ) ||
            ua.indexOf("trident") >= 0 && /(rv)(?::| )([\w.]+)/.exec( ua ) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
            [];

        var platform_match = /(ipad)/.exec( ua ) ||
            /(iphone)/.exec( ua ) ||
            /(android)/.exec( ua ) ||
            /(windows phone)/.exec( ua ) ||
            /(win)/.exec( ua ) ||
            /(mac)/.exec( ua ) ||
            /(linux)/.exec( ua ) ||
            /(cros)/i.exec( ua ) ||
            [];

        return {
            browser: match[ 3 ] || match[ 1 ] || "",
            version: match[ 2 ] || "0",
            platform: platform_match[ 0 ] || ""
        };
    };

    matched = uaMatch( window.navigator.userAgent );
    browser = {};

    if ( matched.browser ) {
        browser[ matched.browser ] = true;
        browser.version = matched.version;
        browser.versionNumber = parseInt(matched.version);
    }

    if ( matched.platform ) {
        browser[ matched.platform ] = true;
    }

    // These are all considered mobile platforms, meaning they run a mobile browser
    if ( browser.android || browser.ipad || browser.iphone || browser[ "windows phone" ] ) {
        browser.mobile = true;
    }

    // These are all considered desktop platforms, meaning they run a desktop browser
    if ( browser.cros || browser.mac || browser.linux || browser.win ) {
        browser.desktop = true;
    }

    // Chrome, Opera 15+ and Safari are webkit based browsers
    if ( browser.chrome || browser.opr || browser.safari ) {
        browser.webkit = true;
    }

    // IE11 has a new token so we will assign it msie to avoid breaking changes
    if ( browser.rv )
    {
        var ie = "msie";

        matched.browser = ie;
        browser[ie] = true;
    }

    // Opera 15+ are identified as opr
    if ( browser.opr )
    {
        var opera = "opera";

        matched.browser = opera;
        browser[opera] = true;
    }

    // Stock Android browsers are marked as Safari on Android.
    if ( browser.safari && browser.android )
    {
        var android = "android";

        matched.browser = android;
        browser[android] = true;
    }

    // Assign the name and platform variable
    browser.name = matched.browser;
    browser.platform = matched.platform;


    return browser;

});
/**=========================================================
 * Module: colors.js
 * Services to retrieve global colors
 =========================================================*/

App.factory('colors', ['APP_COLORS', function(colors) {

    return {
        byName: function(name) {
            return (colors[name] || '#fff');
        }
    };

}]);


/**=========================================================
 * Module: nav-search.js
 * Services to share navbar search functions
 =========================================================*/

App.service('navSearch', function() {
    var navbarFormSelector = 'form.navbar-form';
    return {
        toggle: function() {

            var navbarForm = $(navbarFormSelector);

            navbarForm.toggleClass('open');

            var isOpen = navbarForm.hasClass('open');

            navbarForm.find('input')[isOpen ? 'focus' : 'blur']();

        },

        dismiss: function() {
            $(navbarFormSelector)
                .removeClass('open')      // Close control
                .find('input[type="text"]').blur() // remove focus
                .val('')                    // Empty input
            ;
        }
    };

});
/**=========================================================
 * Module: toggle-state.js
 * Services to share toggle state functionality
 =========================================================*/

App.service('toggleStateService', ['$rootScope', function($rootScope) {

    var storageKeyName  = 'toggleState';

    // Helper object to check for words in a phrase //
    var WordChecker = {
        hasWord: function (phrase, word) {
            return new RegExp('(^|\\s)' + word + '(\\s|$)').test(phrase);
        },
        addWord: function (phrase, word) {
            if (!this.hasWord(phrase, word)) {
                return (phrase + (phrase ? ' ' : '') + word);
            }
        },
        removeWord: function (phrase, word) {
            if (this.hasWord(phrase, word)) {
                return phrase.replace(new RegExp('(^|\\s)*' + word + '(\\s|$)*', 'g'), '');
            }
        }
    };

    // Return service public methods
    return {
        // Add a state to the browser storage to be restored later
        addState: function(classname){
            var data = angular.fromJson($rootScope.$storage[storageKeyName]);

            if(!data)  {
                data = classname;
            }
            else {
                data = WordChecker.addWord(data, classname);
            }

            $rootScope.$storage[storageKeyName] = angular.toJson(data);
        },

        // Remove a state from the browser storage
        removeState: function(classname){
            var data = $rootScope.$storage[storageKeyName];
            // nothing to remove
            if(!data) return;

            data = WordChecker.removeWord(data, classname);

            $rootScope.$storage[storageKeyName] = angular.toJson(data);
        },

        // Load the state string and restore the classlist
        restoreState: function($elem) {
            var data = angular.fromJson($rootScope.$storage[storageKeyName]);

            // nothing to restore
            if(!data) return;
            $elem.addClass(data);
        }

    };

}]);



/**
 * Form Helper
 * <whform type="text" label="Rubrique : " placeholder="Votre nom de rubrique" ng-model="data.Rub.name" error="error.name"></whform>
 */
App.directive("whform", [function() {

    return {
        restrict: 'EA',
        transclude: true,
        require: 'ngModel',
        scope : {
            ngModel     : '=',
            label       : '@',
            placeholder : '@',
            error       : '=',
            options     : '=',
            tagClass    : '@',
            legende     : '@',
            mask        : '@',
            disabled    : '@',
            type        : '@',
            inputStyle  : '@',
            append      : '@',
            required    : '@',
            inputmask   : '@',
            obj         : '@'
        },
        templateUrl: function(elem, attr){

            var tag = (attr.tag) ? attr.tag : 'input';

            if(attr.options) tag = 'select';

            return WH_ROOT + '/app_admin/partials/helper-form/bootstrap/' + tag + '.html';

        },
        link: function(scope, element, attrs, ngModel) {

            if(attrs.inputmask) {

                var el = $('input', element);
                el.inputmask(attrs.inputmask);
            }
        }
    };
}]);