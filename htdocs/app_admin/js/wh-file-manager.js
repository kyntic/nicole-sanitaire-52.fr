
/**=========================================================
 * Module: FileManagerController
 *
 *
 =========================================================*/
App.controller('FileManagerController', ['$scope' , '$log', '$modal', '$timeout', '$parse', function($scope, $log, $modal, $timeout, $parse) {

    $scope.openFileManager = function (model, nbr) {

        var modalInstance = $modal.open({
            templateUrl: WH_ROOT + '/app_admin/views/file-manager/index.html',
            controller: FileManagerInstanceCtrl,
            size: 'lg',
            resolve: {
                nbr: function () {
                    return nbr;
                }
            }
        });

        modalInstance.result.then(function (selectedFiles) {

            $timeout(function() {

                if (nbr == 1) {

                    $parse(model).assign($scope, selectedFiles[0]);

                } else {

                    var val = $parse(model);
                    var val = val($scope);

                    angular.forEach(selectedFiles, function(v) {

                        val.push(v);
                    });

                    $parse(model).assign($scope, val);
                }

                $scope.$apply();
            });
        }, function () {
            console.log('Pas de fichier séléctionné');
        });
    };

    // Please note that $modalInstance represents a modal window (instance) dependency.
    // It is not the same as the $modal service used above.
    var FileManagerInstanceCtrl = function ($scope, $modalInstance, nbr) {

        $scope.nbr = nbr;

        $scope.selectedFiles = [];

        $scope.ok = function () {

            $modalInstance.close($scope.selectedFiles, nbr);

            //$modalInstance.close('closed');
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.sel = function (file) {

            $scope.selectedFiles.push(file);

            $modalInstance.close($scope.selectedFiles, nbr);
        }
    };
}]);

/**=========================================================
 * Module: Dans la popup :
 * File Upload & File library
 *
 =========================================================*/
App.controller('FileUploadController', ['$scope' , '$log', function($scope, $log) {





}]);

App.controller('FileLibraryController', ['$scope' , '$log', '$timeout', '$http', 'toaster', function($scope, $log, $timeout, $http, toaster) {

    'use strict';

    /**
     * Liste des fichiers
     * @type {Array}
     */
    $scope.Files = [];

    $scope.folder_id = null;

    $scope.loadLibrary = function(dossier_id) {

        $scope.loadingFiles = true;

        $http.get(WH_ROOT + '/admin/file_manager/files/index/' + dossier_id, {
            headers :{
                'Content-type': 'application/json'
            }
        }).success(function(data) {

                $scope.Files    = data.Files;

                $scope.Folder   = data.Folder;
                $scope.Chemin   = data.Chemin;

                if (data.Folder.FileFolder) {

                    $scope.folder_id = data.Folder.FileFolder.id;

                } else{
                    $scope.folder_id = 0;
                }

                $scope.loadingFiles = false;

                prepare_upload();

            }).error(function(data) {

                $scope.loadingFiles = false;
            });
    };

    /**
     * On stocke le dernier dossier en session
     */
    $scope.loadLibrary(0);


    /**
     * Les dossiers
     */

    $scope.visible = function(item) {
        if ($scope.query && $scope.query.length > 0
            && item.Page.name.indexOf($scope.query) == -1) {
            return false;
        }
        return true;
    };

    $scope.findNodes = function(){

    };

    $scope.loadFolders = function() {

        $scope.loadingFolder = true;

        $http.get(WH_ROOT + '/admin/file_manager/files/folders/')
            .success(function(data) {

                $scope.folders = data.tree;

                $scope.FoldersList = data.list;

                $scope.loadingFolder = false;

            })
            .error(function() {

                $scope.loadingFolder = false;

            });


    };

    $scope.loadFolders();

    /**
     * Create folder
     */
    $scope.create_folder = function () {

        $scope.loadingNewFolder = true;

        if (!$scope.newFolder.FileFolder.edit) {

            $scope.newFolder.FileFolder.parent_id = $scope.Folder.FileFolder.id;
        }

        $http.post(WH_ROOT + '/admin/file_manager/files/new_folder/', $scope.newFolder)
            .success(function(data) {

                $scope.loadLibrary(data.id);
                $scope.newFolder.FileFolder.name = '';
                $scope.newFolder.FileFolder.edit = false;

                $scope.loadingNewFolder = false;

                $scope.loadFolders();
            })
            .error(function(data) {

            });
    };

    /**
     * Partie Upload :
     * @type {Array}
     */
    $scope.fileUploadList = [];

    $scope.removeFile = function(index) {
        $scope.fileUploadList.splice(index, 1);
    };

    var prepare_upload = function() {

            var progressbar = $('#progressbar'),
                bar         = progressbar.find('.progress-bar'),
                settings    = {

                    action: WH_ROOT + '/admin/file_manager/files/upload/' + $scope.folder_id, // upload url

                    allow : '*.(jpg|jpeg|gif|png|doc|docx|pdf|zip|xls|xlsx|mp3|mp4|avi|flv|webm)', // allow only images

                    data: {folder_id: $scope.folder_id},

                    param: 'upfile',

                    loadstart: function() {
                        bar.css('width', '0%').text('0%');
                        progressbar.removeClass('hidden');
                    },

                    progress: function(percent) {
                        percent = Math.ceil(percent);
                        bar.css('width', percent+'%').text(percent+'%');
                    },

                    allcomplete: function(response) {

                        var data = response && angular.fromJson(response);
                        bar.css('width', '100%').text('100%');

                        setTimeout(function(){
                            progressbar.addClass('hidden');
                        }, 250);

                        // Upload Completed
                        if(data && data.File.name) {
                            $scope.$apply(function() {

                                $scope.loadLibrary($scope.folder_id);

                            });
                        }
                    }
                };

            var select = new $.upload.select($('#upload-select'), settings),
                drop   = new $.upload.drop($('#upload-drop'), settings);

    };

    prepare_upload();


    $scope.editFile = function (f) {

        $scope.dataFile = f;
        $scope.formEditFile = true;
    };

    $scope.enrFile = function (f) {

        $scope.loadingFiles = true;

        $http.post(WH_ROOT + '/admin/file_manager/files/edit', $scope.dataFile)
            .success(function(data) {

                $scope.formEditFile = false;

                $scope.loadingFiles = false;

                $scope.loadLibrary($scope.dataFile.File.file_folder_id);


            })
            .error(function(error){

                $scope.log = error;
            });
    };

    $scope.deleteFile = function ($index) {


        if(confirm('Etes vous sûre de vouloir supprimer ce fichier ? ')) {


            $http.get(WH_ROOT + '/admin/file_manager/files/delete/' + $scope.Files[$index].File.id)
                .success(function(data) {

                    var folder_id = $scope.Files[$index].File.file_folder_id;

                    $scope.Files.splice($index, 1);

                    //$scope.loadLibrary(folder_id);

                })
                .error(function(error) {

                    $scope.log = error;
                });
        }
    };

    /**
     * Edit the content of a folder
     *
     * @param f
     */
    $scope.editFolder = function(f)
    {
        $scope.newFolder = f;
        $scope.newFolder.FileFolder.edit = true;
    };

    /**
     * Delete folder
     *
     * @param f
     */
    $scope.deleteFolder = function(f)
    {
        if (confirm('Etes vous sûre de vouloir supprimer ce dossier ? ')) {

            $scope.loadingFolder = true;

            $http.get(WH_ROOT + '/admin/file_manager/files/delete_folder/' + f.FileFolder.id)
                .success(function(data) {

                    $scope.loadLibrary(f.FileFolder.parent_id);

                    $scope.loadFolders();

                    $scope.loadingFolder = false;
                })
                .error(function(error) {

                    $scope.log = error;

                    $scope.loadingFolder = false;
                });
        }
    };

    /**
     * Import files from deposit into current folder
     *
     * @param folder
     */
    $scope.importFilesFromDeposit = function(folder)
    {


        $http.get(WH_ROOT + '/admin/file_manager/files/import_from_deposit/' + folder.FileFolder.id)
            .success(function(data) {
                console.log(data);
                if (data.status == 1) {

                    toaster.pop('success', data.message);

                    $scope.loadLibrary(folder.FileFolder.id);

                } else {

                    toaster.pop('error', data.message);


                }
            })
            .error(function(reponse) {

                console.log(reponse);

                toaster.pop('error', 'Une erreur est survenue');
            });


    }
}]);
