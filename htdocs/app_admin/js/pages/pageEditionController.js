/**=========================================================
 * ACTU : EDITION
 =========================================================*/
App.controller(
    'PageEditionCtrl',
    ['$scope', '$http', '$state', '$log', '$rootScope', '$routeParams', 'toaster', 'Page', 'Typepage', 'Module', '$filter', '$modal',
        function ($scope, $http, $state, $log, $rootScope, $routeParams, toaster, Page, Typepage, Module, $filter, $modal) {

            /**
             * Calendrier
             */
            $scope.today = function () {
                $scope.dt = new Date();
            };

            $scope.clear = function () {
                $scope.dt = null;
            };

            $scope.open = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.opened = true;
            };

            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            $scope.Templates = Page.Templates;
            $scope.Menus = Page.Menus;

            /**
             * Données initiales
             *
             */
            $scope.Parents = [];
            $scope.data = [];

            Page.getList().then(
                function (data) {
                    $scope.Parents = data;

                }, function () {

                }
            );

            Typepage.getList().then(
                function (data) {
                    $scope.Templates = data;

                }, function () {

                }
            );

            $rootScope.loading = true;

            Page.locale = 'fre';

            Page.getById($state.params.id).then(function (data) {

                $scope.data = data;

                if ($state.params.parent_id) {

                    $scope.data.Page.parent_id = $state.params.parent_id;
                }

                if ($state.params.parent_id) {

                    getParent();
                }

                $rootScope.loading = false;

            }, function () {

                toaster.pop('warning', 'Attention', 'Impossible de retrouver la page');

                $rootScope.loading = false;
            });

            $rootScope.loading = true;

            // TypePAge.getRubriquesOptions().then(function (response) {

            //     if (response.statut == 1) {

            //         $scope.rubriquesOptions = response.data;

            //     } else {

            //         $scope.log = response;

            //     }

            //     $rootScope.loading = false;

            // }, function (error) {

            //     $scope.log = error;

            //     $rootScope.loading = false;

            // });

            /**
             * Si page parent appelée et mode création
             */
            var getParent = function () {

                Page.getById($state.params.parent_id).then(function (data) {

                    $scope.data.Chemin = data.Chemin;

                    $rootScope.loading = false;

                }, function () {

                    toaster.pop('warning', 'Attention', 'Impossible de retrouver la page');

                    $rootScope.loading = false;
                });
            };

            $scope.addGroup = function () {

                $modal.open({
                    templateUrl: 'app_admin/views/groups/add.html',
                    controller: function ($scope, $modalInstance, groups) {

                        /**
                         * Load the groups
                         */
                        $scope.groups = groups;
                        $scope.data = [];

                        /**
                         * Submit the form
                         */
                        $scope.addGroupSubmit = function () {

                            /**
                             * Returns the data
                             */
                            $modalInstance.close($scope.data);
                        };

                        $scope.cancel = function () {

                            $modalInstance.dismiss('cancel');
                        };
                    },
                    resolve: {
                        groups: function () {

                            /**
                             * Get the ldap groups
                             */
                            LdapGroup.getAll().then(
                                function (data) {

                                    /**
                                     * Store LdapGroups
                                     */
                                    $scope.LdapGroups = data;

                                }, function () {

                                }
                            );

                            return $scope.LdapGroups;
                        }
                    }
                }).result.then(function (data) {

                        console.log(data);

                        Right.addGroup(data).then(function (response) {

                            console.log(response);
                        }, function () {

                        });
                    }
                );
            };

            /**
             * Enregistrement
             */
            $scope.submit = function () {

                $scope.log = '';
                $rootScope.loading = true;

                Page.save($scope.data).then(function (reponse) {

                    $rootScope.loading = false;

                    if (reponse.ok) {

                        $scope.data.Page.id = reponse.id;

                        toaster.pop('success', 'Enregistrement', 'Enregistrement effectué');

                    } else {

                        $scope.log = reponse;

                        toaster.pop('warning', 'Attention', 'Une erreur est survenue');

                    }
                }, function (erreur) {

                    toaster.pop('danger', 'Erreur', erreur);

                    $scope.log = erreur.data;

                    $rootScope.loading = false;
                });
            };

            $scope.sel_lang = function () {

                $rootScope.loading = true;

                var locale = $scope.data.Page.locale;

                Page.locale = locale;

                /**
                 * On récupère le contenu dans l'autre locale
                 */
                Page.getById($scope.data.Page.id).then(
                    /**
                     * Si il y a on affiche, sinon on ne fait rien
                     *
                     * @param data
                     */
                        function (data) {

                        if (data.Page.id) {

                            $scope.data = data;
                            $scope.data.Page.locale = locale;
                        }

                        $rootScope.loading = false;
                    },
                    function () {

                        $rootScope.loading = false;
                    }
                );
            }
        }]);
