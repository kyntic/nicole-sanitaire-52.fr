
/**=========================================================
 * CONTROLLER Rights INDEX
 =========================================================*/
App.controller('RightsCtrl', ['$scope', function ($scope) {


}]);

App.controller('RightsIndexCtrl', ['$scope' , '$log', '$rootScope', '$http', 'toaster', function ($scope, $log, $rootScope, $http, toaster) {

    'use strict';

    /**
     * Liste des groups :
     * @type {Array}
     */
    $scope.Groups = [];

    $scope.load = function (){

        $rootScope.loading = true;

        $http.get(WH_ROOT + '/admin/right/rights/index').
            success(function(data) {

                $scope.Groups       = data.groups;
                $rootScope.loading  = false;

            }).
            error(function() {

                $rootScope.loading = false;
            });
    };

    /**
     * Import/Update the actions
     */
    $scope.whUpdateAros = function()
    {
        $http.get(WH_ROOT + '/admin/right/rights/update_aros')
            .success(function(data) {

                /**
                 * Success
                 */
                if (data.status == 1) {

                    /**
                     * Reload the scope
                     */
                    $scope.load();

                    /**
                     * Pop a message
                     */
                    toaster.pop('success', 'Opération réussie', data.message);
                }

                /**
                 * Error
                 */
                if (data.status == 0) {

                    toaster.pop('error', 'Echec de l\'opération', data.message);
                }
            })
            .error(function() {

                /**
                 * Pop a message
                 */
                toaster.pop('error', 'Echec de l\'opération', 'Le serveur n\'a pas répondu');
            });
    };

    /**
     * Import/Update the groups
     */
    $scope.whUpdateAcos = function()
    {
        $http.get(WH_ROOT + '/admin/right/rights/update_acos')
            .success(function(data) {

                /**
                 * Success
                 */
                if (data.status == 1) {

                    /**
                     * Reload the scope
                     */
                    $scope.load();

                    /**
                     * Pop a message
                     */
                    toaster.pop('success', 'Opération réussie', data.message);
                }

                /**
                 * Error
                 */
                if (data.status == 0) {

                    toaster.pop('error', 'Echec de l\'opération', data.message);
                }

            })
            .error(function() {

                /**
                 * Pop a message
                 */
                toaster.pop('error', 'Echec de l\'opération', 'Le serveur n\'a pas répondu');
            });
    };

    /**
     * Drop Actions/Groups
     */
    $scope.whDrop = function()
    {
        $http.get(WH_ROOT + '/admin/right/rights/drop')
            .success(function(data) {

                /**
                 * Success
                 */
                if (data.status == 1) {

                    /**
                     * Reload the scope
                     */
                    $scope.load();

                    /**
                     * Pop a message
                     */
                    toaster.pop('success', 'Opération réussie', data.message);
                }

                /**
                 * Error
                 */
                if (data.status == 0) {

                    toaster.pop('error', 'Echec de l\'opération', data.message);
                }
            })
            .error(function() {

                /**
                 * Pop a message
                 */
                toaster.pop('error', 'Echec de l\'opération', 'Le serveur n\'a pas répondu');
            });
    };

    /**
     * Drop all the permissions
     */
    $scope.whDropPermissions = function()
    {
        $http.get(WH_ROOT + '/admin/right/rights/drop_permissions')
            .success(function(data) {

                /**
                 * Success
                 */
                if (data.status == 1) {

                    /**
                     * Reload the scope
                     */
                    $scope.load();

                    /**
                     * Pop a message
                     */
                    toaster.pop('success', 'Opération réussie', data.message);
                }

                /**
                 * Error
                 */
                if (data.status == 0) {

                    toaster.pop('error', 'Echec de l\'opération', data.message);
                }
            })
            .error(function() {

                /**
                 * Pop a message
                 */
                toaster.pop('error', 'Echec de l\'opération', 'Le serveur n\'a pas répondu');
            });
    };

    $scope.load();
}]);

/**
 * Rights Edition Controller
 */
App.controller('RightsEditCtrl', ['$scope' , '$log', '$rootScope', '$http', 'toaster', '$state', function ($scope, $log, $rootScope, $http, toaster, $state) {

    'use strict';

    $rootScope.loading = true;

    $http.get(WH_ROOT + '/admin/right/rights/edit/' + $state.params.id)
        .success(function(data) {

            /**
             * Assign the data to the scope
             */
            $scope.Aro  = data.Aro;
            $scope.Acos = data.Acos;

            $rootScope.loading = false;
        })
        .error(function() {

            $rootScope.loading = false;
        });

    /**
     * Grant rights
     */
    $scope.whGrant = function($aroId, $acoId, $action)
    {
        /**
         * The data we send
         *
         * @type {{action: *, aroId: *, acoId: *}}
         */
        var dataToSend = {
            'actionPath'    : $action,
            'aroId'         : $aroId,
            'acoId'         : $acoId,
            'action'        : 'grant'
        };

        $http.post(WH_ROOT + '/admin/right/rights/update_rights/', dataToSend)
            .success(function(data) {

                /**
                 * Pop a message
                 */
                toaster.pop('success', 'Opération réussie', data.message);

            })
            .error(function(){
                toaster.pop('error', 'Echec de l\'opération', 'Droits non mis à jour');
            });
    };

    /**
     * Inherits rights
     */
    $scope.whInherit = function($aroId, $acoId, $action)
    {
        /**
         * The data we send
         *
         * @type {{action: *, aroId: *, acoId: *}}
         */
        var dataToSend = {
            'actionPath'    : $action,
            'aroId'         : $aroId,
            'acoId'         : $acoId,
            'action'        : 'inherit'
        };

        $http.post(WH_ROOT + '/admin/right/rights/update_rights/', dataToSend)
            .success(function(data) {

                /**
                 * Pop a message
                 */
                toaster.pop('success', 'Opération réussie', data.message);

            })
            .error(function(){
                toaster.pop('error', 'Echec de l\'opération', 'Droits non mis à jour');
            });
    };

    /**
     * Deny rights
     */
    $scope.whDeny = function($aroId, $acoId, $action)
    {
        /**
         * The data we send
         *
         * @type {{action: *, aroId: *, acoId: *}}
         */
        var dataToSend = {
            'actionPath'    : $action,
            'aroId'         : $aroId,
            'acoId'         : $acoId,
            'action'        : 'deny'
        };

        $http.post(WH_ROOT + '/admin/right/rights/update_rights/', dataToSend)
            .success(function(data) {

                /**
                 * Pop a message
                 */
                toaster.pop('success', 'Opération réussie', data.message);

            })
            .error(function(){
                toaster.pop('error', 'Echec de l\'opération', 'Droits non mis à jour');
            });
    }
}]);