/**=========================================================
 * SERVICE : Formulaire
 =========================================================*/
App.service('Formulaire', function($http, $q, $log, Module) {

    this.params = {

        Url : {
            save    : '/admin/form_manager/forms/edit/',
            all     : '/admin/form_manager/forms/index/',
            delete  : '/admin/form_manager/forms/delete/'
        }

    }

    this.liste = [];
    this.loaded = false;


    /**
     * Liste de toutes les calendriers
     * @returns {promise}
     */
    this.getAll = function () {

        var _this = this, d = $q.defer();

        if(_this.loaded) {

            d.resolve(_this.liste);

        } else {

            $http.get(this.params.Url.all)

                .success(function(reponse){

                    _this.liste = reponse;
                    _this.loaded = true;

                    d.resolve(_this.liste);

                })
                .error(function(res){

                    d.reject(res);

                });

        }

        return d.promise;

    }

    /**
     * Enregistrment d'un évènement
     * @returns {promise}
     */
    this.save = function (data) {

        var _this = this, d = $q.defer();

        $http.post(this.params.Url.save, data)

            .success(function(reponse){

                d.resolve(reponse.id);
                _this.loaded = false;

            })
            .error(function(res){

                d.reject(res);

            });


        return d.promise;

    }

    /**
     * Supprimer un formulaire
     * @param data
     */
    this.delete = function (index) {

        var _this = this, d = $q.defer();

        $http.get(this.params.Url.delete + _this.liste[index].Form.id)

            .success(function(data) {

                if(data.ok) {

                    d.resolve('ok');
                    _this.loaded = false;

                }else{

                    d.reject('Une erreur est survenue ');

                }

            })
            .error(function(x) {

                d.reject(x);

            });

        return d.promise; //On retourne le résultat de promise

    };

});

App.controller('FormulairesIndexCtrl', ['$scope' , '$log', '$rootScope', '$http', '$state', '$modal', 'Formulaire', 'toaster', function($scope, $log, $rootScope, $http, $state, $modal, Formulaire, toaster) {

    'use strict';

    $scope.Formulaires = [];

    /**
     * Action d'edtion d'un calendrier
     * @param index
     */
    $scope.editFormulaire = function (index) {

        var modalInstance = $modal.open({
                templateUrl: '/edit.html',
                controller: ModalInstanceCtrl,
                resolve: {
                    data: function () {
                        if (!isNaN(index)) {

                            return $scope.Formulaires[index];

                        } else {
                            return false;
                        }

                    }
                }
            })
            .result.then(
                function (id) {

                    $scope.loadFormulaires(id);

                }
        );

    }

    /**
     * Controlleur de la popup d'edition d'un calendrier
     * @param $scope
     * @param $modalInstance
     * @param data
     * @constructor
     */
    var ModalInstanceCtrl = function ($scope, $modalInstance, data, Page) {

        $scope.Pages = [];

        Page.getList().then(
            function(data) {
                $scope.Pages = data;
            }
        );


        if(!data) {

            data = {
                Form : {

                    etat_id : 'draft'

                }


            }

        }


        $scope.data = data;

        $scope.ok = function () {

            Formulaire.save($scope.data).then(
              function(id) {

                  $modalInstance.close(id);

              },
              function(x) {

                  $scope.log = x;

              }

            );

        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    };

    $scope.loadFormulaires = function () {

        $rootScope.loading = true;

        Formulaire.getAll().
            then(
                function(data){

                    $scope.Formulaires = data;
                    $rootScope.loading = false;

                },
                function(response){

                    $rootScope.loading = false;

                }
            );

    }

    $scope.loadFormulaires();

    /**
     * Delete
     */
    $scope.delete = function(index) {

        if(confirm('Etes vous sûr de vouloir supprimer ce formulaire ? ')) {

            $rootScope.loading  = true;

            Formulaire.delete(index).then(function(reponse) {

                $rootScope.loading = false;

                toaster.pop('success', 'Formulaire supprimé', '');

                $scope.Formulaires.splice(index, 1);

            }, function(erreur) {

                toaster.pop('danger', 'Erreur', erreur);

                $rootScope.loading = false;
            });


        }

    };


}]);


/**=========================================================
 * SERVICE : FormElement
 =========================================================*/
App.service('FormElement', function($http, $q, $log, Module) {

    this.params = {

        Url : {
            save    : '/admin/form_manager/form_elements/edit/',
            all     : '/admin/form_manager/form_elements/list/',
            delete  : '/admin/form_manager/form_elements/delete/'
        }

    }

    this.liste = [];
    this.loaded = false;


    /**
     * Liste de toutes les calendriers
     * @returns {promise}
     */
    this.getAll = function (form_id) {

        var _this = this, d = $q.defer();


        $http.get(this.params.Url.all + form_id)

            .success(function(reponse){

                _this.liste = reponse;
                _this.loaded = true;

                d.resolve(_this.liste);

            })
            .error(function(res){

                d.reject(res);

            });


        return d.promise;

    }

    /**
     * Enregistrment d'un évènement
     * @returns {promise}
     */
    this.save = function (data) {

        var _this = this, d = $q.defer();

        $http.post(this.params.Url.save, data)

            .success(function(reponse){

                d.resolve(reponse.id);
                _this.loaded = false;

            })
            .error(function(res){

                d.reject(res);

            });


        return d.promise;

    }

    /**
     * Supprimer un formulaire
     * @param data
     */
    this.delete = function (index) {

        var _this = this, d = $q.defer();

        $http.get(this.params.Url.delete + _this.liste[index].FormElement.id)

            .success(function(data) {

                if(data.ok) {

                    d.resolve('ok');
                    _this.loaded = false;

                }else{

                    d.reject('Une erreur est survenue ');

                }

            })
            .error(function(x) {

                d.reject(x);

            });

        return d.promise; //On retourne le résultat de promise

    };


});


/**=========================================================
 * SERVICE : FormElement
 =========================================================*/
App.service('Rule', function($http, $q, $log, Module) {

    this.params = {

        Url : {
            getTagsList  : '/admin/form_manager/rules/getTags'
        }

    }

    this.liste = [];
    this.loaded = false;


    /**
     * Liste de toutes les calendriers
     * @returns {promise}
     */
    this.getTagsList = function($type) {

        var _this = this, d = $q.defer();

        if(_this.loaded) {

            d.resolve(_this.liste);

        }else{

            $http

                .get(this.params.Url.getTagsList + '/' + $type)

                .success(function(response) {

                    _this.liste = response;

                    _this.loaded = true;

                    d.resolve(_this.liste);

                })
                .error(function(x) {

                    d.reject(x);

                });

        }

        return d.promise;

    };


});

App.controller('FormElementsIndexCtrl', ['$scope' , '$log', '$rootScope', '$http', '$state', '$modal', 'FormElement', 'Rule', 'toaster', 'Page', 'Formulaire', function($scope, $log, $rootScope, $http, $state, $modal, FormElement, Rule, toaster, Page, Formulaire) {

    'use strict';


    /**
     * Rubriqes
     */
    $rootScope.loading = true;

    $scope.Rubriques = [];

    Page.getList().then(
        function(data) {
            $scope.Rubriques = data;

        }
    );

    $http.get(WH_ROOT + '/admin/form_manager/forms/edit/' + $state.params.form_id)
        .success(function(data) {

            $scope.data = data;

            $rootScope.loading = false;

        })
        .error(function(x) {

            $rootScope.loading = false;

        });

    $scope.edit = function () {

        $rootScope.loading = true;

        Formulaire.save($scope.data).then(

            function(id) {

                $rootScope.loading = false;
            },
            function(x) {

                $rootScope.loading = false;
                $scope.log = x;
            }
        );
    };

    $scope.FormElements = [];

    /**
     * Action d'edtion
     * @param index
     */
    $scope.editFormElement = function (index) {

        var modalInstance = $modal.open({
                templateUrl: '/edit.html',
                controller: ModalInstanceEditCtrl,
                size : 'lg',
                resolve: {
                    data: function () {
                        if (!isNaN(index)) {

                            return $scope.FormElements[index];

                        } else {

                            return false;
                        }

                    }
                }
            })
            .result.then(
                function (id) {

                    $scope.loadElements(id);

                }
        );

    }


    /**
     * Controlleur de la popup d'edition d'un calendrier
     * @param $scope
     * @param $modalInstance
     * @param data
     * @constructor
     */
    var ModalInstanceEditCtrl = function ($scope, $modalInstance, data, FormElement, $state) {

        $scope.tagTypes = [];

        $scope.choixCheck = [
            {id : 0, label : 'Non'},
            {id : 1, label : 'Oui'}
        ];

        if(!data) {

            $scope.data = {

                FormElement : {
                    mandatory   : 0,
                    type        : 'input',
                    sub_type    : 'text' ,
                    form_id     : $state.params.form_id
                }
            };


        } else {

            $scope.data = data;

        }

        $http.get(WH_ROOT + '/admin/form_manager/rules/get_all')
            .success(function(data) {

                $scope.tags = data;

                angular.forEach(data.data.tag, function(v) {

                    $scope.tagTypes.push(
                        {
                            id      : v['@type'],
                            label   : v['@name'],
                            inf     : v['type']
                        }
                    );

                    console.log($scope.tagTypes);

                });


            })
            .error(function() {


            });





        $scope.ok = function () {

            FormElement.save($scope.data).then(
              function(id) {

                  $modalInstance.close(id);

              },
              function(x) {

                  $scope.log = x;

              }

            );

        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    };




    $scope.loadElements = function () {

        $rootScope.loading = true;

        FormElement.getAll($state.params.form_id).
            then(
                function(data){

                    $scope.FormElements = data;
                    $rootScope.loading = false;

                },
                function(response){

                    $rootScope.loading = false;

                }
            );

    }

    $scope.loadElements();

    /**
     * Delete
     */
    $scope.delete = function(index) {

        if(confirm('Etes vous sûr de vouloir supprimer cet élément de formulaire ? ')) {

            $rootScope.loading  = true;

            FormElement.delete(index).then(function(reponse) {

                $rootScope.loading = false;

                toaster.pop('success', 'Elément de formulaire supprimé', '');

                $scope.FormElements.splice(index, 1);

            }, function(erreur) {

                toaster.pop('danger', 'Erreur', erreur);

                $rootScope.loading = false;
            });


        }

    };


}]);
