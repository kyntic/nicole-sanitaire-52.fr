App.controller('AttributeFamiliesEditCtrl', ['$scope', '$http', '$state', '$log', '$rootScope', '$routeParams', 'toaster', 'AttributeFamily', 'Attribute', function($scope, $http, $state, $log, $rootScope, $routeParams, toaster, AttributeFamily, Attribute) {

    $scope.log = '';

    $scope.data = {};
    $scope.attributes = [];
    $scope.attributesOptions = [];
    $scope.attributeFamiliesOptions = [];

    $scope.init = function() {

        $scope.data = {};

        $scope.loadAttributeFamily();
        $scope.loadAttributes();
        $scope.loadAttributesOptions();
        $scope.loadAttributeFamiliesOptions();

    };

    /**
     * Charge les informations de la permanence
     */
    $scope.loadAttributeFamily = function() {

        $rootScope.loading  = true;

        AttributeFamily.getById($state.params.attribute_family_id).then(function (response) {

            if (response.statut == 1) {

                $scope.data = response.data;

            } else if (response.statut == 0) {

                $scope.data = {};

            } else {

                $scope.log = response;

            }

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    };

    /**
     * Charge la liste des options du select des familles d'attribut
     */
    $scope.loadAttributesOptions = function () {

        $rootScope.loading = true;

        Attribute.getOptions().then(function (response) {

            if (response.statut == 1) {

                $scope.attributesOptions = response.data;

            } else {

                $scope.log = response;

            }

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    }

    /**
     * Charge la liste des options du select des familles d'attribut
     */
    $scope.loadAttributeFamiliesOptions = function () {

        $rootScope.loading = true;

        AttributeFamily.getOptions().then(function (response) {

            if (response.statut == 1) {

                $scope.attributeFamiliesOptions = response.data;

            } else {

                $scope.log = response;

            }

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    }

    /**
     * Charge la liste des options du select des familles d'attribut
     */
    $scope.loadAttributes = function () {

        $rootScope.loading = true;

        Attribute.findAll($state.params.attribute_family_id).then(function (response) {

            if (response.statut == 1) {

                $scope.attributes = $.map(response.data, function (i) {return i;});

            } else {

                $scope.log = response;

            }

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    }

    /**
     * Enregistrement
     */
    $scope.submit = function() {

        $scope.log          = '';
        $rootScope.loading  = true;

        AttributeFamily.save($scope.data).then(function(response) {

            if (response.statut == 1) {

                $scope.data.AttributeFamily.id = response.data.AttributeFamily.id;

                toaster.pop('success', 'Enregistrement effectué');

            } else {

                $scope.log = response;

            }

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    };

    $scope.submitAttribute = function() {

        $scope.log          = '';
        $rootScope.loading  = true;

        $scope.data.Attribute.attribute_family_id = $state.params.attribute_family_id;

        Attribute.save($scope.data).then(function(response) {

            if (response.statut == 1) {

                $scope.data.Attribute.id = response.data.Attribute.id;

                toaster.pop('success', 'Enregistrement effectué');

            } else {

                $scope.log = response;

            }

            $scope.loadAttributes();

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    };

    $scope.newAttribute = function() {

        $scope.data.Attribute = {};

    }

    /**
     * Edition
     */
    $scope.editAttribute = function(id) {

        $rootScope.loading  = true;

        Attribute.getById(id).then(function (response) {

            if (response.statut == 1) {

                $scope.data.Attribute = response.data.Attribute;

            } else if (response.statut == 0) {

                $scope.data = {};

            } else {

                $scope.log = response;

            }

            $scope.loadAttributes();

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    };

    /**
     * Supression
     */
    $scope.deleteAttribute = function(id) {

        if(confirm('Êtes-vous sûr de vouloir supprimer cet attribut ? ')) {

            $rootScope.loading  = true;

            Attribute.delete(id).then(function(response) {

                if (response.statut == 1) {

                    toaster.pop('success', 'Attribut supprimé');

                    $scope.attributes.splice(index, 1);

                } else {

                    $scope.log = response;

                }

                $scope.loadAttributes();

                $rootScope.loading = false;

            }, function (error) {

                $scope.log = error;

                $rootScope.loading = false;

            });

        }

    };

    /**
     * Ordre
     */
    $scope.sortableAttributes = {

        stop: function (e, ui) {

            var attributes = $scope.attributes.map(function (i) {
                return i.Attribute.id;
            }).join(',');

            Attribute.ordre(attributes).then(
                function (data) {

                    toaster.pop('success', 'Ordre enregistré');

                },
                function (data) {

                    $scope.log = data;

                    toaster.pop('danger', 'Une erreur est survenue');
                }
            );

        }

    };

    /**
     * Lancement de la fonction init
     */
    $scope.init();

}]);
