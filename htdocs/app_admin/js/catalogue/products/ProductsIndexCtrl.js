App.controller('ProductsIndexCtrl', ['$scope' , '$log', '$rootScope', '$http', '$state', 'toaster', 'Product', function($scope, $log, $rootScope, $http, $state, toaster, Product) {

    'use strict';

    $scope.log = '';

    $scope.products = [];

    $scope.init = function () {

        $scope.loadProducts();

    }

    /**
     * Charge la liste des familles d'attribut
     */
    $scope.loadProducts = function () {

        $rootScope.loading = true;

        Product.findAll().then(function (response) {

            if (response.statut == 1) {

                $scope.products = response.data;

            } else {

                $scope.log = response;

            }

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    }

    /**
     * Supression
     */
    $scope.delete = function(index) {

        if(confirm('Êtes-vous sûr de vouloir supprimer ce produit ? ')) {

            $rootScope.loading  = true;

            Product.delete($scope.products[index].Product.id).then(function(response) {

                if (response.statut == 1) {

                    toaster.pop('success', 'Famille d\'attribut supprimée');

                    $scope.products.splice(index, 1);

                } else {

                    $scope.log = response;

                }

                $rootScope.loading = false;

            }, function (error) {

                $scope.log = error;

                $rootScope.loading = false;

            });

        }

    };

    /**
     * Supression
     */
    $scope.duplique = function(index) {

        if(confirm('Êtes-vous sûr de vouloir dupliquer ce produit ? ')) {

            $rootScope.loading  = true;

            Product.duplique($scope.products[index].Product.id).then(function(response) {

                if (response.statut == 1) {

                    toaster.pop('success', 'Produit dupliqué');
                    $scope.loadProducts();

                } else {

                    $scope.log = response;

                }

                $rootScope.loading = false;

            }, function (error) {

                $scope.log = error;

                $rootScope.loading = false;

            });

        }

    };

    /**
     * Lancement de la fonction init
     */
    $scope.init();

}]);
