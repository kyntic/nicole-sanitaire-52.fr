
/**=========================================================
 * CONTROLLER USERS INDEX
 =========================================================*/
App.controller('UsersCtrl', ['$scope', function ($scope) {


}]);

App.controller('UsersIndexCtrl', ['$scope' , '$log', '$rootScope', '$http', 'toaster', function ($scope, $log, $rootScope, $http, toaster) {

    'use strict';

    /**
     * Liste des users :
     * @type {Array}
     */
    $scope.Users = [];


    $scope.load = function () {

        $rootScope.loading = true;

        $http.get(WH_ROOT + '/admin/users/index').
            success(function (data) {

                $scope.Users = data.Users;

                $rootScope.loading = false;

            }).
            error(function (data) {

                $rootScope.loading = false;

            });

    }

    $scope.load();


    /**
     * Delete
     */
    $scope.delete = function (index) {

        if (confirm('Etes vous sûre de supprimer ?')) {

            $http
                .post(WH_ROOT + '/admin/users/delete/' + $scope.Users[index].User.id)

                .then(function (response) {

                    $scope.Users.splice(index, 1);
                    toaster.pop('success', 'Opération réussie', 'Membre supprimé');

                }

            );

        }

    }



}]);

App.controller('UsersEditCtrl', ['$scope' , '$log', '$rootScope', '$http', 'toaster', '$state', function ($scope, $log, $rootScope, $http, toaster, $state) {

    'use strict';


    $scope.data = {
        UserGroup : []
    };




    $rootScope.loading = true;

    $http.get(WH_ROOT + '/admin/users/edit/' + $state.params.id)
        .success(function(data) {

            if(data.Data) $scope.data = data.Data;
            $scope.Groups = data.Groups;

            $rootScope.loading = false;


        })
        .error(function() {

            $rootScope.loading = false;

        });



    $scope.send_password = function () {

        $http.get(WH_ROOT + '/admin/users/envoyer_email/' + $scope.data.User.id)
            .success(function(data) {

                if(data.ok) {

                    toaster.pop('success', 'Emai envoyé !');

                }else{

                    toaster.pop('error', 'Une erreur est survenue');
                }

            })


    }


    $scope.submit = function () {

        $rootScope.loading = true;

        $scope.error = [];


        console.log($scope.data);

        $http.post(WH_ROOT + '/admin/users/edit', $scope.data)

            .success(function(data) {

                if (data.ok) {

                    toaster.pop('success', 'Enregistrement effectué avec succès. ');
                    $scope.data.User.id = data.id;

                }else{

                    toaster.pop('error', 'Le formulaire contient des erreurs ');
                    $scope.error = data.errors;
                    console.log(data);

                }

                $rootScope.loading = false;

            })
            .error(function(data) {

                $rootScope.loading = false;

            });
    }
}]);