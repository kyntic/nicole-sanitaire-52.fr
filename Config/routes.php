<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */

	Router::connect('/:language/:controller/:action/*',
                       array(),
                       array('language' => '[a-z]{3}'));

	Router::connect('/', array('controller' => 'homes', 'action' => 'index'));
	Router::connect('/admin',array('controller' => 'users', 'action' => 'home', 'prefix' => 'admin', 'language' => 'fre'));
	Router::connect('/membre',array('controller' => 'users', 'action' => 'home', 'prefix' => 'membre'));
	Router::connect('/login',array('controller' => 'users', 'action' => 'login'));

/**
 * Traitement (pour sitemap.xml)
 */
	
Router::parseExtensions('rss');

Router::connect(
    '/sitemap.xml',
    array('controller' => 'Sitemap', 'action' => 'siteMapXml')

);


// les pages
Router::connect(
    '/page/:slug/:id', // E.g. /blog/3-CakePHP_Rocks
    array('controller' => 'pages', 'action' => 'view'),
    array(
        'pass' 	=> array('id', 'slug'),
        'id' 	=> '[0-9]+',
        'slug' 	=> '[a-zA-Z\-_\/]+'
    )
);


// les pages
Router::connect(
    '/:slug/p-:id', // E.g. /blog/3-CakePHP_Rocks
    array('controller' => 'pages', 'action' => 'view'),
    array(
        'pass' 	=> array('id'),
        'id' 	=> '[0-9]+',
        'slug' 	=> '[a-zA-Z0-9\-_\/]+'
    )
);

// les pages
Router::connect(
    '/:slug/actu-:id.html', // E.g. /blog/3-CakePHP_Rocks
    array('plugin' => 'blog', 'controller' => 'actualites', 'action' => 'view'),
    array(
        'pass' 	=> array('id'),
        'id' 	=> '[0-9]+',
        'slug' 	=> '[a-zA-Z0-9\-_\/]+'
    )
);

// les
Router::connect(
    '/:slug/even-:id.html', // E.g. /blog/3-CakePHP_Rocks
    array('plugin' => 'blog', 'controller' => 'evens', 'action' => 'view'),
    array(
        'pass' 	=> array('id'),
        'id' 	=> '[0-9]+',
        'slug' 	=> '[a-zA-Z0-9\-_\/]+'
    )
);


// Agenda ICS
Router::connect(
    '/agenda/:slug-:id.ics', // E.g. /blog/3-CakePHP_Rocks
    array('plugin' => 'blog', 'controller' => 'agendas', 'action' => 'ical'),
    array(
        'pass' 	=> array('id'),
        'id' 	=> '[0-9]+',
        'slug' 	=> '[a-zA-Z\-_\/]+'
    )
);

// Recherche
Router::connect(
    '/recherche', // E.g. /blog/3-CakePHP_Rocks
    array('controller' => 'recherches', 'action' => 'index', 'plugin' => 'recherche')
);


Router::connect(
      '/product/:slug/:id', // E.g. /blog/3-CakePHP_Rocks
      array('plugin' => 'catalogue', 'controller' => 'products', 'action' => 'view'),
      array(
	      'pass' 	=> array('id', 'slug'),
	      'id' 	=> '[0-9]+',
	      'slug' 	=> '[a-zA-Z\-_\/]+'
      )
);

Router::connect('/min-js', array('plugin' => 'Minify', 'controller' => 'minify', 'action' => 'index', 'js'));
Router::connect('/min-css', array('plugin' => 'Minify', 'controller' => 'minify', 'action' => 'index', 'css'));


/**
 * Load the CakePHP default routes. Remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';


