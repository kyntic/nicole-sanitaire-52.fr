<?php

/**
 * Class ActualitesController
 */
class AgendasController extends BlogAppController
{
    /**
     * Admin index
     *
     * @return void
     */
    public function admin_index()
    {


        echo json_encode($this->Agenda->find('all', array('order' => 'Agenda.created DESC')));

        exit();

    }

    /**
     * Admin edit
     *
     * @param null $id
     */
    public function admin_edit($id = null)
    {

        $res['ok'] = false;

        if (!empty($this->data)) {

            $data = $this->data;

            /**
             * Enregistrement
             */
            if ($this->Agenda->save($data)) {

                $res['ok'] = true;
                $res['id'] = $this->Agenda->id;

            } else {

                $res['ok'] = false;
            }

            exit(json_encode($res));
        }

        $this->loadModel('Page');

        $res['Data']        = false;

        if (!empty($id)) $res['Data'] = $this->Agenda->findById($id);

        exit(json_encode($res));

    }

    /**
     * View : Pour afficher tous les évènements du calendrier
     * @param null $id
     */
    public function admin_view($id)
    {

        $this->loadModel('Blog.Even');

        $agenda = $this->Agenda->findById($id);

        $agenda['Evens'] = $this->Even->find('all', array(
            'conditions' => array(
                'Even.even_date_deb >= ' => date('Y-m-d', strtotime('-2 week')),
                'Even.agenda_id' => $id,
                'Even.type' => 'even'
            )
        ));

        echo json_encode($agenda);

        exit();

    }

    /**
     * Admin delete
     *
     * @param int $id
     */
    /**
     * Admin delete
     *
     * @param $id
     */
    public function admin_delete($id)
    {

        $res['ok'] = false;


        $this->Agenda->delete($id);

        $this->loadModel('Blog.Article');
        $this->Article->deleteAll(array('Article.agenda_id' => $id));

        $res['ok'] = true;

        echo json_encode($res);

        exit();


    }
    /**
     * Admin view
     *
     * @param int $id
     */
    public function view($id)
    {
        $this->Cont = $this->Agenda->findById($id);
    }


    /**
     * Génération d'un flux ical
     * @param $id
     */
    public function ical ($id) {

        $this->loadModel('Blog.Even');

        $this->loadModel('Parametre');

        $agenda = $this->Agenda->findById($id);

        $params = $this->Parametre->find('first');


        $filename = strtolower(Inflector::slug($agenda['Agenda']['name']));

        // Variables used in this script:
        //   $summary     - text title of the event
        //   $datestart   - the starting date (in seconds since unix epoch)
        //   $dateend     - the ending date (in seconds since unix epoch)
        //   $address     - the event's address
        //   $uri         - the URL of the event (add http://)
        //   $description - text description of the event
        //   $filename    - the name of this file for saving (e.g. my-event-name.ics)
        //
        // Notes:
        //  - the UID should be unique to the event, so in this case I'm just using
        //    uniqid to create a uid, but you could do whatever you'd like.
        //
        //  - iCal requires a date format of "yyyymmddThhiissZ". The "T" and "Z"
        //    characters are not placeholders, just plain ol' characters. The "T"
        //    character acts as a delimeter between the date (yyyymmdd) and the time
        //    (hhiiss), and the "Z" states that the date is in UTC time. Note that if
        //    you don't want to use UTC time, you must prepend your date-time values
        //    with a TZID property. See RFC 5545 section 3.3.5
        //
        //  - The Content-Disposition: attachment; header tells the browser to save/open
        //    the file. The filename param sets the name of the file, so you could set
        //    it as "my-event-name.ics" or something similar.
        //
        //  - Read up on RFC 5545, the iCalendar specification. There is a lot of helpful
        //    info in there, such as formatting rules. There are also many more options
        //    to set, including alarms, invitees, busy status, etc.
        //
        //      https://www.ietf.org/rfc/rfc5545.txt

        // 1. Set the correct headers for this file
        header('Content-type: text/calendar; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $filename . '.ics');

        // 2. Define helper functions

        // Converts a unix timestamp to an ics-friendly format
        // NOTE: "Z" means that this timestamp is a UTC timestamp. If you need
        // to set a locale, remove the "\Z" and modify DTEND, DTSTAMP and DTSTART
        // with TZID properties (see RFC 5545 section 3.3.5 for info)
        //
        // Also note that we are using "H" instead of "g" because iCalendar's Time format
        // requires 24-hour time (see RFC 5545 section 3.3.12 for info).
        function dateToCal($timestamp, $allday = false) {
            return ($allday) ? date('Ymd', $timestamp) : date('Ymd\THis\Z', $timestamp);
        }

        // Escapes a string of characters
        function escapeString($string) {
            return preg_replace('/([\,;])/','\\\$1', $string);
        }

        echo 'BEGIN:VCALENDAR'."\r\n";
        echo 'PRODID:-//'.$params['Parametre']['raison_sociale'].' v1.0//EN'."\r\n";
        echo 'VERSION:2.0'."\r\n";
        echo 'CALSCALE:GREGORIAN'."\r\n";
        echo 'METHOD:PUBLISH'."\r\n";
        echo 'X-WR-CALNAME:' . escapeString($agenda['Agenda']['name']) . "\r\n";
        echo 'X-WR-TIMEZONE:Europe/Paris' . "\r\n";
        echo 'X-WR-CALDESC:'.escapeString($agenda['Agenda']['description']) . "\r\n";


        /**
         * Evens
         */
        $Evens = $this->Even->find('all', array(
            'conditions' => array(
                'Even.even_date_deb >= ' => date('Y-m-d', strtotime('-1 year')),
                'Even.agenda_id' => $id,
                'Even.type' => 'even'
            )
        ));

        foreach($Evens as $v) : ;


            echo 'BEGIN:VEVENT'."\r\n";
            //echo 'URL:'.FULL_BASE_URL.$v['Even']['url']."\r\n"; //Url
            echo 'DTSTART:'.dateToCal(strtotime($v['Even']['even_date_deb']), $v['Even']['all_day'])."\r\n";
            echo 'DTEND:'.dateToCal(strtotime($v['Even']['even_date_fin']), $v['Even']['all_day'])."\r\n";
            echo 'DTSTAMP:'.dateToCal(strtotime($v['Even']['created']))."\r\n"; //Date de création
            echo 'UID:'.uniqid()."\r";
            echo 'CREATED:'.dateToCal(strtotime($v['Even']['created']))."\r\n"; //Date de création
            echo 'DESCRIPTION:'.escapeString($v['Even']['resume'])."\r\n"; //Description
            echo 'LAST-MODIFIED:'.dateToCal(strtotime($v['Even']['updated']))."\r\n"; //Dernière modif
            echo 'LOCATION:'.escapeString($v['Even']['adresse'])."\r\n"; //Adresse
            echo 'SEQUENCE:0'."\r\n";
            echo 'STATUS:CONFIRMED'."\r\n"; // (optional. default value is CONFIRMED and can be, TENTATIVE or CANCELLED)
            echo 'SUMMARY:'.escapeString($v['Even']['name'])."\r\n"; // //Titre
            echo 'TRANSP:OPAQUE'."\r\n";
            echo 'END:VEVENT'."\r\n";


        endforeach;

        echo 'END:VCALENDAR'."\r\n";

        exit();

    }

}