<?php

/**
 * Class ActualitesController
 */
class ActualitesController extends BlogAppController
{
    /**
     * Admin index
     *
     * @return void
     */
    public function admin_index()
    {

        echo json_encode($this->Actualite->find('all', array('order' => 'Actualite.created DESC')));

        exit();

    }



    /**
     * Admin edit
     *
     * @param null $id
     */
    public function admin_edit($id = null)
    {

        $this->loadModel('ModuleManager.ContentModule');
        $this->loadModel('ContentAssoc');
        $this->loadModel('Page');

        $data = $this->data;

        if (!empty($data)) {

            /**
             * Enregistrement des données de la page
             */
            if(empty($data['Actualite']['push_accueil'])) $data['Actualite']['push_accueil'] = 0;
            if(empty($data['Actualite']['acces_membre'])) $data['Actualite']['acces_membre'] = 0;

            $data['Actualite']['publication_date_fin'] = 0;

            if ($this->Actualite->save($data)) {

                $id = $this->Actualite->id;

                //Modules
                $this->ContentModule->deleteAll(array('ContentModule.model' => 'Actualite', 'ContentModule.model_id' => $id));

                if (is_array($data['Contents'])) {

                    foreach($data['Contents'] as $k => $v) {

                        $this->ContentModule->save(array(
                            'id'             => null,
                            'model'         => 'Actualite',
                            'model_id'      => $id,
                            'position'      => $k,
                            'module'        => $v['ContentModule']['module'],
                            'content'       => $v['ContentModule']['content']
                        ));
                    }
                }

                //ContentAss
                $ContentAss = array();
                if(!empty($data['Page']['ids'])) $ContentAss = $data['Page']['ids'];
                if(!empty($data['Actualite']['page_id'])) $ContentAss[] = $data['Actualite']['page_id'];
                $this->ContentAssoc->deleteAll(array('ContentAssoc.model' => 'Actualite', 'ContentAssoc.model_id' => $id));

                foreach($ContentAss as $i) {

                    $this->ContentAssoc->save(array('ContentAssoc' => array(
                        'id' => null,
                        'model'         => 'Actualite',
                        'model_id'      => $id,
                        'model_asso'    => 'Page',
                        'model_asso_id'    => $i
                    )));

                }


                $res['ok'] = true;
                $res['id'] = $id;

            } else {

                $res['ok'] = false;
            }

            exit(json_encode($res));
        }


        exit();

    }


    /**
     * Admin view
     *
     * @param null $id
     */
    public function admin_view($id = null)
    {

        $this->loadModel('ModuleManager.ContentModule');
        $this->loadModel('ContentAssoc');
        $this->loadModel('Page');

        $res['Data']        = false;
        $res['Parents']     = $this->Page->generateTreeList(array('Page.template' => 'themeActu'), '{n}.Page.id', '{n}.Page.name', ' - ');

        if (!empty($id)) {

            $res['Data'] = $this->Actualite->findById($id);

            $res['Data']['Contents'] = $this->ContentModule->find(
                'all',
                array(
                    'order' => 'ContentModule.position',
                    'conditions' => array(
                        'ContentModule.model'       => 'Actualite',
                        'ContentModule.model_id'    => $id
                    )
                )
            );

            $res['Data']['Page']['ids'] = $this->ContentAssoc->find('list', array('fields'  => array('model_asso_id'), 'group' => array('ContentAssoc.model_asso_id'), 'conditions' => array('ContentAssoc.model' => 'Actualite', 'ContentAssoc.model_id' => $id)));
        }


        exit(json_encode($res));
    }

    /**
     * Admin delete
     *
     * @param int $id
     */
    /**
     * Admin delete
     *
     * @param $id
     */
    public function admin_delete($id)
    {

        $res['ok'] = false;



        $this->Actualite->delete($id);

        $this->loadModel('ModuleManager.ContentModule');
        $this->ContentModule->deleteAll(array('ContentModule.model' => 'Actualite', 'ContentModule.model_id' => $id));

        $res['ok'] = true;

        echo json_encode($res);

        exit();


    }
    /**
     * Admin view
     *
     * @param int $id
     */
    public function view($id)
    {
        $this->loadModel('Page');
        $this->loadModel('Blog.ActualitePage');
        $this->loadModel('Blog.Even');

        $this->Cont = $this->Actualite->findById($id);

        if($this->Cont['Actualite']['type'] == 'even') $this->redirect('/blog/evens/view/' . $id);

        /*
         * Liste des pages parentes
         */
        $pageIds = array();

        $_breadcrumb = array();

        if ($this->Cont['Actualite']['page_id']) {

            $Page   = $this->Page->findById($this->Cont['Actualite']['page_id']);

            $Path = $this->Page->getPath($Page['Page']['id']);

            foreach($Path as $v) $pageIds[] = $v['Page']['id'];

            $_breadcrumb['Model'] = 'Page';
            $_breadcrumb['Chemin'] = $Path;

        }

        /**
         * Contents
         */

        $this->loadModel('ModuleManager.ContentModule');

        $opts = array();
        $opts['conditions']['ContentModule.model']      = 'Actualite';
        $opts['conditions']['ContentModule.model_id']   = $this->Cont['Actualite']['id'];
        $opts['order']                                  = 'ContentModule.position ASC';
        $ContentModules = $this->ContentModule->find('all', $opts);

        $this->set(compact('Page', '_breadcrumb', 'ContentModules'));

        $actualites = $this->Actualite->find('all', array(
            'limit' => 20,
            'conditions' => array(
                'Actualite.etat_id' => 'publish',
                'Actualite.type'    => $this->Cont['Actualite']['type'],
                'Actualite.page_id' => $this->Cont['Page']['id']
            ),
            'order' => array('Actualite.created' => 'DESC')

        ));

        $this->set('actualites', $actualites);

        if($this->Cont['Actualite']['type'] == 'realisation') $this->render('realisation');

    }
}