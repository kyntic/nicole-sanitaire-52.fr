<?php
$this->extend('/Templates/template_default');

$this->element('balises_metas', array('metas' => $Cont['Even']));

foreach($_breadcrumb['Chemin'] as $v) $this->Html->addCrumb($v[$_breadcrumb['Model']]['name'], $v[$_breadcrumb['Model']]['url']);

$this->assign('h1', $Cont['Even']['h1']);

//if(!empty($_menuPages)) $this->assign('menu_col', $this->element('menu_col', array('menus' => $_menuPages)));

//Affichage petite infos date auteur
$this->start('date_maj');
?>
    <p class="meta text-muted">
        <?php if(!empty($Cont['Auteur'])) ?>Par: <a href="#"><?=$Cont['Auteur']['nom_connexion'];?></a>
        | Posté le: <?=$Cont['Even']['created'];?>
    </p>
<?php
$this->end();


$this->start('body');
?>

<blockquote class="highlight-border" style="background: #F5F5F5;">

    <table class="table table-stripped">

        <tr>
            <td><strong>Dates : </strong></td>
            <td>Du <?=$this->WhDate->show($Cont['Even']['even_date_deb'], !$Cont['Even']['all_day']);?> au <?=$this->WhDate->show($Cont['Even']['even_date_fin'], !$Cont['Even']['all_day']);?></td>
        </tr>
        <tr>
            <td><strong>Lieu :  </strong></td>
            <td><?=$Cont['Even']['adresse'];?></td>
        </tr>
        <?php if($Cont['Even']['suscribe']) : ?>
        <tr>
            <td><strong>Nombre de places :  </strong></td>
            <td><?=$Cont['Even']['suscribe_nbr'];?></td>
        </tr>
        <tr>
            <td><strong>Nombre de places restantes : </strong></td>
            <td><?=$subscriptionsLeft;?></td>
        </tr>
        <?php endif;?>

    </table>

</blockquote>

<?php if ($displayForm) : ?>
<div class="contenuPage col-md-8 col-sm-7">
    <h2 class="title">Participer à cet événement :</h2>
    <?=$this->Form->create('EvenInscrit', array('class' => 'contact-form', 'url' => '/blog/evens/subscribe'));?>
    <?=$this->FrontForm->input('EvenInscrit.first_name', 'Prénom :', array('class' => 'form-control'))?>
    <?=$this->FrontForm->input('EvenInscrit.name', 'Nom de famille :', array('class' => 'form-control'))?>
    <?=$this->FrontForm->input('EvenInscrit.email', 'Email :', array('class' => 'form-control'))?>
    <?=$this->Form->hidden('EvenInscrit.even_id', array('value' => $Cont['Even']['id']));?>
    <div class="form-action">
        <?=$this->Form->button('Inscription <i class="fa fa-chevron-right"></i>', array('class' => 'btn btn-theme', 'escape' => false, 'type' => 'submit'));?>
    </div>
    <?=$this->Form->end();?>
</div>
<?php endif; ?>

<?php

foreach($ContentModules as $module) {

    switch ($module['ContentModule']['module']) {

        default :

	        $element = 'ModuleManager.' . $module['ContentModule']['module'];
	        if (!empty($module['ContentModule']['template'])) {
		        $element .= $module['ContentModule']['template'];
	        }

            echo $this->element($element, array('content' => $module['ContentModule']));

            break;
    }
}

$this->end();

/**
 * We have news
 */
if (!empty($actualites)) {

    $this->assign('actu_col', $this->element('Blog.liste_actualite_col', array('actualites' => $actualites)));
}

/**
 * We have events
 */
if (!empty($evenements)) {

    $this->assign('event_col', $this->element('Blog.liste_evenement_col', array('evenements' => $evenements)));
}


$this->start('resume_col');

?>
<section class="widget has-divider">
    <h3 class="title"><?=$Cont['Agenda']['name'];?></h3>
    <p><textarea disabled="disabled" class="form-control"><?=$Cont['Agenda']['url_ics'];?></textarea></p>
    <p>Abonnez vous à tous les événements de <strong><i><?=$Cont['Agenda']['name'];?></i></strong> en ajoutant ce lien à votre télpéhone, ical mac ou outlook. </p>
</section>

<?php if (!empty($Cont['Even']['resume'])) : ?>
    <section class="widget has-divider">
        <h3 class="title"><?=$Cont['Even']['name'];?></h3>
        <p><?=$Cont['Even']['resume'];?></p>
    </section>
<?php endif; ?>
<?php $this->end();?>
