<?php if(!empty($actualites)){?>
    <section class="actualites">

        <h3 class="title">Nos dernières actualités</h3>

        <div class="section-content">
            <?php foreach($actualites as $k => $v) : ?>
                <article class="article-item row page-row">
                    <div class="img col-md-3 col-sm-12 col-xs-12">
                        <?=(!empty($v['Actualite']['vignette'])) ? $this->Html->image($v['Actualite']['vignette']->File->url, array('url' => $v['Actualite']['url'])) : '';?>
                    </div>
                    <div class="details col-md-9 col-sm-12 col-xs-12">
                        <h5 class="title"><?=$this->Html->link($v['Actualite']['name'], $v['Actualite']['url']);?></h5>
                    </div><!--//details-->
                </article>
            <?php endforeach;?>
        </div>
    </section>
<?php }?>
