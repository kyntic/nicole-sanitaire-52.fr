<?php if(!empty($temoignages)) :?>
    <section class="testimonials">
        <h1 class="section-heading text-highlight"><span class="line"> Témoignages</span></h1>
        <div class="carousel-controls">
            <a class="prev" href="#testimonials-carousel" data-slide="prev"><i class="fa fa-caret-left"></i></a>
            <a class="next" href="#testimonials-carousel" data-slide="next"><i class="fa fa-caret-right"></i></a>
        </div>
        <div class="section-content">
            <div id="testimonials-carousel" class="testimonials-carousel carousel slide">
                <div class="carousel-inner">

                    <?php foreach($temoignages as $k => $v) :;?>
                        <div class="item <?php if($k == 0) echo 'active';?>">
                            <blockquote class="quote">
                                <p><i class="fa fa-quote-left"></i><?=$v['Actualite']['resume'];?></p>
                            </blockquote>
                            <div class="row">
                                <p class="people col-md-8 col-sm-3 col-xs-8"><span class="name"><?php echo $v['Actualite']['name'];?></span><br /><span class="title"><?php echo $v['Actualite']['h1'];?></span></p>
                                <?php if(!empty($v['Actualite']['vignette'])) echo $this->Html->image($v['Actualite']['vignette']->File->url, array('class' => 'profile col-md-4 pull-right')); ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif;?>