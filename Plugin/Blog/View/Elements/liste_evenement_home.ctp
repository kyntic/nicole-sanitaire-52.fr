<?php if(!empty($evenements)){?>
    <section class="events">
        <h1 class="section-heading text-highlight"><span class="line">Nos événements</span></h1>
        <div class="section-content">
            <?php foreach($evenements as $k => $v) :?>
                <div class="event-item">
                    <p class="date-label">
                        <span class="month"><?php echo $this->WhDate->show($v['Even']['even_date_deb'],'mois_court');?></span>
                        <span class="date-number"><?php echo $this->WhDate->show($v['Even']['even_date_deb'],'jour');?></span>
                    </p>
                    <div class="details">
                        <h2 class="title"><?php echo $this->Html->link($v['Even']['h1'],$v['Even']['url']);?></h2>
                        <p class="time"><i class="fa fa-clock-o"></i> <?php echo $this->WhDate->show($v['Even']['even_date_deb'],'heure');?> - <?php echo $this->WhDate->show($v['Even']['even_date_fin'],'heure');?></p>
                        <p class="location"><i class="fa fa-map-marker"></i> <?php echo $v['Even']['adresse'];?></p>
                        <p class="location"><i class="glyphicon glyphicon-info-sign"></i>
                            <?php
                            echo $this->Text->truncate(
                                $v['Even']['resume'],
                                150,
                                array(
                                    'ellipsis' => '...',
                                    'exact' => false
                                )
                            );
                            ?>
                        </p>
                    </div>
                </div>
            <?php endforeach;?>

            <a class="read-more" href="/tous-nos-evenements/p-108"><?=__('Tous les évènements');?><i class="fa fa-chevron-right"></i></a>
        </div>
    </section>
<?php }?>