<?php if(!empty($quicklinks)) :?>
<section class="links">
    <h1 class="section-heading text-highlight"><span class="line">Liens rapides</span></h1>
    <div class="section-content">
        <?php foreach($quicklinks as $link) :?>
        <p><?php echo $this->Html->link('<i class="fa fa-caret-right"></i>'.$link['Actualite']['name'],$link['Actualite']['url'], array('title' => $link['Actualite']['meta_title'], 'target' => '_blank', 'escape' => false));?></p>
        <?php endforeach;?>
    </div>
</section>
<?php endif;?>