<?php if(!empty($actualites)){?>

    <section id="actualites" class="news">
        <h2 class="section-heading text-highlight"><span class="line">Dernières actualités</span></h2>
        <div class="carousel-controls">
            <a class="prev" href="#actualites-carousel" data-slide="prev"><i class="fa fa-caret-left"></i></a>
            <a class="next" href="#actualites-carousel" data-slide="next"><i class="fa fa-caret-right"></i></a>
        </div>
        <div class="section-content clearfix">
            <div id="actualites-carousel" class="news-carousel carousel slide">
                <div class="carousel-inner">
                    <?php $ligne = 0;?>
                    <?php foreach($actualites as $k => $v) {?>
                        <?php $active = ''; if($ligne == 0 ) $active = 'active';?>
                        <?php if($k % 3 == 0) echo '<div class="item ligne '.$active.'">';?>

                        <div class="col-md-4 news-item actualite">
                            <h2 class="title"><?php echo $this->Html->link($v['Actualite']['h1'], $v['Actualite']['url'], array());?></h2>
                            <?php if(!empty($v['Actualite']['vignette'])) echo $this->Html->image($v['Actualite']['vignette']->File->url,array('url' => $v['Actualite']['url'], 'class' => 'thumb'));?>
                            <p><?php echo $this->Text->truncate(
                                    $v['Actualite']['resume'],
                                    200,
                                    array(
                                        'ellipsis' => '...',
                                        'exact' => false
                                    )
                                );?>
                            </p>

                            <?php echo $this->Html->link('En savoir plus <i class="fa fa-chevron-right"></i>',$v['Actualite']['url'], array('escape' => false, 'class' => 'read-more'));?>
                        </div>

                        <?php if($k % 3 == 2) echo '</div>';$ligne++;?>
                    <?php }?>
                    <?php if($k %3 != 2) echo '</div>';?>
                </div>
            </div>
        </div>
    </section>
    <?php $this->Html->scriptStart($options = array('block' => 'scriptBottom')); ?>
    jQuery(document).ready(function () {
        jQuery('#actualites-carousel').carousel(
        {
            //interval:7000
        });
    });

    <?php $this->Html->scriptEnd(); ?>
<?php }?>