<section class="listeActualites">
	<?php foreach ($actualites as $key => $actualite) { ?>

		<?php if ($key % 3 == 0) echo '<div class="row">'; ?>

			<div class="col-xs-4">

				<div class="actualite">
					<?= $this->WhFile->show($actualite['Actualite']['vignette'], array(), array('url' => $actualite['Actualite']['url'])); ?>
					<h3><?= $this->Html->link($actualite['Actualite']['name'], $actualite['Actualite']['url']); ?></h3>
					<p><?= $actualite['Actualite']['resume']; ?></p>
				</div>

			</div>

		<?php if ($key % 3 == 2) echo '</div>'; ?>

	<?php } ?>

</section>
