<?php
class GoogleController extends AppController {

    public function admin_home() {

        $data=array(
            array(
                'label' => 'Année en cours',
                'color' => '#768294',
                'data' =>
                    array(
                        array('Mar', rand(50, 70)),
                        array('Apr', rand(70, 100)),
                        array('May', rand(50, 70)),
                        array('Jun', rand(70, 100)),
                        array('Jul', rand(50, 70)),
                        array('Aug', rand(70, 100)),
                        array('Sep', rand(50, 70))
                    )
            ),
            array(
                'label' => 'Moyenne précédente',
                'color' => '#1f92fe',
                'data' =>
                    array(
                        array('Mar', rand(10, 50)),
                        array('Apr', rand(10, 50)),
                        array('May', rand(10, 50)),
                        array('Jun', rand(10, 50)),
                        array('Jul', rand(10, 50)),
                        array('Aug', rand(10, 50)),
                        array('Sep', rand(10, 50))
                    )
            )
        );



        echo json_encode($data);

        exit();

    }


}
?>