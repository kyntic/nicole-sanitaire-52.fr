<!DOCTYPE html>
<html lang="fr">
<head>
    <?php
    echo $this->Html->charset();
    echo '<title>'.Configure::read('Project.nom').'</title>';
    echo $this->Html->meta(array('name' => 'author', 'content' => 'Whatson web - jerome@whatson-web.com'));
    echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'));


    echo $this->Html->meta(array('name' => 'apple-mobile-web-app-capable', 'content' => 'yes'));
    echo $this->Html->meta(array('name' => 'apple-mobile-web-app-status-bar-style', 'content' => 'black'));
    echo $this->Html->meta(array('name' => 'apple-touch-fullscreen', 'content' => 'yes'));

    echo $this->Html->css('http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700');
    echo $this->Html->css('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css');

    ?>
</head>

<body>


<div class="container">
    <?= $this->fetch('content');?>
</div>


<?= $this->Html->script('loading/jquery.showLoading.min.js'); ?>
<?= $this->Html->script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js'); ?>
<?= $this->fetch('scriptBottom'); ?>
<?= $this->fetch('script'); ?>
<?= $this->Js->writeBuffer(); ?>

</body>

</html>