<div class="panel panel-primary">

    <div class="panel-heading">Etape 1 : Général</div>

    <div class="panel-body">

        <div class="panel panel-info">

            <div class="panel-heading">1.1 : Autoriser le multilingue</div>

            <div class="panel-body">
                <p>Dans le fichier <strong>/Config/core.php</strong></p>
                <div class="highlight">
                    <code class="language-html" data-lang="html">Configure::write('multilingue', true);</code>
                </div>
            </div>

        </div>

        <div class="panel panel-info">
            <div class="panel-heading">1.2 : Déclaration des langues</div>
            <div class="panel-body">
                <p>Dans le fichier <strong>/Config/boostrap.php</strong></p>
                <pre>
                    <code class="language-html" data-lang="html">
// Langues acceptées
$languages = array(
    'fr' => 'fre',
    'en' => 'eng',
    'nl' => 'dut'
);
                    </code>
                </pre>
                <p>La clé de ce tableau correspond au code ISO 639-1 et sa valeur au code ISO 639-2 (bibliographic <code class="language-html" data-lang="html">(B)</code> plutôt que terminology si le choix existe)</p>
                <p><?= $this->Html->link('Liste des codes', 'http://www.loc.gov/standards/iso639-2/php/code_list.php', array('target' => '_blank')); ?></p>
            </div>
        </div>

        <div class="panel panel-info">

            <div class="panel-heading">1.3 : Traduction des textes hors base de données</div>

            <div class="panel-body">
                <p>La traduction des textes se fait dans les fichiers situés dans : <strong>/Locale/eng/LC_MESSAGES/default.po</strong></p>
                <p>On utilisera ici également le code ISO 639-2 <code class="language-html" data-lang="html">(B)</code></p>
                <p>Ce fichier est composé d'une suite de traductions sur la format suivant :</p>
                <p><code class="language-html" data-lang="html">msgid="Chaîne a traduire"</code><br>
                <code class="language-html" data-lang="html">msgstr="Chaîne traduite"</code></p>
                <p>La traduction se fait en PHP via la fonction <code class="language-php" data-lang="php">__('Chaine a traduire');</code></p>
                <p>Si la correspondance avec un <code class="language-html" data-lang="html">msgid</code> existe dans le fichier default.po, la chaîne sera remplacée par son <code class="language-html" data-lang="html">msgstr</code>.</p>
                <div class="alert alert-danger"><strong>Achtung !</strong><br>La correspondance se fait au caractère près : <code class="language-html" data-lang="html">msgid="Chaîne a traduire"</code> n'est pas égal à <code class="language-html" data-lang="html">msgid="> Chaine a traduire"</code></div>
                <div class="alert alert-danger"><strong>Achtung !</strong><br>Il existe un cache sur ces fichiers dans <strong>/tmp/cache/persistent/myapp_cake_core_default_eng</strong><br>Ici encore on utlise le code IS 639-2 <code class="language-html" data-lang="html">(B)</code></div>
            </div>

        </div>

        <div class="panel panel-info">

            <div class="panel-heading">1.4 : Génération des fichiers POT</div>

            <div class="panel-body">
                <p>La génération des fichiers se fait dans la console</p>
                <p>Tout d'abord se connecter en SSH sur le serveur ( <code class="language-html" data-lang="html">ssh akatim-11934@nx3217.nexylan.net -p 2121</code>  utiliser la clé ssh correspondant au bon serveur)</p>
                <p>Aller dans la console cake i18n :</p>
                <p><code class="language-html" data-lang="html">golfdebond-14322:~/Console$ cake i18n </code></p>
                <p>La question suivante vous sera posée : <strong>What would you like to do? (E/I/H/Q) </strong></p>
                <p>Ecrivez simplement <strong>"E"</strong> dans la console : <code class="language-html" data-lang="html">> E</code></p>
                <p>Ensuite une nouvelle question vous est posée : <strong>What is the path you would like to extract? [Q]uit [D]one</strong> </p>
                <div class="alert alert-danger"><strong>DOUCEMENT BILOUTE !!!</strong><br>Vous devez inscrire le chemin du dossier dont vous souhaitez générer le fichier POT. <br>
                    On ne va donc pas <strong>TOUT</strong> traduire, on ne traduit que les plugins <strong>que l'ont veut</strong>. (client pas content quand lui voir fichier 6000 lignes à traduire)</div>
                <p>On va donc écrire le chemin d'accès aux vues d'un seul plugin ou seulement les vues users (par exemple)</p>
                <p>Quelques exemples : </p>
                <p> - générer à partir de toutes les vues du plugin "Blog"</p>
                <pre>
                    <code>
[/var/www/golfdebondues.com/Console/] > /var/www/golfdebondues.com/Plugin/Blog/View
                    </code>
                </pre>
                <p> - générer à partir de toutes les vues "Actualités" du plugin "Blog"</p>
                <pre>
                    <code>
[/var/www/golfdebondues.com/Console/] > /var/www/golfdebondues.com/Plugin/Blog/View/Actualites
                    </code>
                </pre>
                <p> - générer à partir de toutes les vues (sans compter les plugins)</p>
                <pre>
                    <code>
[/var/www/golfdebondues.com/Console/] > /var/www/golfdebondues.com/View
                    </code>
                </pre>
                <p> - générer à partir de toutes les vues "Users"</p>
                <pre>
                    <code>
[/var/www/golfdebondues.com/Console/] > /var/www/golfdebondues.com/View/Users
                    </code>
                </pre>
                <p>Après comme le système vous prend pour un noob il va vous reposer la meme question il vous suffira d'appuyer sur <strong>"D"</strong> pour valider</p>
                <p>La question suivante vous demande où vous souhaitez stocker votre fichier POT.</p>
                <div class="alert alert-warning"><strong>LE CONSEIL QUI TUE !!!</strong><br>Je vous conseil de créer un dossier tmp dans votre dossier Locale (exemple : /var/www/golfdebondues.com/Locale/tmp). <br>
                    Pourquoi ? <br>On va tout éviter de polluer le dossier de traduction inutilement <br>Mieux encore on va générer  des fichiers POT différent pour chacuns de nos plugins ou autre</div>
                <p>Exemple d'arborescence : </p>
                <pre>
                    <code>
golfdebondues.com >
    Locale >
        tmp >
            blog >
                default.pot
            partenaires_manager >
                partenaires >
                    default.pot
                partenaire_types >
                    ON EN VEUT PAS
        eng >
            LC_MESSAGES >
                default.pot
        fre >
            LC_MESSAGES >
                default.pot
        dut >
            LC_MESSAGES >
                default.pot
                    </code>
                </pre>
                <p>Une fois que vous aviez écrit correctement un chemin de destination (exemple : <code class="language-html" data-lang="html">[/var/www/golfdebondues.com/Locale/Locale] >  /var/www/golfdebondues.com/Locale/tmp/blog/</code>)<br> il va vous poser une dernière question (savoir s'il le fait pour tous les domaines) : <strong>Would you like to merge all domains strings into the default.pot file? (y/n) </strong><br>
                Par defaut le système choisit <strong>"n"</strong>, mais vous faites comme vous le sentez.</p>
                <p>La génération des fichiers POT étant terminée  vous allez constater qu'il ne génère qu'un seul fichier pour une seul langue. Il vous suffira donc de : 
                <ul>
                    <li>1) <strong>nétoyer</strong> votre fichier default.pot (on a pas besoin de traduire coté admin par exemple).</li>
                    <li>2) faire de tous les fichiers .pot généré (dans vos petits dossiers différents si vous aviez suivi mon conseil) <strong>UN SEUL</strong> fichier .pot .</li>
                    <li>3) remplacer / créer chaque fichier default.pot <strong>pour chaque</strong> langage.</li>
                </ul>
                </p>

            </div>

        </div>

    </div>

</div>

<div class="panel panel-primary">

    <div class="panel-heading">Etape 2 : Mise à jour des tables (Ce point est à répéter pour chaque table)</div>

    <div class="panel-body">

        <div class="panel panel-warning">

            <div class="panel-heading">2.1 : Mise à jour de la table i18n</div>

            <div class="panel-body">
                <p>Cette étape n'a pas besoin d'être réalisée s'il n'y a pas encore d'enregistrements dans la table que vous souhaitez traduire.</p>
                <br>
                <p>Rendez-vous dans le I18nController, puis changez la fonction <code>admin_startMaj</code></p>
                <p>Par défaut, vous n'avez qu'à changer les variables suivantes <code>$plugin</code>, <code>$model</code> et <code>$champs</code>.</p>
                <p>Si vous souhaitez changer les langues, gardez en tête que les valeurs de ce tableau doivent être des codes ISO 639-2 <code>(B)</code></p>
                <p>Une fois ces changements faits, appelez l'url : <code>/admin/i18n_manager/i18n/startMaj</code> et vérifiez que la table i18n a été mise à jour.</p>
            </div>

        </div>

        <div class="panel panel-info">

            <div class="panel-heading">2.2 : Déclarer au Model les champs à traduire</div>

            <div class="panel-body">
                <p>Il faut déclarer dans un <code>$actsAs['Translate']</code> tous les champs à traduire et ce avant tout autre actsAs ( le actsAs Menu sera donc ecrit après).</p>
                <p>Exemple :</p>
                <pre>
                    <code>
$actsAs = array(
    'Translate' => array(
        'name'  =>  'TranslateName'
    )
);
                    </code>
                </pre>
            </div>

        </div>

        <div class="panel panel-primary">

            <div class="panel-heading">CMS CakePHP + SmartAdmin</div>

            <div class="panel-body">

                <div class="panel panel-info">

                    <div class="panel-heading">2.3 : Mise à jour des formulaires admin</div>

                    <div class="panel-body">
                        <p><strong>Procédure sur la dernière version du CMS CakePHP avec SmartAdmin :</strong></p>
                        <p>Il faut ajouter un paramètre <code>trad</code> à <code>true</code> aux <code>input</code></p>
                        <p>Ex : <code>echo $this->Smart->input('champ', 'Label', array('trad' => true)); </code></p>
                    </div>

                </div>

                <div class="panel panel-info">

                    <div class="panel-heading">2.4 : Mise à jour du MenuBehavior </div>

                    <div class="panel-body">
                        <p>CakePHP ne gère pas les <code>$actsAs Translate</code> de façon récursive, il faut donc recréer les datas dans les <code>afterSave</code> des <code>Behavior</code>.</p>
                        <p>Exemple : <strong>/Model/Behavior/MenuBehavior.php</strong> <code>afterSave(&$Model, $created)</code></p>
                        <pre>
                        <code>
// $model       => le model à traduire
// $champ       => champs à traduire
// $Translate   => valeur du champ traduit
// $inf         => les données actuelles, à remplacer
if(Configure::read('multilingue')) {

    //Pour chaque champ de traduction il faut créer le tableau avec les nouvelles infos
    foreach($Model->MenuItem->actsAs['Translate'] as $champ => $Translate) {

        if(isset($inf[$Translate])) {

            $data['MenuItem'][$champ] = array();

            foreach($inf[$Translate] as $v) {

                $data['MenuItem'][$v['field']][$v['locale']] = $v['content'];
            }
        }
    }
}
                        </code>
                        </pre>
                    </div>

                </div>

            </div>

        </div>

        <div class="panel panel-primary">

            <div class="panel-heading">CMS Angular</div>

            <div class="panel-body">

                <div class="panel panel-info">

                    <div class="panel-heading">2.5 : Lancer l'initialisation de la table i18n lors de la création d'un nouvel enregistrement</div>

                    <div class="panel-body">
                        <p>Dans le <code>afterSave</code> du <code>Model</code></p>
                        <pre>
                            <code>
if ($created) {

    $I18nManager = ClassRegistry::init('I18nManager.I18nManager');
    $I18nManager->initI18n('', $this->alias, array_keys($this->actsAs['Translate']), $this->data);
}
                            </code>
                        </pre>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
