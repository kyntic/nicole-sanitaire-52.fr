<?php

class I18nController extends AppController {

    /**
     * Admin edit function
     *
     * @param null $id
     * @return void
     */
    public $components = array('I18nManager.I18n');


    public function admin_startMaj()
    {

        App::uses('I18nManager.I18nComponent', 'Controller/Component');

        $langues = Configure::read('Config.languages');

        $plugin = '';
        $model  = 'Page';
        $champs = array(
            'h1',
            'name',
            'resume'
        );

        if ($this->I18n->majI18n($plugin, $model, $champs, $langues)) {

            echo 'MAJ réussi';

        } else {

            echo 'MAJ echouée';
        }

        exit();

    }

    /**
     * Admin view function
     *
     * @return void
     */
    public function doc()
    {

        $this->layout = 'I18nManager.default';

    }




}