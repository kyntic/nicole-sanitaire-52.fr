<?php

class Product extends CatalogueAppModel {

    public $belongsTo = array(
        'ContentType',
        'Etat',
        'Rubrique' => array(
        	'className' 	=>	'Page',
        	'foreignKey'	=>	'rubrique_id'
        )
    );

    public function beforeSave($options = array()) {

        if(!isset($this->data['Product']['id']) || empty($this->data['Product']['id']) || (isset($this->data['Product']['position']) && $this->data['Product']['position'] == 0))
        {
           if (!empty($this->data['Product']['rubrique_id'])) {

               $positionMax = $this->find('first', array(
                       'conditions' => array('Product.rubrique_id' => $this->data['Product']['rubrique_id']),
                       'order' => array('Product.position' => 'DESC')
                   ));

               $position = (!empty($positionMax)) ? $positionMax['Product']['position'] + 1 : 1;

               $this->data['Product']['position'] = $position;

           }

        }

        if (!empty($this->data['Product']['vignette'])) {
            $this->data['Product']['vignette'] = json_encode($this->data['Product']['vignette']);
        }

        if (!empty($this->data['Product']['galerie'])) {
            $this->data['Product']['galerie'] = json_encode($this->data['Product']['galerie']);
        }

        return true;

    }

    public function afterFind($results, $primary = false) {

    	foreach ($results as &$v) {

    		if (!empty($v['Product']['id'])) {

    			$v['Product']['url'] = '/catalogue/products/view/' . $v['Product']['id'];

                if (!empty($v['Product']['name'])) {

                    $v['Product']['url'] = '/product/' . strtolower(Inflector::slug($v['Product']['name'], '-')) . '/' . $v['Product']['id'];

                }

    		}

            if (!empty($v['Product']['vignette'])) {
                $v['Product']['vignette'] = json_decode($v['Product']['vignette']);
            }

            if (!empty($v['Product']['galerie'])) {
                $v['Product']['galerie'] = json_decode($v['Product']['galerie']);
            }


        }

        return $results;

    }

    /**
     * Retourne les product_ids d'une rubrique
     */
    public function getProductIds($rubrique_id = null)
    {

        if ($rubrique_id == null) {
            return array();
        }

        $productIds = $this->find(
            'list',
            array(
                'fields' => array(
                    'Product.id',
                    'Product.id',
                ),
                'conditions' => array(
                    'Product.rubrique_id' => $rubrique_id
                )
            )
        );

        if (!$productIds) {
            $productIds = array();
        }

        return $productIds;

    }

}
