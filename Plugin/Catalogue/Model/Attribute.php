<?php

class Attribute extends CatalogueAppModel {

    public $belongsTo = array(
        'Catalogue.AttributeFamily'
    );

	public $actsAs = array(
		'Tree'
	);

	public function beforeSave($options = array())
	{

        if(!empty($this->data['Attribute']['id'])) $attributFamille = $this->findById($this->data['Attribute']['id']);

        if(!isset($this->data['Attribute']['id']) || empty($this->data['Attribute']['id']) || $attributFamille['Attribute']['position'] == 0)
        {

        	if (!empty($this->data['Attribute']['attribute_family_id'])) {

				$positionMax = $this->find('first', array(
					'conditions' => array('Attribute.attribute_family_id' => $this->data['Attribute']['attribute_family_id']),
					'order' => array('Attribute.position' => 'DESC')
				));

        	}

        	if (!empty($this->data['Attribute']['parent_id'])) {

				$positionMax = $this->find('first', array(
					'conditions' => array('Attribute.parent_id' => $this->data['Attribute']['parent_id']),
					'order' => array('Attribute.position' => 'DESC')
				));

        	}

			$position = (!empty($positionMax)) ? $positionMax['Attribute']['position'] + 1 : 1;

			$this->data['Attribute']['position'] = $position;

		}

        if (!empty($this->data['Attribute']['vignette'])) {
            $this->data['Attribute']['vignette'] = json_encode($this->data['Attribute']['vignette']);
        }

        if (!empty($this->data['Attribute']['vignette_hover'])) {
            $this->data['Attribute']['vignette_hover'] = json_encode($this->data['Attribute']['vignette_hover']);
        }

	}

    public function afterFind($results, $primary = false) {

    	foreach ($results as &$v) {

            if (!empty($v['Attribute']['vignette'])) {
                $v['Attribute']['vignette'] = json_decode($v['Attribute']['vignette']);
            }

            if (!empty($v['Attribute']['vignette_hover'])) {
                $v['Attribute']['vignette_hover'] = json_decode($v['Attribute']['vignette_hover']);
            }

        }

        return $results;

    }

}