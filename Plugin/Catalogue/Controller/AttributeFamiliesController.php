<?php

class AttributeFamiliesController extends CatalogueAppController {

    /**
     * Admin
     */
    public function admin_index($id = null)
    {

        $data = array(
        	'AttributeFamily' => array(
        		'id' => null
        	)
        );

        if ($id != null) {

            $data = $this->AttributeFamily->findById($id);
            $data['Chemin'] = $this->AttributeFamily->getPath($id);

        }

        $data['children'] = $this->AttributeFamily->find(
        	'all',
        	array(
        		'conditions' => array(
        			'AttributeFamily.parent_id' => $id
        		),
        		'order' => array(
        			'AttributeFamily.position' => 'ASC'
        		)
        	)
        );

        exit(
            json_encode(
                array(
                    'statut'    =>  1,
                    'data'      =>  $data
                )
            )
        );

    }

	/**
	 * Liste de toutes les familles d'attributs
	 */
	public function admin_tree()
	{

		$attributeFamilies = $this->AttributeFamily->find(
			'threaded',
			array(
				'order' => array(
					'AttributeFamily.position'	=>	'ASC'
				)
			)
		);
		if (!$attributeFamilies) {
			$attributeFamilies = array();
		}

        exit(
            json_encode(
                array(
                    'statut'    =>  1,
                    'data'      =>  $attributeFamilies
                )
            )
        );

	}

    /**
     * Retourne toutes les familles de caractéristiques qui ont des caractéristiques
     */
    public function admin_getAssociables()
    {

        $this->loadModel('Catalogue.Attribute');

        $attributeFamilyIds = $this->Attribute->find(
            'list',
            array(
                'fields' => array(
                    'Attribute.attribute_family_id',
                    'Attribute.attribute_family_id'
                )
            )
        );

        $attributeFamilies = $this->AttributeFamily->find(
            'all',
            array(
                'conditions' => array(
                    'AttributeFamily.id'    =>  $attributeFamilyIds
                ),
                'order' => array(
                    'AttributeFamily.name' => 'ASC'
                )
            )
        );

        exit(
            json_encode(
                array(
                    'statut'    =>  1,
                    'data'      =>  $attributeFamilies
                )
            )
        );

    }

    /**
     * Retourne une permanence
     */
    public function admin_view($id = null)
    {

        $attributeFamily = $this->AttributeFamily->findById($id);
        if (!$attributeFamily) {

            exit(
                json_encode(
                    array(
                        'statut'    =>  0
                    )
                )
            );
        }

        exit(
            json_encode(
                array(
                    'statut'    =>  1,
                    'data'      =>  $attributeFamily
                )
            )
        );

    }

    /**
     * Enregistre une famille d'attribut
     */
    public function admin_edit($id = null)
    {

        if (!empty($this->request->data)) {

            $data = $this->request->data;

            if ($this->AttributeFamily->save($data)) {

                if (!$id) {

                    $id = $this->AttributeFamily->getLastInsertID();
                }

                exit(
                    json_encode(
                        array(
                            'statut'    =>  1,
                            'data'      => array(
                                'AttributeFamily' => array(
                                    'id' => $id
                                )
                            )
                        )
                    )
                );

            }

        }

        exit(
            json_encode(
                array(
                    'statut'    =>  0
                )
            )
        );

    }

    /**
     * Supprime une famille d'attribut
     */
    public function admin_delete($id = null)
    {

        if($this->AttributeFamily->delete($id)) {

            exit(
                json_encode(
                    array(
                        'statut'    =>  1
                    )
                )
            );

        }

        exit(
            json_encode(
                array(
                    'statut'    =>  0
                )
            )
        );

    }

    /**
     * Enregistre l'ordre
     */
    public function admin_ordre()
    {

        if (!empty($this->request->data)) {

            $data = $this->request->data['data'];
            $data = explode(',', $data);

            foreach ($data as $position => $id) {

                $this->AttributeFamily->id = $id;

                if (!$this->AttributeFamily->saveField('position', $position + 1)) {

                    exit(
                        json_encode(
                            array(
                                'statut'    =>  0
                            )
                        )
                    );

                }

            }

            exit(
                json_encode(
                    array(
                        'statut'    =>  1
                    )
                )
            );

        }

        exit(
            json_encode(
                array(
                    'statut'    =>  0
                )
            )
        );

    }

    /**
     * Retourne la liste des familles d'attribut
     */
    public function admin_getOptions() {

    	$options = $this->AttributeFamily->generateTreeList(null, '{n}.AttributeFamily.id', '{n}.AttributeFamily.name_admin', ' - ');

        exit(
            json_encode(
                array(
                    'statut'    =>  1,
                    'data'		=>	$options
                )
            )
        );    	

    }

}
