<?php

class AttributesController extends CatalogueAppController {

    private $attributes = array();

    /**
     * Admin
     */
    public function admin_index($attribute_family_id = null)
    {

        $attributes = $this->Attribute->find(
        	'threaded',
        	array(
        		'conditions' => array(
                    'Attribute.attribute_family_id'  =>  $attribute_family_id
        		),
        		'order' => array(
        			'Attribute.position' => 'ASC'
        		)
        	)
        );

        $this->attributes = $this->threadedToArray($attributes);

        exit(
            json_encode(
                array(
                    'statut'    =>  1,
                    'data'      =>  $this->attributes
                )
            )
        );

    }

    /**
     * Transforme un find threaded en find all
     */
    private function threadedToArray($attributes, $niveau = 0)
    {

        foreach ($attributes as $v) {

            $v['Attribute']['niveau'] = $niveau;

            for ($x=0; $x < $niveau; $x++) { 

                $v['Attribute']['name'] = '-' . $v['Attribute']['name'];

            }

            $this->attributes[] = $v;

            if (!empty($v['children'])) {

                $niveau++;
                $this->threadedToArray($v['children'], $niveau);
                $niveau--;

            }

        }

        return $this->attributes;

    }

    /**
     * Retourne une permanence
     */
    public function admin_view($id = null)
    {

        $attribute = $this->Attribute->findById($id);
        if (!$attribute) {

            exit(
                json_encode(
                    array(
                        'statut'    =>  0
                    )
                )
            );
        }

        $children = $this->Attribute->find(
            'all',
            array(
                'conditions' => array(
                    'Attribute.parent_id' => $id
                )
            )
        );

        if (!$children) {

            $children = array();
        }

        $attribute['children'] = $children;

        exit(
            json_encode(
                array(
                    'statut'    =>  1,
                    'data'      =>  $attribute
                )
            )
        );

    }

    /**
     * Enregistre une famille d'attribut
     */
    public function admin_edit($id = null)
    {

        if (!empty($this->request->data)) {

            $data = $this->request->data;

            if ($this->Attribute->save($data)) {

                if (!$id) {

                    $id = $this->Attribute->getLastInsertID();
                }

                exit(
                    json_encode(
                        array(
                            'statut'    =>  1,
                            'data'      => array(
                                'Attribute' => array(
                                    'id' => $id
                                )
                            )
                        )
                    )
                );

            }

        }

        exit(
            json_encode(
                array(
                    'statut'    =>  0
                )
            )
        );

    }

    /**
     * Supprime une famille d'attribut
     */
    public function admin_delete($id = null)
    {

        if($this->Attribute->delete($id)) {

            exit(
                json_encode(
                    array(
                        'statut'    =>  1
                    )
                )
            );

        }

        exit(
            json_encode(
                array(
                    'statut'    =>  0
                )
            )
        );

    }

    /**
     * Enregistre l'ordre
     */
    public function admin_ordre()
    {

        if (!empty($this->request->data)) {

            $data = $this->request->data['data'];
            $data = explode(',', $data);

            foreach ($data as $position => $id) {

                $this->Attribute->id = $id;

                if (!$this->Attribute->saveField('position', $position + 1)) {

                    exit(
                        json_encode(
                            array(
                                'statut'    =>  0
                            )
                        )
                    );

                }

            }

            // On réorganise l'arbre
            $id = $this->Attribute->field('parent_id', array('Attribute.id' => $id));

            if (!$id) {
                $id = null;
            }

            $this->Attribute->reorder(
                array(
                    'id'    =>  $id,
                    'field' =>  'Attribute.position',
                    'order' =>  'ASC'
                )
            );

            exit(
                json_encode(
                    array(
                        'statut'    =>  1
                    )
                )
            );

        }

        exit(
            json_encode(
                array(
                    'statut'    =>  0
                )
            )
        );

    }

    /**
     * Retourne la liste des familles d'attribut
     */
    public function admin_getOptions() {

        $options = $this->Attribute->generateTreeList(null, '{n}.Attribute.id', '{n}.Attribute.name', ' - ');

        exit(
            json_encode(
                array(
                    'statut'    =>  1,
                    'data'      =>  $options
                )
            )
        );      

    }

    /**
     * Retourne la liste des familles d'attribut
     */
    public function admin_getOptionsMultiple($attribute_family_id = null) {

        $conditions = array();

        if ($attribute_family_id != null) {
            $conditions['Attribute.attribute_family_id'] = $attribute_family_id;
        }

        $attributes = $this->Attribute->find(
            'threaded',
            array(
                'conditions'    =>  $conditions
            )
        );

        $attributes = $this->threadedToArray($attributes);

        $options = array();
        foreach ($attributes as $attribute) {

            $options[] = array(
                'id'    =>  $attribute['Attribute']['id'],
                'label' =>  $attribute['Attribute']['name']
            );

        }

        exit(
            json_encode(
                array(
                    'statut'    =>  1,
                    'data'      =>  $options
                )
            )
        );      

    }

}
