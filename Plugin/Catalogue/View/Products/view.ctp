<?php
	$this->extend('/Templates/template_default');
	$this->element('balises_metas', array('metas' => $product['Product']));

	$this->assign('h1', $product['Product']['h1']);

	foreach($_breadcrumb['Chemin'] as $v) $this->Html->addCrumb($v[$_breadcrumb['Model']]['name'], $v[$_breadcrumb['Model']]['url']);

?>

<?php $this->start('body'); ?>

<div class="container">

	<div class="row">

	    <div class="col-md-3">

	        <h2 class="h1"><span>Affiner la recherche</span></h2>
	        <?= $this->element('Catalogue.filtres'); ?>

	    </div>

	    <div class="col-md-9">

		    <div class="row">

			    <div class="col-md-8">

				    <?php  if (!empty($product['Product']['galerie']->files)) { ?>

					    <div id="carouselProduit" class="carousel slide" data-ride="carousel">

						    <div class="carousel-inner" role="listbox">

							    <?php foreach ($product['Product']['galerie']->files as $k => $v) { ?>

								    <div class="item <?php if($k == 0) echo 'active' ?>">
									    <?= $this->Html->link($this->WhFile->show($v, array('x' => 768, 'y' => 768), array('alt' => $product['Product']['name'])), $v->File->url, array('escape' => false, 'class' => 'fresco', 'data-fresco-group' => 'carouselProduit')); ?>
								    </div>

							    <?php } ?>

						    </div>

						    <ol class="carousel-indicators">

							    <?php foreach ($product['Product']['galerie']->files as $k => $v) { ?><!--

	                                 --><li data-target="#carouselProduit" data-slide-to="<?= $k; ?>" class="<?php if($k == 0) echo 'active' ?>"></li><!--

	                             --><?php } ?>

						    </ol>

					    </div>

					    <?php $this->Html->scriptStart($options = array('block' => 'scriptBottom')); ?>

					    $('#carouselProduit').carousel(
						    {
						        interval    :   5000
						    }
					    );

					    <?php $this->Html->scriptEnd(); ?>

				    <?php } ?>

			    </div>

			    <div class="col-md-4">

				    <h1><?= $product['Product']['h1']; ?></h1>
				    <p class="prix"><?= $this->WhPrix->show($product['Product']['prix_ttc']); ?></p>
				    <p class="resume"><?= $product['Product']['resume']; ?></p>
				    <?= $product['Product']['description']; ?>

				    <?php if (!empty($attributes)) { ?>

					    <section class="listeAttributes">

						    <div class="row">

							    <?php foreach ($attributes as $attribute) { ?>

								    <div class="col-md-6">

									    <div class="attribute">
										    <span><?= $attribute['Attribute']['name']; ?></span>
									    </div>

								    </div>

							    <?php } ?>

						    </div>

					    </section>

				    <?php } ?>

			    </div>

		    </div>

	    </div>

	</div>

</div>

<?php $this->end(); ?>
