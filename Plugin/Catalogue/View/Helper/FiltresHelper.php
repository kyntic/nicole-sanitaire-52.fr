<?php

App::uses('AppHelper', 'View/Helper');

class FiltresHelper extends AppHelper {
    
    public $helpers = array(
        'Html',
        'Form',
        'FileManager.WhFile'
    );

    public $html = '';
    public $niveau = 0;

    public function generateFiltresSelectionnes($attributeFamilies, $attributes) {

        $this->niveau++;

        if ($this->niveau == 1) {
            $this->html .= '<div class="filtresSelectionnes">';
        }

        foreach($attributeFamilies as $attributeFamily) {

            if (!empty($this->data['caracteristiques'][$attributeFamily['AttributeFamily']['id']])) {

                if(!empty($attributes[$attributeFamily['AttributeFamily']['id']])) {
                    $this->generateFiltresSelectionnesAttributes($attributes[$attributeFamily['AttributeFamily']['id']]);
                }

            }

            if(!empty($attributeFamily['children'])) {
                $this->generateFiltresSelectionnes($attributeFamily['children'], $attributes);
            }

        }

        if ($this->niveau == 1) {
            $this->html .= '</div>';
        }

        $this->niveau--;

        return $this->html;

    }

    public function generateFiltres($attributeFamilies, $attributes) {

        $this->niveau++;

        $this->html .= '<ul class="caracteristiqueFamilles">';

        foreach($attributeFamilies as $attributeFamily) {

            if (!empty($attributeFamily['AttributeFamily']['childrenIds'])) {

                $open = false;

                foreach ($attributeFamily['AttributeFamily']['childrenIds'] as $attributeFamilyChildId) {

                    if ($open) {
                        continue;
                    }

                    if (!empty($this->data['caracteristiques'][$attributeFamilyChildId])) {
                        $open = true;
                    }

                }

                if ($open) {
                    $this->html .= '<li class="open" >';
                } else {
                    $this->html .= '<li>';
                }

            } else {

                $this->html .= '<li>';

            }

            $this->html .= $this->Html->link($attributeFamily['AttributeFamily']['name'], '#');

            if(!empty($attributeFamily['children'])) {
                $this->generateFiltres($attributeFamily['children'], $attributes);
            }

            if(!empty($attributes[$attributeFamily['AttributeFamily']['id']])) {
                $this->generateFiltresAttributes($attributes[$attributeFamily['AttributeFamily']['id']]);
            }

            $this->html .= '</li>';

        }
        
        $this->html .= '</ul>';

        $this->niveau--;

        return $this->html;

    }

    public function generateFiltresSelectionnesAttributes($attributes) {

        foreach($attributes as $attribute) {

            if (!empty($this->data['caracteristiques'][$attribute['Attribute']['attribute_family_id']][$attribute['Attribute']['id']])) {

                $this->html .= '<div class="filtre">';
                    $this->html .= $this->Html->link('x', '#caracteristiques' . $attribute['Attribute']['attribute_family_id'] . $attribute['Attribute']['id']);
                    $this->html .= $attribute['Attribute']['name'];
                $this->html .= '</div>';

            }

            if(!empty($attribute['children'])) {
                $this->generateFiltresSelectionnesAttributes($attribute['children']);
            }

        }

        return $this->html;

    }

    public function generateFiltresAttributes($attributes) {

        $this->html .= '<ul class="caracteristiques">';

        foreach($attributes as $attribute) {

            $this->html .= '<li>';

            $this->html .= $this->Form->input('caracteristiques.' . $attribute['Attribute']['attribute_family_id'] . '.'  . $attribute['Attribute']['id'], array('label' => $attribute['Attribute']['name'], 'div' => false, 'type' => 'checkbox'));

            if(!empty($attribute['children'])) {
                $this->generateFiltresAttributes($attribute['children']);
            }

            $this->html .= '</li>';

        }
        
        $this->html .= '</ul>';

        return $this->html;

    }

    public function generateBlocsHome($attributeFamilies, $attributes) {

        foreach ($attributeFamilies as $attributeFamily) {

            $this->html .= '<div class="caracteristiqueFamille caracteristiqueFamille_' . $attributeFamily['AttributeFamily']['id'] . '">';

            $this->html .= '<p class="titre">' . $attributeFamily['AttributeFamily']['name'] . '</p>';

            if (!empty($attributeFamily['children'])) {
                $this->generateBlocsHome($attributeFamily['children'], $attributes);
            }

            if (!empty($attributes[$attributeFamily['AttributeFamily']['id']])) {

                $this->html .= '<div class="caracteristiques">';

                foreach ($attributes[$attributeFamily['AttributeFamily']['id']] as $attribute) {

                    $this->html .= '<div class="caracteristique">';

                        $this->html .= $this->Html->link($this->WhFile->show($attribute['Attribute']['vignette']) . $this->WhFile->show($attribute['Attribute']['vignette_hover'], array(), array('class' => 'hover')) . '<span class="txt" >' . $attribute['Attribute']['name'] . '</span><span class="hover" style="color:#' . $attribute['Attribute']['code_hexa'] . '" >' . $attribute['Attribute']['name'] . '</span>', '#', array('escape' => false));

                        $this->html .= $this->Form->create('Filtres', array('url' => '/recherche/', 'id' => 'FiltresIndexForm' . $attributeFamily['AttributeFamily']['id'] . '.' . $attribute['Attribute']['id']));
                            $this->html .= $this->Form->hidden('caracteristiques.' . $attributeFamily['AttributeFamily']['id'] . '.' . $attribute['Attribute']['id'], array('value' => 1, 'id' => 'filtreCaracteristiques.' . $attributeFamily['AttributeFamily']['id'] . '.' . $attribute['Attribute']['id']));
                        $this->html .= $this->Form->end();

                    $this->html .= '</div>';

                }

                $this->html .= '</div>';

            }

            $this->html .= '</div>';

        }

        return $this->html;

    }


}