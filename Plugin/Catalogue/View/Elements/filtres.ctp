<?= $this->Form->create('Filtres', array('id' => 'FiltresForm')); ?>
    <?= $this->Filtres->generateFiltresSelectionnes($filtres['attributeFamilies'], $filtres['attributes']); ?>
    <?php $this->Filtres->html = ''; ?>
    <?= $this->Filtres->generateFiltres($filtres['attributeFamilies'], $filtres['attributes']); ?>
    <?= $this->WhForm->submit('Lancer la recherche'); ?>
<?= $this->Form->end(); ?>

<?php $this->Html->scriptStart($options = array('block' => 'scriptBottom')); ?>

    $('#FiltresForm ul.caracteristiqueFamilles li a').click(function(){

        $(this).parent('li').toggleClass('open');
        return false;

    });

	$('#FiltresForm .filtresSelectionnes .filtre a').click(function(){

		$('#FiltresForm ' + $(this).attr('href')).attr('checked', false);
		$(this).parent('.filtre').remove();
		return false;

	});

	$('#FiltresForm input[type="checkbox"]').change(function(){
		$('#FiltresForm').submit();
	});

<?php $this->Html->scriptEnd(); ?>