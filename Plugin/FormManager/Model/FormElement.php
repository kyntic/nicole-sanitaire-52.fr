<?php

/**
 * Class FormElement
 */
class FormElement extends FormManagerAppModel
{
    /**
     * Before Save
     *
     * @param   array       $options
     * @return  bool|void
     */
    public function beforeSave($options = array())
    {

        if (isset($this->data['FormElement']['configuration'])) {

            /**
             * The options
             */
            $configuration = $this->data['FormElement']['configuration'];

            /**
             * The search elements
             */
            $search = array(
                '\n',
                '\r',
                '\t'
            );

            $configuration = str_replace($search, '', $configuration);

            $this->data['FormElement']['configuration'] = $configuration;
        }

        return true;
    }

    public function afterSave($created) {

        if(!empty($this->data['FormElement']['form_id'])) {

            /**
             * Count the number of elements in the form
             */
            $elements_number = $this->find(
                'count',
                array(
                    'conditions' => array(
                        'FormElement.form_id' => $this->data['FormElement']['form_id']
                    )
                )
            );

            $Form = ClassRegistry::init('FormManager.Form');

            $Form->id = $this->data['FormElement']['form_id'];
            $Form->saveField('elements_number', $elements_number);

        }

    }

    /**
     * The after find
     *
     * @param   array       $results
     * @param   bool        $primary
     * @return  mixed|void
     */
    public function afterFind($results, $primary = false)
    {
        foreach ($results as &$result) {

            $result['FormElement']['configuration'] = json_decode($result['FormElement']['configuration'], true);
        }

        return $results;
    }
}