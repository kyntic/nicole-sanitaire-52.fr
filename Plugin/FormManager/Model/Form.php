<?php

/**
 * Class Form
 */
class Form extends FormManagerAppModel
{

    public $belongsTo = array(
        'User',
        'Page'
    );

    /**
     * The export path
     *
     * @var string $_exportPath
     */
    private $_exportFilePath = null;


    public function afterSave($created) {

        if($created) {

            $this->id = $this->data['Form']['id'];
            $this->saveField('user_id', AuthComponent::user('id'));

        }

    }

    /**
     * Exports form data
     *
     * @param   null        $formId
     * @return  bool
     */
    public function export($formId = null)
    {
        if ($formId == null) {

            return false;
        }

        App::uses('File', 'Utility');

        /**
         * The models we need
         */
        $FormEntry      = ClassRegistry::init('FormManager.FormEntry');
        $FormElement    = ClassRegistry::init('FormManager.FormElement');

        /**
         * Define the export path
         */
        $this->_exportFilePath = APP . 'Export/Form/' . 'form_' . $formId . '_export_data.csv';

        /**
         * Opening the file
         */
        $dataFile = new File($this->_exportFilePath, true, 0755);

        /**
         * File is not writable
         */
        if (!$dataFile->writable()) {

            return false;
        }

        /**
         * Get the form elements
         */
        $formElements = $FormElement->find(
            'all',
            array(
                'conditions' => array(
                    'FormElement.form_id' => $formId
                ),
                'order' => array(
                    'FormElement.position ASC'
                )
            )
        );

        if (!$formElements) {

            return false;
        }

        /**
         * Add headers
         */
        foreach ($formElements as $formElement) {

            $dataFile->write($formElement['FormElement']['short_name'] . ';');
        }

        $dataFile->write("\r");

        $formEntries = $FormEntry->find(
            'all',
            array(
                'conditions' => array(
                    'FormEntry.form_id' => $formId
                ),
                'order' => array(
                    'FormElement.id ASC'
                )
            )
        );

        if (!$formEntries) {

            return false;
        }

        foreach ($formEntries as $key) {

            foreach ($key['FormEntry'] as $data) {

                $dataFile->write($data['data'] . ';');
            }

            $dataFile->write("\r");
        }

        $dataFile->close();

        return true;
    }
}