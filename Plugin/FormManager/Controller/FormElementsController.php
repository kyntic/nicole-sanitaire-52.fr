<?php

/**
 * Class FormElementsController
 */
class FormElementsController extends FormManagerAppController
{
    /**
     * List the elements in a form
     *
     * @param null $formId
     */
    public function admin_list($formId = null)
    {
        /**
         * No form id
         */
        if ($formId == null) {

            //On a une erreur
        }

        $elements = $this->FormElement->find(
            'all',
            array(
                'conditions' => array(
                    'FormElement.form_id' => $formId
                ),
                'order' => array(
                    'FormElement.position' => 'ASC'
                )
            )
        );

        echo json_encode($elements);
        exit();
    }

    /**
     * Change position
     *
     * @return json
     */
    public function admin_ordre()
    {
        $return['statut']  = 0;
        $return['number']  = 0;

        if (!empty($this->request->data)) {

            foreach ($this->request->data['page'] as $position => $id) {

                $this->FormElement->id = $id;

                if ($this->FormElement->saveField('position', $position)) {

                    $return['number']++;
                }
            }
        }

        $return['statut'] = 1;

        exit(json_encode($return));
    }

    /**
     * Edition of the basic elements
     *
     * @param null $formElementId
     */
    public function admin_edit($formElementId = null)
    {

        if (!empty($this->request->data)) {

            $data = $this->request->data;
            if(!empty($data['FormElement']['configuration'])) $data['FormElement']['configuration'] = json_encode($data['FormElement']['configuration']);

            $this->FormElement->save($data);

        }

        exit();


    }

    public function admin_delete($formElementId = null)
    {

        $res['ok'] = false;

        $this->FormElement->delete($formElementId);

        $res['ok'] = true;

        echo json_encode($res);

        exit();

    }

}