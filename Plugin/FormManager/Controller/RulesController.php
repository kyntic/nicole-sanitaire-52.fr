<?php

/**
 * Class RulesController
 */
class RulesController extends FormManagerAppController
{

    public function admin_getTags($type = null)
    {

        if(empty($type)) $type = null;

        if($type) {

            $tmpTagsLists = $this->Rule->getTagTypes($type);

        } else {

            $tmpTagsLists = $this->Rule->getTagsList();

        }

    	foreach($tmpTagsLists as $k => $v) {

    		$tagsLists[] = array(
    			'id'	=>	$k,
    			'label'	=>	$v
    		);

    	}


        echo json_encode($tagsLists);

        exit();

    }

    public function admin_get_all()
    {


        /**
         * XML Class
         */
        App::uses('Xml', 'Utility');

        /**
         * Get the rules from XML
         */
        $tagsLists = Xml::toArray(Xml::build(APP . 'Plugin/FormManager/rules/rules.xml'));

        echo json_encode($tagsLists);

        exit();

    }


}