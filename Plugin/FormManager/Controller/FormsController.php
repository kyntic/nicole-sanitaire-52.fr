<?php

/**
 * Class FormsController
 */
class FormsController extends FormManagerAppController
{
    /**
     * The helpers
     *
     * @var array $helpers
     */
    public $helpers = array(
        'FrontForm'
    );

    /**
     * The components
     *
     * @var array $components
     */
    public $components = array(
        'RequestHandler'
    );

    /**
     * List all the forms
     *
     * @return void
     */
    public function admin_index()
    {
        $forms = $this->Form->find('all');

        // if ($forms) {

        //     foreach ($forms as &$form) {

        //         if (isset($form['Form']['user_id'])) {

        //             $this->loadModel('Ldap.LdapUser');

        //             $user = $this->LdapUser->findById($form['Form']['user_id']);

        //             /**
        //              * If we have a user
        //              */
        //             if ($user) {

        //                 $form['User'] = $user['LdapUser'];
        //             }
        //         }
        //     }
        // }

        echo json_encode($forms);
        exit();

    }

    /**
     * Display the form
     *
     * @param   null        $pageId
     * @return  bool|array
     */
    public function display($pageId = null)
    {
        /**
         * No form id
         */
        if ($pageId == null) {

            return false;
        }

        /**
         * The model we need
         */
        $this->loadModel('FormManager.FormElement');
        $this->loadModel('FormManager.FormEntry');

        /**
         * Form has already been submitted
         */
        $validatedByUserIp = $this->FormEntry->find(
            'count',
            array(
                'FormEntry.user_ip' => $this->request->clientIp()
            )
        );

        if ($validatedByUserIp > 0) {

            //return false;
        }

        /**
         * Load the form
         */
        $form = $this->Form->find('first', array('conditions' => array('Form.page_id' => $pageId)));

        if (!$form) {

            return false;
        }

        /**
         * Get the form elements
         */
        $formElements = $this->FormElement->find(
            'all',
            array(
                'conditions' => array(
                    'FormElement.form_id' => $form['Form']['id']
                ),
                'order' => array(
                    'FormElement.position' => 'ASC'
                )
            )
        );

        $return = array(
            'form'          => $form,
            'formElements'  => $formElements
        );

        return $return;
    }

    /**
     * Submit the data
     */
    public function submit()
    {
        if (!empty($this->request->data)) {

            $this->loadModel('FormManager.FormEntry');

            $data = $this->request->data;

            /**
             * Get the user IP
             */
            $userIp = $this->request->clientIp();

            /**
             * Get the user ID
             */
            $userId = AuthComponent::user('id');

            $collectionId = uniqid();

            /**
             * TODO : Ajouter de la validation Cake pour les elements.
             */
            foreach ($data['Form']['element'] as $elementId => $elementValue) {

                if (is_array($elementValue)) {

                    $elementValue = json_encode($elementValue);
                }

                /**
                 * The data to save
                 */
                $dataToSave = array(
                    'FormEntry' => array(
                        'id'            => null,
                        'user_id'       => $userId,
                        'user_ip'       => $userIp,
                        'element_id'    => $elementId,
                        'form_id'       => $data['Form']['id'],
                        'data'          => $elementValue,
                        'collection_id' => $collectionId
                    )
                );

                $this->FormEntry->save($dataToSave);
            }

            $return = array(
                'status'    => 1,
                'message'   => 'Formulaire enregistré avec succès'
            );

            exit(json_encode($return));
        }

        $return = array(
            'status'    => 0,
            'message'   => 'Aucune donnée envoyée'
        );

        exit(json_encode($return));
    }

    /**
     * Edit Form
     *
     * @param null $formId
     */
    public function admin_edit($formId = null)
    {

        if (!empty($this->request->data)) {

            $data = $this->request->data;

            $this->Form->save($data);
        }

        if($formId) {

            echo json_encode($this->Form->findById($formId));

        }


        exit();

    }

    /**
     * Create form
     *
     * @return void
     */
    public function admin_create()
    {
        /**
         * If we have data
         */
        if (!empty($this->request->data)) {

            $data = $this->request->data;

            if ($this->Form->save($data)) {

                $this->Session->setFlash('Formulaire ajouté avec succès', 'success');
                $this->redirect('/admin/form_manager/forms/edit/' . $this->Form->getLastInsertID());
            }

            $this->Session->setFlash('Formulaire non ajouté', 'error');
            $this->redirect(array('action' => 'admin_index'));
        }

        /**
         * Get the user ID
         */
        $userId = AuthComponent::user('id');

        $this->set('userId', $userId);
    }

    /**
     * Add an element to the form
     *
     * @param null $formId
     */
    public function admin_add_element($formId = null)
    {
        if (!empty($this->request->data)) {

            /**
             * The model we need
             */
            $this->loadModel('FormManager.FormElement');

            $data = $this->request->data;

            if ($this->FormElement->save($data)) {

                $return = array(
                    'status'    => 1,
                    'message'   => 'Elément ajouté avec succès'
                );

                exit(json_encode($return));
            }

            $return = array(
                'status'    => 0,
                'message'   => 'Elément non ajouté'
            );

            exit(json_encode($return));
        }

        /**
         * No form ID
         */
        if ($formId == null) {

            $return = array(
                'status'    => 0,
                'message'   => 'Id de formulaire manquant'
            );

            exit(json_encode($return));
        }

        /**
         * The model we need
         */
        $this->loadModel('FormManager.Rule');

        /**
         * Get all the tag types
         */
        $tagTypes = $this->Rule->getTagsList();

        /**
         * Get the user id
         */
        $userId = AuthComponent::user('id');

        /**
         * Pass data to the view
         */
        $this->set('tagTypes', $tagTypes);
        $this->set('formId', $formId);
        $this->set('userId', $userId);

        $this->layout = 'admin_popup';
    }

    /**
     * Delete form
     *
     * @param null $formId
     */
    public function admin_delete($formId = null)
    {

        $res['ok'] = false;

        $this->Form->delete($formId);

        $this->loadModel('FormManager.FormElement');
        $this->FormElement->deleteAll(
            array('FormElement.form_id' => $formId)
        );

        $this->loadModel('FormManager.FormEntry');
        $this->FormEntry->deleteAll(
            array('FormEntry.form_id' => $formId)
        );

        $res['ok'] = true;

        echo json_encode($res);

        exit();

    }

    /**
     * Ajoute le module dans la page
     *
     * @param $id
     * @param $model
     */
    public function admin_add_page($id, $model)
    {
        $this->loadModel('ContentModule');

        if (!empty($this->data)) {

            $data = $this->data;

            $data['ContentModule']['model_asso'] = 'Form';
            $data['ContentModule']['model'] = $model;
            $data['ContentModule']['model_id'] = $id;

            $this->ContentModule->save($data);

            $this->Session->setFlash('Module ajouté', 'success');

            $this->redirect(Controller::referer());
        }

        $this->set('lst', $this->Form->find('list', array('order' => 'Form.name')));

        $this->layout = 'admin_popup';
    }

    /**
     * Export the data
     *
     * @param null $formId
     */
    public function admin_export($formId = null)
    {
        /**
         * No form ID
         */
        if ($formId == null) {

            $this->Session->setFlash('Id du formulaire manquant', 'error');
            $this->redirect($this->referer());
        }

        $this->loadModel('FormManager.FormElement');
        $this->loadModel('FormManager.FormEntry');

        /**
         * Get all the form elements
         */
        $formElements = $this->FormElement->find(
            'all',
            array(
                'conditions' => array(
                    'FormElement.form_id' => $formId
                ),
                'order' => array(
                    'FormElement.position ASC'
                )
            )
        );

        if (!$formElements) {

            $this->Session->setFlash('Eléments du formulaire non trouvés', 'error');
            $this->redirect($this->referer());
        }

        /**
         * Get all the entries
         */
        $formEntries = $this->FormEntry->find(
            'all',
            array(
                'conditions' => array(
                    'FormEntry.form_id' => $formId
                ),
                'order' => array(
                    'FormElement.id ASC'
                )
            )
        );

        if (!$formEntries) {

            $this->Session->setFlash('Aucunes données dans votre formulaire', 'error');
            $this->redirect($this->referer());
        }

        $formattedFormElements = array();

        foreach ($formElements as $formElement) {

            $formattedFormElements[$formElement['FormElement']['id']] = $formElement['FormElement']['short_name'];
        }

        $this->set('formEntries', $formEntries);
        $this->set('formattedFormElements', $formattedFormElements);

        $this->layout = 'export_xls';
    }
}