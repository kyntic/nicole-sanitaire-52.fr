<?php
$this->assign('titre', 'Edition d\'un élément');
echo $this->Smart->create('FormElement', array('class' => 'smart-form'));
?>

<div class="modal-body">
    <fieldset>
        <?=$this->Smart->input('FormElement.name', 'Nom de l\'élément'); ?>
        <?=$this->Smart->input('FormElement.short_name', 'Nom court (Maximum 16 caractères) : ');?>
        <?=$this->Smart->select('FormElement.type', 'Type de l\'élément', $tagTypes, array('class' => 'select2')); ?>
        <label class="checkbox">
            <input type="checkbox" name="data[FormElement][mandatory]" <?=($this->data['FormElement']['mandatory'] ? 'checked' : '');?>>
            <i></i>Obligatoire ?
        </label>
        <?=$this->Smart->hidden('FormElement.user_id');?>
        <?=$this->Smart->hidden('FormElement.form_id');?>
        <?=$this->Smart->hidden('FormElement.id');?>
    </fieldset>
</div>
<div class="modal-footer">
    <footer>

        <?php echo $this->Form->button(__('Enregistrer'), array('type' => 'submit', 'class' => 'btn btn-primary btn-xs'));?>

    </footer>
</div>

<?=$this->Smart->end();?>

<script type="text/javascript">

    /**
     * Form submit action
     */
    $('#FormElementAdminEditForm').on('submit', function(e) {

        e.preventDefault();

        var form = $(this);

        $.ajax({
            type 		: 'POST',
            url 		: $(this).attr('action'),
            data 		: $(this).serialize(),
            cache 	    : false,
            complete	: function() {

                $(form).hideLoading()
            },
            success 	: function(response) {

                /**
                 * Get the response
                 */
                var response = jQuery.parseJSON(response);

                if (response.status == 0) { //Erreur

                    var alert = '<div class="padded"><div class="alert alert-error" style="margin:0;" >' + response.message + '</div></div>';

                    form.prepend(alert);
                }

                if (response.status == 1) { //Ok

                    $('#myModal').modal('hide');

                    loadElements(<?=$formId;?>);
                }
            }
        });
    });
</script>