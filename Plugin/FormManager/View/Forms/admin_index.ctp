<?php
$this->assign('titre', '<i class="fa fa-user"></i> Gestion des formulaires');
$this->Html->addCrumb(__('Gestion des formulaires'), $this->here);


echo $this->Smart->openBox(
    array(
        'icone' => 'fa-list',
        'btns'  => array(
            $this->Html->link('<i class="fa fa-plus-square"></i> Créer un nouveau formulaire', '/admin/form_manager/forms/create', array('escape' => false, 'class' => 'btn btn-xs btn-success'))
        )
    )
);
?>
<div class="widget-body no-padding">

    <table class="table table-normal table-striped">
        <thead>
        <tr>
            <th class="col-md-1"></th>
            <th>Nom</th>
            <th>Nombre d'éléments</th>
            <th>Créé par</th>
        </tr>
        </thead>

        <tbody>
        <?php foreach($forms as $form) : ?>
            <tr>
                <td>
                    <?php echo $this->Html->link('<i class="fa fa-edit"></i>', '/admin/form_manager/forms/edit/' . $form['Form']['id'], array('escape' => false, 'class' => 'btn btn-primary btn-xs')).' ';?>
                    <?php echo $this->Html->link('<i class="fa fa-trash-o"></i>', '/admin/form_manager/forms/delete/' . $form['Form']['id'], array('escape' => false, 'class' => 'btn btn-danger btn-xs'), 'Etes vous sûre de vouloir supprimer cet utilisateur ?');?>




                </td>
                <td><?php echo $form['Form']['name'];?></td>
                <td><?php echo $form['Form']['elements_number'];?></td>
                <td><?php echo (isset($form['User']['cn']) ? $form['User']['cn'] : '');?></td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>

</div>
<?php $this->Smart->closeBox();?>
