<STYLE type="text/css">
    .tableTd {
        border-width: 0.5pt;
        border: solid;
    }
    .tableTdContent{
        border-width: 0.5pt;
        border: solid;
    }
    #titles{
        font-weight: bolder;
    }

</STYLE>
<table>
    <tr id="titles">
        <td class="tableTdContent" >Formulaire</td>
    </tr>
    <tr>
        <?php foreach ($formattedFormElements as $formattedFormElement) : ?>
            <td class="tableTdContent"><?=$formattedFormElement;?></td>
        <?php endforeach; ?>
    </tr>
    <?php $model = 'FormEntry'; foreach($formEntries as $formEntry): ?>
        <tr>
        <?php foreach ($formEntry['FormEntry'] as $subEntry) : ?>
            <td class="tableTdContent"><?=$subEntry['data'];?></td>
        <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
</table>
