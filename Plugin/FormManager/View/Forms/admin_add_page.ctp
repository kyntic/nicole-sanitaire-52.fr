<?php 
$this->assign('titre', 'Ajouter un formulaire');
echo $this->Smart->create('Form', array('class' => 'smart-form'));
?>

<div class="modal-body">
    <fieldset>
        <label class="select" for="">
            <?=$this->Smart->input('ContentModule.model_asso_id', 'Formulaire', array('options' => $lst));?>
        </label>
    </fieldset>
</div>
<div class="modal-footer">
    <footer>

        <?=$this->Form->button('<i class="fa fa-plus-square"></i> '.__('Ajouter le module'), array('type' => 'submit', 'class' => 'btn btn-success btn-xs'));?>

    </footer>
</div>

<?=$this->Smart->end();?>
<script type="text/javascript" >
	

</script>