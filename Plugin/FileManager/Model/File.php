<?php

/**
 * Class File
 */
class File extends AppModel
{
    /**
     * The belongs to
     *
     * @var array $belongsTo
     */
    public $belongsTo = array(
        'FileManager.Extention',
        'Folder' => array(
            'className'     => 'FileManager.FileFolder',
            'foreignKey'    => 'file_folder_id'
        )
    );

    /**
     * After find
     *
     * @param   mixed   $results
     * @param   bool    $primary
     * @return  mixed
     */
    public function afterFind($results, $primary = false)
    {
        foreach ($results as &$v) {

            if (isset($v[$this->alias]['dimenssions'])) {

                $v[$this->alias]['dimenssions'] = json_decode($v[$this->alias]['dimenssions']);
            }
        }

        return $results;
    }

    /**
     * The Uploaded file
     *
     * @param   mixed           $file
     * @param   null            $folderId
     * @return  array|mixed
     */
	public function UploadedFile($file, $folderId = null)
    {
        App::uses('Folder', 'Utility');
        App::uses('File', 'Utility');

        /**
         * On prépare l'upload
         */
        App::import('Component', 'FileManager.Upload');

        $upload = new UploadComponent($file);


        $upload->file_max_size = 100000000;

        /**
         * Initialisation de l'objet retourné
         */
        $file  = array();

        $fileName = Inflector::slug(strtolower($upload->file_src_name_body)).uniqid();

        $folder = 'files/' . date('Y') . '/' . date('m') . '/';

        $dir = new Folder(WWW_ROOT . $folder, true, 0775);

        if ($upload->uploaded) {

            /**
             * Données initiale du fichier
             */
            $data['File']['id']                 = null;
            $data['File']['type']               = ($upload->file_is_image) ? 'image' : 'file'; // Attention, ne peut prendre la valeur document car entre en conflie avec le plugin document
            $data['File']['extention_id']       = $upload->file_src_name_ext;
            $data['File']['slug']               = $fileName;
            $data['File']['nom_fichier']        = $fileName . '.' . $upload->file_src_name_ext;
            $data['File']['poids']              = $upload->file_src_size / 1000 ;
            $data['File']['dossier']            = '/' . $folder;
            $data['File']['url']                = '/' . $folder . $data['File']['nom_fichier'];
            $data['File']['file_folder_id']     = $folderId;
            $data['File']['name']               = $upload->file_src_name_body;
            $data['File']['dimenssions']        = '';


            if ($data['File']['type'] == 'image') {

                $reco = 'normal';

                if ($upload->image_src_x > 1000 || $upload->image_src_y > 1000) {

                    $reco = 'trop grande';
                }

                if ($upload->image_src_x < 50 || $upload->image_src_y < 50) {

                    $reco = 'trop petite';
                }

                $data['File']['dimenssions'] = json_encode(
                    array(
                        'width'     => $upload->image_src_x,
                        'height'    => $upload->image_src_y,
                        'reco'      => $reco
                    )
                );
            }

            /**
             * Traitement sur fichier
             */
            $upload->file_overwrite             = true;
            $upload->file_new_name_body         = $data['File']['slug'];

            /**
             * Envoie du fichier
             */
            $upload->Process($dir->path);

            if ($upload->processed) {

                /**
                 * Enregistrement du fichier en bdo
                 */
                $this->save($data);

                /**
                 * On vide le cache
                 */
                $upload->Clean();

                $file = $this->read();

            } else {

                /**
                 * Vraissemblablement problème d'écriture
                 */
                echo 'error : ' . $upload->error;
            }
        }

        return $file;
	}

    /**
     * The before save
     *
     * @param   array       $options
     * @return  bool|void
     */
    public function beforeSave($options = Array())
    {
        if (!empty($this->data[$this->alias]['dimenssions']) &&
            is_array($this->data[$this->alias]['dimenssions'])
        ) {
            $this->data[$this->alias]['dimenssions'] = json_encode($this->data[$this->alias]['dimenssions']);
        }
    }
}