<?php

/**
 * Class FilesController
 */
class FilesController extends FileManagerAppController
{
    /**
     * The uses
     *
     * @var array $uses
     */
    public $uses = array(
        'FileManager.File',
        'FileManager.FileAssoc'
    );

    /**
     * Admin index
     *
     * @param null $id
     */
    public function admin_index($id = null)
    {
        $this->loadModel('FileManager.FileFolder');

        $data = array(
            'Folder' => array(
                'FileFolder' => array(
                    'id' => null
                ),
                'Chemin' => array()
            ),
            'Files' => array(

            )
        );

        /**
         * 1 - Trouver les fichiers dans le dossier indiqué
         */
        if(!$id) $id = null;

        $data['Files'] = $this->File->find('all', array('order' => 'File.created DESC', 'group' => 'File.id', 'conditions' => array('File.file_folder_id' => $id)));

        /**
         * 2 - Trouver les infos du dossier et son arborescence :
         */
        $this->Session->write('File.folder_id', null);

        if($id) {

            $data['Folder'] = $this->FileFolder->findById($id);
            $data['Chemin'] = $this->FileFolder->getPath($id);

            $this->Session->write('File.folder_id', $id);
        }

        /**
         * Sortie
         */
        echo json_encode($data);

        exit();
    }

    /**
     * Admin folders
     */
    public function admin_folders()
    {
        $this->loadModel('FileManager.FileFolder');

        $data['tree'] = $this->FileFolder->find(
            'threaded', array(
                'order' => 'FileFolder.name'
            )
        );

        $data['list'] = $this->FileFolder->generateTreeList(null, '{n}.FileFolder.id', '{n}.FileFolder.name', ' - ');

        /**
         * Sortie
         */
        echo json_encode($data);

        exit();
    }

    /**
     * Create new folder
     */
    public function admin_new_folder()
    {
        $this->loadModel('FileManager.FileFolder');

        $res['ok'] = false;

        if (!empty($this->data)) {

            $data = $this->data;

            $this->FileFolder->save($data);

            $res['ok'] = true;
            $res['id'] = $this->FileFolder->id;
        }

        /**
         * Sortie
         */
        echo json_encode($res);

        exit();
    }

    /**
     * Delete folder
     *
     * @param $folder_id
     */
    public function admin_delete_folder($folder_id)
    {
        $this->loadModel('FileManager.FileFolder');

        $folds_ids = $this->FileFolder->children($folder_id);

        foreach($folds_ids as $v) {

            $this->FileFolder->delete($v['FileFolder']['id']);

            $this->File->updateAll(array('File.file_folder_id' => null), array('File.file_folder_id' => $v['FileFolder']['id']));
        }

        $this->FileFolder->delete($folder_id);

        $this->File->updateAll(array('File.file_folder_id' => null), array('File.file_folder_id' => $folder_id));

        exit();
    }

    /*
     *
     * ATTENTION SUPPRIMER DE LA BDO LES LIGNE FAISANT REFERENCE
     *
    */
    public function admin_delete($id) {

        $this->loadModel('FileManager.FileAssoc');

        App::uses('Folder', 'Utility');
        App::uses('File', 'Utility');

        $Fichier = $this->File->findById($id);    

        $this->File->delete($id);
        $this->FileAssoc->deleteAll(array('FileAssoc.file_id' => $id));
 
        if(!empty($Fichier['File']['url']) && is_file(WWW_ROOT.$Fichier['File']['url'])) {

            if(unlink(WWW_ROOT . $Fichier['File']['url'])) {

                echo 'oui';

            } else {

                echo 'non';
            }
        }

        exit();
    }

    /**
     * Admin edit
     */
    public function admin_edit()
    {
        if (!empty($this->data)) {

            $data = $this->data;

            $this->File->save($data);
            
            echo json_encode(
                array(
                    'error' => 0,
                    'txt'   => 'Fichier modifié'
                )
            );

            exit();
        }

        exit();
    }

    /**
     * Upload d'un nouveau fichier
     *
     * @return json
     */
    public function admin_upload()
    {
        $folderId = $this->Session->read('File.folder_id');

        if (isset($_FILES['upfile'])) {

            $return = $this->File->UploadedFile($_FILES['upfile'], $folderId);

            echo json_encode($return);
        }

        /**
         * Redactor image plugin
         */
        if (isset($_FILES['file'])) {

            $return = $this->File->UploadedFile($_FILES['file'], null);

            /**
             * This is for the Redactor image plugin
             */
            $return['filelink'] = $return['File']['url'];

            echo json_encode($return);
        }

        exit();
    }

    /**
     * Admin ordre
     *
     * @return json
     */
    public function admin_ordre()
    {
        $this->loadModel('FileManager.FileAssoc');

        $return['statut']  = false;
        $return['nbr']     = 0;

        if (!empty($this->request->data)) {

            foreach ($this->request->data['file_assoc'] as $position => $id) {
                
                $this->FileAssoc->id = $id;

                if ($this->FileAssoc->saveField('position', $position)) {

                   $return['nbr']++;
                }
            }
        }

        $return['statut'] = true;

        exit(json_encode($return));
    }

    /**
     * Import from deposit
     *
     * @param   null    $folderId
     * @return  json
     */
    public function admin_import_from_deposit($folderId = null)
    {
        /**
         * The model we need
         */
        $this->loadModel('FileManager.FileFolder');

        /**
         * The utility
         */
        App::uses('Folder', 'Utility');
        App::uses('File', 'Utility');

        /**
         * No folder id
         */
        if ($folderId == null) {

            $response = array(
                'status'    => 0,
                'message'   => 'Id de dossier manquant'
            );

            exit(json_encode($response));
        }

        /**
         * Get the folder
         */
        $folder = $this->FileFolder->findById($folderId);

        if (!$folder) {

            $response = array(
                'status'    => 0,
                'message'   => 'Dossier non trouvé'
            );

            exit(json_encode($response));
        }

        /**
         * The folder where the files are located
         */
        $depositFolder = new Folder(APP . 'deposit');

        /**
         * Get all the files
         */
        $files = $depositFolder->find('.*');

        foreach ($files as $file) {

            $copiedFile = $this->File->UploadedFile(APP . 'deposit/' . $file, $folder['FileFolder']['id']);

            /**
             * Si le fichier a ete copié
             */
            if ($copiedFile) {

                unlink(APP . 'deposit/' . $file);
            }
        }

        $return = array(
            'status'    => 1,
            'message'   => 'Fichiers copiés avec succès'
        );

        exit(json_encode($return));
    }
}