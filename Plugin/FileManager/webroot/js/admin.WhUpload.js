/*
 * Class WhFiles 
 *
 */

(function($) {
     
	/*
	 * Création du plugin
	 */
	$.fn.WhUpload = function (options) {
	
		var optExtended = $.extend({

			'btn_upload' 	: false, 
			'params_url'	: false


		}, options);

		return this.each(function () {
			
			var WhUp = new WhUpload($(this), optExtended);

			$(this).data('WhUp', WhUp);

		});

	}

	$.fn.WhUploadFinished = function(data) {
	
		this.each(function() {
			
			$this = $(this);
			
        	$(this).data('WhUp').actualise();

        	$('#myModalLarge').modal('hide');

        	if($(this).data('WhUp').options.finish) {

        		$(this).data('WhUp').options.finish();
        		
        	}

        });


    }


	/*
	 * Constructeur
	 */
	function WhUpload(element, options) {
		
		var self 		= this;
		self.el 		= element;
		self.options 	= options;


        if(self.options.btn_upload) {
        	
        	var el = self.options.btn_upload;


        	$(el).click(function () {

        		self.popup();

        	});

        }

        if(self.options.container && self.options.container_url) {

        	self.actualise();

        }
 


	};


	/*
	 * Methode charge le contenu dans le contenaire
	 */
	WhUpload.prototype.actualise = function() {
		
		var self = this;

		$(self.options.container).showLoading();
		
		$(self.options.container).load(self.options.container_url, function() {

			$(self.options.container).hideLoading();

        	if(self.options.finish) {

				self.options.finish();
				        		
			}

			$('.btn-delete', $(self.el)).click(function() {

				if(confirm('Etes vous sûre de voiloir supprimer cet item ? ')) {

					$.ajax({
						  url 		: $(this).attr('href'),
						  cache 	: false, 
						  success 	: function (reponse) {

						  	if(reponse == 1) {

						  		self.actualise();


						  	}else{

						  		alert(reponse);

						  	}

						 }

					});

				}

				return false;
			});

			$('.btn-edit', $(self.el)).click(function() {

				var param = (self.options.params_url) ? self.options.params_url : '';

				$('#myModalLarge').modal({remote : $(this).attr('href') + '/' + param});

				$('#myModalLarge').modal('show');

				return false;

			});


		});

	}

	/*
	 * Créer une popup fonction de l'éléments appelé
	 */   	
   WhUpload.prototype.popup = function() {

		var self = this;

		var param = (self.options.params_url) ? self.options.params_url : '';

		console.log('/admin/file_manager/files/add/el:' + self.el.attr('id') + '/' + param);

		$('#myModalLarge').modal({
			remote : '/admin/file_manager/files/add/el:' + self.el.attr('id') + '/' + param
		});

		$('#myModalLarge').modal('show');

		return false;

	}


})(jQuery);

