<?php
App::uses('AppHelper', 'View/Helper');

class WhFileHelper extends AppHelper {

	public $helpers = array('Html');

	public $upload;

	public $format;

	public $format_def = array(
		'crop'	 	=> true, 
		'x' 		=> 100, 
		'y' 		=> 100
	);


	public $format_named = array(
		'admin' => array(
			'crop'	 	=> true, 
			'x' 		=> 100, 
			'y' 		=> 100
		),
		'liste_rubriques' => array(
			'crop'	 	=> true, 
			'x' 		=> 768, 
			'y' 		=> 768
		),
		'liste_produits' => array(
			'crop'	 	=> true, 
			'x' 		=> 768, 
			'y' 		=> 768
		)
	);


	public function __construct(View $view, $settings = array()) {

        parent::__construct($view, $settings);

        App::import('Component', 'FileManager.Upload');

        $this->format['admin'] = array(
        	'x' 	=> 100, 
        	'y' 	=> 100, 
        	'crop'	=> true
        );


    }

	public function show($data = array(), $format = array(), $params = array()) {

		if(is_object($data)) {

			$data = json_decode(json_encode($data), true);
			$data = $data['File'];
		}

		if(is_array($data)) {

			switch($data['type']) {

				case 'image' : return $this->image($data, $format, $params);
				case 'video' : return $this->video($data, $format, $params);
				case 'fichier' : return $this->fichier($data, $format, $params);

			}

		}else{

			if(empty($data['id'])) return (isset($format['empty'])) ? $format['empty']  : '';

		}

	}


	public function image ($data = array(), $format = array(), $params = array()) {

		if(is_array($data)) {}

		if($format && !is_array($format)) {

			if(isset($this->format_named[$format])) $format = $this->format_named[$format];

		}

		if(!preg_match('#^http#', $data['url']) && !preg_match('#^/#', $data['url'])) $data['url'] = '/'.$data['url'];

		if(!$format) return $this->Html->image($data['url'], $params);

		//On explode les paramètres
		$ext = $format;
		
		if(is_array($ext)) {

			$ext = implode('-', $ext);

		}else{

			$format = $this->formats[$format];

		}

		$format = array_merge($this->format_def, $format);

		$name_body = 'thumb_'.$ext.'_'.$data['slug'];
		$data['dossier'] = 'files/' . date('Y/m/d') . '/';

		$fichier = $data['dossier'] . '/' . $name_body . '.jpg';

		
		if(is_file(WWW_ROOT . $fichier)) {

			return $this->Html->image('/' . $fichier, $params);

		} else {

			$upload = new UploadComponent(WWW_ROOT . $data['url']);

			$upload->file_overwrite        = true;
			$upload->image_resize          = true;	
			$upload->file_new_name_body    = $name_body;	
			$upload->image_y               = $format['y'];
			$upload->image_x               = $format['x'];
			
			if(isset($format['crop']) && $format['crop']) {

				$upload->image_ratio_crop      = true;
				$upload->image_ratio           = true;		

			}else{

				$upload->image_ratio_fill       = true;
				$upload->image_background_color = '#FFFFFF';

			}

			if(isset($format['ratio']) && $format['ratio']) {

				$upload->image_ratio          = true;
				$upload->image_ratio_crop     = false;
				$upload->image_ratio_fill     = false;

				
			}
			
			$upload->image_convert    	   = 'jpg';
		
			$upload->process(WWW_ROOT . $data['dossier']);

            if ($upload->processed) {

            	return $this->Html->image('/'.$fichier, $params);

            } else {

            	return 'miniature non générée';

            }

		}

		return $this->Html->image($data['url'], $params);

	}


	public function video () {

		


	}

	public function fichier () {

		


	}


}
?>