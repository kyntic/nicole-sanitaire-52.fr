<?php 
$this->assign('titre', __('Ajout de média(s)'));
$param_url = array(); foreach($this->request->params['named'] as $k => $v) $param_url[] = $k.':'.$v; $param_url = implode('/', $param_url); //Traformation du tableau en chaine de paramètres pour les urls 

?>

<ul class="nav nav-tabs nav-tabs-left">
	<li class="active"><a href="#file_depot_bliblio" data-toggle="tab"><i class="icon-sitemap"></i> Bibliothèque</a></li>
	<li><a href="#file_depot_ordi" data-toggle="tab"><i class="icon-upload-alt"></i> <span>Depuis votre ordinateur</span></a></li>
</ul>

<div class="tab-content">

	<div class="tab-pane active" id="file_depot_bliblio">
		<iframe src="/admin/file_manager/files/bibliotheque/<?php echo $param_url;?>" frameborder="0" id="File_bibliotheque" style="height: 450px;"></iframe>
	</div>

	<div class="tab-pane " id="file_depot_ordi">
		<div class="padded"><iframe src="/admin/file_manager/files/upload/<?php echo $param_url;?>" frameborder="0"></iframe></div>
	</div>

</div>
