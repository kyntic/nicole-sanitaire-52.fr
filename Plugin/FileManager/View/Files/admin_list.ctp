<?php foreach($Files as $v) : ;?>
<tr id="file_assoc_<?php echo $v['FileAssoc']['id']; ?>" >
	<td><?php echo $this->WhFile->show($v['File'], array('x' => 100, 'y' => 100));?></td>
	<td><?php echo $v['File']['type'];?></td>
	<td><?php echo $v['File']['name'];?></td>
	<td><?php echo $v['File']['description'];?></td>
	<td><?php echo $this->Html->link($v['File']['lien'], $v['File']['lien'], array('target' => '_blank'));?></td>
	<td>
		<?php
			echo $this->Html->link('<i class="fa fa-edit"></i>', '/admin/file_manager/files/edit/'.$v['File']['id'], array('class' => 'btn btn-xs btn-primary btn-edit', 'escape' => false)).' ';
			echo $this->Html->link('<i class="fa fa-trash-o"></i>', '/admin/file_manager/files/delete_assoc/'.$v['FileAssoc']['id'], array('class' => 'btn btn-danger btn-xs btn-delete', 'escape' => false));
		?>
	</td>
</tr>
<?php endforeach;?> 