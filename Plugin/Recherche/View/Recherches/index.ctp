<?php

	$this->element(
		'balises_metas',
		array(
			'metas' => array(
				'meta_title'	=>	'Recherche "' . $this->data['Recherche']['recherche'] . '"'
			)
		)
	);

?>

<div class="page-wrapper">

    <header class="page-heading clearfix">
        <h1 class="heading-title pull-left"><?= 'Recherche "' . $this->data['Recherche']['recherche'] . '"'; ?></h1>
    </header>

    <div class="page-content">

        <div class="row page-row">

			<div class="col-md-3">

                <?= $this->Form->create('Recherche', array('url' => '/recherche', 'class' => 'course-finder-form', 'id' => 'RechercheForm')); ?>

                    <div class="row">

                        <div class="col-md-12">

                            <?= $this->Form->input('Recherche.type.actu', array('label' => 'Article', 'type' => 'checkbox')); ?>
                            <?= $this->Form->input('Recherche.page', array('label' => 'Page', 'type' => 'checkbox', 'options' => false)); ?>
                            <?= $this->Form->input('Recherche.type.video', array('label' => 'Vidéo', 'type' => 'checkbox', 'options' => false)); ?>
                            <?= $this->Form->input('Recherche.type.document', array('label' => 'Document', 'type' => 'checkbox')); ?>
							<?= $this->Form->select('Recherche.page_id', $rechercheSections, array('label' => false, 'div' => false, 'class' => 'form-control subject', 'empty' => 'Choisissez une section')); ?>
							<?= $this->Form->input('Recherche.recherche', array('label' => false, 'div' => false, 'class' => 'form-control pull-left', 'placeholder' => 'Votre recherche')); ?>
                            <button type="submit" class="btn btn-theme"><i class="fa fa-search"></i></button>

                        </div>

                    </div>
                <?= $this->Form->end(); ?>

			</div>

            <div class="col-md-9">

				<div class="row">

					<?php if(!empty($pages)) { ?>

						<div class="col-md-6">

							<div class="section-content">

								<?php foreach($pages as $k => $v) { ?>

									<article class="article-item row page-row">

										<?php if(!empty($v['Page']['vignette'])) { ?>

											<div class="img col-md-3 col-sm-12 col-xs-12">
												<?=(!empty($v['Page']['vignette'])) ? $this->Html->image($v['Page']['vignette']->File->url, array('url' => $v['Page']['url'], 'class' => 'img-responsive')) : '';?>
											</div>
											<div class="details col-md-9 col-sm-12 col-xs-12">

										<?php } else { ?>

											<div class="details col-md-12 col-sm-12 col-xs-12">

										<?php } ?>

											<h5 class="title"><?=$this->Html->link($v['Page']['name'], $v['Page']['url']);?></h5>
											<p><?= $v['Page']['resume']; ?></p>
										</div>

									</article>

								<?php } ?>

							</div>

						</div>

						<div class="col-md-6">

					<?php } else { ?>

						<div class="col-md-12">

					<?php } ?>

							<div class="section-content">

								<?php foreach($actualites as $k => $v) { ?>

									<article class="article-item row page-row">

										<?php if(!empty($v['Actualite']['vignette'])) { ?>

											<div class="img col-md-3 col-sm-12 col-xs-12">
												<?=(!empty($v['Actualite']['vignette'])) ? $this->Html->image($v['Actualite']['vignette']->File->url, array('url' => $v['Actualite']['url'], 'class' => 'img-responsive')) : '';?>
											</div>
											<div class="details col-md-9 col-sm-12 col-xs-12">

										<?php } else { ?>

											<div class="details col-md-12 col-sm-12 col-xs-12">

										<?php } ?>

											<h5 class="title"><?=$this->Html->link($v['Actualite']['name'], $v['Actualite']['url']);?></h5>
											<p><?= $v['Actualite']['resume']; ?></p>
										</div>

									</article>

								<?php } ?>

							</div>

						</div>

				</div>

            </div>

        </div>

    </div>

</div>