<?php

App::uses('Sanitize', 'Utility');

class RecherchesController extends RechercheAppController {
   
	var $uses = array('Catalogue.Produit', 'Catalogue.Rubrique');

	/**
	 * Characters allowed in the search query
	 * Override it with Configure::write('Search.allowed_chars', array(...))
	 *
	 * @var array Defaults to valid french accents
	 */
	var $allowedChars = array(
		' ',
		'â', 'à', 'ä',
		'é', 'è', 'ê', 'ë',
		'î', 'ï',
		'ô', 'ö',
		'ù', 'û', 'ü',
		'ç', 'œ', 'æ', '.', '\\'
	);

	/**
	 * Looks for config options overrides in Configure class
	 * - Search.mode : type of MySQL search, natural or boolean
	 * - Search.allowedChars : characters allowed in the search query
	 */
	function beforeFilter()
	{
		
		// Allowed chars
		if($allowedChars = Configure::read('Recherche.allowedChars'))
		{
			if(is_array($allowedChars))
			{
				$this->allowedChars = $allowedChars;
			}
		}
		
		parent::beforeFilter();
	}
	
	/**
	 * Search function
	 * PRG pattern : http://en.wikipedia.org/wiki/Post/Redirect/Get
	 *
	 * @param string $q Search query
	 */

	public function index($q = null)
	{

		$conditions = array(
			'Actualite' => array(
				'Actualite.etat_id'	=>	'publish'
			),
			'Page' => array(
				'Page.etat_id'	=>	'publish'
			)
		);

		if(!empty($this->request->data))
		{

			$data = $this->request->data;

			$this->Session->write('Recherche.data', $data);

			$this->redirect($this->here);

		} else {

			$data = $this->Session->read('Recherche.data');

		}

		$this->data = $data;

		$listerPages = false;

		if(!empty($this->data['Recherche'])) {

			foreach($this->data['Recherche'] as $champ => $value) {

				if(!$value) continue;

				switch ($champ) {

					// Types d'actualités
					case 'type':

						$types = array();

						foreach($value as $type => $etat) {

							if($etat) $types[] = $type;

						}

						if(!empty($types)) $conditions['Actualite']['Actualite.type'] = $types;

					break;

					// Booléen lister page
					case 'page':

						$listerPages = true;

					break;

					// page_id des actualités
					case 'page_id':

						$conditions['Actualite']['Actualite.page_id'] = $value;

					break;

					// Champ de recherche
					case 'recherche':

						$statement = $this->_prepareForBooleanMode($this->_clean($value));

						$conditions['Page'][] = 'MATCH(Page.h1, Page.name, Page.resume, Page.body) AGAINST("'.$statement.'" IN BOOLEAN MODE)';
						$conditions['Actualite'][] = 'MATCH(Actualite.h1, Actualite.name, Actualite.resume, Actualite.body) AGAINST("'.$statement.'" IN BOOLEAN MODE)';

					break;

				}

			}

		}

		$this->loadModel('Blog.Actualite');
		$actualites = $this->Actualite->find(
			'all',
			array(
				'conditions'	=>	$conditions['Actualite']
			)
		);
		$this->set('actualites', $actualites);

		if($listerPages) {

			$this->loadModel('Page');
			$pages = $this->Page->find(
				'all',
				array(
					'conditions'	=>	$conditions['Page']
				)
			);
			$this->set('pages', $pages);

		}

		$this->loadModel('Page');
        // Chargement des sections de recherche
        $rechercheSections = $this->Page->find(
            'list',
            array(
                'fields' => array(
                    'Page.id',
                    'Page.name'
                ),
                'conditions' => array(
                    'Page.etat_id'      =>  'publish',
                    'Page.parent_id'    =>  null,
                    'Page.id !='        =>  2
                )
            )
        );
        $this->set('rechercheSections', $rechercheSections);

	}


	/**
	 * Prepares a search query for a MySQL Fulltext search in boolean mode
	 *
	 * @param string $query Search terms
	 * @return string Returns a string ready for a MySQL Fulltext search in boolean mode (+term1 +term2 ...)
	 */
	function _prepareForBooleanMode($query)
	{
		$terms = explode(' ', $query);
		
		if(count($terms) < 2)
		{
			return $query;
		}
		
		return '+' . join(' +', $terms);
	}
	
	/**
	 * Prepares a search query for a MySQL Fulltext search in natural language.
	 *
	 * @param string $query Search terms
	 * @return string $query
	 */
	function _prepareForNaturalMode($query)
	{
		// If withQueryExpansion option is not defined
		if($this->withQueryExpansion === null)
		{
			// Force query expansion for less than 2 words
			if(count(explode(' ', $query)) < 2)
			{
				$this->withQueryExpansion = true;
			}
		}
		
		return $query;
	}
	
	/**
	 * Cleans up a string
	 *
	 * @param string $str String to clean up
	 * @return string Clean string
	 */
	function _clean($str)
	{

		$str = Sanitize::paranoid($str, $this->allowedChars);

		$str = preg_replace('/ +/', ' ', $str);
		$str = trim($str);
		
		return $str;
	}

}
?>