<div class="container">

	<div class="row">

	    <div class="col-md-12">

		    <?php if (is_object($content['content'])) { ?>

		        <?php if (isset($content['content']->embed) && !empty($content['content']->embed)) { ?>

				    <?= $content['content']->embed; ?>

				<?php } else if (isset($content['content']->url) && !empty($content['content']->url)) { ?>

		            <iframe width="100%" height="315" src="<?= str_replace('watch?v=', 'embed/', $content['content']->url); ?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>

		        <?php } else if(isset($content['content']->video)) { ?>

				    <video width="100%" height="315" controls="controls" src="<?= $content['content']->video->File->url; ?>">
				    </video>

		        <?php } ?>

		    <?php } ?>

	    </div>

	</div>

</div>