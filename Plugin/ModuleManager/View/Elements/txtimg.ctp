<div class="row">
<?php
switch($content['content']->params->prop) {

    case 'img-right' :

        echo '<div class="col-md-8">'.$content['content']->txt.'</div>';
        echo '<div class="col-md-4">'.$this->Html->image($content['content']->img->File->url, array('title' => $content['content']->img->File->name)).'</div>';

        break;

    case 'img-left' :

        echo '<div class="col-md-4">'.$this->Html->image($content['content']->img->File->url, array('title' => $content['content']->img->File->name)).'</div>';
        echo '<div class="col-md-8">'.$content['content']->txt.'</div>';

        break;

}

?>
</div>
