<div class="full-width-container white-bg gray-border-top gray-border-bott" id="zone_metier">

    <div class="container padding-top48 padding-bottom96 text-center">




        <div class="container  padding-bottom24">

            <ul id="container" class="no-style margin0 q-sand-target four-columns-q-sand padding0 super-list variable-sizes clearfix isotope">


                <?php
                $content['content'] = get_object_vars($content['content']);

                $img = false;
                $docs = false;

                foreach($content['content']['files'] as $v) :

                    if($v->File->type == 'image') {

                        $img = true;
                        ?>

                        <li class="pull-left border-box relative overflow-hidden element * projet_type isotope-item">

                            <div class="relative">
                                <?=$this->Html->link($this->Html->image($v->File->url), $v->File->url, array('title' => $v->File->name, 'class' => 'prettyphoto', 'rel' => 'prettyPhoto', 'escape' => false));?>
                                <span class="hover-wrapper-home">
                                <span class="overflow-hidden display-table">
                                    <span>
                                        <span class="padding-left24 padding-right24 project-links">
                                            <span class="project-info text-center display-block">
                                                <span class="inline-block white-border-bott letters-white padding-bottom12 hover-big-letters"><?=$v->File->name;?></span>
                                            </span>
                                            <a href="<?=$v->File->url;?>" class="inline-block letters-white margin-top12" rel="prettyPhoto[pp_gal1]"><?=$v->File->name;?></a>
                                            <span class="clearfix"></span>
                                            <a href="<?=$v->File->url;?>" class="inline-block letters-white margin-top12" rel="prettyPhoto[pp_gal2]"><?=$v->File->description;?></a>
                                        </span>
                                    </span>
                                </span>
                            </span>
                            </div>

                        </li>
                    <?php }else{$docs = true;} endforeach; ?>
            </ul>

        </div>

    </div>

</div>



<?php
if($img) {
    //$this->Html->script('/college-green/assets/plugins/pretty-photo/js/jquery.prettyPhoto.js', array('block' => 'scriptBottom'));
    $this->Html->css('/themeforest/Site/css/prettyPhoto.css', null, array('inline' => false));
}

if($docs) :;
?>
<div class="row page-row">
    <div class="panel panel-theme">
        <div class="panel-heading">A télécharger</div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Fichier</th>
                    <th>Date</th>
                    <th>Download</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($content['content']['files'] as $v) : ; if($v->File->type == 'image') continue; ?>
                <tr>
                    <td><i class="fa fa-"></i></td>
                    <td><?=$v->File->name;?></td>
                    <td><?=$this->WhDate->show($v->File->created);?></td>
                    <td><a class="btn btn-primary" href="<?=$v->File->url;?>" target="_blank"><i class="fa fa-download"></i></a></td>
                </tr>
                <?php endforeach;?>
                </tbody>
            </table><!--//table-->
        </div><!--//table-responsive-->
    </div>
</div>
<?php endif; ?>