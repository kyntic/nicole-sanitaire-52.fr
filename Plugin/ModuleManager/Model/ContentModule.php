<?php
/**
 * Class ContentModule
 */
class ContentModule extends ModuleManagerAppModel
{

    public $actsAs = array(
        /**'Translate' => array(
            'content'
        )**/
    );


    public function beforeSave ($options = Array()) {

        if(isset($this->data[$this->alias]['content'])) {

            $this->data[$this->alias]['content'] = json_encode($this->data[$this->alias]['content']);

        }

    }

    public function afterFind ($results, $primary = false) {


        if(!empty($results)) {


            foreach($results as &$v) {

                if(!empty($v[$this->alias])) {

                    if(!empty($v[$this->alias]['content'])) {

                        $v[$this->alias]['content'] = json_decode($v[$this->alias]['content']);

                    }
                }

            }

        }

        return $results;


    }

}