<section id="myCarousel" class="carousel slide">

    <div class="carousel-inner">
        <?php $x = 0; foreach($Items as $i): $x++;
            ?>
            <div class="item <?php if($x == 1) echo 'active'; ?>">
                <div class="vignette">
                    <?php if(!empty($i->File->lien)) {
                        echo $this->Html->link($this->Html->image($i->File->url),$i->File->lien, array('escape' => false));
                    }else{
                        echo $this->Html->image($i->File->url);
                    }?>
                </div>
                <div class="text">
                    <?php if(!empty($i->File->name))?><p class="titre"><?php echo $i->File->name;?></p><br/>
                    <?php if(!empty($i->File->description))?><p class="description"><?php echo $i->File->description?></p><br/>

                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <ol class="carousel-indicators">
        <?php $x = 0; foreach($Items as $i):  ?>
            <li data-target="#myCarousel" data-slide-to="<?php echo $x; ?>" <?php if($x == 0) echo 'class="active"'; ?>></li>
            <?php $x++; endforeach; ?>
    </ol>
</section>

<?php
$this->Html->scriptStart(array('block' => 'scriptSuperBottom', 'inline' => false));
?>
$('#myCarousel').carousel({
    interval: 5000,
    pause: null
});
<?php
$this->Html->scriptEnd();
?>

