<?php
$this->assign('titre', '<i class="icon-list"></i> Liste des campagnes');
$this->Html->addCrumb(__('Gestion des campagnes'), '/admin/newsletter_manager/newsletter_campaigns');

echo $this->Smart->openBox(
    array(
        'icone' => 'fa-list',
        'btns'  => array(
            $this->Html->link('<i class="fa fa-plus-square""></i> Créer', array('action' => 'add'), array('escape' => false, 'class' => 'btn btn-success xs', 'data-toggle' => 'modal', 'data-target' => '#popup_ajx'))
        )
    )
);
?>
<div class="widget-body no-padding">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <th>Nom</th>
            <th>Liste</th>
            <th>Template</th>
            <th>Admin</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($campaigns as $campaign): ?>
            <tr>
                <td><?=$campaign['NewsletterCampaign']['id'];?></td>
                <td><?=$campaign['NewsletterCampaign']['name'];?></td>
                <td><?=$campaign['NewsletterList']['name'];?></td>
                <td><?=$campaign['NewsletterCampaign']['template_id'];?></td>
                <td>
                    <?php
                    echo ' ' . $this->Html->link('<i class="fa fa-edit"></i>', '/admin/newsletter_manager/newsletter_campaigns/edit/' . $campaign['NewsletterCampaign']['id'], array('escape' => false, 'class' => 'btn btn-primary btn-xs'));
                    echo ' ' . $this->Html->link('<i class="fa fa-trash-o"></i>', '/admin/newsletter_manager/newsletter_campaigns/delete/'. $campaign['NewsletterCampaign']['id'], array('escape' => false, 'class' => 'btn btn-danger btn-xs'), 'Etes vous sûr de vouloir supprimer cette campagne ?');
                    echo ' ' . $this->Html->link('Envoyer', '/admin/newsletter_manager/newsletter_campaigns/send/'. $campaign['NewsletterCampaign']['id'], array('escape' => false, 'class' => 'btn btn-success btn-xs'));
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php $this->Smart->closeBox();?>
