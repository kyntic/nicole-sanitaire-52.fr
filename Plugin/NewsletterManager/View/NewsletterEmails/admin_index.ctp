<?php $this->assign('titre', '<i class="icon-list"></i> Liste des emails'); ?>
<?php foreach ($emails as $email) : ?>
<tr id="email_<?=$email['NewsletterEmail']['id'];?>">
    <td><?=$email['NewsletterEmail']['id'];?></td>
    <td><?=$email['NewsletterEmail']['email'];?></td>
    <td><?=$email['NewsletterEmail']['first_name']?></td>
    <td><?=$email['NewsletterEmail']['last_name'];?></td>
    <td style="text-align: right;"><?=$this->Html->link('<i class="fa fa-trash-o"></i> Effacer', '/admin/newsletter_manager/newsletter_lists/delete/' . $email['ListEmail']['list_id'] . '/' . $email['ListEmail']['email_id'], array('escape' => false, 'class' => 'btn btn-danger btn-xs', 'data-id' => $email['NewsletterEmail']['id']))?></td>
</tr>
<?php endforeach; ?>

<script type="text/javascript" >
    $('#list-emails .btn-red').click(function(){
        $.ajax({
            type 		: 'POST',
            url 		: $(this).attr('href'),
            complete	: function () {chargeListeEmail();}
        });
        $('#email_' + $(this).data('id')).remove();
        return false;
    });
</script>