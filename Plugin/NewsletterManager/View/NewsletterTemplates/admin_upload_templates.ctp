<?=$this->WhForm->create('NewsletterTemplate', array('url' => '/admin/newsletter_manager/newsletter_templates/upload_templates/' . $this->data['NewsletterTemplate']['id'], 'class' => 'fill-up', 'type' => 'file'));?>
            <table class="table table-normal">
                <tr>
                    <td>Ajouter un template</td>
                    <td class="input">
                        <?=$this->WhForm->input('NewsletterTemplate.file', '', array('type' => 'file', 'label' => false));?>
                    </td>
                </tr>
            </table>
            <div class="box-footer padded">
                <span class="pull-right">
                <?php echo $this->Form->button(__('Ajouter'), array('type' => 'submit', 'class' => 'btn btn-blue btn-small'));?>
                </span>
            </div>
            <?=$this->WhForm->end();?>