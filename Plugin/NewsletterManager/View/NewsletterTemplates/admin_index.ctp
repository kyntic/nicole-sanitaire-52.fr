<?php
$this->assign('titre', '<i class="fa fa-list"></i> Liste des templates');
$this->Html->addCrumb(__('Liste des templates'), '/admin/newsletter_manager/newsletter_templates');


echo $this->Smart->openBox(
    array(
        'icone' => 'fa-list',
        'btns'  => array(
            $this->Html->link('<i class="fa fa-plus-square""></i> Créer', array('action' => 'add'), array('escape' => false, 'class' => 'btn btn-success', 'data-toggle' => 'modal', 'data-target' => '#popup_ajx'))
        )
    )
);
?>
<div class="widget-body no-padding">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <th>Nom</th>
            <th>Dossier</th>
            <th>Admin</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($templates as $template): ?>
            <tr>
                <td><?=$template['NewsletterTemplate']['id'];?></td>
                <td><?=$template['NewsletterTemplate']['name'];?></td>
                <td><?=$template['NewsletterTemplate']['folder_name'];?></td>
                <td>
                    <?php
                    echo ' ' . $this->Html->link('<i class="fa-edit fa "></i>', '/admin/newsletter_manager/newsletter_templates/edit/' . $template['NewsletterTemplate']['id'], array('escape' => false, 'class' => 'btn btn-primary btn-xs'));
                    echo ' ' . $this->Html->link('<i class="fa fa-trash-o "></i>', '/admin/newsletter_manager/newsletter_templates/delete/'. $template['NewsletterTemplate']['id'], array('escape' => false, 'class' => 'btn btn-danger btn-xs'), 'Etes vous sûr de vouloir supprimer ce template ?');
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php $this->Smart->closeBox();?>
