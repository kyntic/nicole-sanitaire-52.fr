<?php
$this->assign('titre', '<i class="icon-enveloppe"></i> Édition d\'un template');
$this->Html->addCrumb(__('Gestion des templates'), '/admin/newsletter_manager/newsletter_templates/');
?>
<div class="row">
    <article class="col-sm-12 col-md-12 col-lg-9">

        <?php
        echo $this->Smart->openBox(
            array(
                'icone' => 'fa-edit',
                'titre' => 'Informations'
            )
        );
        ?>
        <div class="widget-body no-padding">

            <?php echo $this->Smart->create('NewsletterTemplateEdit', array('class' => 'smart-form'));?>
            <fieldset>

                <?=$this->Smart->input('NewsletterTemplate.name', 'Nom du template');?>
                <?=$this->Smart->input('NewsletterTemplate.folder_name', 'Nom du dossier', array('disabled'));?>
                <?=$this->Form->input('NewsletterTemplate.folder_name', array('type' => 'hidden'));?>
                <div class="alert alert-info">
                    <h4>Information!</h4>
                    Vos images sont stockées dans le dossier : <strong><?='/htdocs/' . $templateFolder . $this->data['NewsletterTemplate']['folder_name'] . '/';?></strong><br />
                    Pour qu'elles apparaissent dans le courriel vous devez renseigner le lien suivant : <strong><?=$httpLink;?> - votre_image.png</strong>
                </div>
                <?=$this->Smart->input('NewsletterTemplate.content', 'Contenu du template', array('type' => 'textarea', 'value' => $templateContent, 'class' => 'tinyMce', 'style' => 'width:100%;'));?>

            </fieldset>
            <footer>

                <?php echo $this->Smart->submit();?>

            </footer>
            <?=$this->Smart->end();?>
            <?=$this->element('Tinymce.toolbarre', array('Textarea' => '.tinyMce', 'css_files' => FULL_BASE_URL.'/css/less/style.css,'.FULL_BASE_URL.'/bootstrap/css/bootstrap.css'));?>


        </div>

        <?=$this->Smart->closeBox();?>

    </article>

    <article class="col-sm-12 col-md-3 col-lg-3">

        <?php if(!$this->data['NewsletterTemplate']['is_uploaded']) : ?>

            <?=$this->Smart->openBox(array('icone' => 'fa-info', 'titre' => 'Enregistrer'));?>
            <?=$this->Smart->create('NewsletterTemplate', array('url' => '/admin/newsletter_manager/newsletter_templates/upload_templates/' . $this->data['NewsletterTemplate']['id'], 'type' => 'file'));?>

            <div class="widget-body no-padding smart-form">

                <table class="table table-normal table-striped table-bordered th-right">
                    <tr>
                        <td>Ajouter un template</td>
                        <td class="input">
                            <?=$this->Smart->input('NewsletterTemplate.file', '', array('type' => 'file', 'label' => false));?>
                        </td>
                    </tr>
                </table>

                <footer>
                    <?=$this->Form->button(__('Enregistrer'), array('type' => 'submit', 'class' => 'btn btn-primary btn-xs'));?>
                </footer>

            </div>

            <?=$this->Smart->end();?>

            <?=$this->Smart->closeBox();?>

        <?php endif; ?>
    </article>



</div>
