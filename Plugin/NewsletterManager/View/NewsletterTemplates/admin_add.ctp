<?php $this->assign('titre', 'Création de template'); ?>
<?=$this->WhForm->create('NewsletterTemplate', array('url' => '/admin/newsletter_manager/newsletter_templates/add', 'class' => 'form-horizontal fill-up'));?>
    <div class="padded">
        <?=$this->WhForm->input('NewsletterTemplate.name', 'Nom du template');?>
    </div>
    <div class="modal-footer">
        <button class="btn btn-blue" type="submit">Enregistrer</button>
    </div>
<?=$this->WhForm->end();?>