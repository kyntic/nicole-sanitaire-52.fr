<?php

/**
 * Class NewsletterInscritsController
 */
class NewsletterInscritsController extends NewsletterManagerAppController
{
    public function inscription()
    {
        $this->layout = 'ajax';

        $result = array();

        if (!empty($this->request->data)) {

            $data = $this->request->data;

            if ($this->NewsletterInscrit->find('first', array('conditions' => array('NewsletterInscrit.email' => $data['NewsletterInscrit']['email'])))) {

                if ($this->NewsletterInscrit->deleteAll(array('NewsletterInscrit.email' => $data['NewsletterInscrit']['email']))) {

                    //$this->Session->setFlash('Ce mail a été désinscrit !', 'success');
                    $statut['statut'] = 1;
                    $statut['message']['titre'] = 'Ce mail a été désinscrit!';
                    $statut['message']['texte'] = 'Ce mail a été désinscrit!';

                }

            } else {

                $this->NewsletterInscrit->set($data);

                if ($this->NewsletterInscrit->save($data)) {

                    //$this->Session->setFlash('Votre mail a été ajouté !', 'success');
                    $statut['statut'] = 1;
                    $statut['message']['titre'] = 'Votre mail a été ajouté !';
                    $statut['message']['texte'] = 'Votre mail a été ajouté !';

                } else {

                    //$this->Session->setFlash('Veuillez saisir un email valide !', 'error');
                    $statut['statut'] = 0;
                    $statut['erreurs']['err'][] = 'Veuillez saisir un email valide !';

                }

            }

            exit(json_encode($statut));
            $this->redirect(Controller::referer());

        }

    }

    public function admin_index()
    {

        $inscrits = $this->NewsletterInscrit->find(
            'all'
        );

        exit(
            json_encode(
                array(
                    'statut'    =>  1,
                    'data'      =>  $inscrits
                )
            )
        );

    }

    public function admin_exporter()
    {

        $inscrits = $this->NewsletterInscrit->find(
            'all'
        );
        $this->set('inscrits', $inscrits);

        $this->layout = 'export_xls';

    }

}
