<?php

/**
 * Class NewsletterListsController
 */
class NewsletterListsController extends NewsletterManagerAppController
{

    /**
     * Default admin action
     */
    public function admin_index()
    {
        $this->loadModel('NewsletterManager.ListEmail');

        $lists = $this->NewsletterList->find('all');

        $this->set('lists', $lists);

        $this->set('MenuAdminActives', array(80, 82));
    }

    /**
     * Editing a list
     */
    public function admin_edit($id = null)
    {
        if ($id == null) {
            $this->redirect('/admin/newsletter_manager/newsletter_lists/');
        }

        if (!empty($this->request->data)) {
            $data = $this->request->data;

            $this->NewsletterList->id = $id;

            if ($this->NewsletterList->save($data)) {
                $this->Session->setFlash('Liste sauvegardé', 'admin_success');
                $this->redirect('/admin/newsletter_manager/newsletter_lists/edit/' . $id);
            }
        }

        $this->data = $this->NewsletterList->find(
            'first',
            array(
                'conditions' => array(
                    'NewsletterList.id' => $id
                )
            )
        );

        $this->set('MenuAdminActives', array(80, 82));
    }

    /**
     * Delete a list
     */
    public function admin_delete($idList = null, $idEmail = null)
    {
        if ($idList !== null && $idEmail !== null) {
            $this->loadModel('NewsletterManager.ListEmail');

            $this->ListEmail->deleteAll(
                array(
                    'ListEmail.list_id'     => $idList,
                    'ListEmail.email_id'    => $idEmail
                )
            );
        }

        exit();
    }

    /**
     * Adding a list
     */
    public function admin_add()
    {
        if(!empty($this->request->data)) {

            $data = $this->request->data;

            if ($this->NewsletterList->save($data)) {

                $this->Session->setFlash('Liste créé', 'admin_success');

                $id = $this->NewsletterList->getLastInsertID();

                if ($id) {

                    $this->redirect('/admin/newsletter_manager/newsletter_lists/edit/' . $id);
                }
            }
        }

        $this->layout = 'admin_popup';
    }
}