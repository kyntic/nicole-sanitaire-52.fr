<?php

class NewsletterManagerAppController extends AppController
{
    /**
     * @var string
     */
    protected $_templatesFolder;

    /**
     * Function executed before other actions
     */
    public function beforeFilter()
    {
        parent::beforeFilter();

        $this->_templatesFolder = 'templates/';
    }
}
