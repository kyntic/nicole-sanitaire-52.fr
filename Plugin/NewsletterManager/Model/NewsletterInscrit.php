<?php
class NewsletterInscrit extends AppModel {

    public $validate = array(
        'email' => array(
            'rule' => 'email',
            'required'  => true, 
            'allowEmpty' => false, 
            'message'   => 'Le champ doit être un email valide'
        )
    );
}
