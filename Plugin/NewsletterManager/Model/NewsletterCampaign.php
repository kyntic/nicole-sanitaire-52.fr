<?php

/**
 * Class NewsletterCampaing
 */
class NewsletterCampaign extends AppModel
{
    /**
     * The belongs to
     *
     * @var array $belongsTo
     */
    public $belongsTo = array(
        'NewsletterList' => array(
            'className'     => 'NewsletterManager.NewsletterList',
            'foreignKey'    => 'list_id'
        )
    );

    /**
     * The validation
     *
     * @var array $validate
     */
    public $validate = array(
        /**'send_to' => array(
            'nonVide' => array(
                'rule'          => 'notEmpty',
                'required'      => true,
                'allowEmpty'    => false,
                'message'       => 'Veuillez renseigner une liste de diffusion'
            )
        ),**/
        'default_from_subject' => array(
            'nonVide' => array(
                'rule'          => 'notEmpty',
                'required'      => true,
                'allowEmpty'    => false,
                'message'       => 'Veuillez renseigner un sujet'
            )
        )
    );

    /**
     * The after find
     *
     * @param   mixed       $results
     * @param   bool        $primary
     * @return  mixed|void
     */
    public function afterFind($results, $primary = false)
    {
        foreach ($results as &$result) {

            if (!empty($result[$this->alias]['message'])) {

                if ($result[$this->alias]['type'] == 'email') {

                    $result[$this->alias]['mail_message'] = $result[$this->alias]['message'];

                } else {

                    $result[$this->alias]['sms_message'] = $result[$this->alias]['message'];
                }
            }

            if (!empty($result[$this->alias]['send_to'])) {

                $result[$this->alias]['send_to'] = json_decode($result[$this->alias]['send_to'], true);
            }
        }

        return $results;
    }

    /**
     *
     *
     * @param   array       $options
     * @return  bool|void
     */
    public function beforeSave($options = array())
    {
        if (isset($this->data[$this->alias]['sujet']) &&
            empty($this->data[$this->alias]['name'])
        ) {

            $this->data[$this->alias]['name'] = $this->data[$this->alias]['sujet'];
        }

        /**
         * Encode the send to
         */
        if (isset($this->data[$this->alias]['send_to']) &&
            is_array($this->data[$this->alias]['send_to'])
        ) {
            $this->data[$this->alias]['send_to'] = json_encode($this->data[$this->alias]['send_to']);
        }
    }
}
