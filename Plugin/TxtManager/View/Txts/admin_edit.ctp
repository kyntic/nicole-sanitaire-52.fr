<?php 
$this->assign('titre', 'Edition d\'un texte');
$this->Html->addCrumb(__('Gestion des textes'), '/admin/txt_manager/txts');
$this->Html->addCrumb(__('Edition d\'un texte'), $this->here);
?>
<div class="row">
    <article class="col-sm-12 col-md-12 col-lg-6">

    <?php 
        echo $this->Smart->openBox(
            array(
                'icone' => 'fa-edit', 
                'titre' => 'Textes'
            )
        );
    ?>
        <div class="widget-body no-padding">
            
            <?php echo $this->Smart->create('Parametre', array('class' => 'smart-form'));?>

            <fieldset>
                <?php echo $this->Smart->textarea('Txt.description', 'Description : '); ?>
                <?php echo $this->Smart->textarea('Txt.txt', 'Contenu : ', array('style' => 'min-height:450px;', 'class' => 'tinyMce')); ?>
            </fieldset>

            <footer><?php echo $this->Smart->submit(); ?></footer>

            <?php 
                echo $this->Smart->hidden('Txt.id');
                echo $this->Smart->end();
            ?>
        </div>
        <?php echo $this->Smart->closeBox();?>

    </article>
</div>

<?php echo $this->element('Tinymce.toolbarre', array('Textarea' => '.tinyMce', 'css_files' => FULL_BASE_URL.'/css/less/style.css,'.FULL_BASE_URL.'/bootstrap/css/bootstrap.css')); ?>