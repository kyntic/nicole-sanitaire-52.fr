<?php

/**
STRUCTURE : 

CREATE TABLE IF NOT EXISTS `txts` (
  `id` varchar(128) NOT NULL,
  `type` varchar(128) DEFAULT NULL COMMENT '(email ou contenu)',
  `name` varchar(256) DEFAULT NULL,
  `plugin` varchar(128) DEFAULT NULL,
  `controller` varchar(128) DEFAULT NULL,
  `description` text NOT NULL,
  `txt` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

*/

/**
 * Class Txt
 */
class Txt extends AppModel
{
    public function prepare($id, $var = null)
    {
        $remp = array();

        $i = 0;

        //Taille du array
        if ($var) {

            if (!is_array($var)) $var = array(0 => $var);

            if (!is_numeric(key($var))) $var = array(0 => $var);

            foreach ($var as $tab) {

                foreach ($tab as $k => $v) {

                    foreach ($v as $x => $y) {

                        if (!is_array($y)) {

                            $patt[$i] = '#\{' . $k . '.' . $x . '\}#';
                            $remp[$i] = $y;

                            $i++;
                        }
                    }
                }
            }
        }

        $patt['FULL_BASE_URL'] = '#\{FULL_BASE_URL\}#';
        $remp['FULL_BASE_URL'] = FULL_BASE_URL;

        if (!is_array($id)) $id = array($id);

        $texte = array();

        foreach ($id as $i) {

            $txt = $this->findById($i);

            if (!$txt) {

                $this->save(array(
                    'Txt' => array(
                        'id' => $i,
                        'txt' => 'Texte à rédiger'
                    )
                ));

                $txt['Txt']['txt'] = 'Texte à rédiger';

            }

            $texte[$i] = preg_replace($patt, $remp, $txt['Txt']['txt']);

        }

        return $texte;

    }

    public function beforeSave()
    {
        if (!empty($this->data['Txt']['id'])) {

            $this->data['Txt']['id'] = Inflector::slug($this->data['Txt']['id']);

        }
    }
}
