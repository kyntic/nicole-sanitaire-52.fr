<?php

/**
 * Class Contact
 */


App::uses('Parametre', 'Parametre');

class Contact extends AppModel
{

    public $useTable = false;

    /**
     * Validation rules
     *
     * @var array $validate
     */
    public $validate = array(
        'nom' => array(
            'rule' => 'notEmpty',
            'required'  => true, 
            'allowEmpty' => false, 
            'message'   => 'Ce champ ne doit pas être vide'
        ),
        'email' => array(
            'rule' => 'email',
            'required'  => true, 
            'allowEmpty' => false, 
            'message'   => 'Vous devez entrer une adresse email valide'
        ),
        'sujet' => array(
            'rule' => 'notEmpty',
            'required'  => true, 
            'allowEmpty' => false, 
            'message'   => 'Ce champ ne doit pas être vide'
        ),
        'message' => array(
            'rule' => 'notEmpty',
            'required'  => true, 
            'allowEmpty' => false, 
            'message'   => 'Ce champ ne doit pas être vide'
        )
    );


    /**
     * The belongs to
     *
     * @var array $belongsTo
     */
    public $belongsTo = array(
	        'ContentType'
    );

    /**
     * The has one
     *
     * @var array $hasOne
     */
    public $hasOne = array( 
        'Vignette' => array(
            'className'     => 'FileManager.FileModelView',
            'foreignKey'    => 'model_id', 
            'conditions'    => array('Vignette.model' => 'Page', 'Vignette.default' => 1, 'Vignette.group' => 'vignette'),
        )
    );

    /**
     * The act as
     *
     * @var array $actAs
     */
    //public $actsAs = array('Menu');


    public function beforeSave($options = array())
    {
        if(!empty($this->data[$this->name]['name']) && !empty($this->data[$this->name]['id'])) {

            $this->data[$this->name]['url'] = '/contact/'.$this->data[$this->name]['id'];

        }

        return true;
    }

    public function afterFind($results, $primary = false)
    {
    	foreach ($results as $k => $v) {
    	
    		if(!empty($v[$this->name]['url_r'])) $results[$k][$this->name]['url'] = $v[$this->name]['url_r'];

        }

    	return $results;

    }

	public function send($data) {

        $data['Contact']['Correspondances'] = array(
            'nom'        =>  'Nom : ', 
            'email'      =>  'Email : ', 
            'sujet'      =>  'Sujet : ', 
            'message'    =>  'Message : '
        );

        App::uses('CakeEmail', 'Network/Email');

        $email = new CakeEmail();

        if (!empty($data['Contact']['email_to'])) {

            $email->to($data['Contact']['email_to']);
        } else {

            $Parametre = ClassRegistry::init('Parametre');
            $parametres = $Parametre->find('first');

            $email->to($parametres['Parametre']['contact_email']);
        }
        $email->from($data['Contact']['email']);
        $email->subject('Demande depuis votre site web');
        $email->emailFormat('html');
        $email->template('default');
        $email->viewVars($data['Contact']);

        return $email->send();
	}

}
