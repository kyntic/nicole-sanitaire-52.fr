<h3>Nous Contacter</h3>
<div class="row">
    <p class="adr clearfix col-md-12 col-sm-4">
        <i class="fa fa-map-marker pull-left"></i>
        <span class="adr-group pull-left">
            <span class="street-address"><?=Configure::read('Params.adresse_1');?></span><br>
            <span class="region"><?=Configure::read('Params.adresse_2');?></span><br>
            <span class="postal-code"><?=Configure::read('Params.cp');?></span><br>
            <span class="country-name"><?=Configure::read('Params.ville');?></span>
        </span>
    </p>
    <p class="tel col-md-12 col-sm-4"><i class="fa fa-phone"></i><?=Configure::read('Params.telephone');?></p>
    <p class="email col-md-12 col-sm-4"><i class="fa fa-envelope"></i><a href="<?=Configure::read('Params.contact_page');?>">Nous contacter</a></p>
</div>